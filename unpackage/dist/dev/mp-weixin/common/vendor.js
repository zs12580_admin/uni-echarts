(global["webpackJsonp"] = global["webpackJsonp"] || []).push([["common/vendor"],[
/* 0 */,
/* 1 */
/*!*********************************************************!*\
  !*** ./node_modules/@dcloudio/uni-mp-weixin/dist/wx.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var objectKeys = ['qy', 'env', 'error', 'version', 'lanDebug', 'cloud', 'serviceMarket', 'router', 'worklet', '__webpack_require_UNI_MP_PLUGIN__'];
var singlePageDisableKey = ['lanDebug', 'router', 'worklet'];
var target = typeof globalThis !== 'undefined' ? globalThis : function () {
  return this;
}();
var key = ['w', 'x'].join('');
var oldWx = target[key];
var launchOption = oldWx.getLaunchOptionsSync ? oldWx.getLaunchOptionsSync() : null;
function isWxKey(key) {
  if (launchOption && launchOption.scene === 1154 && singlePageDisableKey.includes(key)) {
    return false;
  }
  return objectKeys.indexOf(key) > -1 || typeof oldWx[key] === 'function';
}
function initWx() {
  var newWx = {};
  for (var _key in oldWx) {
    if (isWxKey(_key)) {
      // TODO wrapper function
      newWx[_key] = oldWx[_key];
    }
  }
  return newWx;
}
target[key] = initWx();
var _default = target[key];
exports.default = _default;

/***/ }),
/* 2 */
/*!************************************************************!*\
  !*** ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(wx, global) {

var _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ 4);
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createApp = createApp;
exports.createComponent = createComponent;
exports.createPage = createPage;
exports.createPlugin = createPlugin;
exports.createSubpackageApp = createSubpackageApp;
exports.default = void 0;
var _slicedToArray2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/slicedToArray */ 5));
var _defineProperty2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/defineProperty */ 11));
var _construct2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/construct */ 15));
var _toConsumableArray2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/toConsumableArray */ 18));
var _typeof2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/typeof */ 13));
var _uniI18n = __webpack_require__(/*! @dcloudio/uni-i18n */ 22);
var _vue = _interopRequireDefault(__webpack_require__(/*! vue */ 25));
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { (0, _defineProperty2.default)(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
var realAtob;
var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
var b64re = /^(?:[A-Za-z\d+/]{4})*?(?:[A-Za-z\d+/]{2}(?:==)?|[A-Za-z\d+/]{3}=?)?$/;
if (typeof atob !== 'function') {
  realAtob = function realAtob(str) {
    str = String(str).replace(/[\t\n\f\r ]+/g, '');
    if (!b64re.test(str)) {
      throw new Error("Failed to execute 'atob' on 'Window': The string to be decoded is not correctly encoded.");
    }

    // Adding the padding if missing, for semplicity
    str += '=='.slice(2 - (str.length & 3));
    var bitmap;
    var result = '';
    var r1;
    var r2;
    var i = 0;
    for (; i < str.length;) {
      bitmap = b64.indexOf(str.charAt(i++)) << 18 | b64.indexOf(str.charAt(i++)) << 12 | (r1 = b64.indexOf(str.charAt(i++))) << 6 | (r2 = b64.indexOf(str.charAt(i++)));
      result += r1 === 64 ? String.fromCharCode(bitmap >> 16 & 255) : r2 === 64 ? String.fromCharCode(bitmap >> 16 & 255, bitmap >> 8 & 255) : String.fromCharCode(bitmap >> 16 & 255, bitmap >> 8 & 255, bitmap & 255);
    }
    return result;
  };
} else {
  // 注意atob只能在全局对象上调用，例如：`const Base64 = {atob};Base64.atob('xxxx')`是错误的用法
  realAtob = atob;
}
function b64DecodeUnicode(str) {
  return decodeURIComponent(realAtob(str).split('').map(function (c) {
    return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
  }).join(''));
}
function getCurrentUserInfo() {
  var token = wx.getStorageSync('uni_id_token') || '';
  var tokenArr = token.split('.');
  if (!token || tokenArr.length !== 3) {
    return {
      uid: null,
      role: [],
      permission: [],
      tokenExpired: 0
    };
  }
  var userInfo;
  try {
    userInfo = JSON.parse(b64DecodeUnicode(tokenArr[1]));
  } catch (error) {
    throw new Error('获取当前用户信息出错，详细错误信息为：' + error.message);
  }
  userInfo.tokenExpired = userInfo.exp * 1000;
  delete userInfo.exp;
  delete userInfo.iat;
  return userInfo;
}
function uniIdMixin(Vue) {
  Vue.prototype.uniIDHasRole = function (roleId) {
    var _getCurrentUserInfo = getCurrentUserInfo(),
      role = _getCurrentUserInfo.role;
    return role.indexOf(roleId) > -1;
  };
  Vue.prototype.uniIDHasPermission = function (permissionId) {
    var _getCurrentUserInfo2 = getCurrentUserInfo(),
      permission = _getCurrentUserInfo2.permission;
    return this.uniIDHasRole('admin') || permission.indexOf(permissionId) > -1;
  };
  Vue.prototype.uniIDTokenValid = function () {
    var _getCurrentUserInfo3 = getCurrentUserInfo(),
      tokenExpired = _getCurrentUserInfo3.tokenExpired;
    return tokenExpired > Date.now();
  };
}
var _toString = Object.prototype.toString;
var hasOwnProperty = Object.prototype.hasOwnProperty;
function isFn(fn) {
  return typeof fn === 'function';
}
function isStr(str) {
  return typeof str === 'string';
}
function isObject(obj) {
  return obj !== null && (0, _typeof2.default)(obj) === 'object';
}
function isPlainObject(obj) {
  return _toString.call(obj) === '[object Object]';
}
function hasOwn(obj, key) {
  return hasOwnProperty.call(obj, key);
}
function noop() {}

/**
 * Create a cached version of a pure function.
 */
function cached(fn) {
  var cache = Object.create(null);
  return function cachedFn(str) {
    var hit = cache[str];
    return hit || (cache[str] = fn(str));
  };
}

/**
 * Camelize a hyphen-delimited string.
 */
var camelizeRE = /-(\w)/g;
var camelize = cached(function (str) {
  return str.replace(camelizeRE, function (_, c) {
    return c ? c.toUpperCase() : '';
  });
});
function sortObject(obj) {
  var sortObj = {};
  if (isPlainObject(obj)) {
    Object.keys(obj).sort().forEach(function (key) {
      sortObj[key] = obj[key];
    });
  }
  return !Object.keys(sortObj) ? obj : sortObj;
}
var HOOKS = ['invoke', 'success', 'fail', 'complete', 'returnValue'];
var globalInterceptors = {};
var scopedInterceptors = {};
function mergeHook(parentVal, childVal) {
  var res = childVal ? parentVal ? parentVal.concat(childVal) : Array.isArray(childVal) ? childVal : [childVal] : parentVal;
  return res ? dedupeHooks(res) : res;
}
function dedupeHooks(hooks) {
  var res = [];
  for (var i = 0; i < hooks.length; i++) {
    if (res.indexOf(hooks[i]) === -1) {
      res.push(hooks[i]);
    }
  }
  return res;
}
function removeHook(hooks, hook) {
  var index = hooks.indexOf(hook);
  if (index !== -1) {
    hooks.splice(index, 1);
  }
}
function mergeInterceptorHook(interceptor, option) {
  Object.keys(option).forEach(function (hook) {
    if (HOOKS.indexOf(hook) !== -1 && isFn(option[hook])) {
      interceptor[hook] = mergeHook(interceptor[hook], option[hook]);
    }
  });
}
function removeInterceptorHook(interceptor, option) {
  if (!interceptor || !option) {
    return;
  }
  Object.keys(option).forEach(function (hook) {
    if (HOOKS.indexOf(hook) !== -1 && isFn(option[hook])) {
      removeHook(interceptor[hook], option[hook]);
    }
  });
}
function addInterceptor(method, option) {
  if (typeof method === 'string' && isPlainObject(option)) {
    mergeInterceptorHook(scopedInterceptors[method] || (scopedInterceptors[method] = {}), option);
  } else if (isPlainObject(method)) {
    mergeInterceptorHook(globalInterceptors, method);
  }
}
function removeInterceptor(method, option) {
  if (typeof method === 'string') {
    if (isPlainObject(option)) {
      removeInterceptorHook(scopedInterceptors[method], option);
    } else {
      delete scopedInterceptors[method];
    }
  } else if (isPlainObject(method)) {
    removeInterceptorHook(globalInterceptors, method);
  }
}
function wrapperHook(hook, params) {
  return function (data) {
    return hook(data, params) || data;
  };
}
function isPromise(obj) {
  return !!obj && ((0, _typeof2.default)(obj) === 'object' || typeof obj === 'function') && typeof obj.then === 'function';
}
function queue(hooks, data, params) {
  var promise = false;
  for (var i = 0; i < hooks.length; i++) {
    var hook = hooks[i];
    if (promise) {
      promise = Promise.resolve(wrapperHook(hook, params));
    } else {
      var res = hook(data, params);
      if (isPromise(res)) {
        promise = Promise.resolve(res);
      }
      if (res === false) {
        return {
          then: function then() {}
        };
      }
    }
  }
  return promise || {
    then: function then(callback) {
      return callback(data);
    }
  };
}
function wrapperOptions(interceptor) {
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  ['success', 'fail', 'complete'].forEach(function (name) {
    if (Array.isArray(interceptor[name])) {
      var oldCallback = options[name];
      options[name] = function callbackInterceptor(res) {
        queue(interceptor[name], res, options).then(function (res) {
          /* eslint-disable no-mixed-operators */
          return isFn(oldCallback) && oldCallback(res) || res;
        });
      };
    }
  });
  return options;
}
function wrapperReturnValue(method, returnValue) {
  var returnValueHooks = [];
  if (Array.isArray(globalInterceptors.returnValue)) {
    returnValueHooks.push.apply(returnValueHooks, (0, _toConsumableArray2.default)(globalInterceptors.returnValue));
  }
  var interceptor = scopedInterceptors[method];
  if (interceptor && Array.isArray(interceptor.returnValue)) {
    returnValueHooks.push.apply(returnValueHooks, (0, _toConsumableArray2.default)(interceptor.returnValue));
  }
  returnValueHooks.forEach(function (hook) {
    returnValue = hook(returnValue) || returnValue;
  });
  return returnValue;
}
function getApiInterceptorHooks(method) {
  var interceptor = Object.create(null);
  Object.keys(globalInterceptors).forEach(function (hook) {
    if (hook !== 'returnValue') {
      interceptor[hook] = globalInterceptors[hook].slice();
    }
  });
  var scopedInterceptor = scopedInterceptors[method];
  if (scopedInterceptor) {
    Object.keys(scopedInterceptor).forEach(function (hook) {
      if (hook !== 'returnValue') {
        interceptor[hook] = (interceptor[hook] || []).concat(scopedInterceptor[hook]);
      }
    });
  }
  return interceptor;
}
function invokeApi(method, api, options) {
  for (var _len = arguments.length, params = new Array(_len > 3 ? _len - 3 : 0), _key = 3; _key < _len; _key++) {
    params[_key - 3] = arguments[_key];
  }
  var interceptor = getApiInterceptorHooks(method);
  if (interceptor && Object.keys(interceptor).length) {
    if (Array.isArray(interceptor.invoke)) {
      var res = queue(interceptor.invoke, options);
      return res.then(function (options) {
        // 重新访问 getApiInterceptorHooks, 允许 invoke 中再次调用 addInterceptor,removeInterceptor
        return api.apply(void 0, [wrapperOptions(getApiInterceptorHooks(method), options)].concat(params));
      });
    } else {
      return api.apply(void 0, [wrapperOptions(interceptor, options)].concat(params));
    }
  }
  return api.apply(void 0, [options].concat(params));
}
var promiseInterceptor = {
  returnValue: function returnValue(res) {
    if (!isPromise(res)) {
      return res;
    }
    return new Promise(function (resolve, reject) {
      res.then(function (res) {
        if (res[0]) {
          reject(res[0]);
        } else {
          resolve(res[1]);
        }
      });
    });
  }
};
var SYNC_API_RE = /^\$|Window$|WindowStyle$|sendHostEvent|sendNativeEvent|restoreGlobal|requireGlobal|getCurrentSubNVue|getMenuButtonBoundingClientRect|^report|interceptors|Interceptor$|getSubNVueById|requireNativePlugin|upx2px|hideKeyboard|canIUse|^create|Sync$|Manager$|base64ToArrayBuffer|arrayBufferToBase64|getLocale|setLocale|invokePushCallback|getWindowInfo|getDeviceInfo|getAppBaseInfo|getSystemSetting|getAppAuthorizeSetting|initUTS|requireUTS|registerUTS/;
var CONTEXT_API_RE = /^create|Manager$/;

// Context例外情况
var CONTEXT_API_RE_EXC = ['createBLEConnection'];

// 同步例外情况
var ASYNC_API = ['createBLEConnection', 'createPushMessage'];
var CALLBACK_API_RE = /^on|^off/;
function isContextApi(name) {
  return CONTEXT_API_RE.test(name) && CONTEXT_API_RE_EXC.indexOf(name) === -1;
}
function isSyncApi(name) {
  return SYNC_API_RE.test(name) && ASYNC_API.indexOf(name) === -1;
}
function isCallbackApi(name) {
  return CALLBACK_API_RE.test(name) && name !== 'onPush';
}
function handlePromise(promise) {
  return promise.then(function (data) {
    return [null, data];
  }).catch(function (err) {
    return [err];
  });
}
function shouldPromise(name) {
  if (isContextApi(name) || isSyncApi(name) || isCallbackApi(name)) {
    return false;
  }
  return true;
}

/* eslint-disable no-extend-native */
if (!Promise.prototype.finally) {
  Promise.prototype.finally = function (callback) {
    var promise = this.constructor;
    return this.then(function (value) {
      return promise.resolve(callback()).then(function () {
        return value;
      });
    }, function (reason) {
      return promise.resolve(callback()).then(function () {
        throw reason;
      });
    });
  };
}
function promisify(name, api) {
  if (!shouldPromise(name) || !isFn(api)) {
    return api;
  }
  return function promiseApi() {
    var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    for (var _len2 = arguments.length, params = new Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
      params[_key2 - 1] = arguments[_key2];
    }
    if (isFn(options.success) || isFn(options.fail) || isFn(options.complete)) {
      return wrapperReturnValue(name, invokeApi.apply(void 0, [name, api, options].concat(params)));
    }
    return wrapperReturnValue(name, handlePromise(new Promise(function (resolve, reject) {
      invokeApi.apply(void 0, [name, api, Object.assign({}, options, {
        success: resolve,
        fail: reject
      })].concat(params));
    })));
  };
}
var EPS = 1e-4;
var BASE_DEVICE_WIDTH = 750;
var isIOS = false;
var deviceWidth = 0;
var deviceDPR = 0;
function checkDeviceWidth() {
  var _wx$getSystemInfoSync = wx.getSystemInfoSync(),
    platform = _wx$getSystemInfoSync.platform,
    pixelRatio = _wx$getSystemInfoSync.pixelRatio,
    windowWidth = _wx$getSystemInfoSync.windowWidth; // uni=>wx runtime 编译目标是 uni 对象，内部不允许直接使用 uni

  deviceWidth = windowWidth;
  deviceDPR = pixelRatio;
  isIOS = platform === 'ios';
}
function upx2px(number, newDeviceWidth) {
  if (deviceWidth === 0) {
    checkDeviceWidth();
  }
  number = Number(number);
  if (number === 0) {
    return 0;
  }
  var result = number / BASE_DEVICE_WIDTH * (newDeviceWidth || deviceWidth);
  if (result < 0) {
    result = -result;
  }
  result = Math.floor(result + EPS);
  if (result === 0) {
    if (deviceDPR === 1 || !isIOS) {
      result = 1;
    } else {
      result = 0.5;
    }
  }
  return number < 0 ? -result : result;
}
var LOCALE_ZH_HANS = 'zh-Hans';
var LOCALE_ZH_HANT = 'zh-Hant';
var LOCALE_EN = 'en';
var LOCALE_FR = 'fr';
var LOCALE_ES = 'es';
var messages = {};
var locale;
{
  locale = normalizeLocale(wx.getSystemInfoSync().language) || LOCALE_EN;
}
function initI18nMessages() {
  if (!isEnableLocale()) {
    return;
  }
  var localeKeys = Object.keys(__uniConfig.locales);
  if (localeKeys.length) {
    localeKeys.forEach(function (locale) {
      var curMessages = messages[locale];
      var userMessages = __uniConfig.locales[locale];
      if (curMessages) {
        Object.assign(curMessages, userMessages);
      } else {
        messages[locale] = userMessages;
      }
    });
  }
}
initI18nMessages();
var i18n = (0, _uniI18n.initVueI18n)(locale, {});
var t = i18n.t;
var i18nMixin = i18n.mixin = {
  beforeCreate: function beforeCreate() {
    var _this = this;
    var unwatch = i18n.i18n.watchLocale(function () {
      _this.$forceUpdate();
    });
    this.$once('hook:beforeDestroy', function () {
      unwatch();
    });
  },
  methods: {
    $$t: function $$t(key, values) {
      return t(key, values);
    }
  }
};
var setLocale = i18n.setLocale;
var getLocale = i18n.getLocale;
function initAppLocale(Vue, appVm, locale) {
  var state = Vue.observable({
    locale: locale || i18n.getLocale()
  });
  var localeWatchers = [];
  appVm.$watchLocale = function (fn) {
    localeWatchers.push(fn);
  };
  Object.defineProperty(appVm, '$locale', {
    get: function get() {
      return state.locale;
    },
    set: function set(v) {
      state.locale = v;
      localeWatchers.forEach(function (watch) {
        return watch(v);
      });
    }
  });
}
function isEnableLocale() {
  return typeof __uniConfig !== 'undefined' && __uniConfig.locales && !!Object.keys(__uniConfig.locales).length;
}
function include(str, parts) {
  return !!parts.find(function (part) {
    return str.indexOf(part) !== -1;
  });
}
function startsWith(str, parts) {
  return parts.find(function (part) {
    return str.indexOf(part) === 0;
  });
}
function normalizeLocale(locale, messages) {
  if (!locale) {
    return;
  }
  locale = locale.trim().replace(/_/g, '-');
  if (messages && messages[locale]) {
    return locale;
  }
  locale = locale.toLowerCase();
  if (locale === 'chinese') {
    // 支付宝
    return LOCALE_ZH_HANS;
  }
  if (locale.indexOf('zh') === 0) {
    if (locale.indexOf('-hans') > -1) {
      return LOCALE_ZH_HANS;
    }
    if (locale.indexOf('-hant') > -1) {
      return LOCALE_ZH_HANT;
    }
    if (include(locale, ['-tw', '-hk', '-mo', '-cht'])) {
      return LOCALE_ZH_HANT;
    }
    return LOCALE_ZH_HANS;
  }
  var lang = startsWith(locale, [LOCALE_EN, LOCALE_FR, LOCALE_ES]);
  if (lang) {
    return lang;
  }
}
// export function initI18n() {
//   const localeKeys = Object.keys(__uniConfig.locales || {})
//   if (localeKeys.length) {
//     localeKeys.forEach((locale) =>
//       i18n.add(locale, __uniConfig.locales[locale])
//     )
//   }
// }

function getLocale$1() {
  // 优先使用 $locale
  if (isFn(getApp)) {
    var app = getApp({
      allowDefault: true
    });
    if (app && app.$vm) {
      return app.$vm.$locale;
    }
  }
  return normalizeLocale(wx.getSystemInfoSync().language) || LOCALE_EN;
}
function setLocale$1(locale) {
  var app = isFn(getApp) ? getApp() : false;
  if (!app) {
    return false;
  }
  var oldLocale = app.$vm.$locale;
  if (oldLocale !== locale) {
    app.$vm.$locale = locale;
    onLocaleChangeCallbacks.forEach(function (fn) {
      return fn({
        locale: locale
      });
    });
    return true;
  }
  return false;
}
var onLocaleChangeCallbacks = [];
function onLocaleChange(fn) {
  if (onLocaleChangeCallbacks.indexOf(fn) === -1) {
    onLocaleChangeCallbacks.push(fn);
  }
}
if (typeof global !== 'undefined') {
  global.getLocale = getLocale$1;
}
var interceptors = {
  promiseInterceptor: promiseInterceptor
};
var baseApi = /*#__PURE__*/Object.freeze({
  __proto__: null,
  upx2px: upx2px,
  getLocale: getLocale$1,
  setLocale: setLocale$1,
  onLocaleChange: onLocaleChange,
  addInterceptor: addInterceptor,
  removeInterceptor: removeInterceptor,
  interceptors: interceptors
});
function findExistsPageIndex(url) {
  var pages = getCurrentPages();
  var len = pages.length;
  while (len--) {
    var page = pages[len];
    if (page.$page && page.$page.fullPath === url) {
      return len;
    }
  }
  return -1;
}
var redirectTo = {
  name: function name(fromArgs) {
    if (fromArgs.exists === 'back' && fromArgs.delta) {
      return 'navigateBack';
    }
    return 'redirectTo';
  },
  args: function args(fromArgs) {
    if (fromArgs.exists === 'back' && fromArgs.url) {
      var existsPageIndex = findExistsPageIndex(fromArgs.url);
      if (existsPageIndex !== -1) {
        var delta = getCurrentPages().length - 1 - existsPageIndex;
        if (delta > 0) {
          fromArgs.delta = delta;
        }
      }
    }
  }
};
var previewImage = {
  args: function args(fromArgs) {
    var currentIndex = parseInt(fromArgs.current);
    if (isNaN(currentIndex)) {
      return;
    }
    var urls = fromArgs.urls;
    if (!Array.isArray(urls)) {
      return;
    }
    var len = urls.length;
    if (!len) {
      return;
    }
    if (currentIndex < 0) {
      currentIndex = 0;
    } else if (currentIndex >= len) {
      currentIndex = len - 1;
    }
    if (currentIndex > 0) {
      fromArgs.current = urls[currentIndex];
      fromArgs.urls = urls.filter(function (item, index) {
        return index < currentIndex ? item !== urls[currentIndex] : true;
      });
    } else {
      fromArgs.current = urls[0];
    }
    return {
      indicator: false,
      loop: false
    };
  }
};
var UUID_KEY = '__DC_STAT_UUID';
var deviceId;
function useDeviceId(result) {
  deviceId = deviceId || wx.getStorageSync(UUID_KEY);
  if (!deviceId) {
    deviceId = Date.now() + '' + Math.floor(Math.random() * 1e7);
    wx.setStorage({
      key: UUID_KEY,
      data: deviceId
    });
  }
  result.deviceId = deviceId;
}
function addSafeAreaInsets(result) {
  if (result.safeArea) {
    var safeArea = result.safeArea;
    result.safeAreaInsets = {
      top: safeArea.top,
      left: safeArea.left,
      right: result.windowWidth - safeArea.right,
      bottom: result.screenHeight - safeArea.bottom
    };
  }
}
function populateParameters(result) {
  var _result$brand = result.brand,
    brand = _result$brand === void 0 ? '' : _result$brand,
    _result$model = result.model,
    model = _result$model === void 0 ? '' : _result$model,
    _result$system = result.system,
    system = _result$system === void 0 ? '' : _result$system,
    _result$language = result.language,
    language = _result$language === void 0 ? '' : _result$language,
    theme = result.theme,
    version = result.version,
    platform = result.platform,
    fontSizeSetting = result.fontSizeSetting,
    SDKVersion = result.SDKVersion,
    pixelRatio = result.pixelRatio,
    deviceOrientation = result.deviceOrientation;
  // const isQuickApp = "mp-weixin".indexOf('quickapp-webview') !== -1

  var extraParam = {};

  // osName osVersion
  var osName = '';
  var osVersion = '';
  {
    osName = system.split(' ')[0] || '';
    osVersion = system.split(' ')[1] || '';
  }
  var hostVersion = version;

  // deviceType
  var deviceType = getGetDeviceType(result, model);

  // deviceModel
  var deviceBrand = getDeviceBrand(brand);

  // hostName
  var _hostName = getHostName(result);

  // deviceOrientation
  var _deviceOrientation = deviceOrientation; // 仅 微信 百度 支持

  // devicePixelRatio
  var _devicePixelRatio = pixelRatio;

  // SDKVersion
  var _SDKVersion = SDKVersion;

  // hostLanguage
  var hostLanguage = language.replace(/_/g, '-');

  // wx.getAccountInfoSync

  var parameters = {
    appId: "__UNI__FE8DE4B",
    appName: "demo",
    appVersion: "1.0.0",
    appVersionCode: "100",
    appLanguage: getAppLanguage(hostLanguage),
    uniCompileVersion: "3.8.12",
    uniRuntimeVersion: "3.8.12",
    uniPlatform: undefined || "mp-weixin",
    deviceBrand: deviceBrand,
    deviceModel: model,
    deviceType: deviceType,
    devicePixelRatio: _devicePixelRatio,
    deviceOrientation: _deviceOrientation,
    osName: osName.toLocaleLowerCase(),
    osVersion: osVersion,
    hostTheme: theme,
    hostVersion: hostVersion,
    hostLanguage: hostLanguage,
    hostName: _hostName,
    hostSDKVersion: _SDKVersion,
    hostFontSizeSetting: fontSizeSetting,
    windowTop: 0,
    windowBottom: 0,
    // TODO
    osLanguage: undefined,
    osTheme: undefined,
    ua: undefined,
    hostPackageName: undefined,
    browserName: undefined,
    browserVersion: undefined
  };
  Object.assign(result, parameters, extraParam);
}
function getGetDeviceType(result, model) {
  var deviceType = result.deviceType || 'phone';
  {
    var deviceTypeMaps = {
      ipad: 'pad',
      windows: 'pc',
      mac: 'pc'
    };
    var deviceTypeMapsKeys = Object.keys(deviceTypeMaps);
    var _model = model.toLocaleLowerCase();
    for (var index = 0; index < deviceTypeMapsKeys.length; index++) {
      var _m = deviceTypeMapsKeys[index];
      if (_model.indexOf(_m) !== -1) {
        deviceType = deviceTypeMaps[_m];
        break;
      }
    }
  }
  return deviceType;
}
function getDeviceBrand(brand) {
  var deviceBrand = brand;
  if (deviceBrand) {
    deviceBrand = brand.toLocaleLowerCase();
  }
  return deviceBrand;
}
function getAppLanguage(defaultLanguage) {
  return getLocale$1 ? getLocale$1() : defaultLanguage;
}
function getHostName(result) {
  var _platform = 'WeChat';
  var _hostName = result.hostName || _platform; // mp-jd
  {
    if (result.environment) {
      _hostName = result.environment;
    } else if (result.host && result.host.env) {
      _hostName = result.host.env;
    }
  }
  return _hostName;
}
var getSystemInfo = {
  returnValue: function returnValue(result) {
    useDeviceId(result);
    addSafeAreaInsets(result);
    populateParameters(result);
  }
};
var showActionSheet = {
  args: function args(fromArgs) {
    if ((0, _typeof2.default)(fromArgs) === 'object') {
      fromArgs.alertText = fromArgs.title;
    }
  }
};
var getAppBaseInfo = {
  returnValue: function returnValue(result) {
    var _result = result,
      version = _result.version,
      language = _result.language,
      SDKVersion = _result.SDKVersion,
      theme = _result.theme;
    var _hostName = getHostName(result);
    var hostLanguage = language.replace('_', '-');
    result = sortObject(Object.assign(result, {
      appId: "__UNI__FE8DE4B",
      appName: "demo",
      appVersion: "1.0.0",
      appVersionCode: "100",
      appLanguage: getAppLanguage(hostLanguage),
      hostVersion: version,
      hostLanguage: hostLanguage,
      hostName: _hostName,
      hostSDKVersion: SDKVersion,
      hostTheme: theme
    }));
  }
};
var getDeviceInfo = {
  returnValue: function returnValue(result) {
    var _result2 = result,
      brand = _result2.brand,
      model = _result2.model;
    var deviceType = getGetDeviceType(result, model);
    var deviceBrand = getDeviceBrand(brand);
    useDeviceId(result);
    result = sortObject(Object.assign(result, {
      deviceType: deviceType,
      deviceBrand: deviceBrand,
      deviceModel: model
    }));
  }
};
var getWindowInfo = {
  returnValue: function returnValue(result) {
    addSafeAreaInsets(result);
    result = sortObject(Object.assign(result, {
      windowTop: 0,
      windowBottom: 0
    }));
  }
};
var getAppAuthorizeSetting = {
  returnValue: function returnValue(result) {
    var locationReducedAccuracy = result.locationReducedAccuracy;
    result.locationAccuracy = 'unsupported';
    if (locationReducedAccuracy === true) {
      result.locationAccuracy = 'reduced';
    } else if (locationReducedAccuracy === false) {
      result.locationAccuracy = 'full';
    }
  }
};

// import navigateTo from 'uni-helpers/navigate-to'

var compressImage = {
  args: function args(fromArgs) {
    // https://developers.weixin.qq.com/community/develop/doc/000c08940c865011298e0a43256800?highLine=compressHeight
    if (fromArgs.compressedHeight && !fromArgs.compressHeight) {
      fromArgs.compressHeight = fromArgs.compressedHeight;
    }
    if (fromArgs.compressedWidth && !fromArgs.compressWidth) {
      fromArgs.compressWidth = fromArgs.compressedWidth;
    }
  }
};
var protocols = {
  redirectTo: redirectTo,
  // navigateTo,  // 由于在微信开发者工具的页面参数，会显示__id__参数，因此暂时关闭mp-weixin对于navigateTo的AOP
  previewImage: previewImage,
  getSystemInfo: getSystemInfo,
  getSystemInfoSync: getSystemInfo,
  showActionSheet: showActionSheet,
  getAppBaseInfo: getAppBaseInfo,
  getDeviceInfo: getDeviceInfo,
  getWindowInfo: getWindowInfo,
  getAppAuthorizeSetting: getAppAuthorizeSetting,
  compressImage: compressImage
};
var todos = ['vibrate', 'preloadPage', 'unPreloadPage', 'loadSubPackage'];
var canIUses = [];
var CALLBACKS = ['success', 'fail', 'cancel', 'complete'];
function processCallback(methodName, method, returnValue) {
  return function (res) {
    return method(processReturnValue(methodName, res, returnValue));
  };
}
function processArgs(methodName, fromArgs) {
  var argsOption = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
  var returnValue = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  var keepFromArgs = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;
  if (isPlainObject(fromArgs)) {
    // 一般 api 的参数解析
    var toArgs = keepFromArgs === true ? fromArgs : {}; // returnValue 为 false 时，说明是格式化返回值，直接在返回值对象上修改赋值
    if (isFn(argsOption)) {
      argsOption = argsOption(fromArgs, toArgs) || {};
    }
    for (var key in fromArgs) {
      if (hasOwn(argsOption, key)) {
        var keyOption = argsOption[key];
        if (isFn(keyOption)) {
          keyOption = keyOption(fromArgs[key], fromArgs, toArgs);
        }
        if (!keyOption) {
          // 不支持的参数
          console.warn("The '".concat(methodName, "' method of platform '\u5FAE\u4FE1\u5C0F\u7A0B\u5E8F' does not support option '").concat(key, "'"));
        } else if (isStr(keyOption)) {
          // 重写参数 key
          toArgs[keyOption] = fromArgs[key];
        } else if (isPlainObject(keyOption)) {
          // {name:newName,value:value}可重新指定参数 key:value
          toArgs[keyOption.name ? keyOption.name : key] = keyOption.value;
        }
      } else if (CALLBACKS.indexOf(key) !== -1) {
        if (isFn(fromArgs[key])) {
          toArgs[key] = processCallback(methodName, fromArgs[key], returnValue);
        }
      } else {
        if (!keepFromArgs) {
          toArgs[key] = fromArgs[key];
        }
      }
    }
    return toArgs;
  } else if (isFn(fromArgs)) {
    fromArgs = processCallback(methodName, fromArgs, returnValue);
  }
  return fromArgs;
}
function processReturnValue(methodName, res, returnValue) {
  var keepReturnValue = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
  if (isFn(protocols.returnValue)) {
    // 处理通用 returnValue
    res = protocols.returnValue(methodName, res);
  }
  return processArgs(methodName, res, returnValue, {}, keepReturnValue);
}
function wrapper(methodName, method) {
  if (hasOwn(protocols, methodName)) {
    var protocol = protocols[methodName];
    if (!protocol) {
      // 暂不支持的 api
      return function () {
        console.error("Platform '\u5FAE\u4FE1\u5C0F\u7A0B\u5E8F' does not support '".concat(methodName, "'."));
      };
    }
    return function (arg1, arg2) {
      // 目前 api 最多两个参数
      var options = protocol;
      if (isFn(protocol)) {
        options = protocol(arg1);
      }
      arg1 = processArgs(methodName, arg1, options.args, options.returnValue);
      var args = [arg1];
      if (typeof arg2 !== 'undefined') {
        args.push(arg2);
      }
      if (isFn(options.name)) {
        methodName = options.name(arg1);
      } else if (isStr(options.name)) {
        methodName = options.name;
      }
      var returnValue = wx[methodName].apply(wx, args);
      if (isSyncApi(methodName)) {
        // 同步 api
        return processReturnValue(methodName, returnValue, options.returnValue, isContextApi(methodName));
      }
      return returnValue;
    };
  }
  return method;
}
var todoApis = Object.create(null);
var TODOS = ['onTabBarMidButtonTap', 'subscribePush', 'unsubscribePush', 'onPush', 'offPush', 'share'];
function createTodoApi(name) {
  return function todoApi(_ref) {
    var fail = _ref.fail,
      complete = _ref.complete;
    var res = {
      errMsg: "".concat(name, ":fail method '").concat(name, "' not supported")
    };
    isFn(fail) && fail(res);
    isFn(complete) && complete(res);
  };
}
TODOS.forEach(function (name) {
  todoApis[name] = createTodoApi(name);
});
var providers = {
  oauth: ['weixin'],
  share: ['weixin'],
  payment: ['wxpay'],
  push: ['weixin']
};
function getProvider(_ref2) {
  var service = _ref2.service,
    success = _ref2.success,
    fail = _ref2.fail,
    complete = _ref2.complete;
  var res = false;
  if (providers[service]) {
    res = {
      errMsg: 'getProvider:ok',
      service: service,
      provider: providers[service]
    };
    isFn(success) && success(res);
  } else {
    res = {
      errMsg: 'getProvider:fail service not found'
    };
    isFn(fail) && fail(res);
  }
  isFn(complete) && complete(res);
}
var extraApi = /*#__PURE__*/Object.freeze({
  __proto__: null,
  getProvider: getProvider
});
var getEmitter = function () {
  var Emitter;
  return function getUniEmitter() {
    if (!Emitter) {
      Emitter = new _vue.default();
    }
    return Emitter;
  };
}();
function apply(ctx, method, args) {
  return ctx[method].apply(ctx, args);
}
function $on() {
  return apply(getEmitter(), '$on', Array.prototype.slice.call(arguments));
}
function $off() {
  return apply(getEmitter(), '$off', Array.prototype.slice.call(arguments));
}
function $once() {
  return apply(getEmitter(), '$once', Array.prototype.slice.call(arguments));
}
function $emit() {
  return apply(getEmitter(), '$emit', Array.prototype.slice.call(arguments));
}
var eventApi = /*#__PURE__*/Object.freeze({
  __proto__: null,
  $on: $on,
  $off: $off,
  $once: $once,
  $emit: $emit
});

/**
 * 框架内 try-catch
 */
/**
 * 开发者 try-catch
 */
function tryCatch(fn) {
  return function () {
    try {
      return fn.apply(fn, arguments);
    } catch (e) {
      // TODO
      console.error(e);
    }
  };
}
function getApiCallbacks(params) {
  var apiCallbacks = {};
  for (var name in params) {
    var param = params[name];
    if (isFn(param)) {
      apiCallbacks[name] = tryCatch(param);
      delete params[name];
    }
  }
  return apiCallbacks;
}
var cid;
var cidErrMsg;
var enabled;
function normalizePushMessage(message) {
  try {
    return JSON.parse(message);
  } catch (e) {}
  return message;
}
function invokePushCallback(args) {
  if (args.type === 'enabled') {
    enabled = true;
  } else if (args.type === 'clientId') {
    cid = args.cid;
    cidErrMsg = args.errMsg;
    invokeGetPushCidCallbacks(cid, args.errMsg);
  } else if (args.type === 'pushMsg') {
    var message = {
      type: 'receive',
      data: normalizePushMessage(args.message)
    };
    for (var i = 0; i < onPushMessageCallbacks.length; i++) {
      var callback = onPushMessageCallbacks[i];
      callback(message);
      // 该消息已被阻止
      if (message.stopped) {
        break;
      }
    }
  } else if (args.type === 'click') {
    onPushMessageCallbacks.forEach(function (callback) {
      callback({
        type: 'click',
        data: normalizePushMessage(args.message)
      });
    });
  }
}
var getPushCidCallbacks = [];
function invokeGetPushCidCallbacks(cid, errMsg) {
  getPushCidCallbacks.forEach(function (callback) {
    callback(cid, errMsg);
  });
  getPushCidCallbacks.length = 0;
}
function getPushClientId(args) {
  if (!isPlainObject(args)) {
    args = {};
  }
  var _getApiCallbacks = getApiCallbacks(args),
    success = _getApiCallbacks.success,
    fail = _getApiCallbacks.fail,
    complete = _getApiCallbacks.complete;
  var hasSuccess = isFn(success);
  var hasFail = isFn(fail);
  var hasComplete = isFn(complete);
  Promise.resolve().then(function () {
    if (typeof enabled === 'undefined') {
      enabled = false;
      cid = '';
      cidErrMsg = 'uniPush is not enabled';
    }
    getPushCidCallbacks.push(function (cid, errMsg) {
      var res;
      if (cid) {
        res = {
          errMsg: 'getPushClientId:ok',
          cid: cid
        };
        hasSuccess && success(res);
      } else {
        res = {
          errMsg: 'getPushClientId:fail' + (errMsg ? ' ' + errMsg : '')
        };
        hasFail && fail(res);
      }
      hasComplete && complete(res);
    });
    if (typeof cid !== 'undefined') {
      invokeGetPushCidCallbacks(cid, cidErrMsg);
    }
  });
}
var onPushMessageCallbacks = [];
// 不使用 defineOnApi 实现，是因为 defineOnApi 依赖 UniServiceJSBridge ，该对象目前在小程序上未提供，故简单实现
var onPushMessage = function onPushMessage(fn) {
  if (onPushMessageCallbacks.indexOf(fn) === -1) {
    onPushMessageCallbacks.push(fn);
  }
};
var offPushMessage = function offPushMessage(fn) {
  if (!fn) {
    onPushMessageCallbacks.length = 0;
  } else {
    var index = onPushMessageCallbacks.indexOf(fn);
    if (index > -1) {
      onPushMessageCallbacks.splice(index, 1);
    }
  }
};
var baseInfo = wx.getAppBaseInfo && wx.getAppBaseInfo();
if (!baseInfo) {
  baseInfo = wx.getSystemInfoSync();
}
var host = baseInfo ? baseInfo.host : null;
var shareVideoMessage = host && host.env === 'SAAASDK' ? wx.miniapp.shareVideoMessage : wx.shareVideoMessage;
var api = /*#__PURE__*/Object.freeze({
  __proto__: null,
  shareVideoMessage: shareVideoMessage,
  getPushClientId: getPushClientId,
  onPushMessage: onPushMessage,
  offPushMessage: offPushMessage,
  invokePushCallback: invokePushCallback
});
var mocks = ['__route__', '__wxExparserNodeId__', '__wxWebviewId__'];
function findVmByVueId(vm, vuePid) {
  var $children = vm.$children;
  // 优先查找直属(反向查找:https://github.com/dcloudio/uni-app/issues/1200)
  for (var i = $children.length - 1; i >= 0; i--) {
    var childVm = $children[i];
    if (childVm.$scope._$vueId === vuePid) {
      return childVm;
    }
  }
  // 反向递归查找
  var parentVm;
  for (var _i = $children.length - 1; _i >= 0; _i--) {
    parentVm = findVmByVueId($children[_i], vuePid);
    if (parentVm) {
      return parentVm;
    }
  }
}
function initBehavior(options) {
  return Behavior(options);
}
function isPage() {
  return !!this.route;
}
function initRelation(detail) {
  this.triggerEvent('__l', detail);
}
function selectAllComponents(mpInstance, selector, $refs) {
  var components = mpInstance.selectAllComponents(selector) || [];
  components.forEach(function (component) {
    var ref = component.dataset.ref;
    $refs[ref] = component.$vm || toSkip(component);
    {
      if (component.dataset.vueGeneric === 'scoped') {
        component.selectAllComponents('.scoped-ref').forEach(function (scopedComponent) {
          selectAllComponents(scopedComponent, selector, $refs);
        });
      }
    }
  });
}
function syncRefs(refs, newRefs) {
  var oldKeys = (0, _construct2.default)(Set, (0, _toConsumableArray2.default)(Object.keys(refs)));
  var newKeys = Object.keys(newRefs);
  newKeys.forEach(function (key) {
    var oldValue = refs[key];
    var newValue = newRefs[key];
    if (Array.isArray(oldValue) && Array.isArray(newValue) && oldValue.length === newValue.length && newValue.every(function (value) {
      return oldValue.includes(value);
    })) {
      return;
    }
    refs[key] = newValue;
    oldKeys.delete(key);
  });
  oldKeys.forEach(function (key) {
    delete refs[key];
  });
  return refs;
}
function initRefs(vm) {
  var mpInstance = vm.$scope;
  var refs = {};
  Object.defineProperty(vm, '$refs', {
    get: function get() {
      var $refs = {};
      selectAllComponents(mpInstance, '.vue-ref', $refs);
      // TODO 暂不考虑 for 中的 scoped
      var forComponents = mpInstance.selectAllComponents('.vue-ref-in-for') || [];
      forComponents.forEach(function (component) {
        var ref = component.dataset.ref;
        if (!$refs[ref]) {
          $refs[ref] = [];
        }
        $refs[ref].push(component.$vm || toSkip(component));
      });
      return syncRefs(refs, $refs);
    }
  });
}
function handleLink(event) {
  var _ref3 = event.detail || event.value,
    vuePid = _ref3.vuePid,
    vueOptions = _ref3.vueOptions; // detail 是微信,value 是百度(dipatch)

  var parentVm;
  if (vuePid) {
    parentVm = findVmByVueId(this.$vm, vuePid);
  }
  if (!parentVm) {
    parentVm = this.$vm;
  }
  vueOptions.parent = parentVm;
}
function markMPComponent(component) {
  // 在 Vue 中标记为小程序组件
  var IS_MP = '__v_isMPComponent';
  Object.defineProperty(component, IS_MP, {
    configurable: true,
    enumerable: false,
    value: true
  });
  return component;
}
function toSkip(obj) {
  var OB = '__ob__';
  var SKIP = '__v_skip';
  if (isObject(obj) && Object.isExtensible(obj)) {
    // 避免被 @vue/composition-api 观测
    Object.defineProperty(obj, OB, {
      configurable: true,
      enumerable: false,
      value: (0, _defineProperty2.default)({}, SKIP, true)
    });
  }
  return obj;
}
var WORKLET_RE = /_(.*)_worklet_factory_/;
function initWorkletMethods(mpMethods, vueMethods) {
  if (vueMethods) {
    Object.keys(vueMethods).forEach(function (name) {
      var matches = name.match(WORKLET_RE);
      if (matches) {
        var workletName = matches[1];
        mpMethods[name] = vueMethods[name];
        mpMethods[workletName] = vueMethods[workletName];
      }
    });
  }
}
var MPPage = Page;
var MPComponent = Component;
var customizeRE = /:/g;
var customize = cached(function (str) {
  return camelize(str.replace(customizeRE, '-'));
});
function initTriggerEvent(mpInstance) {
  var oldTriggerEvent = mpInstance.triggerEvent;
  var newTriggerEvent = function newTriggerEvent(event) {
    for (var _len3 = arguments.length, args = new Array(_len3 > 1 ? _len3 - 1 : 0), _key3 = 1; _key3 < _len3; _key3++) {
      args[_key3 - 1] = arguments[_key3];
    }
    // 事件名统一转驼峰格式，仅处理：当前组件为 vue 组件、当前组件为 vue 组件子组件
    if (this.$vm || this.dataset && this.dataset.comType) {
      event = customize(event);
    } else {
      // 针对微信/QQ小程序单独补充驼峰格式事件，以兼容历史项目
      var newEvent = customize(event);
      if (newEvent !== event) {
        oldTriggerEvent.apply(this, [newEvent].concat(args));
      }
    }
    return oldTriggerEvent.apply(this, [event].concat(args));
  };
  try {
    // 京东小程序 triggerEvent 为只读
    mpInstance.triggerEvent = newTriggerEvent;
  } catch (error) {
    mpInstance._triggerEvent = newTriggerEvent;
  }
}
function initHook(name, options, isComponent) {
  var oldHook = options[name];
  options[name] = function () {
    markMPComponent(this);
    initTriggerEvent(this);
    if (oldHook) {
      for (var _len4 = arguments.length, args = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
        args[_key4] = arguments[_key4];
      }
      return oldHook.apply(this, args);
    }
  };
}
if (!MPPage.__$wrappered) {
  MPPage.__$wrappered = true;
  Page = function Page() {
    var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    initHook('onLoad', options);
    return MPPage(options);
  };
  Page.after = MPPage.after;
  Component = function Component() {
    var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    initHook('created', options);
    return MPComponent(options);
  };
}
var PAGE_EVENT_HOOKS = ['onPullDownRefresh', 'onReachBottom', 'onAddToFavorites', 'onShareTimeline', 'onShareAppMessage', 'onPageScroll', 'onResize', 'onTabItemTap'];
function initMocks(vm, mocks) {
  var mpInstance = vm.$mp[vm.mpType];
  mocks.forEach(function (mock) {
    if (hasOwn(mpInstance, mock)) {
      vm[mock] = mpInstance[mock];
    }
  });
}
function hasHook(hook, vueOptions) {
  if (!vueOptions) {
    return true;
  }
  if (_vue.default.options && Array.isArray(_vue.default.options[hook])) {
    return true;
  }
  vueOptions = vueOptions.default || vueOptions;
  if (isFn(vueOptions)) {
    if (isFn(vueOptions.extendOptions[hook])) {
      return true;
    }
    if (vueOptions.super && vueOptions.super.options && Array.isArray(vueOptions.super.options[hook])) {
      return true;
    }
    return false;
  }
  if (isFn(vueOptions[hook]) || Array.isArray(vueOptions[hook])) {
    return true;
  }
  var mixins = vueOptions.mixins;
  if (Array.isArray(mixins)) {
    return !!mixins.find(function (mixin) {
      return hasHook(hook, mixin);
    });
  }
}
function initHooks(mpOptions, hooks, vueOptions) {
  hooks.forEach(function (hook) {
    if (hasHook(hook, vueOptions)) {
      mpOptions[hook] = function (args) {
        return this.$vm && this.$vm.__call_hook(hook, args);
      };
    }
  });
}
function initUnknownHooks(mpOptions, vueOptions) {
  var excludes = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];
  findHooks(vueOptions).forEach(function (hook) {
    return initHook$1(mpOptions, hook, excludes);
  });
}
function findHooks(vueOptions) {
  var hooks = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
  if (vueOptions) {
    Object.keys(vueOptions).forEach(function (name) {
      if (name.indexOf('on') === 0 && isFn(vueOptions[name])) {
        hooks.push(name);
      }
    });
  }
  return hooks;
}
function initHook$1(mpOptions, hook, excludes) {
  if (excludes.indexOf(hook) === -1 && !hasOwn(mpOptions, hook)) {
    mpOptions[hook] = function (args) {
      return this.$vm && this.$vm.__call_hook(hook, args);
    };
  }
}
function initVueComponent(Vue, vueOptions) {
  vueOptions = vueOptions.default || vueOptions;
  var VueComponent;
  if (isFn(vueOptions)) {
    VueComponent = vueOptions;
  } else {
    VueComponent = Vue.extend(vueOptions);
  }
  vueOptions = VueComponent.options;
  return [VueComponent, vueOptions];
}
function initSlots(vm, vueSlots) {
  if (Array.isArray(vueSlots) && vueSlots.length) {
    var $slots = Object.create(null);
    vueSlots.forEach(function (slotName) {
      $slots[slotName] = true;
    });
    vm.$scopedSlots = vm.$slots = $slots;
  }
}
function initVueIds(vueIds, mpInstance) {
  vueIds = (vueIds || '').split(',');
  var len = vueIds.length;
  if (len === 1) {
    mpInstance._$vueId = vueIds[0];
  } else if (len === 2) {
    mpInstance._$vueId = vueIds[0];
    mpInstance._$vuePid = vueIds[1];
  }
}
function initData(vueOptions, context) {
  var data = vueOptions.data || {};
  var methods = vueOptions.methods || {};
  if (typeof data === 'function') {
    try {
      data = data.call(context); // 支持 Vue.prototype 上挂的数据
    } catch (e) {
      if (Object({"VUE_APP_DARK_MODE":"false","VUE_APP_NAME":"demo","VUE_APP_PLATFORM":"mp-weixin","NODE_ENV":"development","BASE_URL":"/"}).VUE_APP_DEBUG) {
        console.warn('根据 Vue 的 data 函数初始化小程序 data 失败，请尽量确保 data 函数中不访问 vm 对象，否则可能影响首次数据渲染速度。', data);
      }
    }
  } else {
    try {
      // 对 data 格式化
      data = JSON.parse(JSON.stringify(data));
    } catch (e) {}
  }
  if (!isPlainObject(data)) {
    data = {};
  }
  Object.keys(methods).forEach(function (methodName) {
    if (context.__lifecycle_hooks__.indexOf(methodName) === -1 && !hasOwn(data, methodName)) {
      data[methodName] = methods[methodName];
    }
  });
  return data;
}
var PROP_TYPES = [String, Number, Boolean, Object, Array, null];
function createObserver(name) {
  return function observer(newVal, oldVal) {
    if (this.$vm) {
      this.$vm[name] = newVal; // 为了触发其他非 render watcher
    }
  };
}

function initBehaviors(vueOptions, initBehavior) {
  var vueBehaviors = vueOptions.behaviors;
  var vueExtends = vueOptions.extends;
  var vueMixins = vueOptions.mixins;
  var vueProps = vueOptions.props;
  if (!vueProps) {
    vueOptions.props = vueProps = [];
  }
  var behaviors = [];
  if (Array.isArray(vueBehaviors)) {
    vueBehaviors.forEach(function (behavior) {
      behaviors.push(behavior.replace('uni://', "wx".concat("://")));
      if (behavior === 'uni://form-field') {
        if (Array.isArray(vueProps)) {
          vueProps.push('name');
          vueProps.push('value');
        } else {
          vueProps.name = {
            type: String,
            default: ''
          };
          vueProps.value = {
            type: [String, Number, Boolean, Array, Object, Date],
            default: ''
          };
        }
      }
    });
  }
  if (isPlainObject(vueExtends) && vueExtends.props) {
    behaviors.push(initBehavior({
      properties: initProperties(vueExtends.props, true)
    }));
  }
  if (Array.isArray(vueMixins)) {
    vueMixins.forEach(function (vueMixin) {
      if (isPlainObject(vueMixin) && vueMixin.props) {
        behaviors.push(initBehavior({
          properties: initProperties(vueMixin.props, true)
        }));
      }
    });
  }
  return behaviors;
}
function parsePropType(key, type, defaultValue, file) {
  // [String]=>String
  if (Array.isArray(type) && type.length === 1) {
    return type[0];
  }
  return type;
}
function initProperties(props) {
  var isBehavior = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
  var file = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';
  var options = arguments.length > 3 ? arguments[3] : undefined;
  var properties = {};
  if (!isBehavior) {
    properties.vueId = {
      type: String,
      value: ''
    };
    {
      if (options.virtualHost) {
        properties.virtualHostStyle = {
          type: null,
          value: ''
        };
        properties.virtualHostClass = {
          type: null,
          value: ''
        };
      }
    }
    // scopedSlotsCompiler auto
    properties.scopedSlotsCompiler = {
      type: String,
      value: ''
    };
    properties.vueSlots = {
      // 小程序不能直接定义 $slots 的 props，所以通过 vueSlots 转换到 $slots
      type: null,
      value: [],
      observer: function observer(newVal, oldVal) {
        var $slots = Object.create(null);
        newVal.forEach(function (slotName) {
          $slots[slotName] = true;
        });
        this.setData({
          $slots: $slots
        });
      }
    };
  }
  if (Array.isArray(props)) {
    // ['title']
    props.forEach(function (key) {
      properties[key] = {
        type: null,
        observer: createObserver(key)
      };
    });
  } else if (isPlainObject(props)) {
    // {title:{type:String,default:''},content:String}
    Object.keys(props).forEach(function (key) {
      var opts = props[key];
      if (isPlainObject(opts)) {
        // title:{type:String,default:''}
        var value = opts.default;
        if (isFn(value)) {
          value = value();
        }
        opts.type = parsePropType(key, opts.type);
        properties[key] = {
          type: PROP_TYPES.indexOf(opts.type) !== -1 ? opts.type : null,
          value: value,
          observer: createObserver(key)
        };
      } else {
        // content:String
        var type = parsePropType(key, opts);
        properties[key] = {
          type: PROP_TYPES.indexOf(type) !== -1 ? type : null,
          observer: createObserver(key)
        };
      }
    });
  }
  return properties;
}
function wrapper$1(event) {
  // TODO 又得兼容 mpvue 的 mp 对象
  try {
    event.mp = JSON.parse(JSON.stringify(event));
  } catch (e) {}
  event.stopPropagation = noop;
  event.preventDefault = noop;
  event.target = event.target || {};
  if (!hasOwn(event, 'detail')) {
    event.detail = {};
  }
  if (hasOwn(event, 'markerId')) {
    event.detail = (0, _typeof2.default)(event.detail) === 'object' ? event.detail : {};
    event.detail.markerId = event.markerId;
  }
  if (isPlainObject(event.detail)) {
    event.target = Object.assign({}, event.target, event.detail);
  }
  return event;
}
function getExtraValue(vm, dataPathsArray) {
  var context = vm;
  dataPathsArray.forEach(function (dataPathArray) {
    var dataPath = dataPathArray[0];
    var value = dataPathArray[2];
    if (dataPath || typeof value !== 'undefined') {
      // ['','',index,'disable']
      var propPath = dataPathArray[1];
      var valuePath = dataPathArray[3];
      var vFor;
      if (Number.isInteger(dataPath)) {
        vFor = dataPath;
      } else if (!dataPath) {
        vFor = context;
      } else if (typeof dataPath === 'string' && dataPath) {
        if (dataPath.indexOf('#s#') === 0) {
          vFor = dataPath.substr(3);
        } else {
          vFor = vm.__get_value(dataPath, context);
        }
      }
      if (Number.isInteger(vFor)) {
        context = value;
      } else if (!propPath) {
        context = vFor[value];
      } else {
        if (Array.isArray(vFor)) {
          context = vFor.find(function (vForItem) {
            return vm.__get_value(propPath, vForItem) === value;
          });
        } else if (isPlainObject(vFor)) {
          context = Object.keys(vFor).find(function (vForKey) {
            return vm.__get_value(propPath, vFor[vForKey]) === value;
          });
        } else {
          console.error('v-for 暂不支持循环数据：', vFor);
        }
      }
      if (valuePath) {
        context = vm.__get_value(valuePath, context);
      }
    }
  });
  return context;
}
function processEventExtra(vm, extra, event, __args__) {
  var extraObj = {};
  if (Array.isArray(extra) && extra.length) {
    /**
     *[
     *    ['data.items', 'data.id', item.data.id],
     *    ['metas', 'id', meta.id]
     *],
     *[
     *    ['data.items', 'data.id', item.data.id],
     *    ['metas', 'id', meta.id]
     *],
     *'test'
     */
    extra.forEach(function (dataPath, index) {
      if (typeof dataPath === 'string') {
        if (!dataPath) {
          // model,prop.sync
          extraObj['$' + index] = vm;
        } else {
          if (dataPath === '$event') {
            // $event
            extraObj['$' + index] = event;
          } else if (dataPath === 'arguments') {
            extraObj['$' + index] = event.detail ? event.detail.__args__ || __args__ : __args__;
          } else if (dataPath.indexOf('$event.') === 0) {
            // $event.target.value
            extraObj['$' + index] = vm.__get_value(dataPath.replace('$event.', ''), event);
          } else {
            extraObj['$' + index] = vm.__get_value(dataPath);
          }
        }
      } else {
        extraObj['$' + index] = getExtraValue(vm, dataPath);
      }
    });
  }
  return extraObj;
}
function getObjByArray(arr) {
  var obj = {};
  for (var i = 1; i < arr.length; i++) {
    var element = arr[i];
    obj[element[0]] = element[1];
  }
  return obj;
}
function processEventArgs(vm, event) {
  var args = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];
  var extra = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : [];
  var isCustom = arguments.length > 4 ? arguments[4] : undefined;
  var methodName = arguments.length > 5 ? arguments[5] : undefined;
  var isCustomMPEvent = false; // wxcomponent 组件，传递原始 event 对象

  // fixed 用户直接触发 mpInstance.triggerEvent
  var __args__ = isPlainObject(event.detail) ? event.detail.__args__ || [event.detail] : [event.detail];
  if (isCustom) {
    // 自定义事件
    isCustomMPEvent = event.currentTarget && event.currentTarget.dataset && event.currentTarget.dataset.comType === 'wx';
    if (!args.length) {
      // 无参数，直接传入 event 或 detail 数组
      if (isCustomMPEvent) {
        return [event];
      }
      return __args__;
    }
  }
  var extraObj = processEventExtra(vm, extra, event, __args__);
  var ret = [];
  args.forEach(function (arg) {
    if (arg === '$event') {
      if (methodName === '__set_model' && !isCustom) {
        // input v-model value
        ret.push(event.target.value);
      } else {
        if (isCustom && !isCustomMPEvent) {
          ret.push(__args__[0]);
        } else {
          // wxcomponent 组件或内置组件
          ret.push(event);
        }
      }
    } else {
      if (Array.isArray(arg) && arg[0] === 'o') {
        ret.push(getObjByArray(arg));
      } else if (typeof arg === 'string' && hasOwn(extraObj, arg)) {
        ret.push(extraObj[arg]);
      } else {
        ret.push(arg);
      }
    }
  });
  return ret;
}
var ONCE = '~';
var CUSTOM = '^';
function isMatchEventType(eventType, optType) {
  return eventType === optType || optType === 'regionchange' && (eventType === 'begin' || eventType === 'end');
}
function getContextVm(vm) {
  var $parent = vm.$parent;
  // 父组件是 scoped slots 或者其他自定义组件时继续查找
  while ($parent && $parent.$parent && ($parent.$options.generic || $parent.$parent.$options.generic || $parent.$scope._$vuePid)) {
    $parent = $parent.$parent;
  }
  return $parent && $parent.$parent;
}
function handleEvent(event) {
  var _this2 = this;
  event = wrapper$1(event);

  // [['tap',[['handle',[1,2,a]],['handle1',[1,2,a]]]]]
  var dataset = (event.currentTarget || event.target).dataset;
  if (!dataset) {
    return console.warn('事件信息不存在');
  }
  var eventOpts = dataset.eventOpts || dataset['event-opts']; // 支付宝 web-view 组件 dataset 非驼峰
  if (!eventOpts) {
    return console.warn('事件信息不存在');
  }

  // [['handle',[1,2,a]],['handle1',[1,2,a]]]
  var eventType = event.type;
  var ret = [];
  eventOpts.forEach(function (eventOpt) {
    var type = eventOpt[0];
    var eventsArray = eventOpt[1];
    var isCustom = type.charAt(0) === CUSTOM;
    type = isCustom ? type.slice(1) : type;
    var isOnce = type.charAt(0) === ONCE;
    type = isOnce ? type.slice(1) : type;
    if (eventsArray && isMatchEventType(eventType, type)) {
      eventsArray.forEach(function (eventArray) {
        var methodName = eventArray[0];
        if (methodName) {
          var handlerCtx = _this2.$vm;
          if (handlerCtx.$options.generic) {
            // mp-weixin,mp-toutiao 抽象节点模拟 scoped slots
            handlerCtx = getContextVm(handlerCtx) || handlerCtx;
          }
          if (methodName === '$emit') {
            handlerCtx.$emit.apply(handlerCtx, processEventArgs(_this2.$vm, event, eventArray[1], eventArray[2], isCustom, methodName));
            return;
          }
          var handler = handlerCtx[methodName];
          if (!isFn(handler)) {
            var _type = _this2.$vm.mpType === 'page' ? 'Page' : 'Component';
            var path = _this2.route || _this2.is;
            throw new Error("".concat(_type, " \"").concat(path, "\" does not have a method \"").concat(methodName, "\""));
          }
          if (isOnce) {
            if (handler.once) {
              return;
            }
            handler.once = true;
          }
          var params = processEventArgs(_this2.$vm, event, eventArray[1], eventArray[2], isCustom, methodName);
          params = Array.isArray(params) ? params : [];
          // 参数尾部增加原始事件对象用于复杂表达式内获取额外数据
          if (/=\s*\S+\.eventParams\s*\|\|\s*\S+\[['"]event-params['"]\]/.test(handler.toString())) {
            // eslint-disable-next-line no-sparse-arrays
            params = params.concat([,,,,,,,,,, event]);
          }
          ret.push(handler.apply(handlerCtx, params));
        }
      });
    }
  });
  if (eventType === 'input' && ret.length === 1 && typeof ret[0] !== 'undefined') {
    return ret[0];
  }
}
var eventChannels = {};
function getEventChannel(id) {
  var eventChannel = eventChannels[id];
  delete eventChannels[id];
  return eventChannel;
}
var hooks = ['onShow', 'onHide', 'onError', 'onPageNotFound', 'onThemeChange', 'onUnhandledRejection'];
function initEventChannel() {
  _vue.default.prototype.getOpenerEventChannel = function () {
    // 微信小程序使用自身getOpenerEventChannel
    {
      return this.$scope.getOpenerEventChannel();
    }
  };
  var callHook = _vue.default.prototype.__call_hook;
  _vue.default.prototype.__call_hook = function (hook, args) {
    if (hook === 'onLoad' && args && args.__id__) {
      this.__eventChannel__ = getEventChannel(args.__id__);
      delete args.__id__;
    }
    return callHook.call(this, hook, args);
  };
}
function initScopedSlotsParams() {
  var center = {};
  var parents = {};
  function currentId(fn) {
    var vueIds = this.$options.propsData.vueId;
    if (vueIds) {
      var vueId = vueIds.split(',')[0];
      fn(vueId);
    }
  }
  _vue.default.prototype.$hasSSP = function (vueId) {
    var slot = center[vueId];
    if (!slot) {
      parents[vueId] = this;
      this.$on('hook:destroyed', function () {
        delete parents[vueId];
      });
    }
    return slot;
  };
  _vue.default.prototype.$getSSP = function (vueId, name, needAll) {
    var slot = center[vueId];
    if (slot) {
      var params = slot[name] || [];
      if (needAll) {
        return params;
      }
      return params[0];
    }
  };
  _vue.default.prototype.$setSSP = function (name, value) {
    var index = 0;
    currentId.call(this, function (vueId) {
      var slot = center[vueId];
      var params = slot[name] = slot[name] || [];
      params.push(value);
      index = params.length - 1;
    });
    return index;
  };
  _vue.default.prototype.$initSSP = function () {
    currentId.call(this, function (vueId) {
      center[vueId] = {};
    });
  };
  _vue.default.prototype.$callSSP = function () {
    currentId.call(this, function (vueId) {
      if (parents[vueId]) {
        parents[vueId].$forceUpdate();
      }
    });
  };
  _vue.default.mixin({
    destroyed: function destroyed() {
      var propsData = this.$options.propsData;
      var vueId = propsData && propsData.vueId;
      if (vueId) {
        delete center[vueId];
        delete parents[vueId];
      }
    }
  });
}
function parseBaseApp(vm, _ref4) {
  var mocks = _ref4.mocks,
    initRefs = _ref4.initRefs;
  initEventChannel();
  {
    initScopedSlotsParams();
  }
  if (vm.$options.store) {
    _vue.default.prototype.$store = vm.$options.store;
  }
  uniIdMixin(_vue.default);
  _vue.default.prototype.mpHost = "mp-weixin";
  _vue.default.mixin({
    beforeCreate: function beforeCreate() {
      if (!this.$options.mpType) {
        return;
      }
      this.mpType = this.$options.mpType;
      this.$mp = (0, _defineProperty2.default)({
        data: {}
      }, this.mpType, this.$options.mpInstance);
      this.$scope = this.$options.mpInstance;
      delete this.$options.mpType;
      delete this.$options.mpInstance;
      if (this.mpType === 'page' && typeof getApp === 'function') {
        // hack vue-i18n
        var app = getApp();
        if (app.$vm && app.$vm.$i18n) {
          this._i18n = app.$vm.$i18n;
        }
      }
      if (this.mpType !== 'app') {
        initRefs(this);
        initMocks(this, mocks);
      }
    }
  });
  var appOptions = {
    onLaunch: function onLaunch(args) {
      if (this.$vm) {
        // 已经初始化过了，主要是为了百度，百度 onShow 在 onLaunch 之前
        return;
      }
      {
        if (wx.canIUse && !wx.canIUse('nextTick')) {
          // 事实 上2.2.3 即可，简单使用 2.3.0 的 nextTick 判断
          console.error('当前微信基础库版本过低，请将 微信开发者工具-详情-项目设置-调试基础库版本 更换为`2.3.0`以上');
        }
      }
      this.$vm = vm;
      this.$vm.$mp = {
        app: this
      };
      this.$vm.$scope = this;
      // vm 上也挂载 globalData
      this.$vm.globalData = this.globalData;
      this.$vm._isMounted = true;
      this.$vm.__call_hook('mounted', args);
      this.$vm.__call_hook('onLaunch', args);
    }
  };

  // 兼容旧版本 globalData
  appOptions.globalData = vm.$options.globalData || {};
  // 将 methods 中的方法挂在 getApp() 中
  var methods = vm.$options.methods;
  if (methods) {
    Object.keys(methods).forEach(function (name) {
      appOptions[name] = methods[name];
    });
  }
  initAppLocale(_vue.default, vm, normalizeLocale(wx.getSystemInfoSync().language) || LOCALE_EN);
  initHooks(appOptions, hooks);
  initUnknownHooks(appOptions, vm.$options);
  return appOptions;
}
function parseApp(vm) {
  return parseBaseApp(vm, {
    mocks: mocks,
    initRefs: initRefs
  });
}
function createApp(vm) {
  App(parseApp(vm));
  return vm;
}
var encodeReserveRE = /[!'()*]/g;
var encodeReserveReplacer = function encodeReserveReplacer(c) {
  return '%' + c.charCodeAt(0).toString(16);
};
var commaRE = /%2C/g;

// fixed encodeURIComponent which is more conformant to RFC3986:
// - escapes [!'()*]
// - preserve commas
var encode = function encode(str) {
  return encodeURIComponent(str).replace(encodeReserveRE, encodeReserveReplacer).replace(commaRE, ',');
};
function stringifyQuery(obj) {
  var encodeStr = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : encode;
  var res = obj ? Object.keys(obj).map(function (key) {
    var val = obj[key];
    if (val === undefined) {
      return '';
    }
    if (val === null) {
      return encodeStr(key);
    }
    if (Array.isArray(val)) {
      var result = [];
      val.forEach(function (val2) {
        if (val2 === undefined) {
          return;
        }
        if (val2 === null) {
          result.push(encodeStr(key));
        } else {
          result.push(encodeStr(key) + '=' + encodeStr(val2));
        }
      });
      return result.join('&');
    }
    return encodeStr(key) + '=' + encodeStr(val);
  }).filter(function (x) {
    return x.length > 0;
  }).join('&') : null;
  return res ? "?".concat(res) : '';
}
function parseBaseComponent(vueComponentOptions) {
  var _ref5 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
    isPage = _ref5.isPage,
    initRelation = _ref5.initRelation;
  var needVueOptions = arguments.length > 2 ? arguments[2] : undefined;
  var _initVueComponent = initVueComponent(_vue.default, vueComponentOptions),
    _initVueComponent2 = (0, _slicedToArray2.default)(_initVueComponent, 2),
    VueComponent = _initVueComponent2[0],
    vueOptions = _initVueComponent2[1];
  var options = _objectSpread({
    multipleSlots: true,
    // styleIsolation: 'apply-shared',
    addGlobalClass: true
  }, vueOptions.options || {});
  {
    // 微信 multipleSlots 部分情况有 bug，导致内容顺序错乱 如 u-list，提供覆盖选项
    if (vueOptions['mp-weixin'] && vueOptions['mp-weixin'].options) {
      Object.assign(options, vueOptions['mp-weixin'].options);
    }
  }
  var componentOptions = {
    options: options,
    data: initData(vueOptions, _vue.default.prototype),
    behaviors: initBehaviors(vueOptions, initBehavior),
    properties: initProperties(vueOptions.props, false, vueOptions.__file, options),
    lifetimes: {
      attached: function attached() {
        var properties = this.properties;
        var options = {
          mpType: isPage.call(this) ? 'page' : 'component',
          mpInstance: this,
          propsData: properties
        };
        initVueIds(properties.vueId, this);

        // 处理父子关系
        initRelation.call(this, {
          vuePid: this._$vuePid,
          vueOptions: options
        });

        // 初始化 vue 实例
        this.$vm = new VueComponent(options);

        // 处理$slots,$scopedSlots（暂不支持动态变化$slots）
        initSlots(this.$vm, properties.vueSlots);

        // 触发首次 setData
        this.$vm.$mount();
      },
      ready: function ready() {
        // 当组件 props 默认值为 true，初始化时传入 false 会导致 created,ready 触发, 但 attached 不触发
        // https://developers.weixin.qq.com/community/develop/doc/00066ae2844cc0f8eb883e2a557800
        if (this.$vm) {
          this.$vm._isMounted = true;
          this.$vm.__call_hook('mounted');
          this.$vm.__call_hook('onReady');
        }
      },
      detached: function detached() {
        this.$vm && this.$vm.$destroy();
      }
    },
    pageLifetimes: {
      show: function show(args) {
        this.$vm && this.$vm.__call_hook('onPageShow', args);
      },
      hide: function hide() {
        this.$vm && this.$vm.__call_hook('onPageHide');
      },
      resize: function resize(size) {
        this.$vm && this.$vm.__call_hook('onPageResize', size);
      }
    },
    methods: {
      __l: handleLink,
      __e: handleEvent
    }
  };
  // externalClasses
  if (vueOptions.externalClasses) {
    componentOptions.externalClasses = vueOptions.externalClasses;
  }
  if (Array.isArray(vueOptions.wxsCallMethods)) {
    vueOptions.wxsCallMethods.forEach(function (callMethod) {
      componentOptions.methods[callMethod] = function (args) {
        return this.$vm[callMethod](args);
      };
    });
  }
  if (needVueOptions) {
    return [componentOptions, vueOptions, VueComponent];
  }
  if (isPage) {
    return componentOptions;
  }
  return [componentOptions, VueComponent];
}
function parseComponent(vueComponentOptions, needVueOptions) {
  return parseBaseComponent(vueComponentOptions, {
    isPage: isPage,
    initRelation: initRelation
  }, needVueOptions);
}
var hooks$1 = ['onShow', 'onHide', 'onUnload'];
hooks$1.push.apply(hooks$1, PAGE_EVENT_HOOKS);
function parseBasePage(vuePageOptions) {
  var _parseComponent = parseComponent(vuePageOptions, true),
    _parseComponent2 = (0, _slicedToArray2.default)(_parseComponent, 2),
    pageOptions = _parseComponent2[0],
    vueOptions = _parseComponent2[1];
  initHooks(pageOptions.methods, hooks$1, vueOptions);
  pageOptions.methods.onLoad = function (query) {
    this.options = query;
    var copyQuery = Object.assign({}, query);
    delete copyQuery.__id__;
    this.$page = {
      fullPath: '/' + (this.route || this.is) + stringifyQuery(copyQuery)
    };
    this.$vm.$mp.query = query; // 兼容 mpvue
    this.$vm.__call_hook('onLoad', query);
  };
  {
    initUnknownHooks(pageOptions.methods, vuePageOptions, ['onReady']);
  }
  {
    initWorkletMethods(pageOptions.methods, vueOptions.methods);
  }
  return pageOptions;
}
function parsePage(vuePageOptions) {
  return parseBasePage(vuePageOptions);
}
function createPage(vuePageOptions) {
  {
    return Component(parsePage(vuePageOptions));
  }
}
function createComponent(vueOptions) {
  {
    return Component(parseComponent(vueOptions));
  }
}
function createSubpackageApp(vm) {
  var appOptions = parseApp(vm);
  var app = getApp({
    allowDefault: true
  });
  vm.$scope = app;
  var globalData = app.globalData;
  if (globalData) {
    Object.keys(appOptions.globalData).forEach(function (name) {
      if (!hasOwn(globalData, name)) {
        globalData[name] = appOptions.globalData[name];
      }
    });
  }
  Object.keys(appOptions).forEach(function (name) {
    if (!hasOwn(app, name)) {
      app[name] = appOptions[name];
    }
  });
  if (isFn(appOptions.onShow) && wx.onAppShow) {
    wx.onAppShow(function () {
      for (var _len5 = arguments.length, args = new Array(_len5), _key5 = 0; _key5 < _len5; _key5++) {
        args[_key5] = arguments[_key5];
      }
      vm.__call_hook('onShow', args);
    });
  }
  if (isFn(appOptions.onHide) && wx.onAppHide) {
    wx.onAppHide(function () {
      for (var _len6 = arguments.length, args = new Array(_len6), _key6 = 0; _key6 < _len6; _key6++) {
        args[_key6] = arguments[_key6];
      }
      vm.__call_hook('onHide', args);
    });
  }
  if (isFn(appOptions.onLaunch)) {
    var args = wx.getLaunchOptionsSync && wx.getLaunchOptionsSync();
    vm.__call_hook('onLaunch', args);
  }
  return vm;
}
function createPlugin(vm) {
  var appOptions = parseApp(vm);
  if (isFn(appOptions.onShow) && wx.onAppShow) {
    wx.onAppShow(function () {
      for (var _len7 = arguments.length, args = new Array(_len7), _key7 = 0; _key7 < _len7; _key7++) {
        args[_key7] = arguments[_key7];
      }
      vm.__call_hook('onShow', args);
    });
  }
  if (isFn(appOptions.onHide) && wx.onAppHide) {
    wx.onAppHide(function () {
      for (var _len8 = arguments.length, args = new Array(_len8), _key8 = 0; _key8 < _len8; _key8++) {
        args[_key8] = arguments[_key8];
      }
      vm.__call_hook('onHide', args);
    });
  }
  if (isFn(appOptions.onLaunch)) {
    var args = wx.getLaunchOptionsSync && wx.getLaunchOptionsSync();
    vm.__call_hook('onLaunch', args);
  }
  return vm;
}
todos.forEach(function (todoApi) {
  protocols[todoApi] = false;
});
canIUses.forEach(function (canIUseApi) {
  var apiName = protocols[canIUseApi] && protocols[canIUseApi].name ? protocols[canIUseApi].name : canIUseApi;
  if (!wx.canIUse(apiName)) {
    protocols[canIUseApi] = false;
  }
});
var uni = {};
if (typeof Proxy !== 'undefined' && "mp-weixin" !== 'app-plus') {
  uni = new Proxy({}, {
    get: function get(target, name) {
      if (hasOwn(target, name)) {
        return target[name];
      }
      if (baseApi[name]) {
        return baseApi[name];
      }
      if (api[name]) {
        return promisify(name, api[name]);
      }
      {
        if (extraApi[name]) {
          return promisify(name, extraApi[name]);
        }
        if (todoApis[name]) {
          return promisify(name, todoApis[name]);
        }
      }
      if (eventApi[name]) {
        return eventApi[name];
      }
      return promisify(name, wrapper(name, wx[name]));
    },
    set: function set(target, name, value) {
      target[name] = value;
      return true;
    }
  });
} else {
  Object.keys(baseApi).forEach(function (name) {
    uni[name] = baseApi[name];
  });
  {
    Object.keys(todoApis).forEach(function (name) {
      uni[name] = promisify(name, todoApis[name]);
    });
    Object.keys(extraApi).forEach(function (name) {
      uni[name] = promisify(name, extraApi[name]);
    });
  }
  Object.keys(eventApi).forEach(function (name) {
    uni[name] = eventApi[name];
  });
  Object.keys(api).forEach(function (name) {
    uni[name] = promisify(name, api[name]);
  });
  Object.keys(wx).forEach(function (name) {
    if (hasOwn(wx, name) || hasOwn(protocols, name)) {
      uni[name] = promisify(name, wrapper(name, wx[name]));
    }
  });
}
wx.createApp = createApp;
wx.createPage = createPage;
wx.createComponent = createComponent;
wx.createSubpackageApp = createSubpackageApp;
wx.createPlugin = createPlugin;
var uni$1 = uni;
var _default = uni$1;
exports.default = _default;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/wx.js */ 1)["default"], __webpack_require__(/*! ./../../../webpack/buildin/global.js */ 3)))

/***/ }),
/* 3 */
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 4 */
/*!**********************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/interopRequireDefault.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}
module.exports = _interopRequireDefault, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 5 */
/*!**************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/slicedToArray.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var arrayWithHoles = __webpack_require__(/*! ./arrayWithHoles.js */ 6);
var iterableToArrayLimit = __webpack_require__(/*! ./iterableToArrayLimit.js */ 7);
var unsupportedIterableToArray = __webpack_require__(/*! ./unsupportedIterableToArray.js */ 8);
var nonIterableRest = __webpack_require__(/*! ./nonIterableRest.js */ 10);
function _slicedToArray(arr, i) {
  return arrayWithHoles(arr) || iterableToArrayLimit(arr, i) || unsupportedIterableToArray(arr, i) || nonIterableRest();
}
module.exports = _slicedToArray, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 6 */
/*!***************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/arrayWithHoles.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _arrayWithHoles(arr) {
  if (Array.isArray(arr)) return arr;
}
module.exports = _arrayWithHoles, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 7 */
/*!*********************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/iterableToArrayLimit.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _iterableToArrayLimit(arr, i) {
  var _i = null == arr ? null : "undefined" != typeof Symbol && arr[Symbol.iterator] || arr["@@iterator"];
  if (null != _i) {
    var _s,
      _e,
      _x,
      _r,
      _arr = [],
      _n = !0,
      _d = !1;
    try {
      if (_x = (_i = _i.call(arr)).next, 0 === i) {
        if (Object(_i) !== _i) return;
        _n = !1;
      } else for (; !(_n = (_s = _x.call(_i)).done) && (_arr.push(_s.value), _arr.length !== i); _n = !0) {
        ;
      }
    } catch (err) {
      _d = !0, _e = err;
    } finally {
      try {
        if (!_n && null != _i["return"] && (_r = _i["return"](), Object(_r) !== _r)) return;
      } finally {
        if (_d) throw _e;
      }
    }
    return _arr;
  }
}
module.exports = _iterableToArrayLimit, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 8 */
/*!***************************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/unsupportedIterableToArray.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var arrayLikeToArray = __webpack_require__(/*! ./arrayLikeToArray.js */ 9);
function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return arrayLikeToArray(o, minLen);
}
module.exports = _unsupportedIterableToArray, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 9 */
/*!*****************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/arrayLikeToArray.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;
  for (var i = 0, arr2 = new Array(len); i < len; i++) {
    arr2[i] = arr[i];
  }
  return arr2;
}
module.exports = _arrayLikeToArray, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 10 */
/*!****************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/nonIterableRest.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _nonIterableRest() {
  throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}
module.exports = _nonIterableRest, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 11 */
/*!***************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/defineProperty.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var toPropertyKey = __webpack_require__(/*! ./toPropertyKey.js */ 12);
function _defineProperty(obj, key, value) {
  key = toPropertyKey(key);
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }
  return obj;
}
module.exports = _defineProperty, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 12 */
/*!**************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/toPropertyKey.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__(/*! ./typeof.js */ 13)["default"];
var toPrimitive = __webpack_require__(/*! ./toPrimitive.js */ 14);
function _toPropertyKey(arg) {
  var key = toPrimitive(arg, "string");
  return _typeof(key) === "symbol" ? key : String(key);
}
module.exports = _toPropertyKey, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 13 */
/*!*******************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/typeof.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _typeof(obj) {
  "@babel/helpers - typeof";

  return (module.exports = _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) {
    return typeof obj;
  } : function (obj) {
    return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
  }, module.exports.__esModule = true, module.exports["default"] = module.exports), _typeof(obj);
}
module.exports = _typeof, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 14 */
/*!************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/toPrimitive.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__(/*! ./typeof.js */ 13)["default"];
function _toPrimitive(input, hint) {
  if (_typeof(input) !== "object" || input === null) return input;
  var prim = input[Symbol.toPrimitive];
  if (prim !== undefined) {
    var res = prim.call(input, hint || "default");
    if (_typeof(res) !== "object") return res;
    throw new TypeError("@@toPrimitive must return a primitive value.");
  }
  return (hint === "string" ? String : Number)(input);
}
module.exports = _toPrimitive, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 15 */
/*!**********************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/construct.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var setPrototypeOf = __webpack_require__(/*! ./setPrototypeOf.js */ 16);
var isNativeReflectConstruct = __webpack_require__(/*! ./isNativeReflectConstruct.js */ 17);
function _construct(Parent, args, Class) {
  if (isNativeReflectConstruct()) {
    module.exports = _construct = Reflect.construct.bind(), module.exports.__esModule = true, module.exports["default"] = module.exports;
  } else {
    module.exports = _construct = function _construct(Parent, args, Class) {
      var a = [null];
      a.push.apply(a, args);
      var Constructor = Function.bind.apply(Parent, a);
      var instance = new Constructor();
      if (Class) setPrototypeOf(instance, Class.prototype);
      return instance;
    }, module.exports.__esModule = true, module.exports["default"] = module.exports;
  }
  return _construct.apply(null, arguments);
}
module.exports = _construct, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 16 */
/*!***************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/setPrototypeOf.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _setPrototypeOf(o, p) {
  module.exports = _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  }, module.exports.__esModule = true, module.exports["default"] = module.exports;
  return _setPrototypeOf(o, p);
}
module.exports = _setPrototypeOf, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 17 */
/*!*************************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/isNativeReflectConstruct.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _isNativeReflectConstruct() {
  if (typeof Reflect === "undefined" || !Reflect.construct) return false;
  if (Reflect.construct.sham) return false;
  if (typeof Proxy === "function") return true;
  try {
    Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {}));
    return true;
  } catch (e) {
    return false;
  }
}
module.exports = _isNativeReflectConstruct, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 18 */
/*!******************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/toConsumableArray.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var arrayWithoutHoles = __webpack_require__(/*! ./arrayWithoutHoles.js */ 19);
var iterableToArray = __webpack_require__(/*! ./iterableToArray.js */ 20);
var unsupportedIterableToArray = __webpack_require__(/*! ./unsupportedIterableToArray.js */ 8);
var nonIterableSpread = __webpack_require__(/*! ./nonIterableSpread.js */ 21);
function _toConsumableArray(arr) {
  return arrayWithoutHoles(arr) || iterableToArray(arr) || unsupportedIterableToArray(arr) || nonIterableSpread();
}
module.exports = _toConsumableArray, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 19 */
/*!******************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/arrayWithoutHoles.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var arrayLikeToArray = __webpack_require__(/*! ./arrayLikeToArray.js */ 9);
function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) return arrayLikeToArray(arr);
}
module.exports = _arrayWithoutHoles, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 20 */
/*!****************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/iterableToArray.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _iterableToArray(iter) {
  if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter);
}
module.exports = _iterableToArray, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 21 */
/*!******************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/nonIterableSpread.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}
module.exports = _nonIterableSpread, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 22 */
/*!*************************************************************!*\
  !*** ./node_modules/@dcloudio/uni-i18n/dist/uni-i18n.es.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(uni, global) {

var _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ 4);
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LOCALE_ZH_HANT = exports.LOCALE_ZH_HANS = exports.LOCALE_FR = exports.LOCALE_ES = exports.LOCALE_EN = exports.I18n = exports.Formatter = void 0;
exports.compileI18nJsonStr = compileI18nJsonStr;
exports.hasI18nJson = hasI18nJson;
exports.initVueI18n = initVueI18n;
exports.isI18nStr = isI18nStr;
exports.isString = void 0;
exports.normalizeLocale = normalizeLocale;
exports.parseI18nJson = parseI18nJson;
exports.resolveLocale = resolveLocale;
var _slicedToArray2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/slicedToArray */ 5));
var _classCallCheck2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ 23));
var _createClass2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/createClass */ 24));
var _typeof2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/typeof */ 13));
var isObject = function isObject(val) {
  return val !== null && (0, _typeof2.default)(val) === 'object';
};
var defaultDelimiters = ['{', '}'];
var BaseFormatter = /*#__PURE__*/function () {
  function BaseFormatter() {
    (0, _classCallCheck2.default)(this, BaseFormatter);
    this._caches = Object.create(null);
  }
  (0, _createClass2.default)(BaseFormatter, [{
    key: "interpolate",
    value: function interpolate(message, values) {
      var delimiters = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : defaultDelimiters;
      if (!values) {
        return [message];
      }
      var tokens = this._caches[message];
      if (!tokens) {
        tokens = parse(message, delimiters);
        this._caches[message] = tokens;
      }
      return compile(tokens, values);
    }
  }]);
  return BaseFormatter;
}();
exports.Formatter = BaseFormatter;
var RE_TOKEN_LIST_VALUE = /^(?:\d)+/;
var RE_TOKEN_NAMED_VALUE = /^(?:\w)+/;
function parse(format, _ref) {
  var _ref2 = (0, _slicedToArray2.default)(_ref, 2),
    startDelimiter = _ref2[0],
    endDelimiter = _ref2[1];
  var tokens = [];
  var position = 0;
  var text = '';
  while (position < format.length) {
    var char = format[position++];
    if (char === startDelimiter) {
      if (text) {
        tokens.push({
          type: 'text',
          value: text
        });
      }
      text = '';
      var sub = '';
      char = format[position++];
      while (char !== undefined && char !== endDelimiter) {
        sub += char;
        char = format[position++];
      }
      var isClosed = char === endDelimiter;
      var type = RE_TOKEN_LIST_VALUE.test(sub) ? 'list' : isClosed && RE_TOKEN_NAMED_VALUE.test(sub) ? 'named' : 'unknown';
      tokens.push({
        value: sub,
        type: type
      });
    }
    //  else if (char === '%') {
    //   // when found rails i18n syntax, skip text capture
    //   if (format[position] !== '{') {
    //     text += char
    //   }
    // }
    else {
      text += char;
    }
  }
  text && tokens.push({
    type: 'text',
    value: text
  });
  return tokens;
}
function compile(tokens, values) {
  var compiled = [];
  var index = 0;
  var mode = Array.isArray(values) ? 'list' : isObject(values) ? 'named' : 'unknown';
  if (mode === 'unknown') {
    return compiled;
  }
  while (index < tokens.length) {
    var token = tokens[index];
    switch (token.type) {
      case 'text':
        compiled.push(token.value);
        break;
      case 'list':
        compiled.push(values[parseInt(token.value, 10)]);
        break;
      case 'named':
        if (mode === 'named') {
          compiled.push(values[token.value]);
        } else {
          if (true) {
            console.warn("Type of token '".concat(token.type, "' and format of value '").concat(mode, "' don't match!"));
          }
        }
        break;
      case 'unknown':
        if (true) {
          console.warn("Detect 'unknown' type of token!");
        }
        break;
    }
    index++;
  }
  return compiled;
}
var LOCALE_ZH_HANS = 'zh-Hans';
exports.LOCALE_ZH_HANS = LOCALE_ZH_HANS;
var LOCALE_ZH_HANT = 'zh-Hant';
exports.LOCALE_ZH_HANT = LOCALE_ZH_HANT;
var LOCALE_EN = 'en';
exports.LOCALE_EN = LOCALE_EN;
var LOCALE_FR = 'fr';
exports.LOCALE_FR = LOCALE_FR;
var LOCALE_ES = 'es';
exports.LOCALE_ES = LOCALE_ES;
var hasOwnProperty = Object.prototype.hasOwnProperty;
var hasOwn = function hasOwn(val, key) {
  return hasOwnProperty.call(val, key);
};
var defaultFormatter = new BaseFormatter();
function include(str, parts) {
  return !!parts.find(function (part) {
    return str.indexOf(part) !== -1;
  });
}
function startsWith(str, parts) {
  return parts.find(function (part) {
    return str.indexOf(part) === 0;
  });
}
function normalizeLocale(locale, messages) {
  if (!locale) {
    return;
  }
  locale = locale.trim().replace(/_/g, '-');
  if (messages && messages[locale]) {
    return locale;
  }
  locale = locale.toLowerCase();
  if (locale === 'chinese') {
    // 支付宝
    return LOCALE_ZH_HANS;
  }
  if (locale.indexOf('zh') === 0) {
    if (locale.indexOf('-hans') > -1) {
      return LOCALE_ZH_HANS;
    }
    if (locale.indexOf('-hant') > -1) {
      return LOCALE_ZH_HANT;
    }
    if (include(locale, ['-tw', '-hk', '-mo', '-cht'])) {
      return LOCALE_ZH_HANT;
    }
    return LOCALE_ZH_HANS;
  }
  var locales = [LOCALE_EN, LOCALE_FR, LOCALE_ES];
  if (messages && Object.keys(messages).length > 0) {
    locales = Object.keys(messages);
  }
  var lang = startsWith(locale, locales);
  if (lang) {
    return lang;
  }
}
var I18n = /*#__PURE__*/function () {
  function I18n(_ref3) {
    var locale = _ref3.locale,
      fallbackLocale = _ref3.fallbackLocale,
      messages = _ref3.messages,
      watcher = _ref3.watcher,
      formater = _ref3.formater;
    (0, _classCallCheck2.default)(this, I18n);
    this.locale = LOCALE_EN;
    this.fallbackLocale = LOCALE_EN;
    this.message = {};
    this.messages = {};
    this.watchers = [];
    if (fallbackLocale) {
      this.fallbackLocale = fallbackLocale;
    }
    this.formater = formater || defaultFormatter;
    this.messages = messages || {};
    this.setLocale(locale || LOCALE_EN);
    if (watcher) {
      this.watchLocale(watcher);
    }
  }
  (0, _createClass2.default)(I18n, [{
    key: "setLocale",
    value: function setLocale(locale) {
      var _this = this;
      var oldLocale = this.locale;
      this.locale = normalizeLocale(locale, this.messages) || this.fallbackLocale;
      if (!this.messages[this.locale]) {
        // 可能初始化时不存在
        this.messages[this.locale] = {};
      }
      this.message = this.messages[this.locale];
      // 仅发生变化时，通知
      if (oldLocale !== this.locale) {
        this.watchers.forEach(function (watcher) {
          watcher(_this.locale, oldLocale);
        });
      }
    }
  }, {
    key: "getLocale",
    value: function getLocale() {
      return this.locale;
    }
  }, {
    key: "watchLocale",
    value: function watchLocale(fn) {
      var _this2 = this;
      var index = this.watchers.push(fn) - 1;
      return function () {
        _this2.watchers.splice(index, 1);
      };
    }
  }, {
    key: "add",
    value: function add(locale, message) {
      var override = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
      var curMessages = this.messages[locale];
      if (curMessages) {
        if (override) {
          Object.assign(curMessages, message);
        } else {
          Object.keys(message).forEach(function (key) {
            if (!hasOwn(curMessages, key)) {
              curMessages[key] = message[key];
            }
          });
        }
      } else {
        this.messages[locale] = message;
      }
    }
  }, {
    key: "f",
    value: function f(message, values, delimiters) {
      return this.formater.interpolate(message, values, delimiters).join('');
    }
  }, {
    key: "t",
    value: function t(key, locale, values) {
      var message = this.message;
      if (typeof locale === 'string') {
        locale = normalizeLocale(locale, this.messages);
        locale && (message = this.messages[locale]);
      } else {
        values = locale;
      }
      if (!hasOwn(message, key)) {
        console.warn("Cannot translate the value of keypath ".concat(key, ". Use the value of keypath as default."));
        return key;
      }
      return this.formater.interpolate(message[key], values).join('');
    }
  }]);
  return I18n;
}();
exports.I18n = I18n;
function watchAppLocale(appVm, i18n) {
  // 需要保证 watch 的触发在组件渲染之前
  if (appVm.$watchLocale) {
    // vue2
    appVm.$watchLocale(function (newLocale) {
      i18n.setLocale(newLocale);
    });
  } else {
    appVm.$watch(function () {
      return appVm.$locale;
    }, function (newLocale) {
      i18n.setLocale(newLocale);
    });
  }
}
function getDefaultLocale() {
  if (typeof uni !== 'undefined' && uni.getLocale) {
    return uni.getLocale();
  }
  // 小程序平台，uni 和 uni-i18n 互相引用，导致访问不到 uni，故在 global 上挂了 getLocale
  if (typeof global !== 'undefined' && global.getLocale) {
    return global.getLocale();
  }
  return LOCALE_EN;
}
function initVueI18n(locale) {
  var messages = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var fallbackLocale = arguments.length > 2 ? arguments[2] : undefined;
  var watcher = arguments.length > 3 ? arguments[3] : undefined;
  // 兼容旧版本入参
  if (typeof locale !== 'string') {
    var _ref4 = [messages, locale];
    locale = _ref4[0];
    messages = _ref4[1];
  }
  if (typeof locale !== 'string') {
    // 因为小程序平台，uni-i18n 和 uni 互相引用，导致此时访问 uni 时，为 undefined
    locale = getDefaultLocale();
  }
  if (typeof fallbackLocale !== 'string') {
    fallbackLocale = typeof __uniConfig !== 'undefined' && __uniConfig.fallbackLocale || LOCALE_EN;
  }
  var i18n = new I18n({
    locale: locale,
    fallbackLocale: fallbackLocale,
    messages: messages,
    watcher: watcher
  });
  var _t = function t(key, values) {
    if (typeof getApp !== 'function') {
      // app view
      /* eslint-disable no-func-assign */
      _t = function t(key, values) {
        return i18n.t(key, values);
      };
    } else {
      var isWatchedAppLocale = false;
      _t = function t(key, values) {
        var appVm = getApp().$vm;
        // 可能$vm还不存在，比如在支付宝小程序中，组件定义较早，在props的default里使用了t()函数（如uni-goods-nav），此时app还未初始化
        // options: {
        // 	type: Array,
        // 	default () {
        // 		return [{
        // 			icon: 'shop',
        // 			text: t("uni-goods-nav.options.shop"),
        // 		}, {
        // 			icon: 'cart',
        // 			text: t("uni-goods-nav.options.cart")
        // 		}]
        // 	}
        // },
        if (appVm) {
          // 触发响应式
          appVm.$locale;
          if (!isWatchedAppLocale) {
            isWatchedAppLocale = true;
            watchAppLocale(appVm, i18n);
          }
        }
        return i18n.t(key, values);
      };
    }
    return _t(key, values);
  };
  return {
    i18n: i18n,
    f: function f(message, values, delimiters) {
      return i18n.f(message, values, delimiters);
    },
    t: function t(key, values) {
      return _t(key, values);
    },
    add: function add(locale, message) {
      var override = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
      return i18n.add(locale, message, override);
    },
    watch: function watch(fn) {
      return i18n.watchLocale(fn);
    },
    getLocale: function getLocale() {
      return i18n.getLocale();
    },
    setLocale: function setLocale(newLocale) {
      return i18n.setLocale(newLocale);
    }
  };
}
var isString = function isString(val) {
  return typeof val === 'string';
};
exports.isString = isString;
var formater;
function hasI18nJson(jsonObj, delimiters) {
  if (!formater) {
    formater = new BaseFormatter();
  }
  return walkJsonObj(jsonObj, function (jsonObj, key) {
    var value = jsonObj[key];
    if (isString(value)) {
      if (isI18nStr(value, delimiters)) {
        return true;
      }
    } else {
      return hasI18nJson(value, delimiters);
    }
  });
}
function parseI18nJson(jsonObj, values, delimiters) {
  if (!formater) {
    formater = new BaseFormatter();
  }
  walkJsonObj(jsonObj, function (jsonObj, key) {
    var value = jsonObj[key];
    if (isString(value)) {
      if (isI18nStr(value, delimiters)) {
        jsonObj[key] = compileStr(value, values, delimiters);
      }
    } else {
      parseI18nJson(value, values, delimiters);
    }
  });
  return jsonObj;
}
function compileI18nJsonStr(jsonStr, _ref5) {
  var locale = _ref5.locale,
    locales = _ref5.locales,
    delimiters = _ref5.delimiters;
  if (!isI18nStr(jsonStr, delimiters)) {
    return jsonStr;
  }
  if (!formater) {
    formater = new BaseFormatter();
  }
  var localeValues = [];
  Object.keys(locales).forEach(function (name) {
    if (name !== locale) {
      localeValues.push({
        locale: name,
        values: locales[name]
      });
    }
  });
  localeValues.unshift({
    locale: locale,
    values: locales[locale]
  });
  try {
    return JSON.stringify(compileJsonObj(JSON.parse(jsonStr), localeValues, delimiters), null, 2);
  } catch (e) {}
  return jsonStr;
}
function isI18nStr(value, delimiters) {
  return value.indexOf(delimiters[0]) > -1;
}
function compileStr(value, values, delimiters) {
  return formater.interpolate(value, values, delimiters).join('');
}
function compileValue(jsonObj, key, localeValues, delimiters) {
  var value = jsonObj[key];
  if (isString(value)) {
    // 存在国际化
    if (isI18nStr(value, delimiters)) {
      jsonObj[key] = compileStr(value, localeValues[0].values, delimiters);
      if (localeValues.length > 1) {
        // 格式化国际化语言
        var valueLocales = jsonObj[key + 'Locales'] = {};
        localeValues.forEach(function (localValue) {
          valueLocales[localValue.locale] = compileStr(value, localValue.values, delimiters);
        });
      }
    }
  } else {
    compileJsonObj(value, localeValues, delimiters);
  }
}
function compileJsonObj(jsonObj, localeValues, delimiters) {
  walkJsonObj(jsonObj, function (jsonObj, key) {
    compileValue(jsonObj, key, localeValues, delimiters);
  });
  return jsonObj;
}
function walkJsonObj(jsonObj, walk) {
  if (Array.isArray(jsonObj)) {
    for (var i = 0; i < jsonObj.length; i++) {
      if (walk(jsonObj, i)) {
        return true;
      }
    }
  } else if (isObject(jsonObj)) {
    for (var key in jsonObj) {
      if (walk(jsonObj, key)) {
        return true;
      }
    }
  }
  return false;
}
function resolveLocale(locales) {
  return function (locale) {
    if (!locale) {
      return locale;
    }
    locale = normalizeLocale(locale) || locale;
    return resolveLocaleChain(locale).find(function (locale) {
      return locales.indexOf(locale) > -1;
    });
  };
}
function resolveLocaleChain(locale) {
  var chain = [];
  var tokens = locale.split('-');
  while (tokens.length) {
    chain.push(tokens.join('-'));
    tokens.pop();
  }
  return chain;
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js */ 2)["default"], __webpack_require__(/*! ./../../../webpack/buildin/global.js */ 3)))

/***/ }),
/* 23 */
/*!***************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/classCallCheck.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}
module.exports = _classCallCheck, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 24 */
/*!************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/createClass.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var toPropertyKey = __webpack_require__(/*! ./toPropertyKey.js */ 12);
function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, toPropertyKey(descriptor.key), descriptor);
  }
}
function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  Object.defineProperty(Constructor, "prototype", {
    writable: false
  });
  return Constructor;
}
module.exports = _createClass, module.exports.__esModule = true, module.exports["default"] = module.exports;

/***/ }),
/* 25 */
/*!******************************************************************************************!*\
  !*** ./node_modules/@dcloudio/vue-cli-plugin-uni/packages/mp-vue/dist/mp.runtime.esm.js ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {/*!
 * Vue.js v2.6.11
 * (c) 2014-2023 Evan You
 * Released under the MIT License.
 */
/*  */

var emptyObject = Object.freeze({});

// These helpers produce better VM code in JS engines due to their
// explicitness and function inlining.
function isUndef (v) {
  return v === undefined || v === null
}

function isDef (v) {
  return v !== undefined && v !== null
}

function isTrue (v) {
  return v === true
}

function isFalse (v) {
  return v === false
}

/**
 * Check if value is primitive.
 */
function isPrimitive (value) {
  return (
    typeof value === 'string' ||
    typeof value === 'number' ||
    // $flow-disable-line
    typeof value === 'symbol' ||
    typeof value === 'boolean'
  )
}

/**
 * Quick object check - this is primarily used to tell
 * Objects from primitive values when we know the value
 * is a JSON-compliant type.
 */
function isObject (obj) {
  return obj !== null && typeof obj === 'object'
}

/**
 * Get the raw type string of a value, e.g., [object Object].
 */
var _toString = Object.prototype.toString;

function toRawType (value) {
  return _toString.call(value).slice(8, -1)
}

/**
 * Strict object type check. Only returns true
 * for plain JavaScript objects.
 */
function isPlainObject (obj) {
  return _toString.call(obj) === '[object Object]'
}

function isRegExp (v) {
  return _toString.call(v) === '[object RegExp]'
}

/**
 * Check if val is a valid array index.
 */
function isValidArrayIndex (val) {
  var n = parseFloat(String(val));
  return n >= 0 && Math.floor(n) === n && isFinite(val)
}

function isPromise (val) {
  return (
    isDef(val) &&
    typeof val.then === 'function' &&
    typeof val.catch === 'function'
  )
}

/**
 * Convert a value to a string that is actually rendered.
 */
function toString (val) {
  return val == null
    ? ''
    : Array.isArray(val) || (isPlainObject(val) && val.toString === _toString)
      ? JSON.stringify(val, null, 2)
      : String(val)
}

/**
 * Convert an input value to a number for persistence.
 * If the conversion fails, return original string.
 */
function toNumber (val) {
  var n = parseFloat(val);
  return isNaN(n) ? val : n
}

/**
 * Make a map and return a function for checking if a key
 * is in that map.
 */
function makeMap (
  str,
  expectsLowerCase
) {
  var map = Object.create(null);
  var list = str.split(',');
  for (var i = 0; i < list.length; i++) {
    map[list[i]] = true;
  }
  return expectsLowerCase
    ? function (val) { return map[val.toLowerCase()]; }
    : function (val) { return map[val]; }
}

/**
 * Check if a tag is a built-in tag.
 */
var isBuiltInTag = makeMap('slot,component', true);

/**
 * Check if an attribute is a reserved attribute.
 */
var isReservedAttribute = makeMap('key,ref,slot,slot-scope,is');

/**
 * Remove an item from an array.
 */
function remove (arr, item) {
  if (arr.length) {
    var index = arr.indexOf(item);
    if (index > -1) {
      return arr.splice(index, 1)
    }
  }
}

/**
 * Check whether an object has the property.
 */
var hasOwnProperty = Object.prototype.hasOwnProperty;
function hasOwn (obj, key) {
  return hasOwnProperty.call(obj, key)
}

/**
 * Create a cached version of a pure function.
 */
function cached (fn) {
  var cache = Object.create(null);
  return (function cachedFn (str) {
    var hit = cache[str];
    return hit || (cache[str] = fn(str))
  })
}

/**
 * Camelize a hyphen-delimited string.
 */
var camelizeRE = /-(\w)/g;
var camelize = cached(function (str) {
  return str.replace(camelizeRE, function (_, c) { return c ? c.toUpperCase() : ''; })
});

/**
 * Capitalize a string.
 */
var capitalize = cached(function (str) {
  return str.charAt(0).toUpperCase() + str.slice(1)
});

/**
 * Hyphenate a camelCase string.
 */
var hyphenateRE = /\B([A-Z])/g;
var hyphenate = cached(function (str) {
  return str.replace(hyphenateRE, '-$1').toLowerCase()
});

/**
 * Simple bind polyfill for environments that do not support it,
 * e.g., PhantomJS 1.x. Technically, we don't need this anymore
 * since native bind is now performant enough in most browsers.
 * But removing it would mean breaking code that was able to run in
 * PhantomJS 1.x, so this must be kept for backward compatibility.
 */

/* istanbul ignore next */
function polyfillBind (fn, ctx) {
  function boundFn (a) {
    var l = arguments.length;
    return l
      ? l > 1
        ? fn.apply(ctx, arguments)
        : fn.call(ctx, a)
      : fn.call(ctx)
  }

  boundFn._length = fn.length;
  return boundFn
}

function nativeBind (fn, ctx) {
  return fn.bind(ctx)
}

var bind = Function.prototype.bind
  ? nativeBind
  : polyfillBind;

/**
 * Convert an Array-like object to a real Array.
 */
function toArray (list, start) {
  start = start || 0;
  var i = list.length - start;
  var ret = new Array(i);
  while (i--) {
    ret[i] = list[i + start];
  }
  return ret
}

/**
 * Mix properties into target object.
 */
function extend (to, _from) {
  for (var key in _from) {
    to[key] = _from[key];
  }
  return to
}

/**
 * Merge an Array of Objects into a single Object.
 */
function toObject (arr) {
  var res = {};
  for (var i = 0; i < arr.length; i++) {
    if (arr[i]) {
      extend(res, arr[i]);
    }
  }
  return res
}

/* eslint-disable no-unused-vars */

/**
 * Perform no operation.
 * Stubbing args to make Flow happy without leaving useless transpiled code
 * with ...rest (https://flow.org/blog/2017/05/07/Strict-Function-Call-Arity/).
 */
function noop (a, b, c) {}

/**
 * Always return false.
 */
var no = function (a, b, c) { return false; };

/* eslint-enable no-unused-vars */

/**
 * Return the same value.
 */
var identity = function (_) { return _; };

/**
 * Check if two values are loosely equal - that is,
 * if they are plain objects, do they have the same shape?
 */
function looseEqual (a, b) {
  if (a === b) { return true }
  var isObjectA = isObject(a);
  var isObjectB = isObject(b);
  if (isObjectA && isObjectB) {
    try {
      var isArrayA = Array.isArray(a);
      var isArrayB = Array.isArray(b);
      if (isArrayA && isArrayB) {
        return a.length === b.length && a.every(function (e, i) {
          return looseEqual(e, b[i])
        })
      } else if (a instanceof Date && b instanceof Date) {
        return a.getTime() === b.getTime()
      } else if (!isArrayA && !isArrayB) {
        var keysA = Object.keys(a);
        var keysB = Object.keys(b);
        return keysA.length === keysB.length && keysA.every(function (key) {
          return looseEqual(a[key], b[key])
        })
      } else {
        /* istanbul ignore next */
        return false
      }
    } catch (e) {
      /* istanbul ignore next */
      return false
    }
  } else if (!isObjectA && !isObjectB) {
    return String(a) === String(b)
  } else {
    return false
  }
}

/**
 * Return the first index at which a loosely equal value can be
 * found in the array (if value is a plain object, the array must
 * contain an object of the same shape), or -1 if it is not present.
 */
function looseIndexOf (arr, val) {
  for (var i = 0; i < arr.length; i++) {
    if (looseEqual(arr[i], val)) { return i }
  }
  return -1
}

/**
 * Ensure a function is called only once.
 */
function once (fn) {
  var called = false;
  return function () {
    if (!called) {
      called = true;
      fn.apply(this, arguments);
    }
  }
}

var ASSET_TYPES = [
  'component',
  'directive',
  'filter'
];

var LIFECYCLE_HOOKS = [
  'beforeCreate',
  'created',
  'beforeMount',
  'mounted',
  'beforeUpdate',
  'updated',
  'beforeDestroy',
  'destroyed',
  'activated',
  'deactivated',
  'errorCaptured',
  'serverPrefetch'
];

/*  */



var config = ({
  /**
   * Option merge strategies (used in core/util/options)
   */
  // $flow-disable-line
  optionMergeStrategies: Object.create(null),

  /**
   * Whether to suppress warnings.
   */
  silent: false,

  /**
   * Show production mode tip message on boot?
   */
  productionTip: "development" !== 'production',

  /**
   * Whether to enable devtools
   */
  devtools: "development" !== 'production',

  /**
   * Whether to record perf
   */
  performance: false,

  /**
   * Error handler for watcher errors
   */
  errorHandler: null,

  /**
   * Warn handler for watcher warns
   */
  warnHandler: null,

  /**
   * Ignore certain custom elements
   */
  ignoredElements: [],

  /**
   * Custom user key aliases for v-on
   */
  // $flow-disable-line
  keyCodes: Object.create(null),

  /**
   * Check if a tag is reserved so that it cannot be registered as a
   * component. This is platform-dependent and may be overwritten.
   */
  isReservedTag: no,

  /**
   * Check if an attribute is reserved so that it cannot be used as a component
   * prop. This is platform-dependent and may be overwritten.
   */
  isReservedAttr: no,

  /**
   * Check if a tag is an unknown element.
   * Platform-dependent.
   */
  isUnknownElement: no,

  /**
   * Get the namespace of an element
   */
  getTagNamespace: noop,

  /**
   * Parse the real tag name for the specific platform.
   */
  parsePlatformTagName: identity,

  /**
   * Check if an attribute must be bound using property, e.g. value
   * Platform-dependent.
   */
  mustUseProp: no,

  /**
   * Perform updates asynchronously. Intended to be used by Vue Test Utils
   * This will significantly reduce performance if set to false.
   */
  async: true,

  /**
   * Exposed for legacy reasons
   */
  _lifecycleHooks: LIFECYCLE_HOOKS
});

/*  */

/**
 * unicode letters used for parsing html tags, component names and property paths.
 * using https://www.w3.org/TR/html53/semantics-scripting.html#potentialcustomelementname
 * skipping \u10000-\uEFFFF due to it freezing up PhantomJS
 */
var unicodeRegExp = /a-zA-Z\u00B7\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u037D\u037F-\u1FFF\u200C-\u200D\u203F-\u2040\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD/;

/**
 * Check if a string starts with $ or _
 */
function isReserved (str) {
  var c = (str + '').charCodeAt(0);
  return c === 0x24 || c === 0x5F
}

/**
 * Define a property.
 */
function def (obj, key, val, enumerable) {
  Object.defineProperty(obj, key, {
    value: val,
    enumerable: !!enumerable,
    writable: true,
    configurable: true
  });
}

/**
 * Parse simple path.
 */
var bailRE = new RegExp(("[^" + (unicodeRegExp.source) + ".$_\\d]"));
function parsePath (path) {
  if (bailRE.test(path)) {
    return
  }
  var segments = path.split('.');
  return function (obj) {
    for (var i = 0; i < segments.length; i++) {
      if (!obj) { return }
      obj = obj[segments[i]];
    }
    return obj
  }
}

/*  */

// can we use __proto__?
var hasProto = '__proto__' in {};

// Browser environment sniffing
var inBrowser = typeof window !== 'undefined';
var inWeex = typeof WXEnvironment !== 'undefined' && !!WXEnvironment.platform;
var weexPlatform = inWeex && WXEnvironment.platform.toLowerCase();
var UA = inBrowser && window.navigator.userAgent.toLowerCase();
var isIE = UA && /msie|trident/.test(UA);
var isIE9 = UA && UA.indexOf('msie 9.0') > 0;
var isEdge = UA && UA.indexOf('edge/') > 0;
var isAndroid = (UA && UA.indexOf('android') > 0) || (weexPlatform === 'android');
var isIOS = (UA && /iphone|ipad|ipod|ios/.test(UA)) || (weexPlatform === 'ios');
var isChrome = UA && /chrome\/\d+/.test(UA) && !isEdge;
var isPhantomJS = UA && /phantomjs/.test(UA);
var isFF = UA && UA.match(/firefox\/(\d+)/);

// Firefox has a "watch" function on Object.prototype...
var nativeWatch = ({}).watch;
if (inBrowser) {
  try {
    var opts = {};
    Object.defineProperty(opts, 'passive', ({
      get: function get () {
      }
    })); // https://github.com/facebook/flow/issues/285
    window.addEventListener('test-passive', null, opts);
  } catch (e) {}
}

// this needs to be lazy-evaled because vue may be required before
// vue-server-renderer can set VUE_ENV
var _isServer;
var isServerRendering = function () {
  if (_isServer === undefined) {
    /* istanbul ignore if */
    if (!inBrowser && !inWeex && typeof global !== 'undefined') {
      // detect presence of vue-server-renderer and avoid
      // Webpack shimming the process
      _isServer = global['process'] && global['process'].env.VUE_ENV === 'server';
    } else {
      _isServer = false;
    }
  }
  return _isServer
};

// detect devtools
var devtools = inBrowser && window.__VUE_DEVTOOLS_GLOBAL_HOOK__;

/* istanbul ignore next */
function isNative (Ctor) {
  return typeof Ctor === 'function' && /native code/.test(Ctor.toString())
}

var hasSymbol =
  typeof Symbol !== 'undefined' && isNative(Symbol) &&
  typeof Reflect !== 'undefined' && isNative(Reflect.ownKeys);

var _Set;
/* istanbul ignore if */ // $flow-disable-line
if (typeof Set !== 'undefined' && isNative(Set)) {
  // use native Set when available.
  _Set = Set;
} else {
  // a non-standard Set polyfill that only works with primitive keys.
  _Set = /*@__PURE__*/(function () {
    function Set () {
      this.set = Object.create(null);
    }
    Set.prototype.has = function has (key) {
      return this.set[key] === true
    };
    Set.prototype.add = function add (key) {
      this.set[key] = true;
    };
    Set.prototype.clear = function clear () {
      this.set = Object.create(null);
    };

    return Set;
  }());
}

/*  */

var warn = noop;
var tip = noop;
var generateComponentTrace = (noop); // work around flow check
var formatComponentName = (noop);

if (true) {
  var hasConsole = typeof console !== 'undefined';
  var classifyRE = /(?:^|[-_])(\w)/g;
  var classify = function (str) { return str
    .replace(classifyRE, function (c) { return c.toUpperCase(); })
    .replace(/[-_]/g, ''); };

  warn = function (msg, vm) {
    var trace = vm ? generateComponentTrace(vm) : '';

    if (config.warnHandler) {
      config.warnHandler.call(null, msg, vm, trace);
    } else if (hasConsole && (!config.silent)) {
      console.error(("[Vue warn]: " + msg + trace));
    }
  };

  tip = function (msg, vm) {
    if (hasConsole && (!config.silent)) {
      console.warn("[Vue tip]: " + msg + (
        vm ? generateComponentTrace(vm) : ''
      ));
    }
  };

  formatComponentName = function (vm, includeFile) {
    if (vm.$root === vm) {
      if (vm.$options && vm.$options.__file) { // fixed by xxxxxx
        return ('') + vm.$options.__file
      }
      return '<Root>'
    }
    var options = typeof vm === 'function' && vm.cid != null
      ? vm.options
      : vm._isVue
        ? vm.$options || vm.constructor.options
        : vm;
    var name = options.name || options._componentTag;
    var file = options.__file;
    if (!name && file) {
      var match = file.match(/([^/\\]+)\.vue$/);
      name = match && match[1];
    }

    return (
      (name ? ("<" + (classify(name)) + ">") : "<Anonymous>") +
      (file && includeFile !== false ? (" at " + file) : '')
    )
  };

  var repeat = function (str, n) {
    var res = '';
    while (n) {
      if (n % 2 === 1) { res += str; }
      if (n > 1) { str += str; }
      n >>= 1;
    }
    return res
  };

  generateComponentTrace = function (vm) {
    if (vm._isVue && vm.$parent) {
      var tree = [];
      var currentRecursiveSequence = 0;
      while (vm && vm.$options.name !== 'PageBody') {
        if (tree.length > 0) {
          var last = tree[tree.length - 1];
          if (last.constructor === vm.constructor) {
            currentRecursiveSequence++;
            vm = vm.$parent;
            continue
          } else if (currentRecursiveSequence > 0) {
            tree[tree.length - 1] = [last, currentRecursiveSequence];
            currentRecursiveSequence = 0;
          }
        }
        !vm.$options.isReserved && tree.push(vm);
        vm = vm.$parent;
      }
      return '\n\nfound in\n\n' + tree
        .map(function (vm, i) { return ("" + (i === 0 ? '---> ' : repeat(' ', 5 + i * 2)) + (Array.isArray(vm)
            ? ((formatComponentName(vm[0])) + "... (" + (vm[1]) + " recursive calls)")
            : formatComponentName(vm))); })
        .join('\n')
    } else {
      return ("\n\n(found in " + (formatComponentName(vm)) + ")")
    }
  };
}

/*  */

var uid = 0;

/**
 * A dep is an observable that can have multiple
 * directives subscribing to it.
 */
var Dep = function Dep () {
  this.id = uid++;
  this.subs = [];
};

Dep.prototype.addSub = function addSub (sub) {
  this.subs.push(sub);
};

Dep.prototype.removeSub = function removeSub (sub) {
  remove(this.subs, sub);
};

Dep.prototype.depend = function depend () {
  if (Dep.SharedObject.target) {
    Dep.SharedObject.target.addDep(this);
  }
};

Dep.prototype.notify = function notify () {
  // stabilize the subscriber list first
  var subs = this.subs.slice();
  if ( true && !config.async) {
    // subs aren't sorted in scheduler if not running async
    // we need to sort them now to make sure they fire in correct
    // order
    subs.sort(function (a, b) { return a.id - b.id; });
  }
  for (var i = 0, l = subs.length; i < l; i++) {
    subs[i].update();
  }
};

// The current target watcher being evaluated.
// This is globally unique because only one watcher
// can be evaluated at a time.
// fixed by xxxxxx (nvue shared vuex)
/* eslint-disable no-undef */
Dep.SharedObject = {};
Dep.SharedObject.target = null;
Dep.SharedObject.targetStack = [];

function pushTarget (target) {
  Dep.SharedObject.targetStack.push(target);
  Dep.SharedObject.target = target;
  Dep.target = target;
}

function popTarget () {
  Dep.SharedObject.targetStack.pop();
  Dep.SharedObject.target = Dep.SharedObject.targetStack[Dep.SharedObject.targetStack.length - 1];
  Dep.target = Dep.SharedObject.target;
}

/*  */

var VNode = function VNode (
  tag,
  data,
  children,
  text,
  elm,
  context,
  componentOptions,
  asyncFactory
) {
  this.tag = tag;
  this.data = data;
  this.children = children;
  this.text = text;
  this.elm = elm;
  this.ns = undefined;
  this.context = context;
  this.fnContext = undefined;
  this.fnOptions = undefined;
  this.fnScopeId = undefined;
  this.key = data && data.key;
  this.componentOptions = componentOptions;
  this.componentInstance = undefined;
  this.parent = undefined;
  this.raw = false;
  this.isStatic = false;
  this.isRootInsert = true;
  this.isComment = false;
  this.isCloned = false;
  this.isOnce = false;
  this.asyncFactory = asyncFactory;
  this.asyncMeta = undefined;
  this.isAsyncPlaceholder = false;
};

var prototypeAccessors = { child: { configurable: true } };

// DEPRECATED: alias for componentInstance for backwards compat.
/* istanbul ignore next */
prototypeAccessors.child.get = function () {
  return this.componentInstance
};

Object.defineProperties( VNode.prototype, prototypeAccessors );

var createEmptyVNode = function (text) {
  if ( text === void 0 ) text = '';

  var node = new VNode();
  node.text = text;
  node.isComment = true;
  return node
};

function createTextVNode (val) {
  return new VNode(undefined, undefined, undefined, String(val))
}

// optimized shallow clone
// used for static nodes and slot nodes because they may be reused across
// multiple renders, cloning them avoids errors when DOM manipulations rely
// on their elm reference.
function cloneVNode (vnode) {
  var cloned = new VNode(
    vnode.tag,
    vnode.data,
    // #7975
    // clone children array to avoid mutating original in case of cloning
    // a child.
    vnode.children && vnode.children.slice(),
    vnode.text,
    vnode.elm,
    vnode.context,
    vnode.componentOptions,
    vnode.asyncFactory
  );
  cloned.ns = vnode.ns;
  cloned.isStatic = vnode.isStatic;
  cloned.key = vnode.key;
  cloned.isComment = vnode.isComment;
  cloned.fnContext = vnode.fnContext;
  cloned.fnOptions = vnode.fnOptions;
  cloned.fnScopeId = vnode.fnScopeId;
  cloned.asyncMeta = vnode.asyncMeta;
  cloned.isCloned = true;
  return cloned
}

/*
 * not type checking this file because flow doesn't play well with
 * dynamically accessing methods on Array prototype
 */

var arrayProto = Array.prototype;
var arrayMethods = Object.create(arrayProto);

var methodsToPatch = [
  'push',
  'pop',
  'shift',
  'unshift',
  'splice',
  'sort',
  'reverse'
];

/**
 * Intercept mutating methods and emit events
 */
methodsToPatch.forEach(function (method) {
  // cache original method
  var original = arrayProto[method];
  def(arrayMethods, method, function mutator () {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    var result = original.apply(this, args);
    var ob = this.__ob__;
    var inserted;
    switch (method) {
      case 'push':
      case 'unshift':
        inserted = args;
        break
      case 'splice':
        inserted = args.slice(2);
        break
    }
    if (inserted) { ob.observeArray(inserted); }
    // notify change
    ob.dep.notify();
    return result
  });
});

/*  */

var arrayKeys = Object.getOwnPropertyNames(arrayMethods);

/**
 * In some cases we may want to disable observation inside a component's
 * update computation.
 */
var shouldObserve = true;

function toggleObserving (value) {
  shouldObserve = value;
}

/**
 * Observer class that is attached to each observed
 * object. Once attached, the observer converts the target
 * object's property keys into getter/setters that
 * collect dependencies and dispatch updates.
 */
var Observer = function Observer (value) {
  this.value = value;
  this.dep = new Dep();
  this.vmCount = 0;
  def(value, '__ob__', this);
  if (Array.isArray(value)) {
    if (hasProto) {
      {// fixed by xxxxxx 微信小程序使用 plugins 之后，数组方法被直接挂载到了数组对象上，需要执行 copyAugment 逻辑
        if(value.push !== value.__proto__.push){
          copyAugment(value, arrayMethods, arrayKeys);
        } else {
          protoAugment(value, arrayMethods);
        }
      }
    } else {
      copyAugment(value, arrayMethods, arrayKeys);
    }
    this.observeArray(value);
  } else {
    this.walk(value);
  }
};

/**
 * Walk through all properties and convert them into
 * getter/setters. This method should only be called when
 * value type is Object.
 */
Observer.prototype.walk = function walk (obj) {
  var keys = Object.keys(obj);
  for (var i = 0; i < keys.length; i++) {
    defineReactive$$1(obj, keys[i]);
  }
};

/**
 * Observe a list of Array items.
 */
Observer.prototype.observeArray = function observeArray (items) {
  for (var i = 0, l = items.length; i < l; i++) {
    observe(items[i]);
  }
};

// helpers

/**
 * Augment a target Object or Array by intercepting
 * the prototype chain using __proto__
 */
function protoAugment (target, src) {
  /* eslint-disable no-proto */
  target.__proto__ = src;
  /* eslint-enable no-proto */
}

/**
 * Augment a target Object or Array by defining
 * hidden properties.
 */
/* istanbul ignore next */
function copyAugment (target, src, keys) {
  for (var i = 0, l = keys.length; i < l; i++) {
    var key = keys[i];
    def(target, key, src[key]);
  }
}

/**
 * Attempt to create an observer instance for a value,
 * returns the new observer if successfully observed,
 * or the existing observer if the value already has one.
 */
function observe (value, asRootData) {
  if (!isObject(value) || value instanceof VNode) {
    return
  }
  var ob;
  if (hasOwn(value, '__ob__') && value.__ob__ instanceof Observer) {
    ob = value.__ob__;
  } else if (
    shouldObserve &&
    !isServerRendering() &&
    (Array.isArray(value) || isPlainObject(value)) &&
    Object.isExtensible(value) &&
    !value._isVue &&
    !value.__v_isMPComponent
  ) {
    ob = new Observer(value);
  }
  if (asRootData && ob) {
    ob.vmCount++;
  }
  return ob
}

/**
 * Define a reactive property on an Object.
 */
function defineReactive$$1 (
  obj,
  key,
  val,
  customSetter,
  shallow
) {
  var dep = new Dep();

  var property = Object.getOwnPropertyDescriptor(obj, key);
  if (property && property.configurable === false) {
    return
  }

  // cater for pre-defined getter/setters
  var getter = property && property.get;
  var setter = property && property.set;
  if ((!getter || setter) && arguments.length === 2) {
    val = obj[key];
  }

  var childOb = !shallow && observe(val);
  Object.defineProperty(obj, key, {
    enumerable: true,
    configurable: true,
    get: function reactiveGetter () {
      var value = getter ? getter.call(obj) : val;
      if (Dep.SharedObject.target) { // fixed by xxxxxx
        dep.depend();
        if (childOb) {
          childOb.dep.depend();
          if (Array.isArray(value)) {
            dependArray(value);
          }
        }
      }
      return value
    },
    set: function reactiveSetter (newVal) {
      var value = getter ? getter.call(obj) : val;
      /* eslint-disable no-self-compare */
      if (newVal === value || (newVal !== newVal && value !== value)) {
        return
      }
      /* eslint-enable no-self-compare */
      if ( true && customSetter) {
        customSetter();
      }
      // #7981: for accessor properties without setter
      if (getter && !setter) { return }
      if (setter) {
        setter.call(obj, newVal);
      } else {
        val = newVal;
      }
      childOb = !shallow && observe(newVal);
      dep.notify();
    }
  });
}

/**
 * Set a property on an object. Adds the new property and
 * triggers change notification if the property doesn't
 * already exist.
 */
function set (target, key, val) {
  if ( true &&
    (isUndef(target) || isPrimitive(target))
  ) {
    warn(("Cannot set reactive property on undefined, null, or primitive value: " + ((target))));
  }
  if (Array.isArray(target) && isValidArrayIndex(key)) {
    target.length = Math.max(target.length, key);
    target.splice(key, 1, val);
    return val
  }
  if (key in target && !(key in Object.prototype)) {
    target[key] = val;
    return val
  }
  var ob = (target).__ob__;
  if (target._isVue || (ob && ob.vmCount)) {
     true && warn(
      'Avoid adding reactive properties to a Vue instance or its root $data ' +
      'at runtime - declare it upfront in the data option.'
    );
    return val
  }
  if (!ob) {
    target[key] = val;
    return val
  }
  defineReactive$$1(ob.value, key, val);
  ob.dep.notify();
  return val
}

/**
 * Delete a property and trigger change if necessary.
 */
function del (target, key) {
  if ( true &&
    (isUndef(target) || isPrimitive(target))
  ) {
    warn(("Cannot delete reactive property on undefined, null, or primitive value: " + ((target))));
  }
  if (Array.isArray(target) && isValidArrayIndex(key)) {
    target.splice(key, 1);
    return
  }
  var ob = (target).__ob__;
  if (target._isVue || (ob && ob.vmCount)) {
     true && warn(
      'Avoid deleting properties on a Vue instance or its root $data ' +
      '- just set it to null.'
    );
    return
  }
  if (!hasOwn(target, key)) {
    return
  }
  delete target[key];
  if (!ob) {
    return
  }
  ob.dep.notify();
}

/**
 * Collect dependencies on array elements when the array is touched, since
 * we cannot intercept array element access like property getters.
 */
function dependArray (value) {
  for (var e = (void 0), i = 0, l = value.length; i < l; i++) {
    e = value[i];
    e && e.__ob__ && e.__ob__.dep.depend();
    if (Array.isArray(e)) {
      dependArray(e);
    }
  }
}

/*  */

/**
 * Option overwriting strategies are functions that handle
 * how to merge a parent option value and a child option
 * value into the final value.
 */
var strats = config.optionMergeStrategies;

/**
 * Options with restrictions
 */
if (true) {
  strats.el = strats.propsData = function (parent, child, vm, key) {
    if (!vm) {
      warn(
        "option \"" + key + "\" can only be used during instance " +
        'creation with the `new` keyword.'
      );
    }
    return defaultStrat(parent, child)
  };
}

/**
 * Helper that recursively merges two data objects together.
 */
function mergeData (to, from) {
  if (!from) { return to }
  var key, toVal, fromVal;

  var keys = hasSymbol
    ? Reflect.ownKeys(from)
    : Object.keys(from);

  for (var i = 0; i < keys.length; i++) {
    key = keys[i];
    // in case the object is already observed...
    if (key === '__ob__') { continue }
    toVal = to[key];
    fromVal = from[key];
    if (!hasOwn(to, key)) {
      set(to, key, fromVal);
    } else if (
      toVal !== fromVal &&
      isPlainObject(toVal) &&
      isPlainObject(fromVal)
    ) {
      mergeData(toVal, fromVal);
    }
  }
  return to
}

/**
 * Data
 */
function mergeDataOrFn (
  parentVal,
  childVal,
  vm
) {
  if (!vm) {
    // in a Vue.extend merge, both should be functions
    if (!childVal) {
      return parentVal
    }
    if (!parentVal) {
      return childVal
    }
    // when parentVal & childVal are both present,
    // we need to return a function that returns the
    // merged result of both functions... no need to
    // check if parentVal is a function here because
    // it has to be a function to pass previous merges.
    return function mergedDataFn () {
      return mergeData(
        typeof childVal === 'function' ? childVal.call(this, this) : childVal,
        typeof parentVal === 'function' ? parentVal.call(this, this) : parentVal
      )
    }
  } else {
    return function mergedInstanceDataFn () {
      // instance merge
      var instanceData = typeof childVal === 'function'
        ? childVal.call(vm, vm)
        : childVal;
      var defaultData = typeof parentVal === 'function'
        ? parentVal.call(vm, vm)
        : parentVal;
      if (instanceData) {
        return mergeData(instanceData, defaultData)
      } else {
        return defaultData
      }
    }
  }
}

strats.data = function (
  parentVal,
  childVal,
  vm
) {
  if (!vm) {
    if (childVal && typeof childVal !== 'function') {
       true && warn(
        'The "data" option should be a function ' +
        'that returns a per-instance value in component ' +
        'definitions.',
        vm
      );

      return parentVal
    }
    return mergeDataOrFn(parentVal, childVal)
  }

  return mergeDataOrFn(parentVal, childVal, vm)
};

/**
 * Hooks and props are merged as arrays.
 */
function mergeHook (
  parentVal,
  childVal
) {
  var res = childVal
    ? parentVal
      ? parentVal.concat(childVal)
      : Array.isArray(childVal)
        ? childVal
        : [childVal]
    : parentVal;
  return res
    ? dedupeHooks(res)
    : res
}

function dedupeHooks (hooks) {
  var res = [];
  for (var i = 0; i < hooks.length; i++) {
    if (res.indexOf(hooks[i]) === -1) {
      res.push(hooks[i]);
    }
  }
  return res
}

LIFECYCLE_HOOKS.forEach(function (hook) {
  strats[hook] = mergeHook;
});

/**
 * Assets
 *
 * When a vm is present (instance creation), we need to do
 * a three-way merge between constructor options, instance
 * options and parent options.
 */
function mergeAssets (
  parentVal,
  childVal,
  vm,
  key
) {
  var res = Object.create(parentVal || null);
  if (childVal) {
     true && assertObjectType(key, childVal, vm);
    return extend(res, childVal)
  } else {
    return res
  }
}

ASSET_TYPES.forEach(function (type) {
  strats[type + 's'] = mergeAssets;
});

/**
 * Watchers.
 *
 * Watchers hashes should not overwrite one
 * another, so we merge them as arrays.
 */
strats.watch = function (
  parentVal,
  childVal,
  vm,
  key
) {
  // work around Firefox's Object.prototype.watch...
  if (parentVal === nativeWatch) { parentVal = undefined; }
  if (childVal === nativeWatch) { childVal = undefined; }
  /* istanbul ignore if */
  if (!childVal) { return Object.create(parentVal || null) }
  if (true) {
    assertObjectType(key, childVal, vm);
  }
  if (!parentVal) { return childVal }
  var ret = {};
  extend(ret, parentVal);
  for (var key$1 in childVal) {
    var parent = ret[key$1];
    var child = childVal[key$1];
    if (parent && !Array.isArray(parent)) {
      parent = [parent];
    }
    ret[key$1] = parent
      ? parent.concat(child)
      : Array.isArray(child) ? child : [child];
  }
  return ret
};

/**
 * Other object hashes.
 */
strats.props =
strats.methods =
strats.inject =
strats.computed = function (
  parentVal,
  childVal,
  vm,
  key
) {
  if (childVal && "development" !== 'production') {
    assertObjectType(key, childVal, vm);
  }
  if (!parentVal) { return childVal }
  var ret = Object.create(null);
  extend(ret, parentVal);
  if (childVal) { extend(ret, childVal); }
  return ret
};
strats.provide = mergeDataOrFn;

/**
 * Default strategy.
 */
var defaultStrat = function (parentVal, childVal) {
  return childVal === undefined
    ? parentVal
    : childVal
};

/**
 * Validate component names
 */
function checkComponents (options) {
  for (var key in options.components) {
    validateComponentName(key);
  }
}

function validateComponentName (name) {
  if (!new RegExp(("^[a-zA-Z][\\-\\.0-9_" + (unicodeRegExp.source) + "]*$")).test(name)) {
    warn(
      'Invalid component name: "' + name + '". Component names ' +
      'should conform to valid custom element name in html5 specification.'
    );
  }
  if (isBuiltInTag(name) || config.isReservedTag(name)) {
    warn(
      'Do not use built-in or reserved HTML elements as component ' +
      'id: ' + name
    );
  }
}

/**
 * Ensure all props option syntax are normalized into the
 * Object-based format.
 */
function normalizeProps (options, vm) {
  var props = options.props;
  if (!props) { return }
  var res = {};
  var i, val, name;
  if (Array.isArray(props)) {
    i = props.length;
    while (i--) {
      val = props[i];
      if (typeof val === 'string') {
        name = camelize(val);
        res[name] = { type: null };
      } else if (true) {
        warn('props must be strings when using array syntax.');
      }
    }
  } else if (isPlainObject(props)) {
    for (var key in props) {
      val = props[key];
      name = camelize(key);
      res[name] = isPlainObject(val)
        ? val
        : { type: val };
    }
  } else if (true) {
    warn(
      "Invalid value for option \"props\": expected an Array or an Object, " +
      "but got " + (toRawType(props)) + ".",
      vm
    );
  }
  options.props = res;
}

/**
 * Normalize all injections into Object-based format
 */
function normalizeInject (options, vm) {
  var inject = options.inject;
  if (!inject) { return }
  var normalized = options.inject = {};
  if (Array.isArray(inject)) {
    for (var i = 0; i < inject.length; i++) {
      normalized[inject[i]] = { from: inject[i] };
    }
  } else if (isPlainObject(inject)) {
    for (var key in inject) {
      var val = inject[key];
      normalized[key] = isPlainObject(val)
        ? extend({ from: key }, val)
        : { from: val };
    }
  } else if (true) {
    warn(
      "Invalid value for option \"inject\": expected an Array or an Object, " +
      "but got " + (toRawType(inject)) + ".",
      vm
    );
  }
}

/**
 * Normalize raw function directives into object format.
 */
function normalizeDirectives (options) {
  var dirs = options.directives;
  if (dirs) {
    for (var key in dirs) {
      var def$$1 = dirs[key];
      if (typeof def$$1 === 'function') {
        dirs[key] = { bind: def$$1, update: def$$1 };
      }
    }
  }
}

function assertObjectType (name, value, vm) {
  if (!isPlainObject(value)) {
    warn(
      "Invalid value for option \"" + name + "\": expected an Object, " +
      "but got " + (toRawType(value)) + ".",
      vm
    );
  }
}

/**
 * Merge two option objects into a new one.
 * Core utility used in both instantiation and inheritance.
 */
function mergeOptions (
  parent,
  child,
  vm
) {
  if (true) {
    checkComponents(child);
  }

  if (typeof child === 'function') {
    child = child.options;
  }

  normalizeProps(child, vm);
  normalizeInject(child, vm);
  normalizeDirectives(child);

  // Apply extends and mixins on the child options,
  // but only if it is a raw options object that isn't
  // the result of another mergeOptions call.
  // Only merged options has the _base property.
  if (!child._base) {
    if (child.extends) {
      parent = mergeOptions(parent, child.extends, vm);
    }
    if (child.mixins) {
      for (var i = 0, l = child.mixins.length; i < l; i++) {
        parent = mergeOptions(parent, child.mixins[i], vm);
      }
    }
  }

  var options = {};
  var key;
  for (key in parent) {
    mergeField(key);
  }
  for (key in child) {
    if (!hasOwn(parent, key)) {
      mergeField(key);
    }
  }
  function mergeField (key) {
    var strat = strats[key] || defaultStrat;
    options[key] = strat(parent[key], child[key], vm, key);
  }
  return options
}

/**
 * Resolve an asset.
 * This function is used because child instances need access
 * to assets defined in its ancestor chain.
 */
function resolveAsset (
  options,
  type,
  id,
  warnMissing
) {
  /* istanbul ignore if */
  if (typeof id !== 'string') {
    return
  }
  var assets = options[type];
  // check local registration variations first
  if (hasOwn(assets, id)) { return assets[id] }
  var camelizedId = camelize(id);
  if (hasOwn(assets, camelizedId)) { return assets[camelizedId] }
  var PascalCaseId = capitalize(camelizedId);
  if (hasOwn(assets, PascalCaseId)) { return assets[PascalCaseId] }
  // fallback to prototype chain
  var res = assets[id] || assets[camelizedId] || assets[PascalCaseId];
  if ( true && warnMissing && !res) {
    warn(
      'Failed to resolve ' + type.slice(0, -1) + ': ' + id,
      options
    );
  }
  return res
}

/*  */



function validateProp (
  key,
  propOptions,
  propsData,
  vm
) {
  var prop = propOptions[key];
  var absent = !hasOwn(propsData, key);
  var value = propsData[key];
  // boolean casting
  var booleanIndex = getTypeIndex(Boolean, prop.type);
  if (booleanIndex > -1) {
    if (absent && !hasOwn(prop, 'default')) {
      value = false;
    } else if (value === '' || value === hyphenate(key)) {
      // only cast empty string / same name to boolean if
      // boolean has higher priority
      var stringIndex = getTypeIndex(String, prop.type);
      if (stringIndex < 0 || booleanIndex < stringIndex) {
        value = true;
      }
    }
  }
  // check default value
  if (value === undefined) {
    value = getPropDefaultValue(vm, prop, key);
    // since the default value is a fresh copy,
    // make sure to observe it.
    var prevShouldObserve = shouldObserve;
    toggleObserving(true);
    observe(value);
    toggleObserving(prevShouldObserve);
  }
  if (
    true
  ) {
    assertProp(prop, key, value, vm, absent);
  }
  return value
}

/**
 * Get the default value of a prop.
 */
function getPropDefaultValue (vm, prop, key) {
  // no default, return undefined
  if (!hasOwn(prop, 'default')) {
    return undefined
  }
  var def = prop.default;
  // warn against non-factory defaults for Object & Array
  if ( true && isObject(def)) {
    warn(
      'Invalid default value for prop "' + key + '": ' +
      'Props with type Object/Array must use a factory function ' +
      'to return the default value.',
      vm
    );
  }
  // the raw prop value was also undefined from previous render,
  // return previous default value to avoid unnecessary watcher trigger
  if (vm && vm.$options.propsData &&
    vm.$options.propsData[key] === undefined &&
    vm._props[key] !== undefined
  ) {
    return vm._props[key]
  }
  // call factory function for non-Function types
  // a value is Function if its prototype is function even across different execution context
  return typeof def === 'function' && getType(prop.type) !== 'Function'
    ? def.call(vm)
    : def
}

/**
 * Assert whether a prop is valid.
 */
function assertProp (
  prop,
  name,
  value,
  vm,
  absent
) {
  if (prop.required && absent) {
    warn(
      'Missing required prop: "' + name + '"',
      vm
    );
    return
  }
  if (value == null && !prop.required) {
    return
  }
  var type = prop.type;
  var valid = !type || type === true;
  var expectedTypes = [];
  if (type) {
    if (!Array.isArray(type)) {
      type = [type];
    }
    for (var i = 0; i < type.length && !valid; i++) {
      var assertedType = assertType(value, type[i]);
      expectedTypes.push(assertedType.expectedType || '');
      valid = assertedType.valid;
    }
  }

  if (!valid) {
    warn(
      getInvalidTypeMessage(name, value, expectedTypes),
      vm
    );
    return
  }
  var validator = prop.validator;
  if (validator) {
    if (!validator(value)) {
      warn(
        'Invalid prop: custom validator check failed for prop "' + name + '".',
        vm
      );
    }
  }
}

var simpleCheckRE = /^(String|Number|Boolean|Function|Symbol)$/;

function assertType (value, type) {
  var valid;
  var expectedType = getType(type);
  if (simpleCheckRE.test(expectedType)) {
    var t = typeof value;
    valid = t === expectedType.toLowerCase();
    // for primitive wrapper objects
    if (!valid && t === 'object') {
      valid = value instanceof type;
    }
  } else if (expectedType === 'Object') {
    valid = isPlainObject(value);
  } else if (expectedType === 'Array') {
    valid = Array.isArray(value);
  } else {
    valid = value instanceof type;
  }
  return {
    valid: valid,
    expectedType: expectedType
  }
}

/**
 * Use function string name to check built-in types,
 * because a simple equality check will fail when running
 * across different vms / iframes.
 */
function getType (fn) {
  var match = fn && fn.toString().match(/^\s*function (\w+)/);
  return match ? match[1] : ''
}

function isSameType (a, b) {
  return getType(a) === getType(b)
}

function getTypeIndex (type, expectedTypes) {
  if (!Array.isArray(expectedTypes)) {
    return isSameType(expectedTypes, type) ? 0 : -1
  }
  for (var i = 0, len = expectedTypes.length; i < len; i++) {
    if (isSameType(expectedTypes[i], type)) {
      return i
    }
  }
  return -1
}

function getInvalidTypeMessage (name, value, expectedTypes) {
  var message = "Invalid prop: type check failed for prop \"" + name + "\"." +
    " Expected " + (expectedTypes.map(capitalize).join(', '));
  var expectedType = expectedTypes[0];
  var receivedType = toRawType(value);
  var expectedValue = styleValue(value, expectedType);
  var receivedValue = styleValue(value, receivedType);
  // check if we need to specify expected value
  if (expectedTypes.length === 1 &&
      isExplicable(expectedType) &&
      !isBoolean(expectedType, receivedType)) {
    message += " with value " + expectedValue;
  }
  message += ", got " + receivedType + " ";
  // check if we need to specify received value
  if (isExplicable(receivedType)) {
    message += "with value " + receivedValue + ".";
  }
  return message
}

function styleValue (value, type) {
  if (type === 'String') {
    return ("\"" + value + "\"")
  } else if (type === 'Number') {
    return ("" + (Number(value)))
  } else {
    return ("" + value)
  }
}

function isExplicable (value) {
  var explicitTypes = ['string', 'number', 'boolean'];
  return explicitTypes.some(function (elem) { return value.toLowerCase() === elem; })
}

function isBoolean () {
  var args = [], len = arguments.length;
  while ( len-- ) args[ len ] = arguments[ len ];

  return args.some(function (elem) { return elem.toLowerCase() === 'boolean'; })
}

/*  */

function handleError (err, vm, info) {
  // Deactivate deps tracking while processing error handler to avoid possible infinite rendering.
  // See: https://github.com/vuejs/vuex/issues/1505
  pushTarget();
  try {
    if (vm) {
      var cur = vm;
      while ((cur = cur.$parent)) {
        var hooks = cur.$options.errorCaptured;
        if (hooks) {
          for (var i = 0; i < hooks.length; i++) {
            try {
              var capture = hooks[i].call(cur, err, vm, info) === false;
              if (capture) { return }
            } catch (e) {
              globalHandleError(e, cur, 'errorCaptured hook');
            }
          }
        }
      }
    }
    globalHandleError(err, vm, info);
  } finally {
    popTarget();
  }
}

function invokeWithErrorHandling (
  handler,
  context,
  args,
  vm,
  info
) {
  var res;
  try {
    res = args ? handler.apply(context, args) : handler.call(context);
    if (res && !res._isVue && isPromise(res) && !res._handled) {
      res.catch(function (e) { return handleError(e, vm, info + " (Promise/async)"); });
      // issue #9511
      // avoid catch triggering multiple times when nested calls
      res._handled = true;
    }
  } catch (e) {
    handleError(e, vm, info);
  }
  return res
}

function globalHandleError (err, vm, info) {
  if (config.errorHandler) {
    try {
      return config.errorHandler.call(null, err, vm, info)
    } catch (e) {
      // if the user intentionally throws the original error in the handler,
      // do not log it twice
      if (e !== err) {
        logError(e, null, 'config.errorHandler');
      }
    }
  }
  logError(err, vm, info);
}

function logError (err, vm, info) {
  if (true) {
    warn(("Error in " + info + ": \"" + (err.toString()) + "\""), vm);
  }
  /* istanbul ignore else */
  if ((inBrowser || inWeex) && typeof console !== 'undefined') {
    console.error(err);
  } else {
    throw err
  }
}

/*  */

var callbacks = [];
var pending = false;

function flushCallbacks () {
  pending = false;
  var copies = callbacks.slice(0);
  callbacks.length = 0;
  for (var i = 0; i < copies.length; i++) {
    copies[i]();
  }
}

// Here we have async deferring wrappers using microtasks.
// In 2.5 we used (macro) tasks (in combination with microtasks).
// However, it has subtle problems when state is changed right before repaint
// (e.g. #6813, out-in transitions).
// Also, using (macro) tasks in event handler would cause some weird behaviors
// that cannot be circumvented (e.g. #7109, #7153, #7546, #7834, #8109).
// So we now use microtasks everywhere, again.
// A major drawback of this tradeoff is that there are some scenarios
// where microtasks have too high a priority and fire in between supposedly
// sequential events (e.g. #4521, #6690, which have workarounds)
// or even between bubbling of the same event (#6566).
var timerFunc;

// The nextTick behavior leverages the microtask queue, which can be accessed
// via either native Promise.then or MutationObserver.
// MutationObserver has wider support, however it is seriously bugged in
// UIWebView in iOS >= 9.3.3 when triggered in touch event handlers. It
// completely stops working after triggering a few times... so, if native
// Promise is available, we will use it:
/* istanbul ignore next, $flow-disable-line */
if (typeof Promise !== 'undefined' && isNative(Promise)) {
  var p = Promise.resolve();
  timerFunc = function () {
    p.then(flushCallbacks);
    // In problematic UIWebViews, Promise.then doesn't completely break, but
    // it can get stuck in a weird state where callbacks are pushed into the
    // microtask queue but the queue isn't being flushed, until the browser
    // needs to do some other work, e.g. handle a timer. Therefore we can
    // "force" the microtask queue to be flushed by adding an empty timer.
    if (isIOS) { setTimeout(noop); }
  };
} else if (!isIE && typeof MutationObserver !== 'undefined' && (
  isNative(MutationObserver) ||
  // PhantomJS and iOS 7.x
  MutationObserver.toString() === '[object MutationObserverConstructor]'
)) {
  // Use MutationObserver where native Promise is not available,
  // e.g. PhantomJS, iOS7, Android 4.4
  // (#6466 MutationObserver is unreliable in IE11)
  var counter = 1;
  var observer = new MutationObserver(flushCallbacks);
  var textNode = document.createTextNode(String(counter));
  observer.observe(textNode, {
    characterData: true
  });
  timerFunc = function () {
    counter = (counter + 1) % 2;
    textNode.data = String(counter);
  };
} else if (typeof setImmediate !== 'undefined' && isNative(setImmediate)) {
  // Fallback to setImmediate.
  // Technically it leverages the (macro) task queue,
  // but it is still a better choice than setTimeout.
  timerFunc = function () {
    setImmediate(flushCallbacks);
  };
} else {
  // Fallback to setTimeout.
  timerFunc = function () {
    setTimeout(flushCallbacks, 0);
  };
}

function nextTick (cb, ctx) {
  var _resolve;
  callbacks.push(function () {
    if (cb) {
      try {
        cb.call(ctx);
      } catch (e) {
        handleError(e, ctx, 'nextTick');
      }
    } else if (_resolve) {
      _resolve(ctx);
    }
  });
  if (!pending) {
    pending = true;
    timerFunc();
  }
  // $flow-disable-line
  if (!cb && typeof Promise !== 'undefined') {
    return new Promise(function (resolve) {
      _resolve = resolve;
    })
  }
}

/*  */

/* not type checking this file because flow doesn't play well with Proxy */

var initProxy;

if (true) {
  var allowedGlobals = makeMap(
    'Infinity,undefined,NaN,isFinite,isNaN,' +
    'parseFloat,parseInt,decodeURI,decodeURIComponent,encodeURI,encodeURIComponent,' +
    'Math,Number,Date,Array,Object,Boolean,String,RegExp,Map,Set,JSON,Intl,' +
    'require' // for Webpack/Browserify
  );

  var warnNonPresent = function (target, key) {
    warn(
      "Property or method \"" + key + "\" is not defined on the instance but " +
      'referenced during render. Make sure that this property is reactive, ' +
      'either in the data option, or for class-based components, by ' +
      'initializing the property. ' +
      'See: https://vuejs.org/v2/guide/reactivity.html#Declaring-Reactive-Properties.',
      target
    );
  };

  var warnReservedPrefix = function (target, key) {
    warn(
      "Property \"" + key + "\" must be accessed with \"$data." + key + "\" because " +
      'properties starting with "$" or "_" are not proxied in the Vue instance to ' +
      'prevent conflicts with Vue internals. ' +
      'See: https://vuejs.org/v2/api/#data',
      target
    );
  };

  var hasProxy =
    typeof Proxy !== 'undefined' && isNative(Proxy);

  if (hasProxy) {
    var isBuiltInModifier = makeMap('stop,prevent,self,ctrl,shift,alt,meta,exact');
    config.keyCodes = new Proxy(config.keyCodes, {
      set: function set (target, key, value) {
        if (isBuiltInModifier(key)) {
          warn(("Avoid overwriting built-in modifier in config.keyCodes: ." + key));
          return false
        } else {
          target[key] = value;
          return true
        }
      }
    });
  }

  var hasHandler = {
    has: function has (target, key) {
      var has = key in target;
      var isAllowed = allowedGlobals(key) ||
        (typeof key === 'string' && key.charAt(0) === '_' && !(key in target.$data));
      if (!has && !isAllowed) {
        if (key in target.$data) { warnReservedPrefix(target, key); }
        else { warnNonPresent(target, key); }
      }
      return has || !isAllowed
    }
  };

  var getHandler = {
    get: function get (target, key) {
      if (typeof key === 'string' && !(key in target)) {
        if (key in target.$data) { warnReservedPrefix(target, key); }
        else { warnNonPresent(target, key); }
      }
      return target[key]
    }
  };

  initProxy = function initProxy (vm) {
    if (hasProxy) {
      // determine which proxy handler to use
      var options = vm.$options;
      var handlers = options.render && options.render._withStripped
        ? getHandler
        : hasHandler;
      vm._renderProxy = new Proxy(vm, handlers);
    } else {
      vm._renderProxy = vm;
    }
  };
}

/*  */

var seenObjects = new _Set();

/**
 * Recursively traverse an object to evoke all converted
 * getters, so that every nested property inside the object
 * is collected as a "deep" dependency.
 */
function traverse (val) {
  _traverse(val, seenObjects);
  seenObjects.clear();
}

function _traverse (val, seen) {
  var i, keys;
  var isA = Array.isArray(val);
  if ((!isA && !isObject(val)) || Object.isFrozen(val) || val instanceof VNode) {
    return
  }
  if (val.__ob__) {
    var depId = val.__ob__.dep.id;
    if (seen.has(depId)) {
      return
    }
    seen.add(depId);
  }
  if (isA) {
    i = val.length;
    while (i--) { _traverse(val[i], seen); }
  } else {
    keys = Object.keys(val);
    i = keys.length;
    while (i--) { _traverse(val[keys[i]], seen); }
  }
}

var mark;
var measure;

if (true) {
  var perf = inBrowser && window.performance;
  /* istanbul ignore if */
  if (
    perf &&
    perf.mark &&
    perf.measure &&
    perf.clearMarks &&
    perf.clearMeasures
  ) {
    mark = function (tag) { return perf.mark(tag); };
    measure = function (name, startTag, endTag) {
      perf.measure(name, startTag, endTag);
      perf.clearMarks(startTag);
      perf.clearMarks(endTag);
      // perf.clearMeasures(name)
    };
  }
}

/*  */

var normalizeEvent = cached(function (name) {
  var passive = name.charAt(0) === '&';
  name = passive ? name.slice(1) : name;
  var once$$1 = name.charAt(0) === '~'; // Prefixed last, checked first
  name = once$$1 ? name.slice(1) : name;
  var capture = name.charAt(0) === '!';
  name = capture ? name.slice(1) : name;
  return {
    name: name,
    once: once$$1,
    capture: capture,
    passive: passive
  }
});

function createFnInvoker (fns, vm) {
  function invoker () {
    var arguments$1 = arguments;

    var fns = invoker.fns;
    if (Array.isArray(fns)) {
      var cloned = fns.slice();
      for (var i = 0; i < cloned.length; i++) {
        invokeWithErrorHandling(cloned[i], null, arguments$1, vm, "v-on handler");
      }
    } else {
      // return handler return value for single handlers
      return invokeWithErrorHandling(fns, null, arguments, vm, "v-on handler")
    }
  }
  invoker.fns = fns;
  return invoker
}

function updateListeners (
  on,
  oldOn,
  add,
  remove$$1,
  createOnceHandler,
  vm
) {
  var name, def$$1, cur, old, event;
  for (name in on) {
    def$$1 = cur = on[name];
    old = oldOn[name];
    event = normalizeEvent(name);
    if (isUndef(cur)) {
       true && warn(
        "Invalid handler for event \"" + (event.name) + "\": got " + String(cur),
        vm
      );
    } else if (isUndef(old)) {
      if (isUndef(cur.fns)) {
        cur = on[name] = createFnInvoker(cur, vm);
      }
      if (isTrue(event.once)) {
        cur = on[name] = createOnceHandler(event.name, cur, event.capture);
      }
      add(event.name, cur, event.capture, event.passive, event.params);
    } else if (cur !== old) {
      old.fns = cur;
      on[name] = old;
    }
  }
  for (name in oldOn) {
    if (isUndef(on[name])) {
      event = normalizeEvent(name);
      remove$$1(event.name, oldOn[name], event.capture);
    }
  }
}

/*  */

/*  */

// fixed by xxxxxx (mp properties)
function extractPropertiesFromVNodeData(data, Ctor, res, context) {
  var propOptions = Ctor.options.mpOptions && Ctor.options.mpOptions.properties;
  if (isUndef(propOptions)) {
    return res
  }
  var externalClasses = Ctor.options.mpOptions.externalClasses || [];
  var attrs = data.attrs;
  var props = data.props;
  if (isDef(attrs) || isDef(props)) {
    for (var key in propOptions) {
      var altKey = hyphenate(key);
      var result = checkProp(res, props, key, altKey, true) ||
          checkProp(res, attrs, key, altKey, false);
      // externalClass
      if (
        result &&
        res[key] &&
        externalClasses.indexOf(altKey) !== -1 &&
        context[camelize(res[key])]
      ) {
        // 赋值 externalClass 真正的值(模板里 externalClass 的值可能是字符串)
        res[key] = context[camelize(res[key])];
      }
    }
  }
  return res
}

function extractPropsFromVNodeData (
  data,
  Ctor,
  tag,
  context// fixed by xxxxxx
) {
  // we are only extracting raw values here.
  // validation and default values are handled in the child
  // component itself.
  var propOptions = Ctor.options.props;
  if (isUndef(propOptions)) {
    // fixed by xxxxxx
    return extractPropertiesFromVNodeData(data, Ctor, {}, context)
  }
  var res = {};
  var attrs = data.attrs;
  var props = data.props;
  if (isDef(attrs) || isDef(props)) {
    for (var key in propOptions) {
      var altKey = hyphenate(key);
      if (true) {
        var keyInLowerCase = key.toLowerCase();
        if (
          key !== keyInLowerCase &&
          attrs && hasOwn(attrs, keyInLowerCase)
        ) {
          tip(
            "Prop \"" + keyInLowerCase + "\" is passed to component " +
            (formatComponentName(tag || Ctor)) + ", but the declared prop name is" +
            " \"" + key + "\". " +
            "Note that HTML attributes are case-insensitive and camelCased " +
            "props need to use their kebab-case equivalents when using in-DOM " +
            "templates. You should probably use \"" + altKey + "\" instead of \"" + key + "\"."
          );
        }
      }
      checkProp(res, props, key, altKey, true) ||
      checkProp(res, attrs, key, altKey, false);
    }
  }
  // fixed by xxxxxx
  return extractPropertiesFromVNodeData(data, Ctor, res, context)
}

function checkProp (
  res,
  hash,
  key,
  altKey,
  preserve
) {
  if (isDef(hash)) {
    if (hasOwn(hash, key)) {
      res[key] = hash[key];
      if (!preserve) {
        delete hash[key];
      }
      return true
    } else if (hasOwn(hash, altKey)) {
      res[key] = hash[altKey];
      if (!preserve) {
        delete hash[altKey];
      }
      return true
    }
  }
  return false
}

/*  */

// The template compiler attempts to minimize the need for normalization by
// statically analyzing the template at compile time.
//
// For plain HTML markup, normalization can be completely skipped because the
// generated render function is guaranteed to return Array<VNode>. There are
// two cases where extra normalization is needed:

// 1. When the children contains components - because a functional component
// may return an Array instead of a single root. In this case, just a simple
// normalization is needed - if any child is an Array, we flatten the whole
// thing with Array.prototype.concat. It is guaranteed to be only 1-level deep
// because functional components already normalize their own children.
function simpleNormalizeChildren (children) {
  for (var i = 0; i < children.length; i++) {
    if (Array.isArray(children[i])) {
      return Array.prototype.concat.apply([], children)
    }
  }
  return children
}

// 2. When the children contains constructs that always generated nested Arrays,
// e.g. <template>, <slot>, v-for, or when the children is provided by user
// with hand-written render functions / JSX. In such cases a full normalization
// is needed to cater to all possible types of children values.
function normalizeChildren (children) {
  return isPrimitive(children)
    ? [createTextVNode(children)]
    : Array.isArray(children)
      ? normalizeArrayChildren(children)
      : undefined
}

function isTextNode (node) {
  return isDef(node) && isDef(node.text) && isFalse(node.isComment)
}

function normalizeArrayChildren (children, nestedIndex) {
  var res = [];
  var i, c, lastIndex, last;
  for (i = 0; i < children.length; i++) {
    c = children[i];
    if (isUndef(c) || typeof c === 'boolean') { continue }
    lastIndex = res.length - 1;
    last = res[lastIndex];
    //  nested
    if (Array.isArray(c)) {
      if (c.length > 0) {
        c = normalizeArrayChildren(c, ((nestedIndex || '') + "_" + i));
        // merge adjacent text nodes
        if (isTextNode(c[0]) && isTextNode(last)) {
          res[lastIndex] = createTextVNode(last.text + (c[0]).text);
          c.shift();
        }
        res.push.apply(res, c);
      }
    } else if (isPrimitive(c)) {
      if (isTextNode(last)) {
        // merge adjacent text nodes
        // this is necessary for SSR hydration because text nodes are
        // essentially merged when rendered to HTML strings
        res[lastIndex] = createTextVNode(last.text + c);
      } else if (c !== '') {
        // convert primitive to vnode
        res.push(createTextVNode(c));
      }
    } else {
      if (isTextNode(c) && isTextNode(last)) {
        // merge adjacent text nodes
        res[lastIndex] = createTextVNode(last.text + c.text);
      } else {
        // default key for nested array children (likely generated by v-for)
        if (isTrue(children._isVList) &&
          isDef(c.tag) &&
          isUndef(c.key) &&
          isDef(nestedIndex)) {
          c.key = "__vlist" + nestedIndex + "_" + i + "__";
        }
        res.push(c);
      }
    }
  }
  return res
}

/*  */

function initProvide (vm) {
  var provide = vm.$options.provide;
  if (provide) {
    vm._provided = typeof provide === 'function'
      ? provide.call(vm)
      : provide;
  }
}

function initInjections (vm) {
  var result = resolveInject(vm.$options.inject, vm);
  if (result) {
    toggleObserving(false);
    Object.keys(result).forEach(function (key) {
      /* istanbul ignore else */
      if (true) {
        defineReactive$$1(vm, key, result[key], function () {
          warn(
            "Avoid mutating an injected value directly since the changes will be " +
            "overwritten whenever the provided component re-renders. " +
            "injection being mutated: \"" + key + "\"",
            vm
          );
        });
      } else {}
    });
    toggleObserving(true);
  }
}

function resolveInject (inject, vm) {
  if (inject) {
    // inject is :any because flow is not smart enough to figure out cached
    var result = Object.create(null);
    var keys = hasSymbol
      ? Reflect.ownKeys(inject)
      : Object.keys(inject);

    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];
      // #6574 in case the inject object is observed...
      if (key === '__ob__') { continue }
      var provideKey = inject[key].from;
      var source = vm;
      while (source) {
        if (source._provided && hasOwn(source._provided, provideKey)) {
          result[key] = source._provided[provideKey];
          break
        }
        source = source.$parent;
      }
      if (!source) {
        if ('default' in inject[key]) {
          var provideDefault = inject[key].default;
          result[key] = typeof provideDefault === 'function'
            ? provideDefault.call(vm)
            : provideDefault;
        } else if (true) {
          warn(("Injection \"" + key + "\" not found"), vm);
        }
      }
    }
    return result
  }
}

/*  */



/**
 * Runtime helper for resolving raw children VNodes into a slot object.
 */
function resolveSlots (
  children,
  context
) {
  if (!children || !children.length) {
    return {}
  }
  var slots = {};
  for (var i = 0, l = children.length; i < l; i++) {
    var child = children[i];
    var data = child.data;
    // remove slot attribute if the node is resolved as a Vue slot node
    if (data && data.attrs && data.attrs.slot) {
      delete data.attrs.slot;
    }
    // named slots should only be respected if the vnode was rendered in the
    // same context.
    if ((child.context === context || child.fnContext === context) &&
      data && data.slot != null
    ) {
      var name = data.slot;
      var slot = (slots[name] || (slots[name] = []));
      if (child.tag === 'template') {
        slot.push.apply(slot, child.children || []);
      } else {
        slot.push(child);
      }
    } else {
      // fixed by xxxxxx 临时 hack 掉 uni-app 中的异步 name slot page
      if(child.asyncMeta && child.asyncMeta.data && child.asyncMeta.data.slot === 'page'){
        (slots['page'] || (slots['page'] = [])).push(child);
      }else{
        (slots.default || (slots.default = [])).push(child);
      }
    }
  }
  // ignore slots that contains only whitespace
  for (var name$1 in slots) {
    if (slots[name$1].every(isWhitespace)) {
      delete slots[name$1];
    }
  }
  return slots
}

function isWhitespace (node) {
  return (node.isComment && !node.asyncFactory) || node.text === ' '
}

/*  */

function normalizeScopedSlots (
  slots,
  normalSlots,
  prevSlots
) {
  var res;
  var hasNormalSlots = Object.keys(normalSlots).length > 0;
  var isStable = slots ? !!slots.$stable : !hasNormalSlots;
  var key = slots && slots.$key;
  if (!slots) {
    res = {};
  } else if (slots._normalized) {
    // fast path 1: child component re-render only, parent did not change
    return slots._normalized
  } else if (
    isStable &&
    prevSlots &&
    prevSlots !== emptyObject &&
    key === prevSlots.$key &&
    !hasNormalSlots &&
    !prevSlots.$hasNormal
  ) {
    // fast path 2: stable scoped slots w/ no normal slots to proxy,
    // only need to normalize once
    return prevSlots
  } else {
    res = {};
    for (var key$1 in slots) {
      if (slots[key$1] && key$1[0] !== '$') {
        res[key$1] = normalizeScopedSlot(normalSlots, key$1, slots[key$1]);
      }
    }
  }
  // expose normal slots on scopedSlots
  for (var key$2 in normalSlots) {
    if (!(key$2 in res)) {
      res[key$2] = proxyNormalSlot(normalSlots, key$2);
    }
  }
  // avoriaz seems to mock a non-extensible $scopedSlots object
  // and when that is passed down this would cause an error
  if (slots && Object.isExtensible(slots)) {
    (slots)._normalized = res;
  }
  def(res, '$stable', isStable);
  def(res, '$key', key);
  def(res, '$hasNormal', hasNormalSlots);
  return res
}

function normalizeScopedSlot(normalSlots, key, fn) {
  var normalized = function () {
    var res = arguments.length ? fn.apply(null, arguments) : fn({});
    res = res && typeof res === 'object' && !Array.isArray(res)
      ? [res] // single vnode
      : normalizeChildren(res);
    return res && (
      res.length === 0 ||
      (res.length === 1 && res[0].isComment) // #9658
    ) ? undefined
      : res
  };
  // this is a slot using the new v-slot syntax without scope. although it is
  // compiled as a scoped slot, render fn users would expect it to be present
  // on this.$slots because the usage is semantically a normal slot.
  if (fn.proxy) {
    Object.defineProperty(normalSlots, key, {
      get: normalized,
      enumerable: true,
      configurable: true
    });
  }
  return normalized
}

function proxyNormalSlot(slots, key) {
  return function () { return slots[key]; }
}

/*  */

/**
 * Runtime helper for rendering v-for lists.
 */
function renderList (
  val,
  render
) {
  var ret, i, l, keys, key;
  if (Array.isArray(val) || typeof val === 'string') {
    ret = new Array(val.length);
    for (i = 0, l = val.length; i < l; i++) {
      ret[i] = render(val[i], i, i, i); // fixed by xxxxxx
    }
  } else if (typeof val === 'number') {
    ret = new Array(val);
    for (i = 0; i < val; i++) {
      ret[i] = render(i + 1, i, i, i); // fixed by xxxxxx
    }
  } else if (isObject(val)) {
    if (hasSymbol && val[Symbol.iterator]) {
      ret = [];
      var iterator = val[Symbol.iterator]();
      var result = iterator.next();
      while (!result.done) {
        ret.push(render(result.value, ret.length, i, i++)); // fixed by xxxxxx
        result = iterator.next();
      }
    } else {
      keys = Object.keys(val);
      ret = new Array(keys.length);
      for (i = 0, l = keys.length; i < l; i++) {
        key = keys[i];
        ret[i] = render(val[key], key, i, i); // fixed by xxxxxx
      }
    }
  }
  if (!isDef(ret)) {
    ret = [];
  }
  (ret)._isVList = true;
  return ret
}

/*  */

/**
 * Runtime helper for rendering <slot>
 */
function renderSlot (
  name,
  fallback,
  props,
  bindObject
) {
  var scopedSlotFn = this.$scopedSlots[name];
  var nodes;
  if (scopedSlotFn) { // scoped slot
    props = props || {};
    if (bindObject) {
      if ( true && !isObject(bindObject)) {
        warn(
          'slot v-bind without argument expects an Object',
          this
        );
      }
      props = extend(extend({}, bindObject), props);
    }
    // fixed by xxxxxx app-plus scopedSlot
    nodes = scopedSlotFn(props, this, props._i) || fallback;
  } else {
    nodes = this.$slots[name] || fallback;
  }

  var target = props && props.slot;
  if (target) {
    return this.$createElement('template', { slot: target }, nodes)
  } else {
    return nodes
  }
}

/*  */

/**
 * Runtime helper for resolving filters
 */
function resolveFilter (id) {
  return resolveAsset(this.$options, 'filters', id, true) || identity
}

/*  */

function isKeyNotMatch (expect, actual) {
  if (Array.isArray(expect)) {
    return expect.indexOf(actual) === -1
  } else {
    return expect !== actual
  }
}

/**
 * Runtime helper for checking keyCodes from config.
 * exposed as Vue.prototype._k
 * passing in eventKeyName as last argument separately for backwards compat
 */
function checkKeyCodes (
  eventKeyCode,
  key,
  builtInKeyCode,
  eventKeyName,
  builtInKeyName
) {
  var mappedKeyCode = config.keyCodes[key] || builtInKeyCode;
  if (builtInKeyName && eventKeyName && !config.keyCodes[key]) {
    return isKeyNotMatch(builtInKeyName, eventKeyName)
  } else if (mappedKeyCode) {
    return isKeyNotMatch(mappedKeyCode, eventKeyCode)
  } else if (eventKeyName) {
    return hyphenate(eventKeyName) !== key
  }
}

/*  */

/**
 * Runtime helper for merging v-bind="object" into a VNode's data.
 */
function bindObjectProps (
  data,
  tag,
  value,
  asProp,
  isSync
) {
  if (value) {
    if (!isObject(value)) {
       true && warn(
        'v-bind without argument expects an Object or Array value',
        this
      );
    } else {
      if (Array.isArray(value)) {
        value = toObject(value);
      }
      var hash;
      var loop = function ( key ) {
        if (
          key === 'class' ||
          key === 'style' ||
          isReservedAttribute(key)
        ) {
          hash = data;
        } else {
          var type = data.attrs && data.attrs.type;
          hash = asProp || config.mustUseProp(tag, type, key)
            ? data.domProps || (data.domProps = {})
            : data.attrs || (data.attrs = {});
        }
        var camelizedKey = camelize(key);
        var hyphenatedKey = hyphenate(key);
        if (!(camelizedKey in hash) && !(hyphenatedKey in hash)) {
          hash[key] = value[key];

          if (isSync) {
            var on = data.on || (data.on = {});
            on[("update:" + key)] = function ($event) {
              value[key] = $event;
            };
          }
        }
      };

      for (var key in value) loop( key );
    }
  }
  return data
}

/*  */

/**
 * Runtime helper for rendering static trees.
 */
function renderStatic (
  index,
  isInFor
) {
  var cached = this._staticTrees || (this._staticTrees = []);
  var tree = cached[index];
  // if has already-rendered static tree and not inside v-for,
  // we can reuse the same tree.
  if (tree && !isInFor) {
    return tree
  }
  // otherwise, render a fresh tree.
  tree = cached[index] = this.$options.staticRenderFns[index].call(
    this._renderProxy,
    null,
    this // for render fns generated for functional component templates
  );
  markStatic(tree, ("__static__" + index), false);
  return tree
}

/**
 * Runtime helper for v-once.
 * Effectively it means marking the node as static with a unique key.
 */
function markOnce (
  tree,
  index,
  key
) {
  markStatic(tree, ("__once__" + index + (key ? ("_" + key) : "")), true);
  return tree
}

function markStatic (
  tree,
  key,
  isOnce
) {
  if (Array.isArray(tree)) {
    for (var i = 0; i < tree.length; i++) {
      if (tree[i] && typeof tree[i] !== 'string') {
        markStaticNode(tree[i], (key + "_" + i), isOnce);
      }
    }
  } else {
    markStaticNode(tree, key, isOnce);
  }
}

function markStaticNode (node, key, isOnce) {
  node.isStatic = true;
  node.key = key;
  node.isOnce = isOnce;
}

/*  */

function bindObjectListeners (data, value) {
  if (value) {
    if (!isPlainObject(value)) {
       true && warn(
        'v-on without argument expects an Object value',
        this
      );
    } else {
      var on = data.on = data.on ? extend({}, data.on) : {};
      for (var key in value) {
        var existing = on[key];
        var ours = value[key];
        on[key] = existing ? [].concat(existing, ours) : ours;
      }
    }
  }
  return data
}

/*  */

function resolveScopedSlots (
  fns, // see flow/vnode
  res,
  // the following are added in 2.6
  hasDynamicKeys,
  contentHashKey
) {
  res = res || { $stable: !hasDynamicKeys };
  for (var i = 0; i < fns.length; i++) {
    var slot = fns[i];
    if (Array.isArray(slot)) {
      resolveScopedSlots(slot, res, hasDynamicKeys);
    } else if (slot) {
      // marker for reverse proxying v-slot without scope on this.$slots
      if (slot.proxy) {
        slot.fn.proxy = true;
      }
      res[slot.key] = slot.fn;
    }
  }
  if (contentHashKey) {
    (res).$key = contentHashKey;
  }
  return res
}

/*  */

function bindDynamicKeys (baseObj, values) {
  for (var i = 0; i < values.length; i += 2) {
    var key = values[i];
    if (typeof key === 'string' && key) {
      baseObj[values[i]] = values[i + 1];
    } else if ( true && key !== '' && key !== null) {
      // null is a special value for explicitly removing a binding
      warn(
        ("Invalid value for dynamic directive argument (expected string or null): " + key),
        this
      );
    }
  }
  return baseObj
}

// helper to dynamically append modifier runtime markers to event names.
// ensure only append when value is already string, otherwise it will be cast
// to string and cause the type check to miss.
function prependModifier (value, symbol) {
  return typeof value === 'string' ? symbol + value : value
}

/*  */

function installRenderHelpers (target) {
  target._o = markOnce;
  target._n = toNumber;
  target._s = toString;
  target._l = renderList;
  target._t = renderSlot;
  target._q = looseEqual;
  target._i = looseIndexOf;
  target._m = renderStatic;
  target._f = resolveFilter;
  target._k = checkKeyCodes;
  target._b = bindObjectProps;
  target._v = createTextVNode;
  target._e = createEmptyVNode;
  target._u = resolveScopedSlots;
  target._g = bindObjectListeners;
  target._d = bindDynamicKeys;
  target._p = prependModifier;
}

/*  */

function FunctionalRenderContext (
  data,
  props,
  children,
  parent,
  Ctor
) {
  var this$1 = this;

  var options = Ctor.options;
  // ensure the createElement function in functional components
  // gets a unique context - this is necessary for correct named slot check
  var contextVm;
  if (hasOwn(parent, '_uid')) {
    contextVm = Object.create(parent);
    // $flow-disable-line
    contextVm._original = parent;
  } else {
    // the context vm passed in is a functional context as well.
    // in this case we want to make sure we are able to get a hold to the
    // real context instance.
    contextVm = parent;
    // $flow-disable-line
    parent = parent._original;
  }
  var isCompiled = isTrue(options._compiled);
  var needNormalization = !isCompiled;

  this.data = data;
  this.props = props;
  this.children = children;
  this.parent = parent;
  this.listeners = data.on || emptyObject;
  this.injections = resolveInject(options.inject, parent);
  this.slots = function () {
    if (!this$1.$slots) {
      normalizeScopedSlots(
        data.scopedSlots,
        this$1.$slots = resolveSlots(children, parent)
      );
    }
    return this$1.$slots
  };

  Object.defineProperty(this, 'scopedSlots', ({
    enumerable: true,
    get: function get () {
      return normalizeScopedSlots(data.scopedSlots, this.slots())
    }
  }));

  // support for compiled functional template
  if (isCompiled) {
    // exposing $options for renderStatic()
    this.$options = options;
    // pre-resolve slots for renderSlot()
    this.$slots = this.slots();
    this.$scopedSlots = normalizeScopedSlots(data.scopedSlots, this.$slots);
  }

  if (options._scopeId) {
    this._c = function (a, b, c, d) {
      var vnode = createElement(contextVm, a, b, c, d, needNormalization);
      if (vnode && !Array.isArray(vnode)) {
        vnode.fnScopeId = options._scopeId;
        vnode.fnContext = parent;
      }
      return vnode
    };
  } else {
    this._c = function (a, b, c, d) { return createElement(contextVm, a, b, c, d, needNormalization); };
  }
}

installRenderHelpers(FunctionalRenderContext.prototype);

function createFunctionalComponent (
  Ctor,
  propsData,
  data,
  contextVm,
  children
) {
  var options = Ctor.options;
  var props = {};
  var propOptions = options.props;
  if (isDef(propOptions)) {
    for (var key in propOptions) {
      props[key] = validateProp(key, propOptions, propsData || emptyObject);
    }
  } else {
    if (isDef(data.attrs)) { mergeProps(props, data.attrs); }
    if (isDef(data.props)) { mergeProps(props, data.props); }
  }

  var renderContext = new FunctionalRenderContext(
    data,
    props,
    children,
    contextVm,
    Ctor
  );

  var vnode = options.render.call(null, renderContext._c, renderContext);

  if (vnode instanceof VNode) {
    return cloneAndMarkFunctionalResult(vnode, data, renderContext.parent, options, renderContext)
  } else if (Array.isArray(vnode)) {
    var vnodes = normalizeChildren(vnode) || [];
    var res = new Array(vnodes.length);
    for (var i = 0; i < vnodes.length; i++) {
      res[i] = cloneAndMarkFunctionalResult(vnodes[i], data, renderContext.parent, options, renderContext);
    }
    return res
  }
}

function cloneAndMarkFunctionalResult (vnode, data, contextVm, options, renderContext) {
  // #7817 clone node before setting fnContext, otherwise if the node is reused
  // (e.g. it was from a cached normal slot) the fnContext causes named slots
  // that should not be matched to match.
  var clone = cloneVNode(vnode);
  clone.fnContext = contextVm;
  clone.fnOptions = options;
  if (true) {
    (clone.devtoolsMeta = clone.devtoolsMeta || {}).renderContext = renderContext;
  }
  if (data.slot) {
    (clone.data || (clone.data = {})).slot = data.slot;
  }
  return clone
}

function mergeProps (to, from) {
  for (var key in from) {
    to[camelize(key)] = from[key];
  }
}

/*  */

/*  */

/*  */

/*  */

// inline hooks to be invoked on component VNodes during patch
var componentVNodeHooks = {
  init: function init (vnode, hydrating) {
    if (
      vnode.componentInstance &&
      !vnode.componentInstance._isDestroyed &&
      vnode.data.keepAlive
    ) {
      // kept-alive components, treat as a patch
      var mountedNode = vnode; // work around flow
      componentVNodeHooks.prepatch(mountedNode, mountedNode);
    } else {
      var child = vnode.componentInstance = createComponentInstanceForVnode(
        vnode,
        activeInstance
      );
      child.$mount(hydrating ? vnode.elm : undefined, hydrating);
    }
  },

  prepatch: function prepatch (oldVnode, vnode) {
    var options = vnode.componentOptions;
    var child = vnode.componentInstance = oldVnode.componentInstance;
    updateChildComponent(
      child,
      options.propsData, // updated props
      options.listeners, // updated listeners
      vnode, // new parent vnode
      options.children // new children
    );
  },

  insert: function insert (vnode) {
    var context = vnode.context;
    var componentInstance = vnode.componentInstance;
    if (!componentInstance._isMounted) {
      callHook(componentInstance, 'onServiceCreated');
      callHook(componentInstance, 'onServiceAttached');
      componentInstance._isMounted = true;
      callHook(componentInstance, 'mounted');
    }
    if (vnode.data.keepAlive) {
      if (context._isMounted) {
        // vue-router#1212
        // During updates, a kept-alive component's child components may
        // change, so directly walking the tree here may call activated hooks
        // on incorrect children. Instead we push them into a queue which will
        // be processed after the whole patch process ended.
        queueActivatedComponent(componentInstance);
      } else {
        activateChildComponent(componentInstance, true /* direct */);
      }
    }
  },

  destroy: function destroy (vnode) {
    var componentInstance = vnode.componentInstance;
    if (!componentInstance._isDestroyed) {
      if (!vnode.data.keepAlive) {
        componentInstance.$destroy();
      } else {
        deactivateChildComponent(componentInstance, true /* direct */);
      }
    }
  }
};

var hooksToMerge = Object.keys(componentVNodeHooks);

function createComponent (
  Ctor,
  data,
  context,
  children,
  tag
) {
  if (isUndef(Ctor)) {
    return
  }

  var baseCtor = context.$options._base;

  // plain options object: turn it into a constructor
  if (isObject(Ctor)) {
    Ctor = baseCtor.extend(Ctor);
  }

  // if at this stage it's not a constructor or an async component factory,
  // reject.
  if (typeof Ctor !== 'function') {
    if (true) {
      warn(("Invalid Component definition: " + (String(Ctor))), context);
    }
    return
  }

  // async component
  var asyncFactory;
  if (isUndef(Ctor.cid)) {
    asyncFactory = Ctor;
    Ctor = resolveAsyncComponent(asyncFactory, baseCtor);
    if (Ctor === undefined) {
      // return a placeholder node for async component, which is rendered
      // as a comment node but preserves all the raw information for the node.
      // the information will be used for async server-rendering and hydration.
      return createAsyncPlaceholder(
        asyncFactory,
        data,
        context,
        children,
        tag
      )
    }
  }

  data = data || {};

  // resolve constructor options in case global mixins are applied after
  // component constructor creation
  resolveConstructorOptions(Ctor);

  // transform component v-model data into props & events
  if (isDef(data.model)) {
    transformModel(Ctor.options, data);
  }

  // extract props
  var propsData = extractPropsFromVNodeData(data, Ctor, tag, context); // fixed by xxxxxx

  // functional component
  if (isTrue(Ctor.options.functional)) {
    return createFunctionalComponent(Ctor, propsData, data, context, children)
  }

  // extract listeners, since these needs to be treated as
  // child component listeners instead of DOM listeners
  var listeners = data.on;
  // replace with listeners with .native modifier
  // so it gets processed during parent component patch.
  data.on = data.nativeOn;

  if (isTrue(Ctor.options.abstract)) {
    // abstract components do not keep anything
    // other than props & listeners & slot

    // work around flow
    var slot = data.slot;
    data = {};
    if (slot) {
      data.slot = slot;
    }
  }

  // install component management hooks onto the placeholder node
  installComponentHooks(data);

  // return a placeholder vnode
  var name = Ctor.options.name || tag;
  var vnode = new VNode(
    ("vue-component-" + (Ctor.cid) + (name ? ("-" + name) : '')),
    data, undefined, undefined, undefined, context,
    { Ctor: Ctor, propsData: propsData, listeners: listeners, tag: tag, children: children },
    asyncFactory
  );

  return vnode
}

function createComponentInstanceForVnode (
  vnode, // we know it's MountedComponentVNode but flow doesn't
  parent // activeInstance in lifecycle state
) {
  var options = {
    _isComponent: true,
    _parentVnode: vnode,
    parent: parent
  };
  // check inline-template render functions
  var inlineTemplate = vnode.data.inlineTemplate;
  if (isDef(inlineTemplate)) {
    options.render = inlineTemplate.render;
    options.staticRenderFns = inlineTemplate.staticRenderFns;
  }
  return new vnode.componentOptions.Ctor(options)
}

function installComponentHooks (data) {
  var hooks = data.hook || (data.hook = {});
  for (var i = 0; i < hooksToMerge.length; i++) {
    var key = hooksToMerge[i];
    var existing = hooks[key];
    var toMerge = componentVNodeHooks[key];
    if (existing !== toMerge && !(existing && existing._merged)) {
      hooks[key] = existing ? mergeHook$1(toMerge, existing) : toMerge;
    }
  }
}

function mergeHook$1 (f1, f2) {
  var merged = function (a, b) {
    // flow complains about extra args which is why we use any
    f1(a, b);
    f2(a, b);
  };
  merged._merged = true;
  return merged
}

// transform component v-model info (value and callback) into
// prop and event handler respectively.
function transformModel (options, data) {
  var prop = (options.model && options.model.prop) || 'value';
  var event = (options.model && options.model.event) || 'input'
  ;(data.attrs || (data.attrs = {}))[prop] = data.model.value;
  var on = data.on || (data.on = {});
  var existing = on[event];
  var callback = data.model.callback;
  if (isDef(existing)) {
    if (
      Array.isArray(existing)
        ? existing.indexOf(callback) === -1
        : existing !== callback
    ) {
      on[event] = [callback].concat(existing);
    }
  } else {
    on[event] = callback;
  }
}

/*  */

var SIMPLE_NORMALIZE = 1;
var ALWAYS_NORMALIZE = 2;

// wrapper function for providing a more flexible interface
// without getting yelled at by flow
function createElement (
  context,
  tag,
  data,
  children,
  normalizationType,
  alwaysNormalize
) {
  if (Array.isArray(data) || isPrimitive(data)) {
    normalizationType = children;
    children = data;
    data = undefined;
  }
  if (isTrue(alwaysNormalize)) {
    normalizationType = ALWAYS_NORMALIZE;
  }
  return _createElement(context, tag, data, children, normalizationType)
}

function _createElement (
  context,
  tag,
  data,
  children,
  normalizationType
) {
  if (isDef(data) && isDef((data).__ob__)) {
     true && warn(
      "Avoid using observed data object as vnode data: " + (JSON.stringify(data)) + "\n" +
      'Always create fresh vnode data objects in each render!',
      context
    );
    return createEmptyVNode()
  }
  // object syntax in v-bind
  if (isDef(data) && isDef(data.is)) {
    tag = data.is;
  }
  if (!tag) {
    // in case of component :is set to falsy value
    return createEmptyVNode()
  }
  // warn against non-primitive key
  if ( true &&
    isDef(data) && isDef(data.key) && !isPrimitive(data.key)
  ) {
    {
      warn(
        'Avoid using non-primitive value as key, ' +
        'use string/number value instead.',
        context
      );
    }
  }
  // support single function children as default scoped slot
  if (Array.isArray(children) &&
    typeof children[0] === 'function'
  ) {
    data = data || {};
    data.scopedSlots = { default: children[0] };
    children.length = 0;
  }
  if (normalizationType === ALWAYS_NORMALIZE) {
    children = normalizeChildren(children);
  } else if (normalizationType === SIMPLE_NORMALIZE) {
    children = simpleNormalizeChildren(children);
  }
  var vnode, ns;
  if (typeof tag === 'string') {
    var Ctor;
    ns = (context.$vnode && context.$vnode.ns) || config.getTagNamespace(tag);
    if (config.isReservedTag(tag)) {
      // platform built-in elements
      if ( true && isDef(data) && isDef(data.nativeOn)) {
        warn(
          ("The .native modifier for v-on is only valid on components but it was used on <" + tag + ">."),
          context
        );
      }
      vnode = new VNode(
        config.parsePlatformTagName(tag), data, children,
        undefined, undefined, context
      );
    } else if ((!data || !data.pre) && isDef(Ctor = resolveAsset(context.$options, 'components', tag))) {
      // component
      vnode = createComponent(Ctor, data, context, children, tag);
    } else {
      // unknown or unlisted namespaced elements
      // check at runtime because it may get assigned a namespace when its
      // parent normalizes children
      vnode = new VNode(
        tag, data, children,
        undefined, undefined, context
      );
    }
  } else {
    // direct component options / constructor
    vnode = createComponent(tag, data, context, children);
  }
  if (Array.isArray(vnode)) {
    return vnode
  } else if (isDef(vnode)) {
    if (isDef(ns)) { applyNS(vnode, ns); }
    if (isDef(data)) { registerDeepBindings(data); }
    return vnode
  } else {
    return createEmptyVNode()
  }
}

function applyNS (vnode, ns, force) {
  vnode.ns = ns;
  if (vnode.tag === 'foreignObject') {
    // use default namespace inside foreignObject
    ns = undefined;
    force = true;
  }
  if (isDef(vnode.children)) {
    for (var i = 0, l = vnode.children.length; i < l; i++) {
      var child = vnode.children[i];
      if (isDef(child.tag) && (
        isUndef(child.ns) || (isTrue(force) && child.tag !== 'svg'))) {
        applyNS(child, ns, force);
      }
    }
  }
}

// ref #5318
// necessary to ensure parent re-render when deep bindings like :style and
// :class are used on slot nodes
function registerDeepBindings (data) {
  if (isObject(data.style)) {
    traverse(data.style);
  }
  if (isObject(data.class)) {
    traverse(data.class);
  }
}

/*  */

function initRender (vm) {
  vm._vnode = null; // the root of the child tree
  vm._staticTrees = null; // v-once cached trees
  var options = vm.$options;
  var parentVnode = vm.$vnode = options._parentVnode; // the placeholder node in parent tree
  var renderContext = parentVnode && parentVnode.context;
  vm.$slots = resolveSlots(options._renderChildren, renderContext);
  vm.$scopedSlots = emptyObject;
  // bind the createElement fn to this instance
  // so that we get proper render context inside it.
  // args order: tag, data, children, normalizationType, alwaysNormalize
  // internal version is used by render functions compiled from templates
  vm._c = function (a, b, c, d) { return createElement(vm, a, b, c, d, false); };
  // normalization is always applied for the public version, used in
  // user-written render functions.
  vm.$createElement = function (a, b, c, d) { return createElement(vm, a, b, c, d, true); };

  // $attrs & $listeners are exposed for easier HOC creation.
  // they need to be reactive so that HOCs using them are always updated
  var parentData = parentVnode && parentVnode.data;

  /* istanbul ignore else */
  if (true) {
    defineReactive$$1(vm, '$attrs', parentData && parentData.attrs || emptyObject, function () {
      !isUpdatingChildComponent && warn("$attrs is readonly.", vm);
    }, true);
    defineReactive$$1(vm, '$listeners', options._parentListeners || emptyObject, function () {
      !isUpdatingChildComponent && warn("$listeners is readonly.", vm);
    }, true);
  } else {}
}

var currentRenderingInstance = null;

function renderMixin (Vue) {
  // install runtime convenience helpers
  installRenderHelpers(Vue.prototype);

  Vue.prototype.$nextTick = function (fn) {
    return nextTick(fn, this)
  };

  Vue.prototype._render = function () {
    var vm = this;
    var ref = vm.$options;
    var render = ref.render;
    var _parentVnode = ref._parentVnode;

    if (_parentVnode) {
      vm.$scopedSlots = normalizeScopedSlots(
        _parentVnode.data.scopedSlots,
        vm.$slots,
        vm.$scopedSlots
      );
    }

    // set parent vnode. this allows render functions to have access
    // to the data on the placeholder node.
    vm.$vnode = _parentVnode;
    // render self
    var vnode;
    try {
      // There's no need to maintain a stack because all render fns are called
      // separately from one another. Nested component's render fns are called
      // when parent component is patched.
      currentRenderingInstance = vm;
      vnode = render.call(vm._renderProxy, vm.$createElement);
    } catch (e) {
      handleError(e, vm, "render");
      // return error render result,
      // or previous vnode to prevent render error causing blank component
      /* istanbul ignore else */
      if ( true && vm.$options.renderError) {
        try {
          vnode = vm.$options.renderError.call(vm._renderProxy, vm.$createElement, e);
        } catch (e) {
          handleError(e, vm, "renderError");
          vnode = vm._vnode;
        }
      } else {
        vnode = vm._vnode;
      }
    } finally {
      currentRenderingInstance = null;
    }
    // if the returned array contains only a single node, allow it
    if (Array.isArray(vnode) && vnode.length === 1) {
      vnode = vnode[0];
    }
    // return empty vnode in case the render function errored out
    if (!(vnode instanceof VNode)) {
      if ( true && Array.isArray(vnode)) {
        warn(
          'Multiple root nodes returned from render function. Render function ' +
          'should return a single root node.',
          vm
        );
      }
      vnode = createEmptyVNode();
    }
    // set parent
    vnode.parent = _parentVnode;
    return vnode
  };
}

/*  */

function ensureCtor (comp, base) {
  if (
    comp.__esModule ||
    (hasSymbol && comp[Symbol.toStringTag] === 'Module')
  ) {
    comp = comp.default;
  }
  return isObject(comp)
    ? base.extend(comp)
    : comp
}

function createAsyncPlaceholder (
  factory,
  data,
  context,
  children,
  tag
) {
  var node = createEmptyVNode();
  node.asyncFactory = factory;
  node.asyncMeta = { data: data, context: context, children: children, tag: tag };
  return node
}

function resolveAsyncComponent (
  factory,
  baseCtor
) {
  if (isTrue(factory.error) && isDef(factory.errorComp)) {
    return factory.errorComp
  }

  if (isDef(factory.resolved)) {
    return factory.resolved
  }

  var owner = currentRenderingInstance;
  if (owner && isDef(factory.owners) && factory.owners.indexOf(owner) === -1) {
    // already pending
    factory.owners.push(owner);
  }

  if (isTrue(factory.loading) && isDef(factory.loadingComp)) {
    return factory.loadingComp
  }

  if (owner && !isDef(factory.owners)) {
    var owners = factory.owners = [owner];
    var sync = true;
    var timerLoading = null;
    var timerTimeout = null

    ;(owner).$on('hook:destroyed', function () { return remove(owners, owner); });

    var forceRender = function (renderCompleted) {
      for (var i = 0, l = owners.length; i < l; i++) {
        (owners[i]).$forceUpdate();
      }

      if (renderCompleted) {
        owners.length = 0;
        if (timerLoading !== null) {
          clearTimeout(timerLoading);
          timerLoading = null;
        }
        if (timerTimeout !== null) {
          clearTimeout(timerTimeout);
          timerTimeout = null;
        }
      }
    };

    var resolve = once(function (res) {
      // cache resolved
      factory.resolved = ensureCtor(res, baseCtor);
      // invoke callbacks only if this is not a synchronous resolve
      // (async resolves are shimmed as synchronous during SSR)
      if (!sync) {
        forceRender(true);
      } else {
        owners.length = 0;
      }
    });

    var reject = once(function (reason) {
       true && warn(
        "Failed to resolve async component: " + (String(factory)) +
        (reason ? ("\nReason: " + reason) : '')
      );
      if (isDef(factory.errorComp)) {
        factory.error = true;
        forceRender(true);
      }
    });

    var res = factory(resolve, reject);

    if (isObject(res)) {
      if (isPromise(res)) {
        // () => Promise
        if (isUndef(factory.resolved)) {
          res.then(resolve, reject);
        }
      } else if (isPromise(res.component)) {
        res.component.then(resolve, reject);

        if (isDef(res.error)) {
          factory.errorComp = ensureCtor(res.error, baseCtor);
        }

        if (isDef(res.loading)) {
          factory.loadingComp = ensureCtor(res.loading, baseCtor);
          if (res.delay === 0) {
            factory.loading = true;
          } else {
            timerLoading = setTimeout(function () {
              timerLoading = null;
              if (isUndef(factory.resolved) && isUndef(factory.error)) {
                factory.loading = true;
                forceRender(false);
              }
            }, res.delay || 200);
          }
        }

        if (isDef(res.timeout)) {
          timerTimeout = setTimeout(function () {
            timerTimeout = null;
            if (isUndef(factory.resolved)) {
              reject(
                 true
                  ? ("timeout (" + (res.timeout) + "ms)")
                  : undefined
              );
            }
          }, res.timeout);
        }
      }
    }

    sync = false;
    // return in case resolved synchronously
    return factory.loading
      ? factory.loadingComp
      : factory.resolved
  }
}

/*  */

function isAsyncPlaceholder (node) {
  return node.isComment && node.asyncFactory
}

/*  */

function getFirstComponentChild (children) {
  if (Array.isArray(children)) {
    for (var i = 0; i < children.length; i++) {
      var c = children[i];
      if (isDef(c) && (isDef(c.componentOptions) || isAsyncPlaceholder(c))) {
        return c
      }
    }
  }
}

/*  */

/*  */

function initEvents (vm) {
  vm._events = Object.create(null);
  vm._hasHookEvent = false;
  // init parent attached events
  var listeners = vm.$options._parentListeners;
  if (listeners) {
    updateComponentListeners(vm, listeners);
  }
}

var target;

function add (event, fn) {
  target.$on(event, fn);
}

function remove$1 (event, fn) {
  target.$off(event, fn);
}

function createOnceHandler (event, fn) {
  var _target = target;
  return function onceHandler () {
    var res = fn.apply(null, arguments);
    if (res !== null) {
      _target.$off(event, onceHandler);
    }
  }
}

function updateComponentListeners (
  vm,
  listeners,
  oldListeners
) {
  target = vm;
  updateListeners(listeners, oldListeners || {}, add, remove$1, createOnceHandler, vm);
  target = undefined;
}

function eventsMixin (Vue) {
  var hookRE = /^hook:/;
  Vue.prototype.$on = function (event, fn) {
    var vm = this;
    if (Array.isArray(event)) {
      for (var i = 0, l = event.length; i < l; i++) {
        vm.$on(event[i], fn);
      }
    } else {
      (vm._events[event] || (vm._events[event] = [])).push(fn);
      // optimize hook:event cost by using a boolean flag marked at registration
      // instead of a hash lookup
      if (hookRE.test(event)) {
        vm._hasHookEvent = true;
      }
    }
    return vm
  };

  Vue.prototype.$once = function (event, fn) {
    var vm = this;
    function on () {
      vm.$off(event, on);
      fn.apply(vm, arguments);
    }
    on.fn = fn;
    vm.$on(event, on);
    return vm
  };

  Vue.prototype.$off = function (event, fn) {
    var vm = this;
    // all
    if (!arguments.length) {
      vm._events = Object.create(null);
      return vm
    }
    // array of events
    if (Array.isArray(event)) {
      for (var i$1 = 0, l = event.length; i$1 < l; i$1++) {
        vm.$off(event[i$1], fn);
      }
      return vm
    }
    // specific event
    var cbs = vm._events[event];
    if (!cbs) {
      return vm
    }
    if (!fn) {
      vm._events[event] = null;
      return vm
    }
    // specific handler
    var cb;
    var i = cbs.length;
    while (i--) {
      cb = cbs[i];
      if (cb === fn || cb.fn === fn) {
        cbs.splice(i, 1);
        break
      }
    }
    return vm
  };

  Vue.prototype.$emit = function (event) {
    var vm = this;
    if (true) {
      var lowerCaseEvent = event.toLowerCase();
      if (lowerCaseEvent !== event && vm._events[lowerCaseEvent]) {
        tip(
          "Event \"" + lowerCaseEvent + "\" is emitted in component " +
          (formatComponentName(vm)) + " but the handler is registered for \"" + event + "\". " +
          "Note that HTML attributes are case-insensitive and you cannot use " +
          "v-on to listen to camelCase events when using in-DOM templates. " +
          "You should probably use \"" + (hyphenate(event)) + "\" instead of \"" + event + "\"."
        );
      }
    }
    var cbs = vm._events[event];
    if (cbs) {
      cbs = cbs.length > 1 ? toArray(cbs) : cbs;
      var args = toArray(arguments, 1);
      var info = "event handler for \"" + event + "\"";
      for (var i = 0, l = cbs.length; i < l; i++) {
        invokeWithErrorHandling(cbs[i], vm, args, vm, info);
      }
    }
    return vm
  };
}

/*  */

var activeInstance = null;
var isUpdatingChildComponent = false;

function setActiveInstance(vm) {
  var prevActiveInstance = activeInstance;
  activeInstance = vm;
  return function () {
    activeInstance = prevActiveInstance;
  }
}

function initLifecycle (vm) {
  var options = vm.$options;

  // locate first non-abstract parent
  var parent = options.parent;
  if (parent && !options.abstract) {
    while (parent.$options.abstract && parent.$parent) {
      parent = parent.$parent;
    }
    parent.$children.push(vm);
  }

  vm.$parent = parent;
  vm.$root = parent ? parent.$root : vm;

  vm.$children = [];
  vm.$refs = {};

  vm._watcher = null;
  vm._inactive = null;
  vm._directInactive = false;
  vm._isMounted = false;
  vm._isDestroyed = false;
  vm._isBeingDestroyed = false;
}

function lifecycleMixin (Vue) {
  Vue.prototype._update = function (vnode, hydrating) {
    var vm = this;
    var prevEl = vm.$el;
    var prevVnode = vm._vnode;
    var restoreActiveInstance = setActiveInstance(vm);
    vm._vnode = vnode;
    // Vue.prototype.__patch__ is injected in entry points
    // based on the rendering backend used.
    if (!prevVnode) {
      // initial render
      vm.$el = vm.__patch__(vm.$el, vnode, hydrating, false /* removeOnly */);
    } else {
      // updates
      vm.$el = vm.__patch__(prevVnode, vnode);
    }
    restoreActiveInstance();
    // update __vue__ reference
    if (prevEl) {
      prevEl.__vue__ = null;
    }
    if (vm.$el) {
      vm.$el.__vue__ = vm;
    }
    // if parent is an HOC, update its $el as well
    if (vm.$vnode && vm.$parent && vm.$vnode === vm.$parent._vnode) {
      vm.$parent.$el = vm.$el;
    }
    // updated hook is called by the scheduler to ensure that children are
    // updated in a parent's updated hook.
  };

  Vue.prototype.$forceUpdate = function () {
    var vm = this;
    if (vm._watcher) {
      vm._watcher.update();
    }
  };

  Vue.prototype.$destroy = function () {
    var vm = this;
    if (vm._isBeingDestroyed) {
      return
    }
    callHook(vm, 'beforeDestroy');
    vm._isBeingDestroyed = true;
    // remove self from parent
    var parent = vm.$parent;
    if (parent && !parent._isBeingDestroyed && !vm.$options.abstract) {
      remove(parent.$children, vm);
    }
    // teardown watchers
    if (vm._watcher) {
      vm._watcher.teardown();
    }
    var i = vm._watchers.length;
    while (i--) {
      vm._watchers[i].teardown();
    }
    // remove reference from data ob
    // frozen object may not have observer.
    if (vm._data.__ob__) {
      vm._data.__ob__.vmCount--;
    }
    // call the last hook...
    vm._isDestroyed = true;
    // invoke destroy hooks on current rendered tree
    vm.__patch__(vm._vnode, null);
    // fire destroyed hook
    callHook(vm, 'destroyed');
    // turn off all instance listeners.
    vm.$off();
    // remove __vue__ reference
    if (vm.$el) {
      vm.$el.__vue__ = null;
    }
    // release circular reference (#6759)
    if (vm.$vnode) {
      vm.$vnode.parent = null;
    }
  };
}

function updateChildComponent (
  vm,
  propsData,
  listeners,
  parentVnode,
  renderChildren
) {
  if (true) {
    isUpdatingChildComponent = true;
  }

  // determine whether component has slot children
  // we need to do this before overwriting $options._renderChildren.

  // check if there are dynamic scopedSlots (hand-written or compiled but with
  // dynamic slot names). Static scoped slots compiled from template has the
  // "$stable" marker.
  var newScopedSlots = parentVnode.data.scopedSlots;
  var oldScopedSlots = vm.$scopedSlots;
  var hasDynamicScopedSlot = !!(
    (newScopedSlots && !newScopedSlots.$stable) ||
    (oldScopedSlots !== emptyObject && !oldScopedSlots.$stable) ||
    (newScopedSlots && vm.$scopedSlots.$key !== newScopedSlots.$key)
  );

  // Any static slot children from the parent may have changed during parent's
  // update. Dynamic scoped slots may also have changed. In such cases, a forced
  // update is necessary to ensure correctness.
  var needsForceUpdate = !!(
    renderChildren ||               // has new static slots
    vm.$options._renderChildren ||  // has old static slots
    hasDynamicScopedSlot
  );

  vm.$options._parentVnode = parentVnode;
  vm.$vnode = parentVnode; // update vm's placeholder node without re-render

  if (vm._vnode) { // update child tree's parent
    vm._vnode.parent = parentVnode;
  }
  vm.$options._renderChildren = renderChildren;

  // update $attrs and $listeners hash
  // these are also reactive so they may trigger child update if the child
  // used them during render
  vm.$attrs = parentVnode.data.attrs || emptyObject;
  vm.$listeners = listeners || emptyObject;

  // update props
  if (propsData && vm.$options.props) {
    toggleObserving(false);
    var props = vm._props;
    var propKeys = vm.$options._propKeys || [];
    for (var i = 0; i < propKeys.length; i++) {
      var key = propKeys[i];
      var propOptions = vm.$options.props; // wtf flow?
      props[key] = validateProp(key, propOptions, propsData, vm);
    }
    toggleObserving(true);
    // keep a copy of raw propsData
    vm.$options.propsData = propsData;
  }
  
  // fixed by xxxxxx update properties(mp runtime)
  vm._$updateProperties && vm._$updateProperties(vm);
  
  // update listeners
  listeners = listeners || emptyObject;
  var oldListeners = vm.$options._parentListeners;
  vm.$options._parentListeners = listeners;
  updateComponentListeners(vm, listeners, oldListeners);

  // resolve slots + force update if has children
  if (needsForceUpdate) {
    vm.$slots = resolveSlots(renderChildren, parentVnode.context);
    vm.$forceUpdate();
  }

  if (true) {
    isUpdatingChildComponent = false;
  }
}

function isInInactiveTree (vm) {
  while (vm && (vm = vm.$parent)) {
    if (vm._inactive) { return true }
  }
  return false
}

function activateChildComponent (vm, direct) {
  if (direct) {
    vm._directInactive = false;
    if (isInInactiveTree(vm)) {
      return
    }
  } else if (vm._directInactive) {
    return
  }
  if (vm._inactive || vm._inactive === null) {
    vm._inactive = false;
    for (var i = 0; i < vm.$children.length; i++) {
      activateChildComponent(vm.$children[i]);
    }
    callHook(vm, 'activated');
  }
}

function deactivateChildComponent (vm, direct) {
  if (direct) {
    vm._directInactive = true;
    if (isInInactiveTree(vm)) {
      return
    }
  }
  if (!vm._inactive) {
    vm._inactive = true;
    for (var i = 0; i < vm.$children.length; i++) {
      deactivateChildComponent(vm.$children[i]);
    }
    callHook(vm, 'deactivated');
  }
}

function callHook (vm, hook) {
  // #7573 disable dep collection when invoking lifecycle hooks
  pushTarget();
  var handlers = vm.$options[hook];
  var info = hook + " hook";
  if (handlers) {
    for (var i = 0, j = handlers.length; i < j; i++) {
      invokeWithErrorHandling(handlers[i], vm, null, vm, info);
    }
  }
  if (vm._hasHookEvent) {
    vm.$emit('hook:' + hook);
  }
  popTarget();
}

/*  */

var MAX_UPDATE_COUNT = 100;

var queue = [];
var activatedChildren = [];
var has = {};
var circular = {};
var waiting = false;
var flushing = false;
var index = 0;

/**
 * Reset the scheduler's state.
 */
function resetSchedulerState () {
  index = queue.length = activatedChildren.length = 0;
  has = {};
  if (true) {
    circular = {};
  }
  waiting = flushing = false;
}

// Async edge case #6566 requires saving the timestamp when event listeners are
// attached. However, calling performance.now() has a perf overhead especially
// if the page has thousands of event listeners. Instead, we take a timestamp
// every time the scheduler flushes and use that for all event listeners
// attached during that flush.
var currentFlushTimestamp = 0;

// Async edge case fix requires storing an event listener's attach timestamp.
var getNow = Date.now;

// Determine what event timestamp the browser is using. Annoyingly, the
// timestamp can either be hi-res (relative to page load) or low-res
// (relative to UNIX epoch), so in order to compare time we have to use the
// same timestamp type when saving the flush timestamp.
// All IE versions use low-res event timestamps, and have problematic clock
// implementations (#9632)
if (inBrowser && !isIE) {
  var performance = window.performance;
  if (
    performance &&
    typeof performance.now === 'function' &&
    getNow() > document.createEvent('Event').timeStamp
  ) {
    // if the event timestamp, although evaluated AFTER the Date.now(), is
    // smaller than it, it means the event is using a hi-res timestamp,
    // and we need to use the hi-res version for event listener timestamps as
    // well.
    getNow = function () { return performance.now(); };
  }
}

/**
 * Flush both queues and run the watchers.
 */
function flushSchedulerQueue () {
  currentFlushTimestamp = getNow();
  flushing = true;
  var watcher, id;

  // Sort queue before flush.
  // This ensures that:
  // 1. Components are updated from parent to child. (because parent is always
  //    created before the child)
  // 2. A component's user watchers are run before its render watcher (because
  //    user watchers are created before the render watcher)
  // 3. If a component is destroyed during a parent component's watcher run,
  //    its watchers can be skipped.
  queue.sort(function (a, b) { return a.id - b.id; });

  // do not cache length because more watchers might be pushed
  // as we run existing watchers
  for (index = 0; index < queue.length; index++) {
    watcher = queue[index];
    if (watcher.before) {
      watcher.before();
    }
    id = watcher.id;
    has[id] = null;
    watcher.run();
    // in dev build, check and stop circular updates.
    if ( true && has[id] != null) {
      circular[id] = (circular[id] || 0) + 1;
      if (circular[id] > MAX_UPDATE_COUNT) {
        warn(
          'You may have an infinite update loop ' + (
            watcher.user
              ? ("in watcher with expression \"" + (watcher.expression) + "\"")
              : "in a component render function."
          ),
          watcher.vm
        );
        break
      }
    }
  }

  // keep copies of post queues before resetting state
  var activatedQueue = activatedChildren.slice();
  var updatedQueue = queue.slice();

  resetSchedulerState();

  // call component updated and activated hooks
  callActivatedHooks(activatedQueue);
  callUpdatedHooks(updatedQueue);

  // devtool hook
  /* istanbul ignore if */
  if (devtools && config.devtools) {
    devtools.emit('flush');
  }
}

function callUpdatedHooks (queue) {
  var i = queue.length;
  while (i--) {
    var watcher = queue[i];
    var vm = watcher.vm;
    if (vm._watcher === watcher && vm._isMounted && !vm._isDestroyed) {
      callHook(vm, 'updated');
    }
  }
}

/**
 * Queue a kept-alive component that was activated during patch.
 * The queue will be processed after the entire tree has been patched.
 */
function queueActivatedComponent (vm) {
  // setting _inactive to false here so that a render function can
  // rely on checking whether it's in an inactive tree (e.g. router-view)
  vm._inactive = false;
  activatedChildren.push(vm);
}

function callActivatedHooks (queue) {
  for (var i = 0; i < queue.length; i++) {
    queue[i]._inactive = true;
    activateChildComponent(queue[i], true /* true */);
  }
}

/**
 * Push a watcher into the watcher queue.
 * Jobs with duplicate IDs will be skipped unless it's
 * pushed when the queue is being flushed.
 */
function queueWatcher (watcher) {
  var id = watcher.id;
  if (has[id] == null) {
    has[id] = true;
    if (!flushing) {
      queue.push(watcher);
    } else {
      // if already flushing, splice the watcher based on its id
      // if already past its id, it will be run next immediately.
      var i = queue.length - 1;
      while (i > index && queue[i].id > watcher.id) {
        i--;
      }
      queue.splice(i + 1, 0, watcher);
    }
    // queue the flush
    if (!waiting) {
      waiting = true;

      if ( true && !config.async) {
        flushSchedulerQueue();
        return
      }
      nextTick(flushSchedulerQueue);
    }
  }
}

/*  */



var uid$2 = 0;

/**
 * A watcher parses an expression, collects dependencies,
 * and fires callback when the expression value changes.
 * This is used for both the $watch() api and directives.
 */
var Watcher = function Watcher (
  vm,
  expOrFn,
  cb,
  options,
  isRenderWatcher
) {
  this.vm = vm;
  if (isRenderWatcher) {
    vm._watcher = this;
  }
  vm._watchers.push(this);
  // options
  if (options) {
    this.deep = !!options.deep;
    this.user = !!options.user;
    this.lazy = !!options.lazy;
    this.sync = !!options.sync;
    this.before = options.before;
  } else {
    this.deep = this.user = this.lazy = this.sync = false;
  }
  this.cb = cb;
  this.id = ++uid$2; // uid for batching
  this.active = true;
  this.dirty = this.lazy; // for lazy watchers
  this.deps = [];
  this.newDeps = [];
  this.depIds = new _Set();
  this.newDepIds = new _Set();
  this.expression =  true
    ? expOrFn.toString()
    : undefined;
  // parse expression for getter
  if (typeof expOrFn === 'function') {
    this.getter = expOrFn;
  } else {
    this.getter = parsePath(expOrFn);
    if (!this.getter) {
      this.getter = noop;
       true && warn(
        "Failed watching path: \"" + expOrFn + "\" " +
        'Watcher only accepts simple dot-delimited paths. ' +
        'For full control, use a function instead.',
        vm
      );
    }
  }
  this.value = this.lazy
    ? undefined
    : this.get();
};

/**
 * Evaluate the getter, and re-collect dependencies.
 */
Watcher.prototype.get = function get () {
  pushTarget(this);
  var value;
  var vm = this.vm;
  try {
    value = this.getter.call(vm, vm);
  } catch (e) {
    if (this.user) {
      handleError(e, vm, ("getter for watcher \"" + (this.expression) + "\""));
    } else {
      throw e
    }
  } finally {
    // "touch" every property so they are all tracked as
    // dependencies for deep watching
    if (this.deep) {
      traverse(value);
    }
    popTarget();
    this.cleanupDeps();
  }
  return value
};

/**
 * Add a dependency to this directive.
 */
Watcher.prototype.addDep = function addDep (dep) {
  var id = dep.id;
  if (!this.newDepIds.has(id)) {
    this.newDepIds.add(id);
    this.newDeps.push(dep);
    if (!this.depIds.has(id)) {
      dep.addSub(this);
    }
  }
};

/**
 * Clean up for dependency collection.
 */
Watcher.prototype.cleanupDeps = function cleanupDeps () {
  var i = this.deps.length;
  while (i--) {
    var dep = this.deps[i];
    if (!this.newDepIds.has(dep.id)) {
      dep.removeSub(this);
    }
  }
  var tmp = this.depIds;
  this.depIds = this.newDepIds;
  this.newDepIds = tmp;
  this.newDepIds.clear();
  tmp = this.deps;
  this.deps = this.newDeps;
  this.newDeps = tmp;
  this.newDeps.length = 0;
};

/**
 * Subscriber interface.
 * Will be called when a dependency changes.
 */
Watcher.prototype.update = function update () {
  /* istanbul ignore else */
  if (this.lazy) {
    this.dirty = true;
  } else if (this.sync) {
    this.run();
  } else {
    queueWatcher(this);
  }
};

/**
 * Scheduler job interface.
 * Will be called by the scheduler.
 */
Watcher.prototype.run = function run () {
  if (this.active) {
    var value = this.get();
    if (
      value !== this.value ||
      // Deep watchers and watchers on Object/Arrays should fire even
      // when the value is the same, because the value may
      // have mutated.
      isObject(value) ||
      this.deep
    ) {
      // set new value
      var oldValue = this.value;
      this.value = value;
      if (this.user) {
        try {
          this.cb.call(this.vm, value, oldValue);
        } catch (e) {
          handleError(e, this.vm, ("callback for watcher \"" + (this.expression) + "\""));
        }
      } else {
        this.cb.call(this.vm, value, oldValue);
      }
    }
  }
};

/**
 * Evaluate the value of the watcher.
 * This only gets called for lazy watchers.
 */
Watcher.prototype.evaluate = function evaluate () {
  this.value = this.get();
  this.dirty = false;
};

/**
 * Depend on all deps collected by this watcher.
 */
Watcher.prototype.depend = function depend () {
  var i = this.deps.length;
  while (i--) {
    this.deps[i].depend();
  }
};

/**
 * Remove self from all dependencies' subscriber list.
 */
Watcher.prototype.teardown = function teardown () {
  if (this.active) {
    // remove self from vm's watcher list
    // this is a somewhat expensive operation so we skip it
    // if the vm is being destroyed.
    if (!this.vm._isBeingDestroyed) {
      remove(this.vm._watchers, this);
    }
    var i = this.deps.length;
    while (i--) {
      this.deps[i].removeSub(this);
    }
    this.active = false;
  }
};

/*  */

var sharedPropertyDefinition = {
  enumerable: true,
  configurable: true,
  get: noop,
  set: noop
};

function proxy (target, sourceKey, key) {
  sharedPropertyDefinition.get = function proxyGetter () {
    return this[sourceKey][key]
  };
  sharedPropertyDefinition.set = function proxySetter (val) {
    this[sourceKey][key] = val;
  };
  Object.defineProperty(target, key, sharedPropertyDefinition);
}

function initState (vm) {
  vm._watchers = [];
  var opts = vm.$options;
  if (opts.props) { initProps(vm, opts.props); }
  if (opts.methods) { initMethods(vm, opts.methods); }
  if (opts.data) {
    initData(vm);
  } else {
    observe(vm._data = {}, true /* asRootData */);
  }
  if (opts.computed) { initComputed(vm, opts.computed); }
  if (opts.watch && opts.watch !== nativeWatch) {
    initWatch(vm, opts.watch);
  }
}

function initProps (vm, propsOptions) {
  var propsData = vm.$options.propsData || {};
  var props = vm._props = {};
  // cache prop keys so that future props updates can iterate using Array
  // instead of dynamic object key enumeration.
  var keys = vm.$options._propKeys = [];
  var isRoot = !vm.$parent;
  // root instance props should be converted
  if (!isRoot) {
    toggleObserving(false);
  }
  var loop = function ( key ) {
    keys.push(key);
    var value = validateProp(key, propsOptions, propsData, vm);
    /* istanbul ignore else */
    if (true) {
      var hyphenatedKey = hyphenate(key);
      if (isReservedAttribute(hyphenatedKey) ||
          config.isReservedAttr(hyphenatedKey)) {
        warn(
          ("\"" + hyphenatedKey + "\" is a reserved attribute and cannot be used as component prop."),
          vm
        );
      }
      defineReactive$$1(props, key, value, function () {
        if (!isRoot && !isUpdatingChildComponent) {
          {
            if(vm.mpHost === 'mp-baidu' || vm.mpHost === 'mp-kuaishou' || vm.mpHost === 'mp-xhs'){//百度、快手、小红书 observer 在 setData callback 之后触发，直接忽略该 warn
                return
            }
            //fixed by xxxxxx __next_tick_pending,uni://form-field 时不告警
            if(
                key === 'value' && 
                Array.isArray(vm.$options.behaviors) &&
                vm.$options.behaviors.indexOf('uni://form-field') !== -1
              ){
              return
            }
            if(vm._getFormData){
              return
            }
            var $parent = vm.$parent;
            while($parent){
              if($parent.__next_tick_pending){
                return  
              }
              $parent = $parent.$parent;
            }
          }
          warn(
            "Avoid mutating a prop directly since the value will be " +
            "overwritten whenever the parent component re-renders. " +
            "Instead, use a data or computed property based on the prop's " +
            "value. Prop being mutated: \"" + key + "\"",
            vm
          );
        }
      });
    } else {}
    // static props are already proxied on the component's prototype
    // during Vue.extend(). We only need to proxy props defined at
    // instantiation here.
    if (!(key in vm)) {
      proxy(vm, "_props", key);
    }
  };

  for (var key in propsOptions) loop( key );
  toggleObserving(true);
}

function initData (vm) {
  var data = vm.$options.data;
  data = vm._data = typeof data === 'function'
    ? getData(data, vm)
    : data || {};
  if (!isPlainObject(data)) {
    data = {};
     true && warn(
      'data functions should return an object:\n' +
      'https://vuejs.org/v2/guide/components.html#data-Must-Be-a-Function',
      vm
    );
  }
  // proxy data on instance
  var keys = Object.keys(data);
  var props = vm.$options.props;
  var methods = vm.$options.methods;
  var i = keys.length;
  while (i--) {
    var key = keys[i];
    if (true) {
      if (methods && hasOwn(methods, key)) {
        warn(
          ("Method \"" + key + "\" has already been defined as a data property."),
          vm
        );
      }
    }
    if (props && hasOwn(props, key)) {
       true && warn(
        "The data property \"" + key + "\" is already declared as a prop. " +
        "Use prop default value instead.",
        vm
      );
    } else if (!isReserved(key)) {
      proxy(vm, "_data", key);
    }
  }
  // observe data
  observe(data, true /* asRootData */);
}

function getData (data, vm) {
  // #7573 disable dep collection when invoking data getters
  pushTarget();
  try {
    return data.call(vm, vm)
  } catch (e) {
    handleError(e, vm, "data()");
    return {}
  } finally {
    popTarget();
  }
}

var computedWatcherOptions = { lazy: true };

function initComputed (vm, computed) {
  // $flow-disable-line
  var watchers = vm._computedWatchers = Object.create(null);
  // computed properties are just getters during SSR
  var isSSR = isServerRendering();

  for (var key in computed) {
    var userDef = computed[key];
    var getter = typeof userDef === 'function' ? userDef : userDef.get;
    if ( true && getter == null) {
      warn(
        ("Getter is missing for computed property \"" + key + "\"."),
        vm
      );
    }

    if (!isSSR) {
      // create internal watcher for the computed property.
      watchers[key] = new Watcher(
        vm,
        getter || noop,
        noop,
        computedWatcherOptions
      );
    }

    // component-defined computed properties are already defined on the
    // component prototype. We only need to define computed properties defined
    // at instantiation here.
    if (!(key in vm)) {
      defineComputed(vm, key, userDef);
    } else if (true) {
      if (key in vm.$data) {
        warn(("The computed property \"" + key + "\" is already defined in data."), vm);
      } else if (vm.$options.props && key in vm.$options.props) {
        warn(("The computed property \"" + key + "\" is already defined as a prop."), vm);
      }
    }
  }
}

function defineComputed (
  target,
  key,
  userDef
) {
  var shouldCache = !isServerRendering();
  if (typeof userDef === 'function') {
    sharedPropertyDefinition.get = shouldCache
      ? createComputedGetter(key)
      : createGetterInvoker(userDef);
    sharedPropertyDefinition.set = noop;
  } else {
    sharedPropertyDefinition.get = userDef.get
      ? shouldCache && userDef.cache !== false
        ? createComputedGetter(key)
        : createGetterInvoker(userDef.get)
      : noop;
    sharedPropertyDefinition.set = userDef.set || noop;
  }
  if ( true &&
      sharedPropertyDefinition.set === noop) {
    sharedPropertyDefinition.set = function () {
      warn(
        ("Computed property \"" + key + "\" was assigned to but it has no setter."),
        this
      );
    };
  }
  Object.defineProperty(target, key, sharedPropertyDefinition);
}

function createComputedGetter (key) {
  return function computedGetter () {
    var watcher = this._computedWatchers && this._computedWatchers[key];
    if (watcher) {
      if (watcher.dirty) {
        watcher.evaluate();
      }
      if (Dep.SharedObject.target) {// fixed by xxxxxx
        watcher.depend();
      }
      return watcher.value
    }
  }
}

function createGetterInvoker(fn) {
  return function computedGetter () {
    return fn.call(this, this)
  }
}

function initMethods (vm, methods) {
  var props = vm.$options.props;
  for (var key in methods) {
    if (true) {
      if (typeof methods[key] !== 'function') {
        warn(
          "Method \"" + key + "\" has type \"" + (typeof methods[key]) + "\" in the component definition. " +
          "Did you reference the function correctly?",
          vm
        );
      }
      if (props && hasOwn(props, key)) {
        warn(
          ("Method \"" + key + "\" has already been defined as a prop."),
          vm
        );
      }
      if ((key in vm) && isReserved(key)) {
        warn(
          "Method \"" + key + "\" conflicts with an existing Vue instance method. " +
          "Avoid defining component methods that start with _ or $."
        );
      }
    }
    vm[key] = typeof methods[key] !== 'function' ? noop : bind(methods[key], vm);
  }
}

function initWatch (vm, watch) {
  for (var key in watch) {
    var handler = watch[key];
    if (Array.isArray(handler)) {
      for (var i = 0; i < handler.length; i++) {
        createWatcher(vm, key, handler[i]);
      }
    } else {
      createWatcher(vm, key, handler);
    }
  }
}

function createWatcher (
  vm,
  expOrFn,
  handler,
  options
) {
  if (isPlainObject(handler)) {
    options = handler;
    handler = handler.handler;
  }
  if (typeof handler === 'string') {
    handler = vm[handler];
  }
  return vm.$watch(expOrFn, handler, options)
}

function stateMixin (Vue) {
  // flow somehow has problems with directly declared definition object
  // when using Object.defineProperty, so we have to procedurally build up
  // the object here.
  var dataDef = {};
  dataDef.get = function () { return this._data };
  var propsDef = {};
  propsDef.get = function () { return this._props };
  if (true) {
    dataDef.set = function () {
      warn(
        'Avoid replacing instance root $data. ' +
        'Use nested data properties instead.',
        this
      );
    };
    propsDef.set = function () {
      warn("$props is readonly.", this);
    };
  }
  Object.defineProperty(Vue.prototype, '$data', dataDef);
  Object.defineProperty(Vue.prototype, '$props', propsDef);

  Vue.prototype.$set = set;
  Vue.prototype.$delete = del;

  Vue.prototype.$watch = function (
    expOrFn,
    cb,
    options
  ) {
    var vm = this;
    if (isPlainObject(cb)) {
      return createWatcher(vm, expOrFn, cb, options)
    }
    options = options || {};
    options.user = true;
    var watcher = new Watcher(vm, expOrFn, cb, options);
    if (options.immediate) {
      try {
        cb.call(vm, watcher.value);
      } catch (error) {
        handleError(error, vm, ("callback for immediate watcher \"" + (watcher.expression) + "\""));
      }
    }
    return function unwatchFn () {
      watcher.teardown();
    }
  };
}

/*  */

var uid$3 = 0;

function initMixin (Vue) {
  Vue.prototype._init = function (options) {
    var vm = this;
    // a uid
    vm._uid = uid$3++;

    var startTag, endTag;
    /* istanbul ignore if */
    if ( true && config.performance && mark) {
      startTag = "vue-perf-start:" + (vm._uid);
      endTag = "vue-perf-end:" + (vm._uid);
      mark(startTag);
    }

    // a flag to avoid this being observed
    vm._isVue = true;
    // merge options
    if (options && options._isComponent) {
      // optimize internal component instantiation
      // since dynamic options merging is pretty slow, and none of the
      // internal component options needs special treatment.
      initInternalComponent(vm, options);
    } else {
      vm.$options = mergeOptions(
        resolveConstructorOptions(vm.constructor),
        options || {},
        vm
      );
    }
    /* istanbul ignore else */
    if (true) {
      initProxy(vm);
    } else {}
    // expose real self
    vm._self = vm;
    initLifecycle(vm);
    initEvents(vm);
    initRender(vm);
    callHook(vm, 'beforeCreate');
    !vm._$fallback && initInjections(vm); // resolve injections before data/props  
    initState(vm);
    !vm._$fallback && initProvide(vm); // resolve provide after data/props
    !vm._$fallback && callHook(vm, 'created');      

    /* istanbul ignore if */
    if ( true && config.performance && mark) {
      vm._name = formatComponentName(vm, false);
      mark(endTag);
      measure(("vue " + (vm._name) + " init"), startTag, endTag);
    }

    if (vm.$options.el) {
      vm.$mount(vm.$options.el);
    }
  };
}

function initInternalComponent (vm, options) {
  var opts = vm.$options = Object.create(vm.constructor.options);
  // doing this because it's faster than dynamic enumeration.
  var parentVnode = options._parentVnode;
  opts.parent = options.parent;
  opts._parentVnode = parentVnode;

  var vnodeComponentOptions = parentVnode.componentOptions;
  opts.propsData = vnodeComponentOptions.propsData;
  opts._parentListeners = vnodeComponentOptions.listeners;
  opts._renderChildren = vnodeComponentOptions.children;
  opts._componentTag = vnodeComponentOptions.tag;

  if (options.render) {
    opts.render = options.render;
    opts.staticRenderFns = options.staticRenderFns;
  }
}

function resolveConstructorOptions (Ctor) {
  var options = Ctor.options;
  if (Ctor.super) {
    var superOptions = resolveConstructorOptions(Ctor.super);
    var cachedSuperOptions = Ctor.superOptions;
    if (superOptions !== cachedSuperOptions) {
      // super option changed,
      // need to resolve new options.
      Ctor.superOptions = superOptions;
      // check if there are any late-modified/attached options (#4976)
      var modifiedOptions = resolveModifiedOptions(Ctor);
      // update base extend options
      if (modifiedOptions) {
        extend(Ctor.extendOptions, modifiedOptions);
      }
      options = Ctor.options = mergeOptions(superOptions, Ctor.extendOptions);
      if (options.name) {
        options.components[options.name] = Ctor;
      }
    }
  }
  return options
}

function resolveModifiedOptions (Ctor) {
  var modified;
  var latest = Ctor.options;
  var sealed = Ctor.sealedOptions;
  for (var key in latest) {
    if (latest[key] !== sealed[key]) {
      if (!modified) { modified = {}; }
      modified[key] = latest[key];
    }
  }
  return modified
}

function Vue (options) {
  if ( true &&
    !(this instanceof Vue)
  ) {
    warn('Vue is a constructor and should be called with the `new` keyword');
  }
  this._init(options);
}

initMixin(Vue);
stateMixin(Vue);
eventsMixin(Vue);
lifecycleMixin(Vue);
renderMixin(Vue);

/*  */

function initUse (Vue) {
  Vue.use = function (plugin) {
    var installedPlugins = (this._installedPlugins || (this._installedPlugins = []));
    if (installedPlugins.indexOf(plugin) > -1) {
      return this
    }

    // additional parameters
    var args = toArray(arguments, 1);
    args.unshift(this);
    if (typeof plugin.install === 'function') {
      plugin.install.apply(plugin, args);
    } else if (typeof plugin === 'function') {
      plugin.apply(null, args);
    }
    installedPlugins.push(plugin);
    return this
  };
}

/*  */

function initMixin$1 (Vue) {
  Vue.mixin = function (mixin) {
    this.options = mergeOptions(this.options, mixin);
    return this
  };
}

/*  */

function initExtend (Vue) {
  /**
   * Each instance constructor, including Vue, has a unique
   * cid. This enables us to create wrapped "child
   * constructors" for prototypal inheritance and cache them.
   */
  Vue.cid = 0;
  var cid = 1;

  /**
   * Class inheritance
   */
  Vue.extend = function (extendOptions) {
    extendOptions = extendOptions || {};
    var Super = this;
    var SuperId = Super.cid;
    var cachedCtors = extendOptions._Ctor || (extendOptions._Ctor = {});
    if (cachedCtors[SuperId]) {
      return cachedCtors[SuperId]
    }

    var name = extendOptions.name || Super.options.name;
    if ( true && name) {
      validateComponentName(name);
    }

    var Sub = function VueComponent (options) {
      this._init(options);
    };
    Sub.prototype = Object.create(Super.prototype);
    Sub.prototype.constructor = Sub;
    Sub.cid = cid++;
    Sub.options = mergeOptions(
      Super.options,
      extendOptions
    );
    Sub['super'] = Super;

    // For props and computed properties, we define the proxy getters on
    // the Vue instances at extension time, on the extended prototype. This
    // avoids Object.defineProperty calls for each instance created.
    if (Sub.options.props) {
      initProps$1(Sub);
    }
    if (Sub.options.computed) {
      initComputed$1(Sub);
    }

    // allow further extension/mixin/plugin usage
    Sub.extend = Super.extend;
    Sub.mixin = Super.mixin;
    Sub.use = Super.use;

    // create asset registers, so extended classes
    // can have their private assets too.
    ASSET_TYPES.forEach(function (type) {
      Sub[type] = Super[type];
    });
    // enable recursive self-lookup
    if (name) {
      Sub.options.components[name] = Sub;
    }

    // keep a reference to the super options at extension time.
    // later at instantiation we can check if Super's options have
    // been updated.
    Sub.superOptions = Super.options;
    Sub.extendOptions = extendOptions;
    Sub.sealedOptions = extend({}, Sub.options);

    // cache constructor
    cachedCtors[SuperId] = Sub;
    return Sub
  };
}

function initProps$1 (Comp) {
  var props = Comp.options.props;
  for (var key in props) {
    proxy(Comp.prototype, "_props", key);
  }
}

function initComputed$1 (Comp) {
  var computed = Comp.options.computed;
  for (var key in computed) {
    defineComputed(Comp.prototype, key, computed[key]);
  }
}

/*  */

function initAssetRegisters (Vue) {
  /**
   * Create asset registration methods.
   */
  ASSET_TYPES.forEach(function (type) {
    Vue[type] = function (
      id,
      definition
    ) {
      if (!definition) {
        return this.options[type + 's'][id]
      } else {
        /* istanbul ignore if */
        if ( true && type === 'component') {
          validateComponentName(id);
        }
        if (type === 'component' && isPlainObject(definition)) {
          definition.name = definition.name || id;
          definition = this.options._base.extend(definition);
        }
        if (type === 'directive' && typeof definition === 'function') {
          definition = { bind: definition, update: definition };
        }
        this.options[type + 's'][id] = definition;
        return definition
      }
    };
  });
}

/*  */



function getComponentName (opts) {
  return opts && (opts.Ctor.options.name || opts.tag)
}

function matches (pattern, name) {
  if (Array.isArray(pattern)) {
    return pattern.indexOf(name) > -1
  } else if (typeof pattern === 'string') {
    return pattern.split(',').indexOf(name) > -1
  } else if (isRegExp(pattern)) {
    return pattern.test(name)
  }
  /* istanbul ignore next */
  return false
}

function pruneCache (keepAliveInstance, filter) {
  var cache = keepAliveInstance.cache;
  var keys = keepAliveInstance.keys;
  var _vnode = keepAliveInstance._vnode;
  for (var key in cache) {
    var cachedNode = cache[key];
    if (cachedNode) {
      var name = getComponentName(cachedNode.componentOptions);
      if (name && !filter(name)) {
        pruneCacheEntry(cache, key, keys, _vnode);
      }
    }
  }
}

function pruneCacheEntry (
  cache,
  key,
  keys,
  current
) {
  var cached$$1 = cache[key];
  if (cached$$1 && (!current || cached$$1.tag !== current.tag)) {
    cached$$1.componentInstance.$destroy();
  }
  cache[key] = null;
  remove(keys, key);
}

var patternTypes = [String, RegExp, Array];

var KeepAlive = {
  name: 'keep-alive',
  abstract: true,

  props: {
    include: patternTypes,
    exclude: patternTypes,
    max: [String, Number]
  },

  created: function created () {
    this.cache = Object.create(null);
    this.keys = [];
  },

  destroyed: function destroyed () {
    for (var key in this.cache) {
      pruneCacheEntry(this.cache, key, this.keys);
    }
  },

  mounted: function mounted () {
    var this$1 = this;

    this.$watch('include', function (val) {
      pruneCache(this$1, function (name) { return matches(val, name); });
    });
    this.$watch('exclude', function (val) {
      pruneCache(this$1, function (name) { return !matches(val, name); });
    });
  },

  render: function render () {
    var slot = this.$slots.default;
    var vnode = getFirstComponentChild(slot);
    var componentOptions = vnode && vnode.componentOptions;
    if (componentOptions) {
      // check pattern
      var name = getComponentName(componentOptions);
      var ref = this;
      var include = ref.include;
      var exclude = ref.exclude;
      if (
        // not included
        (include && (!name || !matches(include, name))) ||
        // excluded
        (exclude && name && matches(exclude, name))
      ) {
        return vnode
      }

      var ref$1 = this;
      var cache = ref$1.cache;
      var keys = ref$1.keys;
      var key = vnode.key == null
        // same constructor may get registered as different local components
        // so cid alone is not enough (#3269)
        ? componentOptions.Ctor.cid + (componentOptions.tag ? ("::" + (componentOptions.tag)) : '')
        : vnode.key;
      if (cache[key]) {
        vnode.componentInstance = cache[key].componentInstance;
        // make current key freshest
        remove(keys, key);
        keys.push(key);
      } else {
        cache[key] = vnode;
        keys.push(key);
        // prune oldest entry
        if (this.max && keys.length > parseInt(this.max)) {
          pruneCacheEntry(cache, keys[0], keys, this._vnode);
        }
      }

      vnode.data.keepAlive = true;
    }
    return vnode || (slot && slot[0])
  }
};

var builtInComponents = {
  KeepAlive: KeepAlive
};

/*  */

function initGlobalAPI (Vue) {
  // config
  var configDef = {};
  configDef.get = function () { return config; };
  if (true) {
    configDef.set = function () {
      warn(
        'Do not replace the Vue.config object, set individual fields instead.'
      );
    };
  }
  Object.defineProperty(Vue, 'config', configDef);

  // exposed util methods.
  // NOTE: these are not considered part of the public API - avoid relying on
  // them unless you are aware of the risk.
  Vue.util = {
    warn: warn,
    extend: extend,
    mergeOptions: mergeOptions,
    defineReactive: defineReactive$$1
  };

  Vue.set = set;
  Vue.delete = del;
  Vue.nextTick = nextTick;

  // 2.6 explicit observable API
  Vue.observable = function (obj) {
    observe(obj);
    return obj
  };

  Vue.options = Object.create(null);
  ASSET_TYPES.forEach(function (type) {
    Vue.options[type + 's'] = Object.create(null);
  });

  // this is used to identify the "base" constructor to extend all plain-object
  // components with in Weex's multi-instance scenarios.
  Vue.options._base = Vue;

  extend(Vue.options.components, builtInComponents);

  initUse(Vue);
  initMixin$1(Vue);
  initExtend(Vue);
  initAssetRegisters(Vue);
}

initGlobalAPI(Vue);

Object.defineProperty(Vue.prototype, '$isServer', {
  get: isServerRendering
});

Object.defineProperty(Vue.prototype, '$ssrContext', {
  get: function get () {
    /* istanbul ignore next */
    return this.$vnode && this.$vnode.ssrContext
  }
});

// expose FunctionalRenderContext for ssr runtime helper installation
Object.defineProperty(Vue, 'FunctionalRenderContext', {
  value: FunctionalRenderContext
});

Vue.version = '2.6.11';

/**
 * https://raw.githubusercontent.com/Tencent/westore/master/packages/westore/utils/diff.js
 */
var ARRAYTYPE = '[object Array]';
var OBJECTTYPE = '[object Object]';
var NULLTYPE = '[object Null]';
var UNDEFINEDTYPE = '[object Undefined]';
// const FUNCTIONTYPE = '[object Function]'

function diff(current, pre) {
    var result = {};
    syncKeys(current, pre);
    _diff(current, pre, '', result);
    return result
}

function syncKeys(current, pre) {
    if (current === pre) { return }
    var rootCurrentType = type(current);
    var rootPreType = type(pre);
    if (rootCurrentType == OBJECTTYPE && rootPreType == OBJECTTYPE) {
        if(Object.keys(current).length >= Object.keys(pre).length){
            for (var key in pre) {
                var currentValue = current[key];
                if (currentValue === undefined) {
                    current[key] = null;
                } else {
                    syncKeys(currentValue, pre[key]);
                }
            }
        }
    } else if (rootCurrentType == ARRAYTYPE && rootPreType == ARRAYTYPE) {
        if (current.length >= pre.length) {
            pre.forEach(function (item, index) {
                syncKeys(current[index], item);
            });
        }
    }
}

function nullOrUndefined(currentType, preType) {
    if(
        (currentType === NULLTYPE || currentType === UNDEFINEDTYPE) && 
        (preType === NULLTYPE || preType === UNDEFINEDTYPE)
    ) {
        return false
    }
    return true
}

function _diff(current, pre, path, result) {
    if (current === pre) { return }
    var rootCurrentType = type(current);
    var rootPreType = type(pre);
    if (rootCurrentType == OBJECTTYPE) {
        if (rootPreType != OBJECTTYPE || Object.keys(current).length < Object.keys(pre).length) {
            setResult(result, path, current);
        } else {
            var loop = function ( key ) {
                var currentValue = current[key];
                var preValue = pre[key];
                var currentType = type(currentValue);
                var preType = type(preValue);
                if (currentType != ARRAYTYPE && currentType != OBJECTTYPE) {
                    if (currentValue !== pre[key] && nullOrUndefined(currentType, preType)) {
                        setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                    }
                } else if (currentType == ARRAYTYPE) {
                    if (preType != ARRAYTYPE) {
                        setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                    } else {
                        if (currentValue.length < preValue.length) {
                            setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                        } else {
                            currentValue.forEach(function (item, index) {
                                _diff(item, preValue[index], (path == '' ? '' : path + ".") + key + '[' + index + ']', result);
                            });
                        }
                    }
                } else if (currentType == OBJECTTYPE) {
                    if (preType != OBJECTTYPE || Object.keys(currentValue).length < Object.keys(preValue).length) {
                        setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                    } else {
                        for (var subKey in currentValue) {
                            _diff(currentValue[subKey], preValue[subKey], (path == '' ? '' : path + ".") + key + '.' + subKey, result);
                        }
                    }
                }
            };

            for (var key in current) loop( key );
        }
    } else if (rootCurrentType == ARRAYTYPE) {
        if (rootPreType != ARRAYTYPE) {
            setResult(result, path, current);
        } else {
            if (current.length < pre.length) {
                setResult(result, path, current);
            } else {
                current.forEach(function (item, index) {
                    _diff(item, pre[index], path + '[' + index + ']', result);
                });
            }
        }
    } else {
        setResult(result, path, current);
    }
}

function setResult(result, k, v) {
    // if (type(v) != FUNCTIONTYPE) {
        result[k] = v;
    // }
}

function type(obj) {
    return Object.prototype.toString.call(obj)
}

/*  */

function flushCallbacks$1(vm) {
    if (vm.__next_tick_callbacks && vm.__next_tick_callbacks.length) {
        if (Object({"VUE_APP_DARK_MODE":"false","VUE_APP_NAME":"demo","VUE_APP_PLATFORM":"mp-weixin","NODE_ENV":"development","BASE_URL":"/"}).VUE_APP_DEBUG) {
            var mpInstance = vm.$scope;
            console.log('[' + (+new Date) + '][' + (mpInstance.is || mpInstance.route) + '][' + vm._uid +
                ']:flushCallbacks[' + vm.__next_tick_callbacks.length + ']');
        }
        var copies = vm.__next_tick_callbacks.slice(0);
        vm.__next_tick_callbacks.length = 0;
        for (var i = 0; i < copies.length; i++) {
            copies[i]();
        }
    }
}

function hasRenderWatcher(vm) {
    return queue.find(function (watcher) { return vm._watcher === watcher; })
}

function nextTick$1(vm, cb) {
    //1.nextTick 之前 已 setData 且 setData 还未回调完成
    //2.nextTick 之前存在 render watcher
    if (!vm.__next_tick_pending && !hasRenderWatcher(vm)) {
        if(Object({"VUE_APP_DARK_MODE":"false","VUE_APP_NAME":"demo","VUE_APP_PLATFORM":"mp-weixin","NODE_ENV":"development","BASE_URL":"/"}).VUE_APP_DEBUG){
            var mpInstance = vm.$scope;
            console.log('[' + (+new Date) + '][' + (mpInstance.is || mpInstance.route) + '][' + vm._uid +
                ']:nextVueTick');
        }
        return nextTick(cb, vm)
    }else{
        if(Object({"VUE_APP_DARK_MODE":"false","VUE_APP_NAME":"demo","VUE_APP_PLATFORM":"mp-weixin","NODE_ENV":"development","BASE_URL":"/"}).VUE_APP_DEBUG){
            var mpInstance$1 = vm.$scope;
            console.log('[' + (+new Date) + '][' + (mpInstance$1.is || mpInstance$1.route) + '][' + vm._uid +
                ']:nextMPTick');
        }
    }
    var _resolve;
    if (!vm.__next_tick_callbacks) {
        vm.__next_tick_callbacks = [];
    }
    vm.__next_tick_callbacks.push(function () {
        if (cb) {
            try {
                cb.call(vm);
            } catch (e) {
                handleError(e, vm, 'nextTick');
            }
        } else if (_resolve) {
            _resolve(vm);
        }
    });
    // $flow-disable-line
    if (!cb && typeof Promise !== 'undefined') {
        return new Promise(function (resolve) {
            _resolve = resolve;
        })
    }
}

/*  */

function clearInstance(key, value) {
  // 简易去除 Vue 和小程序组件实例
  if (value) {
    if (value._isVue || value.__v_isMPComponent) {
      return {}
    }
  }
  return value
}

function cloneWithData(vm) {
  // 确保当前 vm 所有数据被同步
  var ret = Object.create(null);
  var dataKeys = [].concat(
    Object.keys(vm._data || {}),
    Object.keys(vm._computedWatchers || {}));

  dataKeys.reduce(function(ret, key) {
    ret[key] = vm[key];
    return ret
  }, ret);

  // vue-composition-api
  var compositionApiState = vm.__composition_api_state__ || vm.__secret_vfa_state__;
  var rawBindings = compositionApiState && compositionApiState.rawBindings;
  if (rawBindings) {
    Object.keys(rawBindings).forEach(function (key) {
      ret[key] = vm[key];
    });
  }

  //TODO 需要把无用数据处理掉，比如 list=>l0 则 list 需要移除，否则多传输一份数据
  Object.assign(ret, vm.$mp.data || {});
  if (
    Array.isArray(vm.$options.behaviors) &&
    vm.$options.behaviors.indexOf('uni://form-field') !== -1
  ) { //form-field
    ret['name'] = vm.name;
    ret['value'] = vm.value;
  }

  return JSON.parse(JSON.stringify(ret, clearInstance))
}

var patch = function(oldVnode, vnode) {
  var this$1 = this;

  if (vnode === null) { //destroy
    return
  }
  if (this.mpType === 'page' || this.mpType === 'component') {
    var mpInstance = this.$scope;
    var data = Object.create(null);
    try {
      data = cloneWithData(this);
    } catch (err) {
      console.error(err);
    }
    data.__webviewId__ = mpInstance.data.__webviewId__;
    var mpData = Object.create(null);
    Object.keys(data).forEach(function (key) { //仅同步 data 中有的数据
      mpData[key] = mpInstance.data[key];
    });
    var diffData = this.$shouldDiffData === false ? data : diff(data, mpData);
    if (Object.keys(diffData).length) {
      if (Object({"VUE_APP_DARK_MODE":"false","VUE_APP_NAME":"demo","VUE_APP_PLATFORM":"mp-weixin","NODE_ENV":"development","BASE_URL":"/"}).VUE_APP_DEBUG) {
        console.log('[' + (+new Date) + '][' + (mpInstance.is || mpInstance.route) + '][' + this._uid +
          ']差量更新',
          JSON.stringify(diffData));
      }
      this.__next_tick_pending = true;
      mpInstance.setData(diffData, function () {
        this$1.__next_tick_pending = false;
        flushCallbacks$1(this$1);
      });
    } else {
      flushCallbacks$1(this);
    }
  }
};

/*  */

function createEmptyRender() {

}

function mountComponent$1(
  vm,
  el,
  hydrating
) {
  if (!vm.mpType) {//main.js 中的 new Vue
    return vm
  }
  if (vm.mpType === 'app') {
    vm.$options.render = createEmptyRender;
  }
  if (!vm.$options.render) {
    vm.$options.render = createEmptyRender;
    if (true) {
      /* istanbul ignore if */
      if ((vm.$options.template && vm.$options.template.charAt(0) !== '#') ||
        vm.$options.el || el) {
        warn(
          'You are using the runtime-only build of Vue where the template ' +
          'compiler is not available. Either pre-compile the templates into ' +
          'render functions, or use the compiler-included build.',
          vm
        );
      } else {
        warn(
          'Failed to mount component: template or render function not defined.',
          vm
        );
      }
    }
  }
  
  !vm._$fallback && callHook(vm, 'beforeMount');

  var updateComponent = function () {
    vm._update(vm._render(), hydrating);
  };

  // we set this to vm._watcher inside the watcher's constructor
  // since the watcher's initial patch may call $forceUpdate (e.g. inside child
  // component's mounted hook), which relies on vm._watcher being already defined
  new Watcher(vm, updateComponent, noop, {
    before: function before() {
      if (vm._isMounted && !vm._isDestroyed) {
        callHook(vm, 'beforeUpdate');
      }
    }
  }, true /* isRenderWatcher */);
  hydrating = false;
  return vm
}

/*  */

function renderClass (
  staticClass,
  dynamicClass
) {
  if (isDef(staticClass) || isDef(dynamicClass)) {
    return concat(staticClass, stringifyClass(dynamicClass))
  }
  /* istanbul ignore next */
  return ''
}

function concat (a, b) {
  return a ? b ? (a + ' ' + b) : a : (b || '')
}

function stringifyClass (value) {
  if (Array.isArray(value)) {
    return stringifyArray(value)
  }
  if (isObject(value)) {
    return stringifyObject(value)
  }
  if (typeof value === 'string') {
    return value
  }
  /* istanbul ignore next */
  return ''
}

function stringifyArray (value) {
  var res = '';
  var stringified;
  for (var i = 0, l = value.length; i < l; i++) {
    if (isDef(stringified = stringifyClass(value[i])) && stringified !== '') {
      if (res) { res += ' '; }
      res += stringified;
    }
  }
  return res
}

function stringifyObject (value) {
  var res = '';
  for (var key in value) {
    if (value[key]) {
      if (res) { res += ' '; }
      res += key;
    }
  }
  return res
}

/*  */

var parseStyleText = cached(function (cssText) {
  var res = {};
  var listDelimiter = /;(?![^(]*\))/g;
  var propertyDelimiter = /:(.+)/;
  cssText.split(listDelimiter).forEach(function (item) {
    if (item) {
      var tmp = item.split(propertyDelimiter);
      tmp.length > 1 && (res[tmp[0].trim()] = tmp[1].trim());
    }
  });
  return res
});

// normalize possible array / string values into Object
function normalizeStyleBinding (bindingStyle) {
  if (Array.isArray(bindingStyle)) {
    return toObject(bindingStyle)
  }
  if (typeof bindingStyle === 'string') {
    return parseStyleText(bindingStyle)
  }
  return bindingStyle
}

/*  */

var MP_METHODS = ['createSelectorQuery', 'createIntersectionObserver', 'selectAllComponents', 'selectComponent'];

function getTarget(obj, path) {
  var parts = path.split('.');
  var key = parts[0];
  if (key.indexOf('__$n') === 0) { //number index
    key = parseInt(key.replace('__$n', ''));
  }
  if (parts.length === 1) {
    return obj[key]
  }
  return getTarget(obj[key], parts.slice(1).join('.'))
}

function internalMixin(Vue) {

  Vue.config.errorHandler = function(err, vm, info) {
    Vue.util.warn(("Error in " + info + ": \"" + (err.toString()) + "\""), vm);
    console.error(err);
    /* eslint-disable no-undef */
    var app = typeof getApp === 'function' && getApp();
    if (app && app.onError) {
      app.onError(err);
    }
  };

  var oldEmit = Vue.prototype.$emit;

  Vue.prototype.$emit = function(event) {
    if (this.$scope && event) {
      var triggerEvent = this.$scope['_triggerEvent'] || this.$scope['triggerEvent'];
      if (triggerEvent) {
        try {
          triggerEvent.call(this.$scope, event, {
            __args__: toArray(arguments, 1)
          });
        } catch (error) {

        }
      }
    }
    return oldEmit.apply(this, arguments)
  };

  Vue.prototype.$nextTick = function(fn) {
    return nextTick$1(this, fn)
  };

  MP_METHODS.forEach(function (method) {
    Vue.prototype[method] = function(args) {
      if (this.$scope && this.$scope[method]) {
        return this.$scope[method](args)
      }
      // mp-alipay
      if (typeof my === 'undefined') {
        return
      }
      if (method === 'createSelectorQuery') {
        /* eslint-disable no-undef */
        return my.createSelectorQuery(args)
      } else if (method === 'createIntersectionObserver') {
        /* eslint-disable no-undef */
        return my.createIntersectionObserver(args)
      }
      // TODO mp-alipay 暂不支持 selectAllComponents,selectComponent
    };
  });

  Vue.prototype.__init_provide = initProvide;

  Vue.prototype.__init_injections = initInjections;

  Vue.prototype.__call_hook = function(hook, args) {
    var vm = this;
    // #7573 disable dep collection when invoking lifecycle hooks
    pushTarget();
    var handlers = vm.$options[hook];
    var info = hook + " hook";
    var ret;
    if (handlers) {
      for (var i = 0, j = handlers.length; i < j; i++) {
        ret = invokeWithErrorHandling(handlers[i], vm, args ? [args] : null, vm, info);
      }
    }
    if (vm._hasHookEvent) {
      vm.$emit('hook:' + hook, args);
    }
    popTarget();
    return ret
  };

  Vue.prototype.__set_model = function(target, key, value, modifiers) {
    if (Array.isArray(modifiers)) {
      if (modifiers.indexOf('trim') !== -1) {
        value = value.trim();
      }
      if (modifiers.indexOf('number') !== -1) {
        value = this._n(value);
      }
    }
    if (!target) {
      target = this;
    }
    // 解决动态属性添加
    Vue.set(target, key, value);
  };

  Vue.prototype.__set_sync = function(target, key, value) {
    if (!target) {
      target = this;
    }
    // 解决动态属性添加
    Vue.set(target, key, value);
  };

  Vue.prototype.__get_orig = function(item) {
    if (isPlainObject(item)) {
      return item['$orig'] || item
    }
    return item
  };

  Vue.prototype.__get_value = function(dataPath, target) {
    return getTarget(target || this, dataPath)
  };


  Vue.prototype.__get_class = function(dynamicClass, staticClass) {
    return renderClass(staticClass, dynamicClass)
  };

  Vue.prototype.__get_style = function(dynamicStyle, staticStyle) {
    if (!dynamicStyle && !staticStyle) {
      return ''
    }
    var dynamicStyleObj = normalizeStyleBinding(dynamicStyle);
    var styleObj = staticStyle ? extend(staticStyle, dynamicStyleObj) : dynamicStyleObj;
    return Object.keys(styleObj).map(function (name) { return ((hyphenate(name)) + ":" + (styleObj[name])); }).join(';')
  };

  Vue.prototype.__map = function(val, iteratee) {
    //TODO 暂不考虑 string
    var ret, i, l, keys, key;
    if (Array.isArray(val)) {
      ret = new Array(val.length);
      for (i = 0, l = val.length; i < l; i++) {
        ret[i] = iteratee(val[i], i);
      }
      return ret
    } else if (isObject(val)) {
      keys = Object.keys(val);
      ret = Object.create(null);
      for (i = 0, l = keys.length; i < l; i++) {
        key = keys[i];
        ret[key] = iteratee(val[key], key, i);
      }
      return ret
    } else if (typeof val === 'number') {
      ret = new Array(val);
      for (i = 0, l = val; i < l; i++) {
        // 第一个参数暂时仍和小程序一致
        ret[i] = iteratee(i, i);
      }
      return ret
    }
    return []
  };

}

/*  */

var LIFECYCLE_HOOKS$1 = [
    //App
    'onLaunch',
    'onShow',
    'onHide',
    'onUniNViewMessage',
    'onPageNotFound',
    'onThemeChange',
    'onError',
    'onUnhandledRejection',
    //Page
    'onInit',
    'onLoad',
    // 'onShow',
    'onReady',
    // 'onHide',
    'onUnload',
    'onPullDownRefresh',
    'onReachBottom',
    'onTabItemTap',
    'onAddToFavorites',
    'onShareTimeline',
    'onShareAppMessage',
    'onResize',
    'onPageScroll',
    'onNavigationBarButtonTap',
    'onBackPress',
    'onNavigationBarSearchInputChanged',
    'onNavigationBarSearchInputConfirmed',
    'onNavigationBarSearchInputClicked',
    'onUploadDouyinVideo',
    'onNFCReadMessage',
    //Component
    // 'onReady', // 兼容旧版本，应该移除该事件
    'onPageShow',
    'onPageHide',
    'onPageResize'
];
function lifecycleMixin$1(Vue) {

    //fixed vue-class-component
    var oldExtend = Vue.extend;
    Vue.extend = function(extendOptions) {
        extendOptions = extendOptions || {};

        var methods = extendOptions.methods;
        if (methods) {
            Object.keys(methods).forEach(function (methodName) {
                if (LIFECYCLE_HOOKS$1.indexOf(methodName)!==-1) {
                    extendOptions[methodName] = methods[methodName];
                    delete methods[methodName];
                }
            });
        }

        return oldExtend.call(this, extendOptions)
    };

    var strategies = Vue.config.optionMergeStrategies;
    var mergeHook = strategies.created;
    LIFECYCLE_HOOKS$1.forEach(function (hook) {
        strategies[hook] = mergeHook;
    });

    Vue.prototype.__lifecycle_hooks__ = LIFECYCLE_HOOKS$1;
}

/*  */

// install platform patch function
Vue.prototype.__patch__ = patch;

// public mount method
Vue.prototype.$mount = function(
    el ,
    hydrating 
) {
    return mountComponent$1(this, el, hydrating)
};

lifecycleMixin$1(Vue);
internalMixin(Vue);

/*  */

/* harmony default export */ __webpack_exports__["default"] = (Vue);

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../webpack/buildin/global.js */ 3)))

/***/ }),
/* 26 */
/*!*********************************************!*\
  !*** C:/Users/ISEN/Desktop/demo/pages.json ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),
/* 27 */,
/* 28 */,
/* 29 */,
/* 30 */,
/* 31 */,
/* 32 */
/*!**********************************************************************************************************!*\
  !*** ./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib/runtime/componentNormalizer.js ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode, /* vue-cli only */
  components, // fixed by xxxxxx auto components
  renderjs // fixed by xxxxxx renderjs
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // fixed by xxxxxx auto components
  if (components) {
    if (!options.components) {
      options.components = {}
    }
    var hasOwn = Object.prototype.hasOwnProperty
    for (var name in components) {
      if (hasOwn.call(components, name) && !hasOwn.call(options.components, name)) {
        options.components[name] = components[name]
      }
    }
  }
  // fixed by xxxxxx renderjs
  if (renderjs) {
    if(typeof renderjs.beforeCreate === 'function'){
			renderjs.beforeCreate = [renderjs.beforeCreate]
		}
    (renderjs.beforeCreate || (renderjs.beforeCreate = [])).unshift(function() {
      this[renderjs.__module] = this
    });
    (options.mixins || (options.mixins = [])).push(renderjs)
  }

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),
/* 33 */
/*!***********************************************************!*\
  !*** C:/Users/ISEN/Desktop/demo/uni.promisify.adaptor.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(uni) {var _typeof = __webpack_require__(/*! @babel/runtime/helpers/typeof */ 13);
uni.addInterceptor({
  returnValue: function returnValue(res) {
    if (!(!!res && (_typeof(res) === "object" || typeof res === "function") && typeof res.then === "function")) {
      return res;
    }
    return new Promise(function (resolve, reject) {
      res.then(function (res) {
        return res[0] ? reject(res[0]) : resolve(res[1]);
      });
    });
  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js */ 2)["default"]))

/***/ }),
/* 34 */,
/* 35 */,
/* 36 */,
/* 37 */,
/* 38 */,
/* 39 */,
/* 40 */,
/* 41 */,
/* 42 */,
/* 43 */,
/* 44 */,
/* 45 */,
/* 46 */,
/* 47 */,
/* 48 */,
/* 49 */
/*!****************************************************************************!*\
  !*** C:/Users/ISEN/Desktop/demo/components/mpvue-echarts/src/wx-canvas.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ 4);
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _classCallCheck2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ 23));
var _createClass2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/createClass */ 24));
var WxCanvas = /*#__PURE__*/function () {
  function WxCanvas(ctx, canvasId) {
    (0, _classCallCheck2.default)(this, WxCanvas);
    this.ctx = ctx;
    this.canvasId = canvasId;
    this.chart = null;
    WxCanvas.initStyle(ctx);
    this.initEvent();
  }
  (0, _createClass2.default)(WxCanvas, [{
    key: "getContext",
    value: function getContext(contextType) {
      return contextType === '2d' ? this.ctx : null;
    }
  }, {
    key: "setChart",
    value: function setChart(chart) {
      this.chart = chart;
    }
  }, {
    key: "attachEvent",
    value: function attachEvent() {
      // noop
    }
  }, {
    key: "detachEvent",
    value: function detachEvent() {
      // noop
    }
  }, {
    key: "initEvent",
    value: function initEvent() {
      var _this = this;
      this.event = {};
      var eventNames = [{
        wxName: 'touchStart',
        ecName: 'mousedown'
      }, {
        wxName: 'touchMove',
        ecName: 'mousemove'
      }, {
        wxName: 'touchEnd',
        ecName: 'mouseup'
      }, {
        wxName: 'touchEnd',
        ecName: 'click'
      }];
      eventNames.forEach(function (name) {
        _this.event[name.wxName] = function (e) {
          var touch = e.mp.touches[0];
          _this.chart._zr.handler.dispatch(name.ecName, {
            zrX: name.wxName === 'tap' ? touch.clientX : touch.x,
            zrY: name.wxName === 'tap' ? touch.clientY : touch.y
          });
        };
      });
    }
  }], [{
    key: "initStyle",
    value: function initStyle(ctx) {
      var _arguments = arguments;
      var styles = ['fillStyle', 'strokeStyle', 'globalAlpha', 'textAlign', 'textBaseAlign', 'shadow', 'lineWidth', 'lineCap', 'lineJoin', 'lineDash', 'miterLimit', 'fontSize'];
      styles.forEach(function (style) {
        Object.defineProperty(ctx, style, {
          set: function set(value) {
            if (style !== 'fillStyle' && style !== 'strokeStyle' || value !== 'none' && value !== null) {
              ctx["set".concat(style.charAt(0).toUpperCase()).concat(style.slice(1))](value);
            }
          }
        });
      });
      ctx.createRadialGradient = function () {
        return ctx.createCircularGradient(_arguments);
      };
    }
  }]);
  return WxCanvas;
}();
exports.default = WxCanvas;

/***/ }),
/* 50 */
/*!******************************************************************************!*\
  !*** C:/Users/ISEN/Desktop/demo/components/mpvue-echarts/src/echarts.min.js ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(wx) {var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var _typeof = __webpack_require__(/*! @babel/runtime/helpers/typeof */ 13);
!function (t, e) {
  "object" == ( false ? undefined : _typeof(exports)) && "undefined" != typeof module ? e(exports) :  true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [exports], __WEBPACK_AMD_DEFINE_FACTORY__ = (e),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : undefined;
}(this, function (t) {
  "use strict";

  function e(t, e) {
    function n() {
      this.constructor = t;
    }
    _Lp(t, e), t.prototype = null === e ? Object.create(e) : (n.prototype = e.prototype, new n());
  }
  function n() {
    for (var t = 0, e = 0, n = arguments.length; n > e; e++) {
      t += arguments[e].length;
    }
    for (var r = Array(t), i = 0, e = 0; n > e; e++) {
      for (var o = arguments[e], a = 0, s = o.length; s > a; a++, i++) {
        r[i] = o[a];
      }
    }
    return r;
  }
  function r(t, e) {
    var n = e.browser,
      r = t.match(/Firefox\/([\d.]+)/),
      i = t.match(/MSIE\s([\d.]+)/) || t.match(/Trident\/.+?rv:(([\d.]+))/),
      o = t.match(/Edge\/([\d.]+)/),
      a = /micromessenger/i.test(t);
    r && (n.firefox = !0, n.version = r[1]), i && (n.ie = !0, n.version = i[1]), o && (n.edge = !0, n.version = o[1]), a && (n.weChat = !0), e.canvasSupported = !!document.createElement("canvas").getContext, e.svgSupported = "undefined" != typeof SVGRect, e.touchEventsSupported = "ontouchstart" in window && !n.ie && !n.edge, e.pointerEventsSupported = "onpointerdown" in window && (n.edge || n.ie && +n.version >= 11), e.domSupported = "undefined" != typeof document;
  }
  function i(t, e) {
    Xp[t] = e;
  }
  function o() {
    return Yp++;
  }
  function a() {
    for (var t = [], e = 0; e < arguments.length; e++) {
      t[e] = arguments[e];
    }
    "undefined" != typeof console && console.error.apply(console, t);
  }
  function s(t) {
    if (null == t || "object" != _typeof(t)) return t;
    var e = t,
      n = Fp.call(t);
    if ("[object Array]" === n) {
      if (!q(t)) {
        e = [];
        for (var r = 0, i = t.length; i > r; r++) {
          e[r] = s(t[r]);
        }
      }
    } else if (zp[n]) {
      if (!q(t)) {
        var o = t.constructor;
        if (o.from) e = o.from(t);else {
          e = new o(t.length);
          for (var r = 0, i = t.length; i > r; r++) {
            e[r] = s(t[r]);
          }
        }
      }
    } else if (!Bp[n] && !q(t) && !L(t)) {
      e = {};
      for (var a in t) {
        t.hasOwnProperty(a) && (e[a] = s(t[a]));
      }
    }
    return e;
  }
  function l(t, e, n) {
    if (!I(e) || !I(t)) return n ? s(e) : t;
    for (var r in e) {
      if (e.hasOwnProperty(r)) {
        var i = t[r],
          o = e[r];
        !I(o) || !I(i) || M(o) || M(i) || L(o) || L(i) || A(o) || A(i) || q(o) || q(i) ? !n && r in t || (t[r] = s(e[r])) : l(i, o, n);
      }
    }
    return t;
  }
  function u(t, e) {
    for (var n = t[0], r = 1, i = t.length; i > r; r++) {
      n = l(n, t[r], e);
    }
    return n;
  }
  function h(t, e) {
    if (Object.assign) Object.assign(t, e);else for (var n in e) {
      e.hasOwnProperty(n) && (t[n] = e[n]);
    }
    return t;
  }
  function c(t, e, n) {
    for (var r = w(e), i = 0; i < r.length; i++) {
      var o = r[i];
      (n ? null != e[o] : null == t[o]) && (t[o] = e[o]);
    }
    return t;
  }
  function f(t, e) {
    if (t) {
      if (t.indexOf) return t.indexOf(e);
      for (var n = 0, r = t.length; r > n; n++) {
        if (t[n] === e) return n;
      }
    }
    return -1;
  }
  function p(t, e) {
    function n() {}
    var r = t.prototype;
    n.prototype = e.prototype, t.prototype = new n();
    for (var i in r) {
      r.hasOwnProperty(i) && (t.prototype[i] = r[i]);
    }
    t.prototype.constructor = t, t.superClass = e;
  }
  function d(t, e, n) {
    if (t = "prototype" in t ? t.prototype : t, e = "prototype" in e ? e.prototype : e, Object.getOwnPropertyNames) for (var r = Object.getOwnPropertyNames(e), i = 0; i < r.length; i++) {
      var o = r[i];
      "constructor" !== o && (n ? null != e[o] : null == t[o]) && (t[o] = e[o]);
    } else c(t, e, n);
  }
  function g(t) {
    return t ? "string" == typeof t ? !1 : "number" == typeof t.length : !1;
  }
  function v(t, e, n) {
    if (t && e) if (t.forEach && t.forEach === Hp) t.forEach(e, n);else if (t.length === +t.length) for (var r = 0, i = t.length; i > r; r++) {
      e.call(n, t[r], r, t);
    } else for (var o in t) {
      t.hasOwnProperty(o) && e.call(n, t[o], o, t);
    }
  }
  function y(t, e, n) {
    if (!t) return [];
    if (!e) return H(t);
    if (t.map && t.map === Gp) return t.map(e, n);
    for (var r = [], i = 0, o = t.length; o > i; i++) {
      r.push(e.call(n, t[i], i, t));
    }
    return r;
  }
  function m(t, e, n, r) {
    if (t && e) {
      for (var i = 0, o = t.length; o > i; i++) {
        n = e.call(r, n, t[i], i, t);
      }
      return n;
    }
  }
  function _(t, e, n) {
    if (!t) return [];
    if (!e) return H(t);
    if (t.filter && t.filter === Vp) return t.filter(e, n);
    for (var r = [], i = 0, o = t.length; o > i; i++) {
      e.call(n, t[i], i, t) && r.push(t[i]);
    }
    return r;
  }
  function x(t, e, n) {
    if (t && e) for (var r = 0, i = t.length; i > r; r++) {
      if (e.call(n, t[r], r, t)) return t[r];
    }
  }
  function w(t) {
    if (!t) return [];
    if (Object.keys) return Object.keys(t);
    var e = [];
    for (var n in t) {
      t.hasOwnProperty(n) && e.push(n);
    }
    return e;
  }
  function b(t, e) {
    for (var n = [], r = 2; r < arguments.length; r++) {
      n[r - 2] = arguments[r];
    }
    return function () {
      return t.apply(e, n.concat(Wp.call(arguments)));
    };
  }
  function S(t) {
    for (var e = [], n = 1; n < arguments.length; n++) {
      e[n - 1] = arguments[n];
    }
    return function () {
      return t.apply(this, e.concat(Wp.call(arguments)));
    };
  }
  function M(t) {
    return Array.isArray ? Array.isArray(t) : "[object Array]" === Fp.call(t);
  }
  function T(t) {
    return "function" == typeof t;
  }
  function C(t) {
    return "string" == typeof t;
  }
  function k(t) {
    return "[object String]" === Fp.call(t);
  }
  function D(t) {
    return "number" == typeof t;
  }
  function I(t) {
    var e = _typeof(t);
    return "function" === e || !!t && "object" === e;
  }
  function A(t) {
    return !!Bp[Fp.call(t)];
  }
  function P(t) {
    return !!zp[Fp.call(t)];
  }
  function L(t) {
    return "object" == _typeof(t) && "number" == typeof t.nodeType && "object" == _typeof(t.ownerDocument);
  }
  function O(t) {
    return null != t.colorStops;
  }
  function R(t) {
    return null != t.image;
  }
  function E(t) {
    return "[object RegExp]" === Fp.call(t);
  }
  function B(t) {
    return t !== t;
  }
  function z() {
    for (var t = [], e = 0; e < arguments.length; e++) {
      t[e] = arguments[e];
    }
    for (var n = 0, r = t.length; r > n; n++) {
      if (null != t[n]) return t[n];
    }
  }
  function F(t, e) {
    return null != t ? t : e;
  }
  function N(t, e, n) {
    return null != t ? t : null != e ? e : n;
  }
  function H(t) {
    for (var e = [], n = 1; n < arguments.length; n++) {
      e[n - 1] = arguments[n];
    }
    return Wp.apply(t, e);
  }
  function V(t) {
    if ("number" == typeof t) return [t, t, t, t];
    var e = t.length;
    return 2 === e ? [t[0], t[1], t[0], t[1]] : 3 === e ? [t[0], t[1], t[2], t[1]] : t;
  }
  function W(t, e) {
    if (!t) throw new Error(e);
  }
  function G(t) {
    return null == t ? null : "function" == typeof t.trim ? t.trim() : t.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "");
  }
  function U(t) {
    t[Kp] = !0;
  }
  function q(t) {
    return t[Kp];
  }
  function X(t) {
    return new $p(t);
  }
  function Y(t, e) {
    for (var n = new t.constructor(t.length + e.length), r = 0; r < t.length; r++) {
      n[r] = t[r];
    }
    for (var i = t.length, r = 0; r < e.length; r++) {
      n[r + i] = e[r];
    }
    return n;
  }
  function j(t, e) {
    var n;
    if (Object.create) n = Object.create(t);else {
      var r = function r() {};
      r.prototype = t, n = new r();
    }
    return e && h(n, e), n;
  }
  function Z(t, e) {
    return t.hasOwnProperty(e);
  }
  function K() {}
  function $(t, e) {
    return null == t && (t = 0), null == e && (e = 0), [t, e];
  }
  function Q(t, e) {
    return t[0] = e[0], t[1] = e[1], t;
  }
  function J(t) {
    return [t[0], t[1]];
  }
  function te(t, e, n) {
    return t[0] = e, t[1] = n, t;
  }
  function ee(t, e, n) {
    return t[0] = e[0] + n[0], t[1] = e[1] + n[1], t;
  }
  function ne(t, e, n, r) {
    return t[0] = e[0] + n[0] * r, t[1] = e[1] + n[1] * r, t;
  }
  function re(t, e, n) {
    return t[0] = e[0] - n[0], t[1] = e[1] - n[1], t;
  }
  function ie(t) {
    return Math.sqrt(oe(t));
  }
  function oe(t) {
    return t[0] * t[0] + t[1] * t[1];
  }
  function ae(t, e, n) {
    return t[0] = e[0] * n[0], t[1] = e[1] * n[1], t;
  }
  function se(t, e, n) {
    return t[0] = e[0] / n[0], t[1] = e[1] / n[1], t;
  }
  function le(t, e) {
    return t[0] * e[0] + t[1] * e[1];
  }
  function ue(t, e, n) {
    return t[0] = e[0] * n, t[1] = e[1] * n, t;
  }
  function he(t, e) {
    var n = ie(e);
    return 0 === n ? (t[0] = 0, t[1] = 0) : (t[0] = e[0] / n, t[1] = e[1] / n), t;
  }
  function ce(t, e) {
    return Math.sqrt((t[0] - e[0]) * (t[0] - e[0]) + (t[1] - e[1]) * (t[1] - e[1]));
  }
  function fe(t, e) {
    return (t[0] - e[0]) * (t[0] - e[0]) + (t[1] - e[1]) * (t[1] - e[1]);
  }
  function pe(t, e) {
    return t[0] = -e[0], t[1] = -e[1], t;
  }
  function de(t, e, n, r) {
    return t[0] = e[0] + r * (n[0] - e[0]), t[1] = e[1] + r * (n[1] - e[1]), t;
  }
  function ge(t, e, n) {
    var r = e[0],
      i = e[1];
    return t[0] = n[0] * r + n[2] * i + n[4], t[1] = n[1] * r + n[3] * i + n[5], t;
  }
  function ve(t, e, n) {
    return t[0] = Math.min(e[0], n[0]), t[1] = Math.min(e[1], n[1]), t;
  }
  function ye(t, e, n) {
    return t[0] = Math.max(e[0], n[0]), t[1] = Math.max(e[1], n[1]), t;
  }
  function me(t, e, n, r, i, o) {
    var a = r + "-" + i,
      s = t.length;
    if (o.hasOwnProperty(a)) return o[a];
    if (1 === e) {
      var l = Math.round(Math.log((1 << s) - 1 & ~i) / sd);
      return t[n][l];
    }
    for (var u = r | 1 << n, h = n + 1; r & 1 << h;) {
      h++;
    }
    for (var c = 0, f = 0, p = 0; s > f; f++) {
      var d = 1 << f;
      d & i || (c += (p % 2 ? -1 : 1) * t[n][f] * me(t, e - 1, h, u, i | d, o), p++);
    }
    return o[a] = c, c;
  }
  function _e(t, e) {
    var n = [[t[0], t[1], 1, 0, 0, 0, -e[0] * t[0], -e[0] * t[1]], [0, 0, 0, t[0], t[1], 1, -e[1] * t[0], -e[1] * t[1]], [t[2], t[3], 1, 0, 0, 0, -e[2] * t[2], -e[2] * t[3]], [0, 0, 0, t[2], t[3], 1, -e[3] * t[2], -e[3] * t[3]], [t[4], t[5], 1, 0, 0, 0, -e[4] * t[4], -e[4] * t[5]], [0, 0, 0, t[4], t[5], 1, -e[5] * t[4], -e[5] * t[5]], [t[6], t[7], 1, 0, 0, 0, -e[6] * t[6], -e[6] * t[7]], [0, 0, 0, t[6], t[7], 1, -e[7] * t[6], -e[7] * t[7]]],
      r = {},
      i = me(n, 8, 0, 0, 0, r);
    if (0 !== i) {
      for (var o = [], a = 0; 8 > a; a++) {
        for (var s = 0; 8 > s; s++) {
          null == o[s] && (o[s] = 0), o[s] += ((a + s) % 2 ? -1 : 1) * me(n, 7, 0 === a ? 1 : 0, 1 << a, 1 << s, r) / i * e[a];
        }
      }
      return function (t, e, n) {
        var r = e * o[6] + n * o[7] + 1;
        t[0] = (e * o[0] + n * o[1] + o[2]) / r, t[1] = (e * o[3] + n * o[4] + o[5]) / r;
      };
    }
  }
  function xe(t, e, n, r, i) {
    if (e.getBoundingClientRect && Ep.domSupported && !Se(e)) {
      var o = e[ld] || (e[ld] = {}),
        a = we(e, o),
        s = be(a, o, i);
      if (s) return s(t, n, r), !0;
    }
    return !1;
  }
  function we(t, e) {
    var n = e.markers;
    if (n) return n;
    n = e.markers = [];
    for (var r = ["left", "right"], i = ["top", "bottom"], o = 0; 4 > o; o++) {
      var a = document.createElement("div"),
        s = a.style,
        l = o % 2,
        u = (o >> 1) % 2;
      s.cssText = ["position: absolute", "visibility: hidden", "padding: 0", "margin: 0", "border-width: 0", "user-select: none", "width:0", "height:0", r[l] + ":0", i[u] + ":0", r[1 - l] + ":auto", i[1 - u] + ":auto", ""].join("!important;"), t.appendChild(a), n.push(a);
    }
    return n;
  }
  function be(t, e, n) {
    for (var r = n ? "invTrans" : "trans", i = e[r], o = e.srcCoords, a = [], s = [], l = !0, u = 0; 4 > u; u++) {
      var h = t[u].getBoundingClientRect(),
        c = 2 * u,
        f = h.left,
        p = h.top;
      a.push(f, p), l = l && o && f === o[c] && p === o[c + 1], s.push(t[u].offsetLeft, t[u].offsetTop);
    }
    return l && i ? i : (e.srcCoords = a, e[r] = n ? _e(s, a) : _e(a, s));
  }
  function Se(t) {
    return "CANVAS" === t.nodeName.toUpperCase();
  }
  function Me(t, e, n, r) {
    return n = n || {}, r || !Ep.canvasSupported ? Te(t, e, n) : Ep.browser.firefox && null != e.layerX && e.layerX !== e.offsetX ? (n.zrX = e.layerX, n.zrY = e.layerY) : null != e.offsetX ? (n.zrX = e.offsetX, n.zrY = e.offsetY) : Te(t, e, n), n;
  }
  function Te(t, e, n) {
    if (Ep.domSupported && t.getBoundingClientRect) {
      var r = e.clientX,
        i = e.clientY;
      if (Se(t)) {
        var o = t.getBoundingClientRect();
        return n.zrX = r - o.left, void (n.zrY = i - o.top);
      }
      if (xe(cd, t, r, i)) return n.zrX = cd[0], void (n.zrY = cd[1]);
    }
    n.zrX = n.zrY = 0;
  }
  function Ce(t) {
    return t || window.event;
  }
  function ke(t, e, n) {
    if (e = Ce(e), null != e.zrX) return e;
    var r = e.type,
      i = r && r.indexOf("touch") >= 0;
    if (i) {
      var o = "touchend" !== r ? e.targetTouches[0] : e.changedTouches[0];
      o && Me(t, o, e, n);
    } else {
      Me(t, e, e, n);
      var a = De(e);
      e.zrDelta = a ? a / 120 : -(e.detail || 0) / 3;
    }
    var s = e.button;
    return null == e.which && void 0 !== s && hd.test(e.type) && (e.which = 1 & s ? 1 : 2 & s ? 3 : 4 & s ? 2 : 0), e;
  }
  function De(t) {
    var e = t.wheelDelta;
    if (e) return e;
    var n = t.deltaX,
      r = t.deltaY;
    if (null == n || null == r) return e;
    var i = Math.abs(0 !== r ? r : n),
      o = r > 0 ? -1 : 0 > r ? 1 : n > 0 ? -1 : 1;
    return 3 * i * o;
  }
  function Ie(t, e, n, r) {
    ud ? t.addEventListener(e, n, r) : t.attachEvent("on" + e, n);
  }
  function Ae(t, e, n, r) {
    ud ? t.removeEventListener(e, n, r) : t.detachEvent("on" + e, n);
  }
  function Pe(t) {
    var e = t[1][0] - t[0][0],
      n = t[1][1] - t[0][1];
    return Math.sqrt(e * e + n * n);
  }
  function Le(t) {
    return [(t[0][0] + t[1][0]) / 2, (t[0][1] + t[1][1]) / 2];
  }
  function Oe(t, e, n) {
    return {
      type: t,
      event: n,
      target: e.target,
      topTarget: e.topTarget,
      cancelBubble: !1,
      offsetX: n.zrX,
      offsetY: n.zrY,
      gestureEvent: n.gestureEvent,
      pinchX: n.pinchX,
      pinchY: n.pinchY,
      pinchScale: n.pinchScale,
      wheelDelta: n.zrDelta,
      zrByTouch: n.zrByTouch,
      which: n.which,
      stop: Re
    };
  }
  function Re() {
    fd(this.event);
  }
  function Ee(t, e, n) {
    if (t[t.rectHover ? "rectContain" : "contain"](e, n)) {
      for (var r = t, i = void 0, o = !1; r;) {
        if (r.ignoreClip && (o = !0), !o) {
          var a = r.getClipPath();
          if (a && !a.contain(e, n)) return !1;
          r.silent && (i = !0);
        }
        var s = r.__hostTarget;
        r = s ? s : r.parent;
      }
      return i ? gd : !0;
    }
    return !1;
  }
  function Be(t, e, n) {
    var r = t.painter;
    return 0 > e || e > r.getWidth() || 0 > n || n > r.getHeight();
  }
  function ze() {
    return [1, 0, 0, 1, 0, 0];
  }
  function Fe(t) {
    return t[0] = 1, t[1] = 0, t[2] = 0, t[3] = 1, t[4] = 0, t[5] = 0, t;
  }
  function Ne(t, e) {
    return t[0] = e[0], t[1] = e[1], t[2] = e[2], t[3] = e[3], t[4] = e[4], t[5] = e[5], t;
  }
  function He(t, e, n) {
    var r = e[0] * n[0] + e[2] * n[1],
      i = e[1] * n[0] + e[3] * n[1],
      o = e[0] * n[2] + e[2] * n[3],
      a = e[1] * n[2] + e[3] * n[3],
      s = e[0] * n[4] + e[2] * n[5] + e[4],
      l = e[1] * n[4] + e[3] * n[5] + e[5];
    return t[0] = r, t[1] = i, t[2] = o, t[3] = a, t[4] = s, t[5] = l, t;
  }
  function Ve(t, e, n) {
    return t[0] = e[0], t[1] = e[1], t[2] = e[2], t[3] = e[3], t[4] = e[4] + n[0], t[5] = e[5] + n[1], t;
  }
  function We(t, e, n) {
    var r = e[0],
      i = e[2],
      o = e[4],
      a = e[1],
      s = e[3],
      l = e[5],
      u = Math.sin(n),
      h = Math.cos(n);
    return t[0] = r * h + a * u, t[1] = -r * u + a * h, t[2] = i * h + s * u, t[3] = -i * u + h * s, t[4] = h * o + u * l, t[5] = h * l - u * o, t;
  }
  function Ge(t, e, n) {
    var r = n[0],
      i = n[1];
    return t[0] = e[0] * r, t[1] = e[1] * i, t[2] = e[2] * r, t[3] = e[3] * i, t[4] = e[4] * r, t[5] = e[5] * i, t;
  }
  function Ue(t, e) {
    var n = e[0],
      r = e[2],
      i = e[4],
      o = e[1],
      a = e[3],
      s = e[5],
      l = n * a - o * r;
    return l ? (l = 1 / l, t[0] = a * l, t[1] = -o * l, t[2] = -r * l, t[3] = n * l, t[4] = (r * s - a * i) * l, t[5] = (o * i - n * s) * l, t) : null;
  }
  function qe(t) {
    var e = ze();
    return Ne(e, t), e;
  }
  function Xe(t) {
    return t > Md || -Md > t;
  }
  function Ye(t) {
    return t = Math.round(t), 0 > t ? 0 : t > 255 ? 255 : t;
  }
  function je(t) {
    return t = Math.round(t), 0 > t ? 0 : t > 360 ? 360 : t;
  }
  function Ze(t) {
    return 0 > t ? 0 : t > 1 ? 1 : t;
  }
  function Ke(t) {
    var e = t;
    return Ye(e.length && "%" === e.charAt(e.length - 1) ? parseFloat(e) / 100 * 255 : parseInt(e, 10));
  }
  function $e(t) {
    var e = t;
    return Ze(e.length && "%" === e.charAt(e.length - 1) ? parseFloat(e) / 100 : parseFloat(e));
  }
  function Qe(t, e, n) {
    return 0 > n ? n += 1 : n > 1 && (n -= 1), 1 > 6 * n ? t + (e - t) * n * 6 : 1 > 2 * n ? e : 2 > 3 * n ? t + (e - t) * (2 / 3 - n) * 6 : t;
  }
  function Je(t, e, n) {
    return t + (e - t) * n;
  }
  function tn(t, e, n, r, i) {
    return t[0] = e, t[1] = n, t[2] = r, t[3] = i, t;
  }
  function en(t, e) {
    return t[0] = e[0], t[1] = e[1], t[2] = e[2], t[3] = e[3], t;
  }
  function nn(t, e) {
    zd && en(zd, e), zd = Bd.put(t, zd || e.slice());
  }
  function rn(t, e) {
    if (t) {
      e = e || [];
      var n = Bd.get(t);
      if (n) return en(e, n);
      t += "";
      var r = t.replace(/ /g, "").toLowerCase();
      if (r in Ed) return en(e, Ed[r]), nn(t, e), e;
      var i = r.length;
      if ("#" !== r.charAt(0)) {
        var o = r.indexOf("("),
          a = r.indexOf(")");
        if (-1 !== o && a + 1 === i) {
          var s = r.substr(0, o),
            l = r.substr(o + 1, a - (o + 1)).split(","),
            u = 1;
          switch (s) {
            case "rgba":
              if (4 !== l.length) return 3 === l.length ? tn(e, +l[0], +l[1], +l[2], 1) : tn(e, 0, 0, 0, 1);
              u = $e(l.pop());
            case "rgb":
              return 3 !== l.length ? void tn(e, 0, 0, 0, 1) : (tn(e, Ke(l[0]), Ke(l[1]), Ke(l[2]), u), nn(t, e), e);
            case "hsla":
              return 4 !== l.length ? void tn(e, 0, 0, 0, 1) : (l[3] = $e(l[3]), on(l, e), nn(t, e), e);
            case "hsl":
              return 3 !== l.length ? void tn(e, 0, 0, 0, 1) : (on(l, e), nn(t, e), e);
            default:
              return;
          }
        }
        tn(e, 0, 0, 0, 1);
      } else {
        if (4 === i || 5 === i) {
          var h = parseInt(r.slice(1, 4), 16);
          return h >= 0 && 4095 >= h ? (tn(e, (3840 & h) >> 4 | (3840 & h) >> 8, 240 & h | (240 & h) >> 4, 15 & h | (15 & h) << 4, 5 === i ? parseInt(r.slice(4), 16) / 15 : 1), nn(t, e), e) : void tn(e, 0, 0, 0, 1);
        }
        if (7 === i || 9 === i) {
          var h = parseInt(r.slice(1, 7), 16);
          return h >= 0 && 16777215 >= h ? (tn(e, (16711680 & h) >> 16, (65280 & h) >> 8, 255 & h, 9 === i ? parseInt(r.slice(7), 16) / 255 : 1), nn(t, e), e) : void tn(e, 0, 0, 0, 1);
        }
      }
    }
  }
  function on(t, e) {
    var n = (parseFloat(t[0]) % 360 + 360) % 360 / 360,
      r = $e(t[1]),
      i = $e(t[2]),
      o = .5 >= i ? i * (r + 1) : i + r - i * r,
      a = 2 * i - o;
    return e = e || [], tn(e, Ye(255 * Qe(a, o, n + 1 / 3)), Ye(255 * Qe(a, o, n)), Ye(255 * Qe(a, o, n - 1 / 3)), 1), 4 === t.length && (e[3] = t[3]), e;
  }
  function an(t) {
    if (t) {
      var e,
        n,
        r = t[0] / 255,
        i = t[1] / 255,
        o = t[2] / 255,
        a = Math.min(r, i, o),
        s = Math.max(r, i, o),
        l = s - a,
        u = (s + a) / 2;
      if (0 === l) e = 0, n = 0;else {
        n = .5 > u ? l / (s + a) : l / (2 - s - a);
        var h = ((s - r) / 6 + l / 2) / l,
          c = ((s - i) / 6 + l / 2) / l,
          f = ((s - o) / 6 + l / 2) / l;
        r === s ? e = f - c : i === s ? e = 1 / 3 + h - f : o === s && (e = 2 / 3 + c - h), 0 > e && (e += 1), e > 1 && (e -= 1);
      }
      var p = [360 * e, n, u];
      return null != t[3] && p.push(t[3]), p;
    }
  }
  function sn(t, e) {
    var n = rn(t);
    if (n) {
      for (var r = 0; 3 > r; r++) {
        n[r] = 0 > e ? n[r] * (1 - e) | 0 : (255 - n[r]) * e + n[r] | 0, n[r] > 255 ? n[r] = 255 : n[r] < 0 && (n[r] = 0);
      }
      return pn(n, 4 === n.length ? "rgba" : "rgb");
    }
  }
  function ln(t) {
    var e = rn(t);
    return e ? ((1 << 24) + (e[0] << 16) + (e[1] << 8) + +e[2]).toString(16).slice(1) : void 0;
  }
  function un(t, e, n) {
    if (e && e.length && t >= 0 && 1 >= t) {
      n = n || [];
      var r = t * (e.length - 1),
        i = Math.floor(r),
        o = Math.ceil(r),
        a = e[i],
        s = e[o],
        l = r - i;
      return n[0] = Ye(Je(a[0], s[0], l)), n[1] = Ye(Je(a[1], s[1], l)), n[2] = Ye(Je(a[2], s[2], l)), n[3] = Ze(Je(a[3], s[3], l)), n;
    }
  }
  function hn(t, e, n) {
    if (e && e.length && t >= 0 && 1 >= t) {
      var r = t * (e.length - 1),
        i = Math.floor(r),
        o = Math.ceil(r),
        a = rn(e[i]),
        s = rn(e[o]),
        l = r - i,
        u = pn([Ye(Je(a[0], s[0], l)), Ye(Je(a[1], s[1], l)), Ye(Je(a[2], s[2], l)), Ze(Je(a[3], s[3], l))], "rgba");
      return n ? {
        color: u,
        leftIndex: i,
        rightIndex: o,
        value: r
      } : u;
    }
  }
  function cn(t, e, n, r) {
    var i = rn(t);
    return t ? (i = an(i), null != e && (i[0] = je(e)), null != n && (i[1] = $e(n)), null != r && (i[2] = $e(r)), pn(on(i), "rgba")) : void 0;
  }
  function fn(t, e) {
    var n = rn(t);
    return n && null != e ? (n[3] = Ze(e), pn(n, "rgba")) : void 0;
  }
  function pn(t, e) {
    if (t && t.length) {
      var n = t[0] + "," + t[1] + "," + t[2];
      return ("rgba" === e || "hsva" === e || "hsla" === e) && (n += "," + t[3]), e + "(" + n + ")";
    }
  }
  function dn(t, e) {
    var n = rn(t);
    return n ? (.299 * n[0] + .587 * n[1] + .114 * n[2]) * n[3] / 255 + (1 - n[3]) * e : 0;
  }
  function gn() {
    var t = Math.round(255 * Math.random()),
      e = Math.round(255 * Math.random()),
      n = Math.round(255 * Math.random());
    return "rgb(" + t + "," + e + "," + n + ")";
  }
  function vn(t, e, n) {
    return (e - t) * n + t;
  }
  function yn(t, e, n) {
    return n > .5 ? e : t;
  }
  function mn(t, e, n, r) {
    for (var i = e.length, o = 0; i > o; o++) {
      t[o] = vn(e[o], n[o], r);
    }
  }
  function _n(t, e, n, r) {
    for (var i = e.length, o = i && e[0].length, a = 0; i > a; a++) {
      t[a] || (t[a] = []);
      for (var s = 0; o > s; s++) {
        t[a][s] = vn(e[a][s], n[a][s], r);
      }
    }
  }
  function xn(t, e, n, r) {
    for (var i = e.length, o = 0; i > o; o++) {
      t[o] = e[o] + n[o] * r;
    }
    return t;
  }
  function wn(t, e, n, r) {
    for (var i = e.length, o = i && e[0].length, a = 0; i > a; a++) {
      t[a] || (t[a] = []);
      for (var s = 0; o > s; s++) {
        t[a][s] = e[a][s] + n[a][s] * r;
      }
    }
    return t;
  }
  function bn(t, e, n) {
    var r = t,
      i = e;
    if (r.push && i.push) {
      var o = r.length,
        a = i.length;
      if (o !== a) {
        var s = o > a;
        if (s) r.length = a;else for (var l = o; a > l; l++) {
          r.push(1 === n ? i[l] : Vd.call(i[l]));
        }
      }
      for (var u = r[0] && r[0].length, l = 0; l < r.length; l++) {
        if (1 === n) isNaN(r[l]) && (r[l] = i[l]);else for (var h = 0; u > h; h++) {
          isNaN(r[l][h]) && (r[l][h] = i[l][h]);
        }
      }
    }
  }
  function Sn(t, e) {
    var n = t.length;
    if (n !== e.length) return !1;
    for (var r = 0; n > r; r++) {
      if (t[r] !== e[r]) return !1;
    }
    return !0;
  }
  function Mn(t, e, n, r, i, o, a) {
    var s = .5 * (n - t),
      l = .5 * (r - e);
    return (2 * (e - n) + s + l) * a + (-3 * (e - n) - 2 * s - l) * o + s * i + e;
  }
  function Tn(t, e, n, r, i, o, a, s) {
    for (var l = e.length, u = 0; l > u; u++) {
      t[u] = Mn(e[u], n[u], r[u], i[u], o, a, s);
    }
  }
  function Cn(t, e, n, r, i, o, a, s) {
    for (var l = e.length, u = e[0].length, h = 0; l > h; h++) {
      t[h] || (t[1] = []);
      for (var c = 0; u > c; c++) {
        t[h][c] = Mn(e[h][c], n[h][c], r[h][c], i[h][c], o, a, s);
      }
    }
  }
  function kn(t) {
    if (g(t)) {
      var e = t.length;
      if (g(t[0])) {
        for (var n = [], r = 0; e > r; r++) {
          n.push(Vd.call(t[r]));
        }
        return n;
      }
      return Vd.call(t);
    }
    return t;
  }
  function Dn(t) {
    return t[0] = Math.floor(t[0]), t[1] = Math.floor(t[1]), t[2] = Math.floor(t[2]), "rgba(" + t.join(",") + ")";
  }
  function In(t) {
    return g(t && t[0]) ? 2 : 1;
  }
  function An(t, e) {
    return xd || (xd = jp().getContext("2d")), wd !== e && (wd = xd.font = e || ng), xd.measureText(t);
  }
  function Pn(t, e) {
    e = e || ng;
    var n = eg[e];
    n || (n = eg[e] = new Rd(500));
    var r = n.get(t);
    return null == r && (r = rg.measureText(t, e).width, n.put(t, r)), r;
  }
  function Ln(t, e, n, r) {
    var i = Pn(t, e),
      o = Bn(e),
      a = Rn(0, i, n),
      s = En(0, o, r),
      l = new tg(a, s, i, o);
    return l;
  }
  function On(t, e, n, r) {
    var i = ((t || "") + "").split("\n"),
      o = i.length;
    if (1 === o) return Ln(i[0], e, n, r);
    for (var a = new tg(0, 0, 0, 0), s = 0; s < i.length; s++) {
      var l = Ln(i[s], e, n, r);
      0 === s ? a.copy(l) : a.union(l);
    }
    return a;
  }
  function Rn(t, e, n) {
    return "right" === n ? t -= e : "center" === n && (t -= e / 2), t;
  }
  function En(t, e, n) {
    return "middle" === n ? t -= e / 2 : "bottom" === n && (t -= e), t;
  }
  function Bn(t) {
    return Pn("国", t);
  }
  function zn(t, e) {
    return "string" == typeof t ? t.lastIndexOf("%") >= 0 ? parseFloat(t) / 100 * e : parseFloat(t) : t;
  }
  function Fn(t, e, n) {
    var r = e.position || "inside",
      i = null != e.distance ? e.distance : 5,
      o = n.height,
      a = n.width,
      s = o / 2,
      l = n.x,
      u = n.y,
      h = "left",
      c = "top";
    if (r instanceof Array) l += zn(r[0], n.width), u += zn(r[1], n.height), h = null, c = null;else switch (r) {
      case "left":
        l -= i, u += s, h = "right", c = "middle";
        break;
      case "right":
        l += i + a, u += s, c = "middle";
        break;
      case "top":
        l += a / 2, u -= i, h = "center", c = "bottom";
        break;
      case "bottom":
        l += a / 2, u += o + i, h = "center";
        break;
      case "inside":
        l += a / 2, u += s, h = "center", c = "middle";
        break;
      case "insideLeft":
        l += i, u += s, c = "middle";
        break;
      case "insideRight":
        l += a - i, u += s, h = "right", c = "middle";
        break;
      case "insideTop":
        l += a / 2, u += i, h = "center";
        break;
      case "insideBottom":
        l += a / 2, u += o - i, h = "center", c = "bottom";
        break;
      case "insideTopLeft":
        l += i, u += i;
        break;
      case "insideTopRight":
        l += a - i, u += i, h = "right";
        break;
      case "insideBottomLeft":
        l += i, u += o - i, c = "bottom";
        break;
      case "insideBottomRight":
        l += a - i, u += o - i, h = "right", c = "bottom";
    }
    return t = t || {}, t.x = l, t.y = u, t.align = h, t.verticalAlign = c, t;
  }
  function Nn(t, e, n, r, i) {
    n = n || {};
    var o = [];
    Gn(t, "", t, e, n, r, o, i);
    var a = o.length,
      s = !1,
      l = n.done,
      u = n.aborted,
      h = function h() {
        s = !0, a--, 0 >= a && (s ? l && l() : u && u());
      },
      c = function c() {
        a--, 0 >= a && (s ? l && l() : u && u());
      };
    a || l && l(), o.length > 0 && n.during && o[0].during(function (t, e) {
      n.during(e);
    });
    for (var f = 0; f < o.length; f++) {
      var p = o[f];
      h && p.done(h), c && p.aborted(c), p.start(n.easing, n.force);
    }
    return o;
  }
  function Hn(t, e, n) {
    for (var r = 0; n > r; r++) {
      t[r] = e[r];
    }
  }
  function Vn(t) {
    return g(t[0]);
  }
  function Wn(t, e, n) {
    if (g(e[n])) {
      if (g(t[n]) || (t[n] = []), P(e[n])) {
        var r = e[n].length;
        t[n].length !== r && (t[n] = new e[n].constructor(r), Hn(t[n], e[n], r));
      } else {
        var i = e[n],
          o = t[n],
          a = i.length;
        if (Vn(i)) for (var s = i[0].length, l = 0; a > l; l++) {
          o[l] ? Hn(o[l], i[l], s) : o[l] = Array.prototype.slice.call(i[l]);
        } else Hn(o, i, a);
        o.length = i.length;
      }
    } else t[n] = e[n];
  }
  function Gn(t, e, n, r, i, o, a, s) {
    for (var l = [], u = [], h = w(r), c = i.duration, p = i.delay, d = i.additive, v = i.setToFinal, y = !I(o), m = 0; m < h.length; m++) {
      var _ = h[m];
      if (null != n[_] && null != r[_] && (y || o[_])) {
        if (I(r[_]) && !g(r[_])) {
          if (e) {
            s || (n[_] = r[_], t.updateDuringAnimation(e));
            continue;
          }
          Gn(t, _, n[_], r[_], i, o && o[_], a, s);
        } else l.push(_), u.push(_);
      } else s || (n[_] = r[_], t.updateDuringAnimation(e), u.push(_));
    }
    var x = l.length;
    if (x > 0 || i.force && !a.length) {
      for (var b = t.animators, S = [], M = 0; M < b.length; M++) {
        b[M].targetName === e && S.push(b[M]);
      }
      if (!d && S.length) for (var M = 0; M < S.length; M++) {
        var T = S[M].stopTracks(u);
        if (T) {
          var C = f(b, S[M]);
          b.splice(C, 1);
        }
      }
      var k = void 0,
        D = void 0,
        A = void 0;
      if (s) {
        D = {}, v && (k = {});
        for (var M = 0; x > M; M++) {
          var _ = l[M];
          D[_] = n[_], v ? k[_] = r[_] : n[_] = r[_];
        }
      } else if (v) {
        A = {};
        for (var M = 0; x > M; M++) {
          var _ = l[M];
          A[_] = kn(n[_]), Wn(n, r, _);
        }
      }
      var P = new Ud(n, !1, d ? S : null);
      P.targetName = e, i.scope && (P.scope = i.scope), v && k && P.whenWithKeys(0, k, l), A && P.whenWithKeys(0, A, l), P.whenWithKeys(null == c ? 500 : c, s ? D : r, l).delay(p || 0), t.addAnimator(P, e), a.push(P);
    }
  }
  function Un(t) {
    for (var e = 0; t >= yg;) {
      e |= 1 & t, t >>= 1;
    }
    return t + e;
  }
  function qn(t, e, n, r) {
    var i = e + 1;
    if (i === n) return 1;
    if (r(t[i++], t[e]) < 0) {
      for (; n > i && r(t[i], t[i - 1]) < 0;) {
        i++;
      }
      Xn(t, e, i);
    } else for (; n > i && r(t[i], t[i - 1]) >= 0;) {
      i++;
    }
    return i - e;
  }
  function Xn(t, e, n) {
    for (n--; n > e;) {
      var r = t[e];
      t[e++] = t[n], t[n--] = r;
    }
  }
  function Yn(t, e, n, r, i) {
    for (r === e && r++; n > r; r++) {
      for (var o, a = t[r], s = e, l = r; l > s;) {
        o = s + l >>> 1, i(a, t[o]) < 0 ? l = o : s = o + 1;
      }
      var u = r - s;
      switch (u) {
        case 3:
          t[s + 3] = t[s + 2];
        case 2:
          t[s + 2] = t[s + 1];
        case 1:
          t[s + 1] = t[s];
          break;
        default:
          for (; u > 0;) {
            t[s + u] = t[s + u - 1], u--;
          }
      }
      t[s] = a;
    }
  }
  function jn(t, e, n, r, i, o) {
    var a = 0,
      s = 0,
      l = 1;
    if (o(t, e[n + i]) > 0) {
      for (s = r - i; s > l && o(t, e[n + i + l]) > 0;) {
        a = l, l = (l << 1) + 1, 0 >= l && (l = s);
      }
      l > s && (l = s), a += i, l += i;
    } else {
      for (s = i + 1; s > l && o(t, e[n + i - l]) <= 0;) {
        a = l, l = (l << 1) + 1, 0 >= l && (l = s);
      }
      l > s && (l = s);
      var u = a;
      a = i - l, l = i - u;
    }
    for (a++; l > a;) {
      var h = a + (l - a >>> 1);
      o(t, e[n + h]) > 0 ? a = h + 1 : l = h;
    }
    return l;
  }
  function Zn(t, e, n, r, i, o) {
    var a = 0,
      s = 0,
      l = 1;
    if (o(t, e[n + i]) < 0) {
      for (s = i + 1; s > l && o(t, e[n + i - l]) < 0;) {
        a = l, l = (l << 1) + 1, 0 >= l && (l = s);
      }
      l > s && (l = s);
      var u = a;
      a = i - l, l = i - u;
    } else {
      for (s = r - i; s > l && o(t, e[n + i + l]) >= 0;) {
        a = l, l = (l << 1) + 1, 0 >= l && (l = s);
      }
      l > s && (l = s), a += i, l += i;
    }
    for (a++; l > a;) {
      var h = a + (l - a >>> 1);
      o(t, e[n + h]) < 0 ? l = h : a = h + 1;
    }
    return l;
  }
  function Kn(t, e) {
    function n(t, e) {
      l[c] = t, u[c] = e, c += 1;
    }
    function r() {
      for (; c > 1;) {
        var t = c - 2;
        if (t >= 1 && u[t - 1] <= u[t] + u[t + 1] || t >= 2 && u[t - 2] <= u[t] + u[t - 1]) u[t - 1] < u[t + 1] && t--;else if (u[t] > u[t + 1]) break;
        o(t);
      }
    }
    function i() {
      for (; c > 1;) {
        var t = c - 2;
        t > 0 && u[t - 1] < u[t + 1] && t--, o(t);
      }
    }
    function o(n) {
      var r = l[n],
        i = u[n],
        o = l[n + 1],
        h = u[n + 1];
      u[n] = i + h, n === c - 3 && (l[n + 1] = l[n + 2], u[n + 1] = u[n + 2]), c--;
      var f = Zn(t[o], t, r, i, 0, e);
      r += f, i -= f, 0 !== i && (h = jn(t[r + i - 1], t, o, h, h - 1, e), 0 !== h && (h >= i ? a(r, i, o, h) : s(r, i, o, h)));
    }
    function a(n, r, i, o) {
      var a = 0;
      for (a = 0; r > a; a++) {
        f[a] = t[n + a];
      }
      var s = 0,
        l = i,
        u = n;
      if (t[u++] = t[l++], 0 !== --o) {
        if (1 === r) {
          for (a = 0; o > a; a++) {
            t[u + a] = t[l + a];
          }
          return void (t[u + o] = f[s]);
        }
        for (var c, p, d, g = h;;) {
          c = 0, p = 0, d = !1;
          do {
            if (e(t[l], f[s]) < 0) {
              if (t[u++] = t[l++], p++, c = 0, 0 === --o) {
                d = !0;
                break;
              }
            } else if (t[u++] = f[s++], c++, p = 0, 1 === --r) {
              d = !0;
              break;
            }
          } while (g > (c | p));
          if (d) break;
          do {
            if (c = Zn(t[l], f, s, r, 0, e), 0 !== c) {
              for (a = 0; c > a; a++) {
                t[u + a] = f[s + a];
              }
              if (u += c, s += c, r -= c, 1 >= r) {
                d = !0;
                break;
              }
            }
            if (t[u++] = t[l++], 0 === --o) {
              d = !0;
              break;
            }
            if (p = jn(f[s], t, l, o, 0, e), 0 !== p) {
              for (a = 0; p > a; a++) {
                t[u + a] = t[l + a];
              }
              if (u += p, l += p, o -= p, 0 === o) {
                d = !0;
                break;
              }
            }
            if (t[u++] = f[s++], 1 === --r) {
              d = !0;
              break;
            }
            g--;
          } while (c >= mg || p >= mg);
          if (d) break;
          0 > g && (g = 0), g += 2;
        }
        if (h = g, 1 > h && (h = 1), 1 === r) {
          for (a = 0; o > a; a++) {
            t[u + a] = t[l + a];
          }
          t[u + o] = f[s];
        } else {
          if (0 === r) throw new Error();
          for (a = 0; r > a; a++) {
            t[u + a] = f[s + a];
          }
        }
      } else for (a = 0; r > a; a++) {
        t[u + a] = f[s + a];
      }
    }
    function s(n, r, i, o) {
      var a = 0;
      for (a = 0; o > a; a++) {
        f[a] = t[i + a];
      }
      var s = n + r - 1,
        l = o - 1,
        u = i + o - 1,
        c = 0,
        p = 0;
      if (t[u--] = t[s--], 0 !== --r) {
        if (1 === o) {
          for (u -= r, s -= r, p = u + 1, c = s + 1, a = r - 1; a >= 0; a--) {
            t[p + a] = t[c + a];
          }
          return void (t[u] = f[l]);
        }
        for (var d = h;;) {
          var g = 0,
            v = 0,
            y = !1;
          do {
            if (e(f[l], t[s]) < 0) {
              if (t[u--] = t[s--], g++, v = 0, 0 === --r) {
                y = !0;
                break;
              }
            } else if (t[u--] = f[l--], v++, g = 0, 1 === --o) {
              y = !0;
              break;
            }
          } while (d > (g | v));
          if (y) break;
          do {
            if (g = r - Zn(f[l], t, n, r, r - 1, e), 0 !== g) {
              for (u -= g, s -= g, r -= g, p = u + 1, c = s + 1, a = g - 1; a >= 0; a--) {
                t[p + a] = t[c + a];
              }
              if (0 === r) {
                y = !0;
                break;
              }
            }
            if (t[u--] = f[l--], 1 === --o) {
              y = !0;
              break;
            }
            if (v = o - jn(t[s], f, 0, o, o - 1, e), 0 !== v) {
              for (u -= v, l -= v, o -= v, p = u + 1, c = l + 1, a = 0; v > a; a++) {
                t[p + a] = f[c + a];
              }
              if (1 >= o) {
                y = !0;
                break;
              }
            }
            if (t[u--] = t[s--], 0 === --r) {
              y = !0;
              break;
            }
            d--;
          } while (g >= mg || v >= mg);
          if (y) break;
          0 > d && (d = 0), d += 2;
        }
        if (h = d, 1 > h && (h = 1), 1 === o) {
          for (u -= r, s -= r, p = u + 1, c = s + 1, a = r - 1; a >= 0; a--) {
            t[p + a] = t[c + a];
          }
          t[u] = f[l];
        } else {
          if (0 === o) throw new Error();
          for (c = u - (o - 1), a = 0; o > a; a++) {
            t[c + a] = f[a];
          }
        }
      } else for (c = u - (o - 1), a = 0; o > a; a++) {
        t[c + a] = f[a];
      }
    }
    var l,
      u,
      h = mg,
      c = 0,
      f = [];
    return l = [], u = [], {
      mergeRuns: r,
      forceMergeRuns: i,
      pushRun: n
    };
  }
  function $n(t, e, n, r) {
    n || (n = 0), r || (r = t.length);
    var i = r - n;
    if (!(2 > i)) {
      var o = 0;
      if (yg > i) return o = qn(t, n, r, e), void Yn(t, n, r, n + o, e);
      var a = Kn(t, e),
        s = Un(i);
      do {
        if (o = qn(t, n, r, e), s > o) {
          var l = i;
          l > s && (l = s), Yn(t, n, n + l, n + o, e), o = l;
        }
        a.pushRun(n, o), a.mergeRuns(), i -= o, n += o;
      } while (0 !== i);
      a.forceMergeRuns();
    }
  }
  function Qn() {
    _g || (_g = !0, console.warn("z / z2 / zlevel of displayable is invalid, which may cause unexpected errors"));
  }
  function Jn(t, e) {
    return t.zlevel === e.zlevel ? t.z === e.z ? t.z2 - e.z2 : t.z - e.z : t.zlevel - e.zlevel;
  }
  function tr(t) {
    var e = t.pointerType;
    return "pen" === e || "touch" === e;
  }
  function er(t) {
    t.touching = !0, null != t.touchTimer && (clearTimeout(t.touchTimer), t.touchTimer = null), t.touchTimer = setTimeout(function () {
      t.touching = !1, t.touchTimer = null;
    }, 700);
  }
  function nr(t) {
    t && (t.zrByTouch = !0);
  }
  function rr(t, e) {
    return ke(t.dom, new Dg(t, e), !0);
  }
  function ir(t, e) {
    for (var n = e, r = !1; n && 9 !== n.nodeType && !(r = n.domBelongToZr || n !== e && n === t.painterRoot);) {
      n = n.parentNode;
    }
    return r;
  }
  function or(t, e) {
    var n = e.domHandlers;
    Ep.pointerEventsSupported ? v(Tg.pointer, function (r) {
      sr(e, r, function (e) {
        n[r].call(t, e);
      });
    }) : (Ep.touchEventsSupported && v(Tg.touch, function (r) {
      sr(e, r, function (i) {
        n[r].call(t, i), er(e);
      });
    }), v(Tg.mouse, function (r) {
      sr(e, r, function (i) {
        i = Ce(i), e.touching || n[r].call(t, i);
      });
    }));
  }
  function ar(t, e) {
    function n(n) {
      function r(r) {
        r = Ce(r), ir(t, r.target) || (r = rr(t, r), e.domHandlers[n].call(t, r));
      }
      sr(e, n, r, {
        capture: !0
      });
    }
    Ep.pointerEventsSupported ? v(Cg.pointer, n) : Ep.touchEventsSupported || v(Cg.mouse, n);
  }
  function sr(t, e, n, r) {
    t.mounted[e] = n, t.listenerOpts[e] = r, Ie(t.domTarget, e, n, r);
  }
  function lr(t) {
    var e = t.mounted;
    for (var n in e) {
      e.hasOwnProperty(n) && Ae(t.domTarget, n, e[n], t.listenerOpts[n]);
    }
    t.mounted = {};
  }
  function ur(t, e, n) {
    return Fg.copy(t.getBoundingRect()), t.transform && Fg.applyTransform(t.transform), Ng.width = e, Ng.height = n, !Fg.intersect(Ng);
  }
  function hr(t) {
    return t > -Wg && Wg > t;
  }
  function cr(t) {
    return t > Wg || -Wg > t;
  }
  function fr(t, e, n, r, i) {
    var o = 1 - i;
    return o * o * (o * t + 3 * i * e) + i * i * (i * r + 3 * o * n);
  }
  function pr(t, e, n, r, i) {
    var o = 1 - i;
    return 3 * (((e - t) * o + 2 * (n - e) * i) * o + (r - n) * i * i);
  }
  function dr(t, e, n, r, i, o) {
    var a = r + 3 * (e - n) - t,
      s = 3 * (n - 2 * e + t),
      l = 3 * (e - t),
      u = t - i,
      h = s * s - 3 * a * l,
      c = s * l - 9 * a * u,
      f = l * l - 3 * s * u,
      p = 0;
    if (hr(h) && hr(c)) {
      if (hr(s)) o[0] = 0;else {
        var d = -l / s;
        d >= 0 && 1 >= d && (o[p++] = d);
      }
    } else {
      var g = c * c - 4 * h * f;
      if (hr(g)) {
        var v = c / h,
          d = -s / a + v,
          y = -v / 2;
        d >= 0 && 1 >= d && (o[p++] = d), y >= 0 && 1 >= y && (o[p++] = y);
      } else if (g > 0) {
        var m = Vg(g),
          _ = h * s + 1.5 * a * (-c + m),
          x = h * s + 1.5 * a * (-c - m);
        _ = 0 > _ ? -Hg(-_, qg) : Hg(_, qg), x = 0 > x ? -Hg(-x, qg) : Hg(x, qg);
        var d = (-s - (_ + x)) / (3 * a);
        d >= 0 && 1 >= d && (o[p++] = d);
      } else {
        var w = (2 * h * s - 3 * a * c) / (2 * Vg(h * h * h)),
          b = Math.acos(w) / 3,
          S = Vg(h),
          M = Math.cos(b),
          d = (-s - 2 * S * M) / (3 * a),
          y = (-s + S * (M + Ug * Math.sin(b))) / (3 * a),
          T = (-s + S * (M - Ug * Math.sin(b))) / (3 * a);
        d >= 0 && 1 >= d && (o[p++] = d), y >= 0 && 1 >= y && (o[p++] = y), T >= 0 && 1 >= T && (o[p++] = T);
      }
    }
    return p;
  }
  function gr(t, e, n, r, i) {
    var o = 6 * n - 12 * e + 6 * t,
      a = 9 * e + 3 * r - 3 * t - 9 * n,
      s = 3 * e - 3 * t,
      l = 0;
    if (hr(a)) {
      if (cr(o)) {
        var u = -s / o;
        u >= 0 && 1 >= u && (i[l++] = u);
      }
    } else {
      var h = o * o - 4 * a * s;
      if (hr(h)) i[0] = -o / (2 * a);else if (h > 0) {
        var c = Vg(h),
          u = (-o + c) / (2 * a),
          f = (-o - c) / (2 * a);
        u >= 0 && 1 >= u && (i[l++] = u), f >= 0 && 1 >= f && (i[l++] = f);
      }
    }
    return l;
  }
  function vr(t, e, n, r, i, o) {
    var a = (e - t) * i + t,
      s = (n - e) * i + e,
      l = (r - n) * i + n,
      u = (s - a) * i + a,
      h = (l - s) * i + s,
      c = (h - u) * i + u;
    o[0] = t, o[1] = a, o[2] = u, o[3] = c, o[4] = c, o[5] = h, o[6] = l, o[7] = r;
  }
  function yr(t, e, n, r, i, o, a, s, l, u, h) {
    var c,
      f,
      p,
      d,
      g,
      v = .005,
      y = 1 / 0;
    Xg[0] = l, Xg[1] = u;
    for (var m = 0; 1 > m; m += .05) {
      Yg[0] = fr(t, n, i, a, m), Yg[1] = fr(e, r, o, s, m), d = nd(Xg, Yg), y > d && (c = m, y = d);
    }
    y = 1 / 0;
    for (var _ = 0; 32 > _ && !(Gg > v); _++) {
      f = c - v, p = c + v, Yg[0] = fr(t, n, i, a, f), Yg[1] = fr(e, r, o, s, f), d = nd(Yg, Xg), f >= 0 && y > d ? (c = f, y = d) : (jg[0] = fr(t, n, i, a, p), jg[1] = fr(e, r, o, s, p), g = nd(jg, Xg), 1 >= p && y > g ? (c = p, y = g) : v *= .5);
    }
    return h && (h[0] = fr(t, n, i, a, c), h[1] = fr(e, r, o, s, c)), Vg(y);
  }
  function mr(t, e, n, r, i, o, a, s, l) {
    for (var u = t, h = e, c = 0, f = 1 / l, p = 1; l >= p; p++) {
      var d = p * f,
        g = fr(t, n, i, a, d),
        v = fr(e, r, o, s, d),
        y = g - u,
        m = v - h;
      c += Math.sqrt(y * y + m * m), u = g, h = v;
    }
    return c;
  }
  function _r(t, e, n, r) {
    var i = 1 - r;
    return i * (i * t + 2 * r * e) + r * r * n;
  }
  function xr(t, e, n, r) {
    return 2 * ((1 - r) * (e - t) + r * (n - e));
  }
  function wr(t, e, n, r, i) {
    var o = t - 2 * e + n,
      a = 2 * (e - t),
      s = t - r,
      l = 0;
    if (hr(o)) {
      if (cr(a)) {
        var u = -s / a;
        u >= 0 && 1 >= u && (i[l++] = u);
      }
    } else {
      var h = a * a - 4 * o * s;
      if (hr(h)) {
        var u = -a / (2 * o);
        u >= 0 && 1 >= u && (i[l++] = u);
      } else if (h > 0) {
        var c = Vg(h),
          u = (-a + c) / (2 * o),
          f = (-a - c) / (2 * o);
        u >= 0 && 1 >= u && (i[l++] = u), f >= 0 && 1 >= f && (i[l++] = f);
      }
    }
    return l;
  }
  function br(t, e, n) {
    var r = t + n - 2 * e;
    return 0 === r ? .5 : (t - e) / r;
  }
  function Sr(t, e, n, r, i) {
    var o = (e - t) * r + t,
      a = (n - e) * r + e,
      s = (a - o) * r + o;
    i[0] = t, i[1] = o, i[2] = s, i[3] = s, i[4] = a, i[5] = n;
  }
  function Mr(t, e, n, r, i, o, a, s, l) {
    var u,
      h = .005,
      c = 1 / 0;
    Xg[0] = a, Xg[1] = s;
    for (var f = 0; 1 > f; f += .05) {
      Yg[0] = _r(t, n, i, f), Yg[1] = _r(e, r, o, f);
      var p = nd(Xg, Yg);
      c > p && (u = f, c = p);
    }
    c = 1 / 0;
    for (var d = 0; 32 > d && !(Gg > h); d++) {
      var g = u - h,
        v = u + h;
      Yg[0] = _r(t, n, i, g), Yg[1] = _r(e, r, o, g);
      var p = nd(Yg, Xg);
      if (g >= 0 && c > p) u = g, c = p;else {
        jg[0] = _r(t, n, i, v), jg[1] = _r(e, r, o, v);
        var y = nd(jg, Xg);
        1 >= v && c > y ? (u = v, c = y) : h *= .5;
      }
    }
    return l && (l[0] = _r(t, n, i, u), l[1] = _r(e, r, o, u)), Vg(c);
  }
  function Tr(t, e, n, r, i, o, a) {
    for (var s = t, l = e, u = 0, h = 1 / a, c = 1; a >= c; c++) {
      var f = c * h,
        p = _r(t, n, i, f),
        d = _r(e, r, o, f),
        g = p - s,
        v = d - l;
      u += Math.sqrt(g * g + v * v), s = p, l = d;
    }
    return u;
  }
  function Cr(t, e, n) {
    if (0 !== t.length) {
      for (var r = t[0], i = r[0], o = r[0], a = r[1], s = r[1], l = 1; l < t.length; l++) {
        r = t[l], i = Zg(i, r[0]), o = Kg(o, r[0]), a = Zg(a, r[1]), s = Kg(s, r[1]);
      }
      e[0] = i, e[1] = a, n[0] = o, n[1] = s;
    }
  }
  function kr(t, e, n, r, i, o) {
    i[0] = Zg(t, n), i[1] = Zg(e, r), o[0] = Kg(t, n), o[1] = Kg(e, r);
  }
  function Dr(t, e, n, r, i, o, a, s, l, u) {
    var h = gr,
      c = fr,
      f = h(t, n, i, a, rv);
    l[0] = 1 / 0, l[1] = 1 / 0, u[0] = -1 / 0, u[1] = -1 / 0;
    for (var p = 0; f > p; p++) {
      var d = c(t, n, i, a, rv[p]);
      l[0] = Zg(d, l[0]), u[0] = Kg(d, u[0]);
    }
    f = h(e, r, o, s, iv);
    for (var p = 0; f > p; p++) {
      var g = c(e, r, o, s, iv[p]);
      l[1] = Zg(g, l[1]), u[1] = Kg(g, u[1]);
    }
    l[0] = Zg(t, l[0]), u[0] = Kg(t, u[0]), l[0] = Zg(a, l[0]), u[0] = Kg(a, u[0]), l[1] = Zg(e, l[1]), u[1] = Kg(e, u[1]), l[1] = Zg(s, l[1]), u[1] = Kg(s, u[1]);
  }
  function Ir(t, e, n, r, i, o, a, s) {
    var l = br,
      u = _r,
      h = Kg(Zg(l(t, n, i), 1), 0),
      c = Kg(Zg(l(e, r, o), 1), 0),
      f = u(t, n, i, h),
      p = u(e, r, o, c);
    a[0] = Zg(t, i, f), a[1] = Zg(e, o, p), s[0] = Kg(t, i, f), s[1] = Kg(e, o, p);
  }
  function Ar(t, e, n, r, i, o, a, s, l) {
    var u = ve,
      h = ye,
      c = Math.abs(i - o);
    if (1e-4 > c % Jg && c > 1e-4) return s[0] = t - n, s[1] = e - r, l[0] = t + n, void (l[1] = e + r);
    if (tv[0] = Qg(i) * n + t, tv[1] = $g(i) * r + e, ev[0] = Qg(o) * n + t, ev[1] = $g(o) * r + e, u(s, tv, ev), h(l, tv, ev), i %= Jg, 0 > i && (i += Jg), o %= Jg, 0 > o && (o += Jg), i > o && !a ? o += Jg : o > i && a && (i += Jg), a) {
      var f = o;
      o = i, i = f;
    }
    for (var p = 0; o > p; p += Math.PI / 2) {
      p > i && (nv[0] = Qg(p) * n + t, nv[1] = $g(p) * r + e, u(s, nv, s), h(l, nv, l));
    }
  }
  function Pr(t) {
    var e = Math.round(t / mv * 1e8) / 1e8;
    return e % 2 * mv;
  }
  function Lr(t, e) {
    var n = Pr(t[0]);
    0 > n && (n += _v);
    var r = n - t[0],
      i = t[1];
    i += r, !e && i - n >= _v ? i = n + _v : e && n - i >= _v ? i = n - _v : !e && n > i ? i = n + (_v - Pr(n - i)) : e && i > n && (i = n - (_v - Pr(i - n))), t[0] = n, t[1] = i;
  }
  function Or(t, e, n, r, i, o, a) {
    if (0 === i) return !1;
    var s = i,
      l = 0,
      u = t;
    if (a > e + s && a > r + s || e - s > a && r - s > a || o > t + s && o > n + s || t - s > o && n - s > o) return !1;
    if (t === n) return Math.abs(o - t) <= s / 2;
    l = (e - r) / (t - n), u = (t * r - n * e) / (t - n);
    var h = l * o - a + u,
      c = h * h / (l * l + 1);
    return s / 2 * s / 2 >= c;
  }
  function Rr(t, e, n, r, i, o, a, s, l, u, h) {
    if (0 === l) return !1;
    var c = l;
    if (h > e + c && h > r + c && h > o + c && h > s + c || e - c > h && r - c > h && o - c > h && s - c > h || u > t + c && u > n + c && u > i + c && u > a + c || t - c > u && n - c > u && i - c > u && a - c > u) return !1;
    var f = yr(t, e, n, r, i, o, a, s, u, h, null);
    return c / 2 >= f;
  }
  function Er(t, e, n, r, i, o, a, s, l) {
    if (0 === a) return !1;
    var u = a;
    if (l > e + u && l > r + u && l > o + u || e - u > l && r - u > l && o - u > l || s > t + u && s > n + u && s > i + u || t - u > s && n - u > s && i - u > s) return !1;
    var h = Mr(t, e, n, r, i, o, s, l, null);
    return u / 2 >= h;
  }
  function Br(t) {
    return t %= Sv, 0 > t && (t += Sv), t;
  }
  function zr(t, e, n, r, i, o, a, s, l) {
    if (0 === a) return !1;
    var u = a;
    s -= t, l -= e;
    var h = Math.sqrt(s * s + l * l);
    if (h - u > n || n > h + u) return !1;
    if (Math.abs(r - i) % Mv < 1e-4) return !0;
    if (o) {
      var c = r;
      r = Br(i), i = Br(c);
    } else r = Br(r), i = Br(i);
    r > i && (i += Mv);
    var f = Math.atan2(l, s);
    return 0 > f && (f += Mv), f >= r && i >= f || f + Mv >= r && i >= f + Mv;
  }
  function Fr(t, e, n, r, i, o) {
    if (o > e && o > r || e > o && r > o) return 0;
    if (r === e) return 0;
    var a = (o - e) / (r - e),
      s = e > r ? 1 : -1;
    (1 === a || 0 === a) && (s = e > r ? .5 : -.5);
    var l = a * (n - t) + t;
    return l === i ? 1 / 0 : l > i ? s : 0;
  }
  function Nr(t, e) {
    return Math.abs(t - e) < kv;
  }
  function Hr() {
    var t = Iv[0];
    Iv[0] = Iv[1], Iv[1] = t;
  }
  function Vr(t, e, n, r, i, o, a, s, l, u) {
    if (u > e && u > r && u > o && u > s || e > u && r > u && o > u && s > u) return 0;
    var h = dr(e, r, o, s, u, Dv);
    if (0 === h) return 0;
    for (var c = 0, f = -1, p = void 0, d = void 0, g = 0; h > g; g++) {
      var v = Dv[g],
        y = 0 === v || 1 === v ? .5 : 1,
        m = fr(t, n, i, a, v);
      l > m || (0 > f && (f = gr(e, r, o, s, Iv), Iv[1] < Iv[0] && f > 1 && Hr(), p = fr(e, r, o, s, Iv[0]), f > 1 && (d = fr(e, r, o, s, Iv[1]))), c += 2 === f ? v < Iv[0] ? e > p ? y : -y : v < Iv[1] ? p > d ? y : -y : d > s ? y : -y : v < Iv[0] ? e > p ? y : -y : p > s ? y : -y);
    }
    return c;
  }
  function Wr(t, e, n, r, i, o, a, s) {
    if (s > e && s > r && s > o || e > s && r > s && o > s) return 0;
    var l = wr(e, r, o, s, Dv);
    if (0 === l) return 0;
    var u = br(e, r, o);
    if (u >= 0 && 1 >= u) {
      for (var h = 0, c = _r(e, r, o, u), f = 0; l > f; f++) {
        var p = 0 === Dv[f] || 1 === Dv[f] ? .5 : 1,
          d = _r(t, n, i, Dv[f]);
        a > d || (h += Dv[f] < u ? e > c ? p : -p : c > o ? p : -p);
      }
      return h;
    }
    var p = 0 === Dv[0] || 1 === Dv[0] ? .5 : 1,
      d = _r(t, n, i, Dv[0]);
    return a > d ? 0 : e > o ? p : -p;
  }
  function Gr(t, e, n, r, i, o, a, s) {
    if (s -= e, s > n || -n > s) return 0;
    var l = Math.sqrt(n * n - s * s);
    Dv[0] = -l, Dv[1] = l;
    var u = Math.abs(r - i);
    if (1e-4 > u) return 0;
    if (u >= Cv - 1e-4) {
      r = 0, i = Cv;
      var h = o ? 1 : -1;
      return a >= Dv[0] + t && a <= Dv[1] + t ? h : 0;
    }
    if (r > i) {
      var c = r;
      r = i, i = c;
    }
    0 > r && (r += Cv, i += Cv);
    for (var f = 0, p = 0; 2 > p; p++) {
      var d = Dv[p];
      if (d + t > a) {
        var g = Math.atan2(s, d),
          h = o ? 1 : -1;
        0 > g && (g = Cv + g), (g >= r && i >= g || g + Cv >= r && i >= g + Cv) && (g > Math.PI / 2 && g < 1.5 * Math.PI && (h = -h), f += h);
      }
    }
    return f;
  }
  function Ur(t, e, n, r, i) {
    for (var o, a, s = t.data, l = t.len(), u = 0, h = 0, c = 0, f = 0, p = 0, d = 0; l > d;) {
      var g = s[d++],
        v = 1 === d;
      switch (g === Tv.M && d > 1 && (n || (u += Fr(h, c, f, p, r, i))), v && (h = s[d], c = s[d + 1], f = h, p = c), g) {
        case Tv.M:
          f = s[d++], p = s[d++], h = f, c = p;
          break;
        case Tv.L:
          if (n) {
            if (Or(h, c, s[d], s[d + 1], e, r, i)) return !0;
          } else u += Fr(h, c, s[d], s[d + 1], r, i) || 0;
          h = s[d++], c = s[d++];
          break;
        case Tv.C:
          if (n) {
            if (Rr(h, c, s[d++], s[d++], s[d++], s[d++], s[d], s[d + 1], e, r, i)) return !0;
          } else u += Vr(h, c, s[d++], s[d++], s[d++], s[d++], s[d], s[d + 1], r, i) || 0;
          h = s[d++], c = s[d++];
          break;
        case Tv.Q:
          if (n) {
            if (Er(h, c, s[d++], s[d++], s[d], s[d + 1], e, r, i)) return !0;
          } else u += Wr(h, c, s[d++], s[d++], s[d], s[d + 1], r, i) || 0;
          h = s[d++], c = s[d++];
          break;
        case Tv.A:
          var y = s[d++],
            m = s[d++],
            _ = s[d++],
            x = s[d++],
            w = s[d++],
            b = s[d++];
          d += 1;
          var S = !!(1 - s[d++]);
          o = Math.cos(w) * _ + y, a = Math.sin(w) * x + m, v ? (f = o, p = a) : u += Fr(h, c, o, a, r, i);
          var M = (r - y) * x / _ + y;
          if (n) {
            if (zr(y, m, x, w, w + b, S, e, M, i)) return !0;
          } else u += Gr(y, m, x, w, w + b, S, M, i);
          h = Math.cos(w + b) * _ + y, c = Math.sin(w + b) * x + m;
          break;
        case Tv.R:
          f = h = s[d++], p = c = s[d++];
          var T = s[d++],
            C = s[d++];
          if (o = f + T, a = p + C, n) {
            if (Or(f, p, o, p, e, r, i) || Or(o, p, o, a, e, r, i) || Or(o, a, f, a, e, r, i) || Or(f, a, f, p, e, r, i)) return !0;
          } else u += Fr(o, p, o, a, r, i), u += Fr(f, a, f, p, r, i);
          break;
        case Tv.Z:
          if (n) {
            if (Or(h, c, f, p, e, r, i)) return !0;
          } else u += Fr(h, c, f, p, r, i);
          h = f, c = p;
      }
    }
    return n || Nr(c, p) || (u += Fr(h, c, f, p, r, i) || 0), 0 !== u;
  }
  function qr(t, e, n) {
    return Ur(t, 0, !1, e, n);
  }
  function Xr(t, e, n, r) {
    return Ur(t, e, !0, n, r);
  }
  function Yr(t, e) {
    var n,
      r,
      i,
      o,
      a,
      s,
      l = t.data,
      u = t.len(),
      h = Rv.M,
      c = Rv.C,
      f = Rv.L,
      p = Rv.R,
      d = Rv.A,
      g = Rv.Q;
    for (i = 0, o = 0; u > i;) {
      switch (n = l[i++], o = i, r = 0, n) {
        case h:
          r = 1;
          break;
        case f:
          r = 1;
          break;
        case c:
          r = 3;
          break;
        case g:
          r = 2;
          break;
        case d:
          var v = e[4],
            y = e[5],
            m = Bv(e[0] * e[0] + e[1] * e[1]),
            _ = Bv(e[2] * e[2] + e[3] * e[3]),
            x = zv(-e[1] / _, e[0] / m);
          l[i] *= m, l[i++] += v, l[i] *= _, l[i++] += y, l[i++] *= m, l[i++] *= _, l[i++] += x, l[i++] += x, i += 2, o = i;
          break;
        case p:
          s[0] = l[i++], s[1] = l[i++], ge(s, s, e), l[o++] = s[0], l[o++] = s[1], s[0] += l[i++], s[1] += l[i++], ge(s, s, e), l[o++] = s[0], l[o++] = s[1];
      }
      for (a = 0; r > a; a++) {
        var w = Ev[a];
        w[0] = l[i++], w[1] = l[i++], ge(w, w, e), l[o++] = w[0], l[o++] = w[1];
      }
    }
    t.increaseVersion();
  }
  function jr(t) {
    return Math.sqrt(t[0] * t[0] + t[1] * t[1]);
  }
  function Zr(t, e) {
    return (t[0] * e[0] + t[1] * e[1]) / (jr(t) * jr(e));
  }
  function Kr(t, e) {
    return (t[0] * e[1] < t[1] * e[0] ? -1 : 1) * Math.acos(Zr(t, e));
  }
  function $r(t, e, n, r, i, o, a, s, l, u, h) {
    var c = l * (Vv / 180),
      f = Hv(c) * (t - n) / 2 + Nv(c) * (e - r) / 2,
      p = -1 * Nv(c) * (t - n) / 2 + Hv(c) * (e - r) / 2,
      d = f * f / (a * a) + p * p / (s * s);
    d > 1 && (a *= Fv(d), s *= Fv(d));
    var g = (i === o ? -1 : 1) * Fv((a * a * s * s - a * a * p * p - s * s * f * f) / (a * a * p * p + s * s * f * f)) || 0,
      v = g * a * p / s,
      y = g * -s * f / a,
      m = (t + n) / 2 + Hv(c) * v - Nv(c) * y,
      _ = (e + r) / 2 + Nv(c) * v + Hv(c) * y,
      x = Kr([1, 0], [(f - v) / a, (p - y) / s]),
      w = [(f - v) / a, (p - y) / s],
      b = [(-1 * f - v) / a, (-1 * p - y) / s],
      S = Kr(w, b);
    if (Zr(w, b) <= -1 && (S = Vv), Zr(w, b) >= 1 && (S = 0), 0 > S) {
      var M = Math.round(S / Vv * 1e6) / 1e6;
      S = 2 * Vv + M % 2 * Vv;
    }
    h.addData(u, m, _, a, s, x, S, c, o);
  }
  function Qr(t) {
    if (!t) return new bv();
    for (var e, n = 0, r = 0, i = n, o = r, a = new bv(), s = bv.CMD, l = t.match(Wv), u = 0; u < l.length; u++) {
      for (var h = l[u], c = h.charAt(0), f = void 0, p = h.match(Gv) || [], d = p.length, g = 0; d > g; g++) {
        p[g] = parseFloat(p[g]);
      }
      for (var v = 0; d > v;) {
        var y = void 0,
          m = void 0,
          _ = void 0,
          x = void 0,
          w = void 0,
          b = void 0,
          S = void 0,
          M = n,
          T = r,
          C = void 0,
          k = void 0;
        switch (c) {
          case "l":
            n += p[v++], r += p[v++], f = s.L, a.addData(f, n, r);
            break;
          case "L":
            n = p[v++], r = p[v++], f = s.L, a.addData(f, n, r);
            break;
          case "m":
            n += p[v++], r += p[v++], f = s.M, a.addData(f, n, r), i = n, o = r, c = "l";
            break;
          case "M":
            n = p[v++], r = p[v++], f = s.M, a.addData(f, n, r), i = n, o = r, c = "L";
            break;
          case "h":
            n += p[v++], f = s.L, a.addData(f, n, r);
            break;
          case "H":
            n = p[v++], f = s.L, a.addData(f, n, r);
            break;
          case "v":
            r += p[v++], f = s.L, a.addData(f, n, r);
            break;
          case "V":
            r = p[v++], f = s.L, a.addData(f, n, r);
            break;
          case "C":
            f = s.C, a.addData(f, p[v++], p[v++], p[v++], p[v++], p[v++], p[v++]), n = p[v - 2], r = p[v - 1];
            break;
          case "c":
            f = s.C, a.addData(f, p[v++] + n, p[v++] + r, p[v++] + n, p[v++] + r, p[v++] + n, p[v++] + r), n += p[v - 2], r += p[v - 1];
            break;
          case "S":
            y = n, m = r, C = a.len(), k = a.data, e === s.C && (y += n - k[C - 4], m += r - k[C - 3]), f = s.C, M = p[v++], T = p[v++], n = p[v++], r = p[v++], a.addData(f, y, m, M, T, n, r);
            break;
          case "s":
            y = n, m = r, C = a.len(), k = a.data, e === s.C && (y += n - k[C - 4], m += r - k[C - 3]), f = s.C, M = n + p[v++], T = r + p[v++], n += p[v++], r += p[v++], a.addData(f, y, m, M, T, n, r);
            break;
          case "Q":
            M = p[v++], T = p[v++], n = p[v++], r = p[v++], f = s.Q, a.addData(f, M, T, n, r);
            break;
          case "q":
            M = p[v++] + n, T = p[v++] + r, n += p[v++], r += p[v++], f = s.Q, a.addData(f, M, T, n, r);
            break;
          case "T":
            y = n, m = r, C = a.len(), k = a.data, e === s.Q && (y += n - k[C - 4], m += r - k[C - 3]), n = p[v++], r = p[v++], f = s.Q, a.addData(f, y, m, n, r);
            break;
          case "t":
            y = n, m = r, C = a.len(), k = a.data, e === s.Q && (y += n - k[C - 4], m += r - k[C - 3]), n += p[v++], r += p[v++], f = s.Q, a.addData(f, y, m, n, r);
            break;
          case "A":
            _ = p[v++], x = p[v++], w = p[v++], b = p[v++], S = p[v++], M = n, T = r, n = p[v++], r = p[v++], f = s.A, $r(M, T, n, r, b, S, _, x, w, f, a);
            break;
          case "a":
            _ = p[v++], x = p[v++], w = p[v++], b = p[v++], S = p[v++], M = n, T = r, n += p[v++], r += p[v++], f = s.A, $r(M, T, n, r, b, S, _, x, w, f, a);
        }
      }
      ("z" === c || "Z" === c) && (f = s.Z, a.addData(f), n = i, r = o), e = f;
    }
    return a.toStatic(), a;
  }
  function Jr(t) {
    return null != t.setData;
  }
  function ti(t, e) {
    var n = Qr(t),
      r = h({}, e);
    return r.buildPath = function (t) {
      if (Jr(t)) {
        t.setData(n.data);
        var e = t.getContext();
        e && t.rebuildPath(e, 1);
      } else {
        var e = t;
        n.rebuildPath(e, 1);
      }
    }, r.applyTransform = function (t) {
      Yr(n, t), this.dirtyShape();
    }, r;
  }
  function ei(t, e) {
    return new Uv(ti(t, e));
  }
  function ni(t, n) {
    var r = ti(t, n),
      i = function (t) {
        function n(e) {
          var n = t.call(this, e) || this;
          return n.applyTransform = r.applyTransform, n.buildPath = r.buildPath, n;
        }
        return e(n, t), n;
      }(Uv);
    return i;
  }
  function ri(t, e) {
    for (var n = [], r = t.length, i = 0; r > i; i++) {
      var o = t[i];
      o.path || o.createPathProxy(), o.shapeChanged() && o.buildPath(o.path, o.shape, !0), n.push(o.path);
    }
    var a = new Ov(e);
    return a.createPathProxy(), a.buildPath = function (t) {
      if (Jr(t)) {
        t.appendPath(n);
        var e = t.getContext();
        e && t.rebuildPath(e, 1);
      }
    }, a;
  }
  function ii(t) {
    return !!(t && "string" != typeof t && t.width && t.height);
  }
  function oi(t, e) {
    var n,
      r,
      i,
      o,
      a = e.x,
      s = e.y,
      l = e.width,
      u = e.height,
      h = e.r;
    0 > l && (a += l, l = -l), 0 > u && (s += u, u = -u), "number" == typeof h ? n = r = i = o = h : h instanceof Array ? 1 === h.length ? n = r = i = o = h[0] : 2 === h.length ? (n = i = h[0], r = o = h[1]) : 3 === h.length ? (n = h[0], r = o = h[1], i = h[2]) : (n = h[0], r = h[1], i = h[2], o = h[3]) : n = r = i = o = 0;
    var c;
    n + r > l && (c = n + r, n *= l / c, r *= l / c), i + o > l && (c = i + o, i *= l / c, o *= l / c), r + i > u && (c = r + i, r *= u / c, i *= u / c), n + o > u && (c = n + o, n *= u / c, o *= u / c), t.moveTo(a + n, s), t.lineTo(a + l - r, s), 0 !== r && t.arc(a + l - r, s + r, r, -Math.PI / 2, 0), t.lineTo(a + l, s + u - i), 0 !== i && t.arc(a + l - i, s + u - i, i, 0, Math.PI / 2), t.lineTo(a + o, s + u), 0 !== o && t.arc(a + o, s + u - o, o, Math.PI / 2, Math.PI), t.lineTo(a, s + n), 0 !== n && t.arc(a + n, s + n, n, Math.PI, 1.5 * Math.PI);
  }
  function ai(t, e, n) {
    if (e) {
      var r = e.x1,
        i = e.x2,
        o = e.y1,
        a = e.y2;
      t.x1 = r, t.x2 = i, t.y1 = o, t.y2 = a;
      var s = n && n.lineWidth;
      return s ? ($v(2 * r) === $v(2 * i) && (t.x1 = t.x2 = li(r, s, !0)), $v(2 * o) === $v(2 * a) && (t.y1 = t.y2 = li(o, s, !0)), t) : t;
    }
  }
  function si(t, e, n) {
    if (e) {
      var r = e.x,
        i = e.y,
        o = e.width,
        a = e.height;
      t.x = r, t.y = i, t.width = o, t.height = a;
      var s = n && n.lineWidth;
      return s ? (t.x = li(r, s, !0), t.y = li(i, s, !0), t.width = Math.max(li(r + o, s, !1) - t.x, 0 === o ? 0 : 1), t.height = Math.max(li(i + a, s, !1) - t.y, 0 === a ? 0 : 1), t) : t;
    }
  }
  function li(t, e, n) {
    if (!e) return t;
    var r = $v(2 * t);
    return (r + $v(e)) % 2 === 0 ? r / 2 : (r + (n ? 1 : -1)) / 2;
  }
  function ui(t, e, n, r, i, o, a) {
    var s = .5 * (n - t),
      l = .5 * (r - e);
    return (2 * (e - n) + s + l) * a + (-3 * (e - n) - 2 * s - l) * o + s * i + e;
  }
  function hi(t, e) {
    for (var n = t.length, r = [], i = 0, o = 1; n > o; o++) {
      i += ce(t[o - 1], t[o]);
    }
    var a = i / 2;
    a = n > a ? n : a;
    for (var o = 0; a > o; o++) {
      var s = o / (a - 1) * (e ? n : n - 1),
        l = Math.floor(s),
        u = s - l,
        h = void 0,
        c = t[l % n],
        f = void 0,
        p = void 0;
      e ? (h = t[(l - 1 + n) % n], f = t[(l + 1) % n], p = t[(l + 2) % n]) : (h = t[0 === l ? l : l - 1], f = t[l > n - 2 ? n - 1 : l + 1], p = t[l > n - 3 ? n - 1 : l + 2]);
      var d = u * u,
        g = u * d;
      r.push([ui(h[0], c[0], f[0], p[0], u, d, g), ui(h[1], c[1], f[1], p[1], u, d, g)]);
    }
    return r;
  }
  function ci(t, e, n, r) {
    var i,
      o,
      a,
      s,
      l = [],
      u = [],
      h = [],
      c = [];
    if (r) {
      a = [1 / 0, 1 / 0], s = [-1 / 0, -1 / 0];
      for (var f = 0, p = t.length; p > f; f++) {
        ve(a, a, t[f]), ye(s, s, t[f]);
      }
      ve(a, a, r[0]), ye(s, s, r[1]);
    }
    for (var f = 0, p = t.length; p > f; f++) {
      var d = t[f];
      if (n) i = t[f ? f - 1 : p - 1], o = t[(f + 1) % p];else {
        if (0 === f || f === p - 1) {
          l.push(J(t[f]));
          continue;
        }
        i = t[f - 1], o = t[f + 1];
      }
      re(u, o, i), ue(u, u, e);
      var g = ce(d, i),
        v = ce(d, o),
        y = g + v;
      0 !== y && (g /= y, v /= y), ue(h, u, -g), ue(c, u, v);
      var m = ee([], d, h),
        _ = ee([], d, c);
      r && (ye(m, m, a), ve(m, m, s), ye(_, _, a), ve(_, _, s)), l.push(m), l.push(_);
    }
    return n && l.push(l.shift()), l;
  }
  function fi(t, e, n) {
    var r = e.smooth,
      i = e.points;
    if (i && i.length >= 2) {
      if (r && "spline" !== r) {
        var o = ci(i, r, n, e.smoothConstraint);
        t.moveTo(i[0][0], i[0][1]);
        for (var a = i.length, s = 0; (n ? a : a - 1) > s; s++) {
          var l = o[2 * s],
            u = o[2 * s + 1],
            h = i[(s + 1) % a];
          t.bezierCurveTo(l[0], l[1], u[0], u[1], h[0], h[1]);
        }
      } else {
        "spline" === r && (i = hi(i, n)), t.moveTo(i[0][0], i[0][1]);
        for (var s = 1, c = i.length; c > s; s++) {
          t.lineTo(i[s][0], i[s][1]);
        }
      }
      n && t.closePath();
    }
  }
  function pi(t) {
    if (C(t)) {
      var e = new DOMParser();
      t = e.parseFromString(t, "text/xml");
    }
    var n = t;
    for (9 === n.nodeType && (n = n.firstChild); "svg" !== n.nodeName.toLowerCase() || 1 !== n.nodeType;) {
      n = n.nextSibling;
    }
    return n;
  }
  function di(t, e) {
    for (var n = t.firstChild; n;) {
      if (1 === n.nodeType) {
        var r = n.getAttribute("offset"),
          i = void 0;
        i = r.indexOf("%") > 0 ? parseInt(r, 10) / 100 : r ? parseFloat(r) : 0;
        var o = n.getAttribute("stop-color") || "#000000";
        e.colorStops.push({
          offset: i,
          color: o
        });
      }
      n = n.nextSibling;
    }
  }
  function gi(t, e) {
    t && t.__inheritedStyle && (e.__inheritedStyle || (e.__inheritedStyle = {}), c(e.__inheritedStyle, t.__inheritedStyle));
  }
  function vi(t) {
    for (var e = G(t).split(gy), n = [], r = 0; r < e.length; r += 2) {
      var i = parseFloat(e[r]),
        o = parseFloat(e[r + 1]);
      n.push([i, o]);
    }
    return n;
  }
  function yi(t, e, n, r) {
    var i = e,
      o = i.__inheritedStyle || {};
    if (1 === t.nodeType && (_i(t, e), h(o, xi(t)), !r)) for (var a in yy) {
      if (yy.hasOwnProperty(a)) {
        var s = t.getAttribute(a);
        null != s && (o[yy[a]] = s);
      }
    }
    i.style = i.style || {}, null != o.fill && (i.style.fill = mi(o.fill, n)), null != o.stroke && (i.style.stroke = mi(o.stroke, n)), v(["lineWidth", "opacity", "fillOpacity", "strokeOpacity", "miterLimit", "fontSize"], function (t) {
      null != o[t] && (i.style[t] = parseFloat(o[t]));
    }), o.textBaseline && "auto" !== o.textBaseline || (o.textBaseline = "alphabetic"), "alphabetic" === o.textBaseline && (o.textBaseline = "bottom"), "start" === o.textAlign && (o.textAlign = "left"), "end" === o.textAlign && (o.textAlign = "right"), v(["lineDashOffset", "lineCap", "lineJoin", "fontWeight", "fontFamily", "fontStyle", "textAlign", "textBaseline"], function (t) {
      null != o[t] && (i.style[t] = o[t]);
    }), o.lineDash && (i.style.lineDash = y(G(o.lineDash).split(gy), function (t) {
      return parseFloat(t);
    })), i.__inheritedStyle = o;
  }
  function mi(t, e) {
    var n = e && t && t.match(my);
    if (n) {
      var r = G(n[1]),
        i = e[r];
      return i;
    }
    return t;
  }
  function _i(t, e) {
    var n = t.getAttribute("transform");
    if (n) {
      n = n.replace(/,/g, " ");
      var r = [],
        i = null;
      n.replace(_y, function (t, e, n) {
        return r.push(e, n), "";
      });
      for (var o = r.length - 1; o > 0; o -= 2) {
        var a = r[o],
          s = r[o - 1],
          l = void 0;
        switch (i = i || ze(), s) {
          case "translate":
            l = G(a).split(gy), Ve(i, i, [parseFloat(l[0]), parseFloat(l[1] || "0")]);
            break;
          case "scale":
            l = G(a).split(gy), Ge(i, i, [parseFloat(l[0]), parseFloat(l[1] || l[0])]);
            break;
          case "rotate":
            l = G(a).split(gy), We(i, i, parseFloat(l[0]));
            break;
          case "skew":
            l = G(a).split(gy), console.warn("Skew transform is not supported yet");
            break;
          case "matrix":
            l = G(a).split(gy), i[0] = parseFloat(l[0]), i[1] = parseFloat(l[1]), i[2] = parseFloat(l[2]), i[3] = parseFloat(l[3]), i[4] = parseFloat(l[4]), i[5] = parseFloat(l[5]);
        }
      }
      e.setLocalTransform(i);
    }
  }
  function xi(t) {
    var e = t.getAttribute("style"),
      n = {};
    if (!e) return n;
    var r = {};
    xy.lastIndex = 0;
    for (var i; null != (i = xy.exec(e));) {
      r[i[1]] = i[2];
    }
    for (var o in yy) {
      yy.hasOwnProperty(o) && null != r[o] && (n[yy[o]] = r[o]);
    }
    return n;
  }
  function wi(t, e, n) {
    var r = e / t.width,
      i = n / t.height,
      o = Math.min(r, i);
    return {
      scale: o,
      x: -(t.x + t.width / 2) * o + e / 2,
      y: -(t.y + t.height / 2) * o + n / 2
    };
  }
  function bi(t, e, n, r, i, o, a, s) {
    var l = n - t,
      u = r - e,
      h = a - i,
      c = s - o,
      f = c * l - h * u;
    return Py > f * f ? void 0 : (f = (h * (e - o) - c * (t - i)) / f, [t + f * l, e + f * u]);
  }
  function Si(t, e, n, r, i, o, a) {
    var s = t - n,
      l = e - r,
      u = (a ? o : -o) / Dy(s * s + l * l),
      h = u * l,
      c = -u * s,
      f = t + h,
      p = e + c,
      d = n + h,
      g = r + c,
      v = (f + d) / 2,
      y = (p + g) / 2,
      m = d - f,
      _ = g - p,
      x = m * m + _ * _,
      w = i - o,
      b = f * g - d * p,
      S = (0 > _ ? -1 : 1) * Dy(Iy(0, w * w * x - b * b)),
      M = (b * _ - m * S) / x,
      T = (-b * m - _ * S) / x,
      C = (b * _ + m * S) / x,
      k = (-b * m + _ * S) / x,
      D = M - v,
      I = T - y,
      A = C - v,
      P = k - y;
    return D * D + I * I > A * A + P * P && (M = C, T = k), {
      cx: M,
      cy: T,
      x01: -h,
      y01: -c,
      x11: M * (i / w - 1),
      y11: T * (i / w - 1)
    };
  }
  function Mi(t, e) {
    var n = Iy(e.r, 0),
      r = Iy(e.r0 || 0, 0),
      i = n > 0,
      o = r > 0;
    if (i || o) {
      if (i || (n = r, r = 0), r > n) {
        var a = n;
        n = r, r = a;
      }
      var s = !!e.clockwise,
        l = e.startAngle,
        u = e.endAngle,
        h = [l, u];
      Lr(h, !s);
      var c = ky(h[0] - h[1]),
        f = e.cx,
        p = e.cy,
        d = e.cornerRadius || 0,
        g = e.innerCornerRadius || 0;
      if (n > Py) {
        if (c > by - Py) t.moveTo(f + n * My(l), p + n * Sy(l)), t.arc(f, p, n, l, u, !s), r > Py && (t.moveTo(f + r * My(u), p + r * Sy(u)), t.arc(f, p, r, u, l, s));else {
          var v = ky(n - r) / 2,
            y = Ay(v, d),
            m = Ay(v, g),
            _ = m,
            x = y,
            w = n * My(l),
            b = n * Sy(l),
            S = r * My(u),
            M = r * Sy(u),
            T = void 0,
            C = void 0,
            k = void 0,
            D = void 0;
          if ((y > Py || m > Py) && (T = n * My(u), C = n * Sy(u), k = r * My(l), D = r * Sy(l), wy > c)) {
            var I = bi(w, b, k, D, T, C, S, M);
            if (I) {
              var A = w - I[0],
                P = b - I[1],
                L = T - I[0],
                O = C - I[1],
                R = 1 / Sy(Ty((A * L + P * O) / (Dy(A * A + P * P) * Dy(L * L + O * O))) / 2),
                E = Dy(I[0] * I[0] + I[1] * I[1]);
              _ = Ay(m, (r - E) / (R - 1)), x = Ay(y, (n - E) / (R + 1));
            }
          }
          if (c > Py) {
            if (x > Py) {
              var B = Si(k, D, w, b, n, x, s),
                z = Si(T, C, S, M, n, x, s);
              t.moveTo(f + B.cx + B.x01, p + B.cy + B.y01), y > x ? t.arc(f + B.cx, p + B.cy, x, Cy(B.y01, B.x01), Cy(z.y01, z.x01), !s) : (t.arc(f + B.cx, p + B.cy, x, Cy(B.y01, B.x01), Cy(B.y11, B.x11), !s), t.arc(f, p, n, Cy(B.cy + B.y11, B.cx + B.x11), Cy(z.cy + z.y11, z.cx + z.x11), !s), t.arc(f + z.cx, p + z.cy, x, Cy(z.y11, z.x11), Cy(z.y01, z.x01), !s));
            } else t.moveTo(f + w, p + b), t.arc(f, p, n, l, u, !s);
          } else t.moveTo(f + w, p + b);
          if (r > Py && c > Py) {
            if (_ > Py) {
              var B = Si(S, M, T, C, r, -_, s),
                z = Si(w, b, k, D, r, -_, s);
              t.lineTo(f + B.cx + B.x01, p + B.cy + B.y01), m > _ ? t.arc(f + B.cx, p + B.cy, _, Cy(B.y01, B.x01), Cy(z.y01, z.x01), !s) : (t.arc(f + B.cx, p + B.cy, _, Cy(B.y01, B.x01), Cy(B.y11, B.x11), !s), t.arc(f, p, r, Cy(B.cy + B.y11, B.cx + B.x11), Cy(z.cy + z.y11, z.cx + z.x11), s), t.arc(f + z.cx, p + z.cy, _, Cy(z.y11, z.x11), Cy(z.y01, z.x01), !s));
            } else t.lineTo(f + S, p + M), t.arc(f, p, r, u, l, s);
          } else t.lineTo(f + S, p + M);
        }
      } else t.moveTo(f, p);
      t.closePath();
    }
  }
  function Ti(t) {
    if ("string" == typeof t) {
      var e = zy.get(t);
      return e && e.image;
    }
    return t;
  }
  function Ci(t, e, n, r, i) {
    if (t) {
      if ("string" == typeof t) {
        if (e && e.__zrImageSrc === t || !n) return e;
        var o = zy.get(t),
          a = {
            hostEl: n,
            cb: r,
            cbPayload: i
          };
        return o ? (e = o.image, !Di(e) && o.pending.push(a)) : (e = new Image(), e.onload = e.onerror = ki, zy.put(t, e.__cachedImgObj = {
          image: e,
          pending: [a]
        }), e.src = e.__zrImageSrc = t), e;
      }
      return t;
    }
    return e;
  }
  function ki() {
    var t = this.__cachedImgObj;
    this.onload = this.onerror = this.__cachedImgObj = null;
    for (var e = 0; e < t.pending.length; e++) {
      var n = t.pending[e],
        r = n.cb;
      r && r(this, n.cbPayload), n.hostEl.dirty();
    }
    t.pending.length = 0;
  }
  function Di(t) {
    return t && t.width && t.height;
  }
  function Ii(t, e, n, r, i) {
    if (!e) return "";
    var o = (t + "").split("\n");
    i = Ai(e, n, r, i);
    for (var a = 0, s = o.length; s > a; a++) {
      o[a] = Pi(o[a], i);
    }
    return o.join("\n");
  }
  function Ai(t, e, n, r) {
    r = r || {};
    var i = h({}, r);
    i.font = e, n = F(n, "..."), i.maxIterations = F(r.maxIterations, 2);
    var o = i.minChar = F(r.minChar, 0);
    i.cnCharWidth = Pn("国", e);
    var a = i.ascCharWidth = Pn("a", e);
    i.placeholder = F(r.placeholder, "");
    for (var s = t = Math.max(0, t - 1), l = 0; o > l && s >= a; l++) {
      s -= a;
    }
    var u = Pn(n, e);
    return u > s && (n = "", u = 0), s = t - u, i.ellipsis = n, i.ellipsisWidth = u, i.contentWidth = s, i.containerWidth = t, i;
  }
  function Pi(t, e) {
    var n = e.containerWidth,
      r = e.font,
      i = e.contentWidth;
    if (!n) return "";
    var o = Pn(t, r);
    if (n >= o) return t;
    for (var a = 0;; a++) {
      if (i >= o || a >= e.maxIterations) {
        t += e.ellipsis;
        break;
      }
      var s = 0 === a ? Li(t, i, e.ascCharWidth, e.cnCharWidth) : o > 0 ? Math.floor(t.length * i / o) : 0;
      t = t.substr(0, s), o = Pn(t, r);
    }
    return "" === t && (t = e.placeholder), t;
  }
  function Li(t, e, n, r) {
    for (var i = 0, o = 0, a = t.length; a > o && e > i; o++) {
      var s = t.charCodeAt(o);
      i += s >= 0 && 127 >= s ? n : r;
    }
    return o;
  }
  function Oi(t, e) {
    null != t && (t += "");
    var n,
      r = e.overflow,
      i = e.padding,
      o = e.font,
      a = "truncate" === r,
      s = Bn(o),
      l = F(e.lineHeight, s),
      u = "truncate" === e.lineOverflow,
      h = e.width;
    n = null != h && "break" === r || "breakAll" === r ? t ? Fi(t, e.font, h, "breakAll" === r, 0).lines : [] : t ? t.split("\n") : [];
    var c = n.length * l,
      f = F(e.height, c);
    if (c > f && u) {
      var p = Math.floor(f / l);
      n = n.slice(0, p);
    }
    var d = f,
      g = h;
    if (i && (d += i[0] + i[2], null != g && (g += i[1] + i[3])), t && a && null != g) for (var v = Ai(h, o, e.ellipsis, {
        minChar: e.truncateMinChar,
        placeholder: e.placeholder
      }), y = 0; y < n.length; y++) {
      n[y] = Pi(n[y], v);
    }
    if (null == h) {
      for (var m = 0, y = 0; y < n.length; y++) {
        m = Math.max(Pn(n[y], o), m);
      }
      h = m;
    }
    return {
      lines: n,
      height: f,
      outerHeight: d,
      lineHeight: l,
      calculatedLineHeight: s,
      contentHeight: c,
      width: h
    };
  }
  function Ri(t, e) {
    function n(t, e, n) {
      t.width = e, t.lineHeight = n, f += n, p = Math.max(p, e);
    }
    var r = new Vy();
    if (null != t && (t += ""), !t) return r;
    for (var i, o = e.width, a = e.height, s = e.overflow, l = "break" !== s && "breakAll" !== s || null == o ? null : {
        width: o,
        accumWidth: 0,
        breakAll: "breakAll" === s
      }, u = Fy.lastIndex = 0; null != (i = Fy.exec(t));) {
      var h = i.index;
      h > u && Ei(r, t.substring(u, h), e, l), Ei(r, i[2], e, l, i[1]), u = Fy.lastIndex;
    }
    u < t.length && Ei(r, t.substring(u, t.length), e, l);
    var c = [],
      f = 0,
      p = 0,
      d = e.padding,
      g = "truncate" === s,
      v = "truncate" === e.lineOverflow;
    t: for (var y = 0; y < r.lines.length; y++) {
      for (var m = r.lines[y], _ = 0, x = 0, w = 0; w < m.tokens.length; w++) {
        var b = m.tokens[w],
          S = b.styleName && e.rich[b.styleName] || {},
          M = b.textPadding = S.padding,
          T = M ? M[1] + M[3] : 0,
          C = b.font = S.font || e.font;
        b.contentHeight = Bn(C);
        var k = F(S.height, b.contentHeight);
        if (b.innerHeight = k, M && (k += M[0] + M[2]), b.height = k, b.lineHeight = N(S.lineHeight, e.lineHeight, k), b.align = S && S.align || e.align, b.verticalAlign = S && S.verticalAlign || "middle", v && null != a && f + b.lineHeight > a) {
          w > 0 ? (m.tokens = m.tokens.slice(0, w), n(m, x, _), r.lines = r.lines.slice(0, y + 1)) : r.lines = r.lines.slice(0, y);
          break t;
        }
        var D = S.width,
          I = null == D || "auto" === D;
        if ("string" == typeof D && "%" === D.charAt(D.length - 1)) b.percentWidth = D, c.push(b), b.contentWidth = Pn(b.text, C);else {
          if (I) {
            var A = S.backgroundColor,
              P = A && A.image;
            P && (P = Ti(P), Di(P) && (b.width = Math.max(b.width, P.width * k / P.height)));
          }
          var L = g && null != o ? o - x : null;
          null != L && L < b.width ? !I || T > L ? (b.text = "", b.width = b.contentWidth = 0) : (b.text = Ii(b.text, L - T, C, e.ellipsis, {
            minChar: e.truncateMinChar
          }), b.width = b.contentWidth = Pn(b.text, C)) : b.contentWidth = Pn(b.text, C);
        }
        b.width += T, x += b.width, S && (_ = Math.max(_, b.lineHeight));
      }
      n(m, x, _);
    }
    r.outerWidth = r.width = F(o, p), r.outerHeight = r.height = F(a, f), r.contentHeight = f, r.contentWidth = p, d && (r.outerWidth += d[1] + d[3], r.outerHeight += d[0] + d[2]);
    for (var y = 0; y < c.length; y++) {
      var b = c[y],
        O = b.percentWidth;
      b.width = parseInt(O, 10) / 100 * r.width;
    }
    return r;
  }
  function Ei(t, e, n, r, i) {
    var o,
      a,
      s = "" === e,
      l = i && n.rich[i] || {},
      u = t.lines,
      h = l.font || n.font,
      c = !1;
    if (r) {
      var f = l.padding,
        p = f ? f[1] + f[3] : 0;
      if (null != l.width && "auto" !== l.width) {
        var d = Ni(l.width, r.width) + p;
        u.length > 0 && d + r.accumWidth > r.width && (o = e.split("\n"), c = !0), r.accumWidth = d;
      } else {
        var g = Fi(e, h, r.width, r.breakAll, r.accumWidth);
        r.accumWidth = g.accumWidth + p, a = g.linesWidths, o = g.lines;
      }
    } else o = e.split("\n");
    for (var v = 0; v < o.length; v++) {
      var y = o[v],
        m = new Ny();
      if (m.styleName = i, m.text = y, m.isLineHolder = !y && !s, m.width = "number" == typeof l.width ? l.width : a ? a[v] : Pn(y, h), v || c) u.push(new Hy([m]));else {
        var _ = (u[u.length - 1] || (u[0] = new Hy())).tokens,
          x = _.length;
        1 === x && _[0].isLineHolder ? _[0] = m : (y || !x || s) && _.push(m);
      }
    }
  }
  function Bi(t) {
    var e = t.charCodeAt(0);
    return e >= 33 && 255 >= e;
  }
  function zi(t) {
    return Bi(t) ? Wy[t] ? !0 : !1 : !0;
  }
  function Fi(t, e, n, r, i) {
    for (var o = [], a = [], s = "", l = "", u = 0, h = 0, c = 0; c < t.length; c++) {
      var f = t.charAt(c);
      if ("\n" !== f) {
        var p = Pn(f, e),
          d = r ? !1 : !zi(f);
        (o.length ? h + p > n : i + h + p > n) ? h ? (s || l) && (d ? (s || (s = l, l = "", u = 0, h = u), o.push(s), a.push(h - u), l += f, u += p, s = "", h = u) : (l && (s += l, h += u, l = "", u = 0), o.push(s), a.push(h), s = f, h = p)) : d ? (o.push(l), a.push(u), l = f, u = p) : (o.push(f), a.push(p)) : (h += p, d ? (l += f, u += p) : (l && (s += l, l = "", u = 0), s += f));
      } else l && (s += l, h += u), o.push(s), a.push(h), s = "", l = "", u = 0, h = 0;
    }
    return o.length || s || (s = t, l = "", u = 0), l && (s += l), s && (o.push(s), a.push(h)), 1 === o.length && (h += i), {
      accumWidth: h,
      lines: o,
      linesWidths: a
    };
  }
  function Ni(t, e) {
    return "string" == typeof t ? t.lastIndexOf("%") >= 0 ? parseFloat(t) / 100 * e : parseFloat(t) : t;
  }
  function Hi(t) {
    return Vi(t), v(t.rich, Vi), t;
  }
  function Vi(t) {
    if (t) {
      t.font = Xy.makeFont(t);
      var e = t.align;
      "middle" === e && (e = "center"), t.align = null == e || Yy[e] ? e : "left";
      var n = t.verticalAlign;
      "center" === n && (n = "middle"), t.verticalAlign = null == n || jy[n] ? n : "top";
      var r = t.padding;
      r && (t.padding = V(t.padding));
    }
  }
  function Wi(t, e) {
    return null == t || 0 >= e || "transparent" === t || "none" === t ? null : t.image || t.colorStops ? "#000" : t;
  }
  function Gi(t) {
    return null == t || "none" === t ? null : t.image || t.colorStops ? "#000" : t;
  }
  function Ui(t, e, n) {
    return "right" === e ? t - n[1] : "center" === e ? t + n[3] / 2 - n[1] / 2 : t + n[3];
  }
  function qi(t) {
    var e = t.text;
    return null != e && (e += ""), e;
  }
  function Xi(t) {
    return !!(t.backgroundColor || t.borderWidth && t.borderColor);
  }
  function Yi(t, e, n) {
    var r = t.cpx2,
      i = t.cpy2;
    return null === r || null === i ? [(n ? pr : fr)(t.x1, t.cpx1, t.cpx2, t.x2, e), (n ? pr : fr)(t.y1, t.cpy1, t.cpy2, t.y2, e)] : [(n ? xr : _r)(t.x1, t.cpx1, t.x2, e), (n ? xr : _r)(t.y1, t.cpy1, t.y2, e)];
  }
  function ji(t) {
    delete Om[t];
  }
  function Zi(t) {
    if (!t) return !1;
    if ("string" == typeof t) return dn(t, 1) < ag;
    if (t.colorStops) {
      for (var e = t.colorStops, n = 0, r = e.length, i = 0; r > i; i++) {
        n += dn(e[i].color, 1);
      }
      return n /= r, ag > n;
    }
    return !1;
  }
  function Ki(t, e) {
    var n = new Rm(o(), t, e);
    return Om[n.id] = n, n;
  }
  function $i(t) {
    t.dispose();
  }
  function Qi() {
    for (var t in Om) {
      Om.hasOwnProperty(t) && Om[t].dispose();
    }
    Om = {};
  }
  function Ji(t) {
    return Om[t];
  }
  function to(t, e) {
    Lm[t] = e;
  }
  function eo(t) {
    return t.replace(/^\s+|\s+$/g, "");
  }
  function no(t, e, n, r) {
    var i = e[1] - e[0],
      o = n[1] - n[0];
    if (0 === i) return 0 === o ? n[0] : (n[0] + n[1]) / 2;
    if (r) {
      if (i > 0) {
        if (t <= e[0]) return n[0];
        if (t >= e[1]) return n[1];
      } else {
        if (t >= e[0]) return n[0];
        if (t <= e[1]) return n[1];
      }
    } else {
      if (t === e[0]) return n[0];
      if (t === e[1]) return n[1];
    }
    return (t - e[0]) / i * o + n[0];
  }
  function ro(t, e) {
    switch (t) {
      case "center":
      case "middle":
        t = "50%";
        break;
      case "left":
      case "top":
        t = "0%";
        break;
      case "right":
      case "bottom":
        t = "100%";
    }
    return "string" == typeof t ? eo(t).match(/%$/) ? parseFloat(t) / 100 * e : parseFloat(t) : null == t ? 0 / 0 : +t;
  }
  function io(t, e, n) {
    return null == e && (e = 10), e = Math.min(Math.max(0, e), 20), t = (+t).toFixed(e), n ? t : +t;
  }
  function oo(t) {
    return t.sort(function (t, e) {
      return t - e;
    }), t;
  }
  function ao(t) {
    if (t = +t, isNaN(t)) return 0;
    for (var e = 1, n = 0; Math.round(t * e) / e !== t;) {
      e *= 10, n++;
    }
    return n;
  }
  function so(t) {
    var e = t.toString(),
      n = e.indexOf("e");
    if (n > 0) {
      var r = +e.slice(n + 1);
      return 0 > r ? -r : 0;
    }
    var i = e.indexOf(".");
    return 0 > i ? 0 : e.length - 1 - i;
  }
  function lo(t, e) {
    var n = Math.log,
      r = Math.LN10,
      i = Math.floor(n(t[1] - t[0]) / r),
      o = Math.round(n(Math.abs(e[1] - e[0])) / r),
      a = Math.min(Math.max(-i + o, 0), 20);
    return isFinite(a) ? a : 20;
  }
  function uo(t, e, n) {
    if (!t[e]) return 0;
    var r = m(t, function (t, e) {
      return t + (isNaN(e) ? 0 : e);
    }, 0);
    if (0 === r) return 0;
    for (var i = Math.pow(10, n), o = y(t, function (t) {
        return (isNaN(t) ? 0 : t) / r * i * 100;
      }), a = 100 * i, s = y(o, function (t) {
        return Math.floor(t);
      }), l = m(s, function (t, e) {
        return t + e;
      }, 0), u = y(o, function (t, e) {
        return t - s[e];
      }); a > l;) {
      for (var h = Number.NEGATIVE_INFINITY, c = null, f = 0, p = u.length; p > f; ++f) {
        u[f] > h && (h = u[f], c = f);
      }
      ++s[c], u[c] = 0, ++l;
    }
    return s[e] / i;
  }
  function ho(t) {
    var e = 2 * Math.PI;
    return (t % e + e) % e;
  }
  function co(t) {
    return t > -zm && zm > t;
  }
  function fo(t) {
    if (t instanceof Date) return t;
    if ("string" == typeof t) {
      var e = Nm.exec(t);
      if (!e) return new Date(0 / 0);
      if (e[8]) {
        var n = +e[4] || 0;
        return "Z" !== e[8].toUpperCase() && (n -= +e[8].slice(0, 3)), new Date(Date.UTC(+e[1], +(e[2] || 1) - 1, +e[3] || 1, n, +(e[5] || 0), +e[6] || 0, +e[7] || 0));
      }
      return new Date(+e[1], +(e[2] || 1) - 1, +e[3] || 1, +e[4] || 0, +(e[5] || 0), +e[6] || 0, +e[7] || 0);
    }
    return new Date(null == t ? 0 / 0 : Math.round(t));
  }
  function po(t) {
    return Math.pow(10, go(t));
  }
  function go(t) {
    if (0 === t) return 0;
    var e = Math.floor(Math.log(t) / Math.LN10);
    return t / Math.pow(10, e) >= 10 && e++, e;
  }
  function vo(t, e) {
    var n,
      r = go(t),
      i = Math.pow(10, r),
      o = t / i;
    return n = e ? 1.5 > o ? 1 : 2.5 > o ? 2 : 4 > o ? 3 : 7 > o ? 5 : 10 : 1 > o ? 1 : 2 > o ? 2 : 3 > o ? 3 : 5 > o ? 5 : 10, t = n * i, r >= -20 ? +t.toFixed(0 > r ? -r : 0) : t;
  }
  function yo(t, e) {
    var n = (t.length - 1) * e + 1,
      r = Math.floor(n),
      i = +t[r - 1],
      o = n - r;
    return o ? i + o * (t[r] - i) : i;
  }
  function mo(t) {
    function e(t, n, r) {
      return t.interval[r] < n.interval[r] || t.interval[r] === n.interval[r] && (t.close[r] - n.close[r] === (r ? -1 : 1) || !r && e(t, n, 1));
    }
    t.sort(function (t, n) {
      return e(t, n, 0) ? -1 : 1;
    });
    for (var n = -1 / 0, r = 1, i = 0; i < t.length;) {
      for (var o = t[i].interval, a = t[i].close, s = 0; 2 > s; s++) {
        o[s] <= n && (o[s] = n, a[s] = s ? 1 : 1 - r), n = o[s], r = a[s];
      }
      o[0] === o[1] && a[0] * a[1] !== 1 ? t.splice(i, 1) : i++;
    }
    return t;
  }
  function _o(t) {
    var e = parseFloat(t);
    return e == t && (0 !== e || "string" != typeof t || t.indexOf("x") <= 0) ? e : 0 / 0;
  }
  function xo(t) {
    return !isNaN(_o(t));
  }
  function wo() {
    return Math.round(9 * Math.random());
  }
  function bo(t, e) {
    return 0 === e ? t : bo(e, t % e);
  }
  function So(t, e) {
    return null == t ? e : null == e ? t : t * e / bo(t, e);
  }
  function Mo(t) {
    throw new Error(t);
  }
  function To(t) {
    return t instanceof Array ? t : null == t ? [] : [t];
  }
  function Co(t, e, n) {
    if (t) {
      t[e] = t[e] || {}, t.emphasis = t.emphasis || {}, t.emphasis[e] = t.emphasis[e] || {};
      for (var r = 0, i = n.length; i > r; r++) {
        var o = n[r];
        !t.emphasis[e].hasOwnProperty(o) && t[e].hasOwnProperty(o) && (t.emphasis[e][o] = t[e][o]);
      }
    }
  }
  function ko(t) {
    return !I(t) || M(t) || t instanceof Date ? t : t.value;
  }
  function Do(t) {
    return I(t) && !(t instanceof Array);
  }
  function Io(t, e, n) {
    var r = "normalMerge" === n,
      i = "replaceMerge" === n,
      o = "replaceAll" === n;
    t = t || [], e = (e || []).slice();
    var a = X();
    v(e, function (t, n) {
      return I(t) ? void 0 : void (e[n] = null);
    });
    var s = Ao(t, a, n);
    return (r || i) && Po(s, t, a, e), r && Lo(s, e), r || i ? Oo(s, e, i) : o && Ro(s, e), Eo(s), s;
  }
  function Ao(t, e, n) {
    var r = [];
    if ("replaceAll" === n) return r;
    for (var i = 0; i < t.length; i++) {
      var o = t[i];
      o && null != o.id && e.set(o.id, i), r.push({
        existing: "replaceMerge" === n || Ho(o) ? null : o,
        newOption: null,
        keyInfo: null,
        brandNew: null
      });
    }
    return r;
  }
  function Po(t, e, n, r) {
    v(r, function (i, o) {
      if (i && null != i.id) {
        var a = zo(i.id),
          s = n.get(a);
        if (null != s) {
          var l = t[s];
          W(!l.newOption, 'Duplicated option on id "' + a + '".'), l.newOption = i, l.existing = e[s], r[o] = null;
        }
      }
    });
  }
  function Lo(t, e) {
    v(e, function (n, r) {
      if (n && null != n.name) for (var i = 0; i < t.length; i++) {
        var o = t[i].existing;
        if (!t[i].newOption && o && (null == o.id || null == n.id) && !Ho(n) && !Ho(o) && Bo("name", o, n)) return t[i].newOption = n, void (e[r] = null);
      }
    });
  }
  function Oo(t, e, n) {
    v(e, function (e) {
      if (e) {
        for (var r, i = 0; (r = t[i]) && (r.newOption || Ho(r.existing) || r.existing && null != e.id && !Bo("id", e, r.existing));) {
          i++;
        }
        r ? (r.newOption = e, r.brandNew = n) : t.push({
          newOption: e,
          brandNew: n,
          existing: null,
          keyInfo: null
        }), i++;
      }
    });
  }
  function Ro(t, e) {
    v(e, function (e) {
      t.push({
        newOption: e,
        brandNew: !0,
        existing: null,
        keyInfo: null
      });
    });
  }
  function Eo(t) {
    var e = X();
    v(t, function (t) {
      var n = t.existing;
      n && e.set(n.id, t);
    }), v(t, function (t) {
      var n = t.newOption;
      W(!n || null == n.id || !e.get(n.id) || e.get(n.id) === t, "id duplicates: " + (n && n.id)), n && null != n.id && e.set(n.id, t), !t.keyInfo && (t.keyInfo = {});
    }), v(t, function (t, n) {
      var r = t.existing,
        i = t.newOption,
        o = t.keyInfo;
      if (I(i)) {
        if (o.name = null != i.name ? zo(i.name) : r ? r.name : Vm + n, r) o.id = zo(r.id);else if (null != i.id) o.id = zo(i.id);else {
          var a = 0;
          do {
            o.id = "\x00" + o.name + "\x00" + a++;
          } while (e.get(o.id));
        }
        e.set(o.id, t);
      }
    });
  }
  function Bo(t, e, n) {
    var r = Fo(e[t], null),
      i = Fo(n[t], null);
    return null != r && null != i && r === i;
  }
  function zo(t) {
    return Fo(t, "");
  }
  function Fo(t, e) {
    if (null == t) return e;
    var n = _typeof(t);
    return "string" === n ? t : "number" === n || k(t) ? t + "" : e;
  }
  function No(t) {
    var e = t.name;
    return !(!e || !e.indexOf(Vm));
  }
  function Ho(t) {
    return t && null != t.id && 0 === zo(t.id).indexOf(Wm);
  }
  function Vo(t, e, n) {
    v(t, function (t) {
      var r = t.newOption;
      I(r) && (t.keyInfo.mainType = e, t.keyInfo.subType = Wo(e, r, t.existing, n));
    });
  }
  function Wo(t, e, n, r) {
    var i = e.type ? e.type : n ? n.subType : r.determineSubType(t, e);
    return i;
  }
  function Go(t, e) {
    return null != e.dataIndexInside ? e.dataIndexInside : null != e.dataIndex ? M(e.dataIndex) ? y(e.dataIndex, function (e) {
      return t.indexOfRawIndex(e);
    }) : t.indexOfRawIndex(e.dataIndex) : null != e.name ? M(e.name) ? y(e.name, function (e) {
      return t.indexOfName(e);
    }) : t.indexOfName(e.name) : void 0;
  }
  function Uo() {
    var t = "__ec_inner_" + Um++;
    return function (e) {
      return e[t] || (e[t] = {});
    };
  }
  function qo(t, e, n) {
    var r;
    if (C(e)) {
      var i = {};
      i[e + "Index"] = 0, r = i;
    } else r = e;
    var o = X(),
      a = {},
      s = !1;
    v(r, function (t, e) {
      if ("dataIndex" === e || "dataIndexInside" === e) return void (a[e] = t);
      var r = e.match(/^(\w+)(Index|Id|Name)$/) || [],
        i = r[1],
        l = (r[2] || "").toLowerCase();
      if (i && l && !(n && n.includeMainTypes && f(n.includeMainTypes, i) < 0)) {
        s = s || !!i;
        var u = o.get(i) || o.set(i, {});
        u[l] = t;
      }
    });
    var l = n ? n.defaultMainType : null;
    return !s && l && o.set(l, {}), o.each(function (e, r) {
      var i = Xo(t, r, e, {
        useDefault: l === r,
        enableAll: n && null != n.enableAll ? n.enableAll : !0,
        enableNone: n && null != n.enableNone ? n.enableNone : !0
      });
      a[r + "Models"] = i.models, a[r + "Model"] = i.models[0];
    }), a;
  }
  function Xo(t, e, n, r) {
    r = r || qm;
    var i = n.index,
      o = n.id,
      a = n.name,
      s = {
        models: null,
        specified: null != i || null != o || null != a
      };
    if (!s.specified) {
      var l = void 0;
      return s.models = r.useDefault && (l = t.getComponent(e)) ? [l] : [], s;
    }
    return "none" === i || i === !1 ? (W(r.enableNone, '`"none"` or `false` is not a valid value on index option.'), s.models = [], s) : ("all" === i && (W(r.enableAll, '`"all"` is not a valid value on index option.'), i = o = a = null), s.models = t.queryComponents({
      mainType: e,
      index: i,
      id: o,
      name: a
    }), s);
  }
  function Yo(t, e, n) {
    t.setAttribute ? t.setAttribute(e, n) : t[e] = n;
  }
  function jo(t, e) {
    return t.getAttribute ? t.getAttribute(e) : t[e];
  }
  function Zo(t, e, n, r, i) {
    var o = null == e || "auto" === e;
    if (null == r) return r;
    if ("number" == typeof r) {
      var a = vn(n || 0, r, i);
      return io(a, o ? Math.max(so(n || 0), so(r)) : e);
    }
    if ("string" == typeof r) return 1 > i ? n : r;
    for (var s = [], l = n || [], u = r, h = Math.max(l.length, u.length), c = 0; h > c; ++c) {
      var f = t.getDimensionInfo(c);
      if ("ordinal" === f.type) s[c] = (1 > i ? l : u)[c];else {
        var p = l && l[c] ? l[c] : 0,
          d = u[c],
          a = null == l ? r[c] : vn(p, d, i);
        s[c] = io(a, o ? Math.max(so(p), so(d)) : e);
      }
    }
    return s;
  }
  function Ko(t) {
    var e = {
      main: "",
      sub: ""
    };
    if (t) {
      var n = t.split(Xm);
      e.main = n[0] || "", e.sub = n[1] || "";
    }
    return e;
  }
  function $o(t) {
    W(/^[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)?$/.test(t), 'componentType "' + t + '" illegal');
  }
  function Qo(t) {
    return !(!t || !t[jm]);
  }
  function Jo(t) {
    t.$constructor = t, t.extend = function (t) {
      function e() {
        for (var i = [], o = 0; o < arguments.length; o++) {
          i[o] = arguments[o];
        }
        if (t.$constructor) t.$constructor.apply(this, arguments);else {
          if (ta(r)) {
            var a = j(e.prototype, new (r.bind.apply(r, n([void 0], i)))());
            return a;
          }
          r.apply(this, arguments);
        }
      }
      var r = this;
      return e[jm] = !0, h(e.prototype, t), e.extend = this.extend, e.superCall = ra, e.superApply = ia, p(e, this), e.superClass = r, e;
    };
  }
  function ta(t) {
    return "function" == typeof t && /^class\s/.test(Function.prototype.toString.call(t));
  }
  function ea(t, e) {
    t.extend = e.extend;
  }
  function na(t) {
    var e = ["__\x00is_clz", Zm++].join("_");
    t.prototype[e] = !0, t.isInstance = function (t) {
      return !(!t || !t[e]);
    };
  }
  function ra(t, e) {
    for (var n = [], r = 2; r < arguments.length; r++) {
      n[r - 2] = arguments[r];
    }
    return this.superClass.prototype[e].apply(t, n);
  }
  function ia(t, e, n) {
    return this.superClass.prototype[e].apply(t, n);
  }
  function oa(t, e) {
    function n(t) {
      var e = r[t.main];
      return e && e[Ym] || (e = r[t.main] = {}, e[Ym] = !0), e;
    }
    e = e || {};
    var r = {};
    if (t.registerClass = function (t) {
      var e = t.type || t.prototype.type;
      if (e) {
        $o(e), t.prototype.type = e;
        var i = Ko(e);
        if (i.sub) {
          if (i.sub !== Ym) {
            var o = n(i);
            o[i.sub] = t;
          }
        } else r[i.main] = t;
      }
      return t;
    }, t.getClass = function (t, e, n) {
      var i = r[t];
      if (i && i[Ym] && (i = e ? i[e] : null), n && !i) throw new Error(e ? "Component " + t + "." + (e || "") + " not exists. Load it first." : t + ".type should be specified.");
      return i;
    }, t.getClassesByMainType = function (t) {
      var e = Ko(t),
        n = [],
        i = r[e.main];
      return i && i[Ym] ? v(i, function (t, e) {
        e !== Ym && n.push(t);
      }) : n.push(i), n;
    }, t.hasClass = function (t) {
      var e = Ko(t);
      return !!r[e.main];
    }, t.getAllClassMainTypes = function () {
      var t = [];
      return v(r, function (e, n) {
        t.push(n);
      }), t;
    }, t.hasSubTypes = function (t) {
      var e = Ko(t),
        n = r[e.main];
      return n && n[Ym];
    }, e.registerWhenExtend) {
      var i = t.extend;
      i && (t.extend = function (e) {
        var n = i.call(this, e);
        return t.registerClass(n);
      });
    }
  }
  function aa(t, e) {
    for (var n = 0; n < t.length; n++) {
      t[n][1] || (t[n][1] = t[n][0]);
    }
    return e = e || !1, function (n, r, i) {
      for (var o = {}, a = 0; a < t.length; a++) {
        var s = t[a][1];
        if (!(r && f(r, s) >= 0 || i && f(i, s) < 0)) {
          var l = n.getShallow(s, e);
          null != l && (o[t[a][0]] = l);
        }
      }
      return o;
    };
  }
  function sa(t) {
    return null != t && "none" !== t;
  }
  function la(t) {
    if ("string" != typeof t) return t;
    var e = g_.get(t);
    return e || (e = sn(t, -.1), g_.put(t, e)), e;
  }
  function ua(t, e, n) {
    t.onHoverStateChange && (t.hoverState || 0) !== n && t.onHoverStateChange(e), t.hoverState = n;
  }
  function ha(t) {
    ua(t, "emphasis", o_);
  }
  function ca(t) {
    t.hoverState === o_ && ua(t, "normal", r_);
  }
  function fa(t) {
    ua(t, "blur", i_);
  }
  function pa(t) {
    t.hoverState === i_ && ua(t, "normal", r_);
  }
  function da(t) {
    t.selected = !0;
  }
  function ga(t) {
    t.selected = !1;
  }
  function va(t, e, n) {
    e(t, n);
  }
  function ya(t, e, n) {
    va(t, e, n), t.isGroup && t.traverse(function (t) {
      va(t, e, n);
    });
  }
  function ma(t, e, n, r) {
    for (var i = t.style, o = {}, a = 0; a < e.length; a++) {
      var s = e[a],
        l = i[s];
      o[s] = null == l ? r && r[s] : l;
    }
    for (var a = 0; a < t.animators.length; a++) {
      var u = t.animators[a];
      u.__fromStateTransition && u.__fromStateTransition.indexOf(n) < 0 && "style" === u.targetName && u.saveFinalToTarget(o, e);
    }
    return o;
  }
  function _a(t, e, n, r) {
    var i = n && f(n, "select") >= 0,
      o = !1;
    if (t instanceof Ov) {
      var a = n_(t),
        s = i ? a.selectFill || a.normalFill : a.normalFill,
        l = i ? a.selectStroke || a.normalStroke : a.normalStroke;
      if (sa(s) || sa(l)) {
        r = r || {};
        var u = r.style || {};
        !sa(u.fill) && sa(s) ? (o = !0, r = h({}, r), u = h({}, u), u.fill = la(s)) : !sa(u.stroke) && sa(l) && (o || (r = h({}, r), u = h({}, u)), u.stroke = la(l)), r.style = u;
      }
    }
    if (r && null == r.z2) {
      o || (r = h({}, r));
      var c = t.z2EmphasisLift;
      r.z2 = t.z2 + (null != c ? c : l_);
    }
    return r;
  }
  function xa(t, e, n) {
    if (n && null == n.z2) {
      n = h({}, n);
      var r = t.z2SelectLift;
      n.z2 = t.z2 + (null != r ? r : u_);
    }
    return n;
  }
  function wa(t, e, n) {
    var r = f(t.currentStates, e) >= 0,
      i = t.style.opacity,
      o = r ? null : ma(t, ["opacity"], e, {
        opacity: 1
      });
    n = n || {};
    var a = n.style || {};
    return null == a.opacity && (n = h({}, n), a = h({
      opacity: r ? i : .1 * o.opacity
    }, a), n.style = a), n;
  }
  function ba(t, e) {
    var n = this.states[t];
    if (this.style) {
      if ("emphasis" === t) return _a(this, t, e, n);
      if ("blur" === t) return wa(this, t, n);
      if ("select" === t) return xa(this, t, n);
    }
    return n;
  }
  function Sa(t) {
    t.stateProxy = ba;
    var e = t.getTextContent(),
      n = t.getTextGuideLine();
    e && (e.stateProxy = ba), n && (n.stateProxy = ba);
  }
  function Ma(t, e) {
    !La(t, e) && !t.__highByOuter && ya(t, ha);
  }
  function Ta(t, e) {
    !La(t, e) && !t.__highByOuter && ya(t, ca);
  }
  function Ca(t, e) {
    t.__highByOuter |= 1 << (e || 0), ya(t, ha);
  }
  function ka(t, e) {
    !(t.__highByOuter &= ~(1 << (e || 0))) && ya(t, ca);
  }
  function Da(t) {
    ya(t, fa);
  }
  function Ia(t) {
    ya(t, pa);
  }
  function Aa(t) {
    ya(t, da);
  }
  function Pa(t) {
    ya(t, ga);
  }
  function La(t, e) {
    return t.__highDownSilentOnTouch && e.zrByTouch;
  }
  function Oa(t) {
    var e = t.getModel();
    e.eachComponent(function (e, n) {
      var r = "series" === e ? t.getViewOfSeriesModel(n) : t.getViewOfComponentModel(n);
      r.group.traverse(function (t) {
        pa(t);
      });
    });
  }
  function Ra(t, e, n, r, i) {
    function o(t, e) {
      for (var n = 0; n < e.length; n++) {
        var r = t.getItemGraphicEl(e[n]);
        r && Ia(r);
      }
    }
    var a = r.getModel();
    if (n = n || "coordinateSystem", !i) return void Oa(r);
    if (null != t && e && "none" !== e) {
      var s = a.getSeriesByIndex(t),
        l = s.coordinateSystem;
      l && l.master && (l = l.master);
      var u = [];
      a.eachSeries(function (t) {
        var i = s === t,
          a = t.coordinateSystem;
        a && a.master && (a = a.master);
        var h = a && l ? a === l : i;
        if (!("series" === n && !i || "coordinateSystem" === n && !h || "series" === e && i)) {
          var c = r.getViewOfSeriesModel(t);
          if (c.group.traverse(function (t) {
            fa(t);
          }), g(e)) o(t.getData(), e);else if (I(e)) for (var f = w(e), p = 0; p < f.length; p++) {
            o(t.getData(f[p]), e[f[p]]);
          }
          u.push(t);
        }
      }), a.eachComponent(function (t, e) {
        if ("series" !== t) {
          var n = r.getViewOfComponentModel(e);
          n && n.blurSeries && n.blurSeries(u, a);
        }
      });
    }
  }
  function Ea(t, e, n) {
    if (Xa(e)) {
      var r = e.type === h_,
        i = t.seriesIndex,
        o = t.getData(e.dataType),
        a = Go(o, e);
      a = (M(a) ? a[0] : a) || 0;
      var s = o.getItemGraphicEl(a);
      if (!s) for (var l = o.count(), u = 0; !s && l > u;) {
        s = o.getItemGraphicEl(u++);
      }
      if (s) {
        var h = Jm(s);
        Ra(i, h.focus, h.blurScope, n, r);
      } else {
        var c = t.get(["emphasis", "focus"]),
          f = t.get(["emphasis", "blurScope"]);
        null != c && Ra(i, c, f, n, r);
      }
    }
  }
  function Ba(t, e) {
    if (qa(e)) {
      var n = e.dataType,
        r = t.getData(n),
        i = Go(r, e);
      M(i) || (i = [i]), t[e.type === d_ ? "toggleSelect" : e.type === f_ ? "select" : "unselect"](i, n);
    }
  }
  function za(t) {
    var e = t.getAllData();
    v(e, function (e) {
      var n = e.data,
        r = e.type;
      n.eachItemGraphicEl(function (e, n) {
        t.isSelected(n, r) ? Aa(e) : Pa(e);
      });
    });
  }
  function Fa(t) {
    var e = [];
    return t.eachSeries(function (t) {
      var n = t.getAllData();
      v(n, function (n) {
        var r = (n.data, n.type),
          i = t.getSelectedDataIndices();
        if (i.length > 0) {
          var o = {
            dataIndex: i,
            seriesIndex: t.seriesIndex
          };
          null != r && (o.dataType = r), e.push(o);
        }
      });
    }), e;
  }
  function Na(t, e, n) {
    Wa(t, !0), ya(t, Sa), Ha(t, e, n);
  }
  function Ha(t, e, n) {
    var r = Jm(t);
    null != e ? (r.focus = e, r.blurScope = n) : r.focus && (r.focus = null);
  }
  function Va(t, e, n, r) {
    n = n || "itemStyle";
    for (var i = 0; i < v_.length; i++) {
      var o = v_[i],
        a = e.getModel([o, n]),
        s = t.ensureState(o);
      s.style = r ? r(a) : a[y_[n]]();
    }
  }
  function Wa(t, e) {
    var n = e === !1,
      r = t;
    t.highDownSilentOnTouch && (r.__highDownSilentOnTouch = t.highDownSilentOnTouch), (!n || r.__highDownDispatcher) && (r.__highByOuter = r.__highByOuter || 0, r.__highDownDispatcher = !n);
  }
  function Ga(t) {
    return !(!t || !t.__highDownDispatcher);
  }
  function Ua(t) {
    var e = e_[t];
    return null == e && 32 >= t_ && (e = e_[t] = t_++), e;
  }
  function qa(t) {
    var e = t.type;
    return e === f_ || e === p_ || e === d_;
  }
  function Xa(t) {
    var e = t.type;
    return e === h_ || e === c_;
  }
  function Ya(t) {
    var e = n_(t);
    e.normalFill = t.style.fill, e.normalStroke = t.style.stroke;
    var n = t.states.select || {};
    e.selectFill = n.style && n.style.fill || null, e.selectStroke = n.style && n.style.stroke || null;
  }
  function ja(t) {
    return Ov.extend(t);
  }
  function Za(t, e) {
    return w_(t, e);
  }
  function Ka(t, e) {
    x_[t] = e;
  }
  function $a(t) {
    return x_.hasOwnProperty(t) ? x_[t] : void 0;
  }
  function Qa(t, e, n, r) {
    var i = ei(t, e);
    return n && ("center" === r && (n = ts(n, i.getBoundingRect())), es(i, n)), i;
  }
  function Ja(t, e, n) {
    var r = new jv({
      style: {
        image: t,
        x: e.x,
        y: e.y,
        width: e.width,
        height: e.height
      },
      onload: function onload(t) {
        if ("center" === n) {
          var i = {
            width: t.width,
            height: t.height
          };
          r.setStyle(ts(e, i));
        }
      }
    });
    return r;
  }
  function ts(t, e) {
    var n,
      r = e.width / e.height,
      i = t.height * r;
    i <= t.width ? n = t.height : (i = t.width, n = i / r);
    var o = t.x + t.width / 2,
      a = t.y + t.height / 2;
    return {
      x: o - i / 2,
      y: a - n / 2,
      width: i,
      height: n
    };
  }
  function es(t, e) {
    if (t.applyTransform) {
      var n = t.getBoundingRect(),
        r = n.calculateTransform(e);
      t.applyTransform(r);
    }
  }
  function ns(t) {
    return ai(t.shape, t.shape, t.style), t;
  }
  function rs(t) {
    return si(t.shape, t.shape, t.style), t;
  }
  function is(t, e, n, r, i, o, a) {
    var s,
      l = !1;
    "function" == typeof i ? (a = o, o = i, i = null) : I(i) && (o = i.cb, a = i.during, l = i.isFrom, s = i.removeOpt, i = i.dataIndex);
    var u,
      h = "update" === t,
      c = "remove" === t;
    if (r && r.ecModel) {
      var f = r.ecModel.getUpdatePayload();
      u = f && f.animation;
    }
    var p = r && r.isAnimationEnabled();
    if (c || e.stopAnimation("remove"), p) {
      var d = void 0,
        g = void 0,
        v = void 0;
      u ? (d = u.duration || 0, g = u.easing || "cubicOut", v = u.delay || 0) : c ? (s = s || {}, d = F(s.duration, 200), g = F(s.easing, "cubicOut"), v = 0) : (d = r.getShallow(h ? "animationDurationUpdate" : "animationDuration"), g = r.getShallow(h ? "animationEasingUpdate" : "animationEasing"), v = r.getShallow(h ? "animationDelayUpdate" : "animationDelay")), "function" == typeof v && (v = v(i, r.getAnimationDelayParams ? r.getAnimationDelayParams(e, i) : null)), "function" == typeof d && (d = d(i)), d > 0 ? l ? e.animateFrom(n, {
        duration: d,
        delay: v || 0,
        easing: g,
        done: o,
        force: !!o || !!a,
        scope: t,
        during: a
      }) : e.animateTo(n, {
        duration: d,
        delay: v || 0,
        easing: g,
        done: o,
        force: !!o || !!a,
        setToFinal: !0,
        scope: t,
        during: a
      }) : (e.stopAnimation(), !l && e.attr(n), o && o());
    } else e.stopAnimation(), !l && e.attr(n), a && a(1), o && o();
  }
  function os(t, e, n, r, i, o) {
    is("update", t, e, n, r, i, o);
  }
  function as(t, e, n, r, i, o) {
    is("init", t, e, n, r, i, o);
  }
  function ss(t, e, n, r, i, o) {
    hs(t) || is("remove", t, e, n, r, i, o);
  }
  function ls(t, e, n, r) {
    t.removeTextContent(), t.removeTextGuideLine(), ss(t, {
      style: {
        opacity: 0
      }
    }, e, n, r);
  }
  function us(t, e, n) {
    function r() {
      t.parent && t.parent.remove(t);
    }
    t.isGroup ? t.traverse(function (t) {
      t.isGroup || ls(t, e, n, r);
    }) : ls(t, e, n, r);
  }
  function hs(t) {
    if (!t.__zr) return !0;
    for (var e = 0; e < t.animators.length; e++) {
      var n = t.animators[e];
      if ("remove" === n.scope) return !0;
    }
    return !1;
  }
  function cs(t, e) {
    for (var n = Fe([]); t && t !== e;) {
      He(n, t.getLocalTransform(), n), t = t.parent;
    }
    return n;
  }
  function fs(t, e, n) {
    return e && !g(e) && (e = Id.getLocalTransform(e)), n && (e = Ue([], e)), ge([], t, e);
  }
  function ps(t, e, n) {
    var r = 0 === e[4] || 0 === e[5] || 0 === e[0] ? 1 : Math.abs(2 * e[4] / e[0]),
      i = 0 === e[4] || 0 === e[5] || 0 === e[2] ? 1 : Math.abs(2 * e[4] / e[2]),
      o = ["left" === t ? -r : "right" === t ? r : 0, "top" === t ? -i : "bottom" === t ? i : 0];
    return o = fs(o, e, n), Math.abs(o[0]) > Math.abs(o[1]) ? o[0] > 0 ? "right" : "left" : o[1] > 0 ? "bottom" : "top";
  }
  function ds(t) {
    return !t.isGroup;
  }
  function gs(t) {
    return null != t.shape;
  }
  function vs(t, e, n) {
    function r(t) {
      var e = {};
      return t.traverse(function (t) {
        ds(t) && t.anid && (e[t.anid] = t);
      }), e;
    }
    function i(t) {
      var e = {
        x: t.x,
        y: t.y,
        rotation: t.rotation
      };
      return gs(t) && (e.shape = h({}, t.shape)), e;
    }
    if (t && e) {
      var o = r(t);
      e.traverse(function (t) {
        if (ds(t) && t.anid) {
          var e = o[t.anid];
          if (e) {
            var r = i(t);
            t.attr(i(e)), os(t, r, n, Jm(t).dataIndex);
          }
        }
      });
    }
  }
  function ys(t, e) {
    return y(t, function (t) {
      var n = t[0];
      n = m_(n, e.x), n = __(n, e.x + e.width);
      var r = t[1];
      return r = m_(r, e.y), r = __(r, e.y + e.height), [n, r];
    });
  }
  function ms(t, e) {
    var n = m_(t.x, e.x),
      r = __(t.x + t.width, e.x + e.width),
      i = m_(t.y, e.y),
      o = __(t.y + t.height, e.y + e.height);
    return r >= n && o >= i ? {
      x: n,
      y: i,
      width: r - n,
      height: o - i
    } : void 0;
  }
  function _s(t, e, n) {
    var r = h({
        rectHover: !0
      }, e),
      i = r.style = {
        strokeNoScale: !0
      };
    return n = n || {
      x: -1,
      y: -1,
      width: 2,
      height: 2
    }, t ? 0 === t.indexOf("image://") ? (i.image = t.slice(8), c(i, n), new jv(r)) : Qa(t.replace("path://", ""), r, n, "center") : void 0;
  }
  function xs(t, e, n, r, i) {
    for (var o = 0, a = i[i.length - 1]; o < i.length; o++) {
      var s = i[o];
      if (ws(t, e, n, r, s[0], s[1], a[0], a[1])) return !0;
      a = s;
    }
  }
  function ws(t, e, n, r, i, o, a, s) {
    var l = n - t,
      u = r - e,
      h = a - i,
      c = s - o,
      f = bs(h, c, l, u);
    if (Ss(f)) return !1;
    var p = t - i,
      d = e - o,
      g = bs(p, d, l, u) / f;
    if (0 > g || g > 1) return !1;
    var v = bs(p, d, h, c) / f;
    return 0 > v || v > 1 ? !1 : !0;
  }
  function bs(t, e, n, r) {
    return t * r - n * e;
  }
  function Ss(t) {
    return 1e-6 >= t && t >= -1e-6;
  }
  function Ms(t, e) {
    for (var n = 0; n < a_.length; n++) {
      var r = a_[n],
        i = e[r],
        o = t.ensureState(r);
      o.style = o.style || {}, o.style.text = i;
    }
    var a = t.currentStates.slice();
    t.clearStates(!0), t.setStyle({
      text: e.normal
    }), t.useStates(a, !0);
  }
  function Ts(t, e, n) {
    var r,
      i = t.labelFetcher,
      o = t.labelDataIndex,
      a = t.labelDimIndex,
      s = e.normal;
    i && (r = i.getFormattedLabel(o, "normal", null, a, s && s.get("formatter"), null != n ? {
      value: n
    } : null)), null == r && (r = T(t.defaultText) ? t.defaultText(o, t, n) : t.defaultText);
    for (var l = {
        normal: r
      }, u = 0; u < a_.length; u++) {
      var h = a_[u],
        c = e[h];
      l[h] = F(i ? i.getFormattedLabel(o, h, null, a, c && c.get("formatter")) : null, r);
    }
    return l;
  }
  function Cs(t, e, n, r, i) {
    var o = {};
    return ks(o, t, n, r, i), e && h(o, e), o;
  }
  function ks(t, e, n, r, i) {
    n = n || T_;
    var o,
      a = e.ecModel,
      s = a && a.option.textStyle,
      l = Ds(e);
    if (l) {
      o = {};
      for (var u in l) {
        if (l.hasOwnProperty(u)) {
          var h = e.getModel(["rich", u]);
          Is(o[u] = {}, h, s, n, r, i, !1, !0);
        }
      }
    }
    o && (t.rich = o);
    var c = e.get("overflow");
    c && (t.overflow = c);
    var f = e.get("minMargin");
    null != f && (t.margin = f), Is(t, e, s, n, r, i, !0, !1);
  }
  function Ds(t) {
    for (var e; t && t !== t.ecModel;) {
      var n = (t.option || T_).rich;
      if (n) {
        e = e || {};
        for (var r = w(n), i = 0; i < r.length; i++) {
          var o = r[i];
          e[o] = 1;
        }
      }
      t = t.parentModel;
    }
    return e;
  }
  function Is(t, e, n, r, i, o, a, s) {
    n = !i && n || T_;
    var l = r && r.inheritColor,
      u = e.getShallow("color"),
      h = e.getShallow("textBorderColor"),
      c = F(e.getShallow("opacity"), n.opacity);
    ("inherit" === u || "auto" === u) && (u = l ? l : null), ("inherit" === h || "auto" === h) && (h = l ? l : null), o || (u = u || n.color, h = h || n.textBorderColor), null != u && (t.fill = u), null != h && (t.stroke = h);
    var f = F(e.getShallow("textBorderWidth"), n.textBorderWidth);
    null != f && (t.lineWidth = f);
    var p = F(e.getShallow("textBorderType"), n.textBorderType);
    null != p && (t.lineDash = p);
    var d = F(e.getShallow("textBorderDashOffset"), n.textBorderDashOffset);
    null != d && (t.lineDashOffset = d), i || null != c || s || (c = r && r.defaultOpacity), null != c && (t.opacity = c), i || o || null == t.fill && r.inheritColor && (t.fill = r.inheritColor);
    for (var g = 0; g < C_.length; g++) {
      var v = C_[g],
        y = F(e.getShallow(v), n[v]);
      null != y && (t[v] = y);
    }
    for (var g = 0; g < k_.length; g++) {
      var v = k_[g],
        y = e.getShallow(v);
      null != y && (t[v] = y);
    }
    if (null == t.verticalAlign) {
      var m = e.getShallow("baseline");
      null != m && (t.verticalAlign = m);
    }
    if (!a || !r.disableBox) {
      for (var g = 0; g < D_.length; g++) {
        var v = D_[g],
          y = e.getShallow(v);
        null != y && (t[v] = y);
      }
      var _ = e.getShallow("borderType");
      null != _ && (t.borderDash = _), "auto" !== t.backgroundColor && "inherit" !== t.backgroundColor || !l || (t.backgroundColor = l), "auto" !== t.borderColor && "inherit" !== t.borderColor || !l || (t.borderColor = l);
    }
  }
  function As(t, e) {
    var n = e && e.getModel("textStyle");
    return G([t.fontStyle || n && n.getShallow("fontStyle") || "", t.fontWeight || n && n.getShallow("fontWeight") || "", (t.fontSize || n && n.getShallow("fontSize") || 12) + "px", t.fontFamily || n && n.getShallow("fontFamily") || "sans-serif"].join(" "));
  }
  function Ps(t, e, n, r) {
    if (t) {
      var i = I_(t);
      i.prevValue = i.value, i.value = n;
      var o = e.normal;
      i.valueAnimation = o.get("valueAnimation"), i.valueAnimation && (i.precision = o.get("precision"), i.defaultInterpolatedText = r, i.statesModels = e);
    }
  }
  function Ls(t, e, n, r) {
    function i(r) {
      var i = Zo(n, o.precision, s, l, r),
        u = Ts({
          labelDataIndex: e,
          defaultText: a ? a(i) : i + ""
        }, o.statesModels, i);
      Ms(t, u);
    }
    var o = I_(t);
    if (o.valueAnimation) {
      var a = o.defaultInterpolatedText,
        s = o.prevValue,
        l = o.value;
      (null == s ? as : os)(t, {}, r, e, null, i);
    }
  }
  function Os(t) {
    return [t || "", H_++].join("_");
  }
  function Rs(t) {
    var e = {};
    t.registerSubTypeDefaulter = function (t, n) {
      var r = Ko(t);
      e[r.main] = n;
    }, t.determineSubType = function (n, r) {
      var i = r.type;
      if (!i) {
        var o = Ko(n).main;
        t.hasSubTypes(n) && e[o] && (i = e[o](r));
      }
      return i;
    };
  }
  function Es(t, e) {
    function n(t) {
      var n = {},
        o = [];
      return v(t, function (a) {
        var s = r(n, a),
          l = s.originalDeps = e(a),
          u = i(l, t);
        s.entryCount = u.length, 0 === s.entryCount && o.push(a), v(u, function (t) {
          f(s.predecessor, t) < 0 && s.predecessor.push(t);
          var e = r(n, t);
          f(e.successor, t) < 0 && e.successor.push(a);
        });
      }), {
        graph: n,
        noEntryList: o
      };
    }
    function r(t, e) {
      return t[e] || (t[e] = {
        predecessor: [],
        successor: []
      }), t[e];
    }
    function i(t, e) {
      var n = [];
      return v(t, function (t) {
        f(e, t) >= 0 && n.push(t);
      }), n;
    }
    t.topologicalTravel = function (t, e, r, i) {
      function o(t) {
        l[t].entryCount--, 0 === l[t].entryCount && u.push(t);
      }
      function a(t) {
        h[t] = !0, o(t);
      }
      if (t.length) {
        var s = n(e),
          l = s.graph,
          u = s.noEntryList,
          h = {};
        for (v(t, function (t) {
          h[t] = !0;
        }); u.length;) {
          var c = u.pop(),
            f = l[c],
            p = !!h[c];
          p && (r.call(i, c, f.originalDeps.slice()), delete h[c]), v(f.successor, p ? a : o);
        }
        v(h, function () {
          var t = "";
          throw new Error(t);
        });
      }
    };
  }
  function Bs(t, e) {
    t = t.toUpperCase(), Y_[t] = new N_(e), X_[t] = e;
  }
  function zs(t) {
    if (C(t)) {
      var e = X_[t.toUpperCase()] || {};
      return t === G_ || t === U_ ? s(e) : l(s(e), s(X_[q_]), !1);
    }
    return l(s(t), s(X_[q_]), !1);
  }
  function Fs(t) {
    return Y_[t];
  }
  function Ns() {
    return Y_[q_];
  }
  function Hs(t, e) {
    return t += "", "0000".substr(0, e - t.length) + t;
  }
  function Vs(t) {
    switch (t) {
      case "half-year":
      case "quarter":
        return "month";
      case "week":
      case "half-week":
        return "day";
      case "half-day":
      case "quarter-day":
        return "hour";
      default:
        return t;
    }
  }
  function Ws(t) {
    return t === Vs(t);
  }
  function Gs(t) {
    switch (t) {
      case "year":
      case "month":
        return "day";
      case "millisecond":
        return "millisecond";
      default:
        return "second";
    }
  }
  function Us(t, e, n, r) {
    var i = fo(t),
      o = i[js(n)](),
      a = i[Zs(n)]() + 1,
      s = Math.floor((a - 1) / 4) + 1,
      l = i[Ks(n)](),
      u = i["get" + (n ? "UTC" : "") + "Day"](),
      h = i[$s(n)](),
      c = (h - 1) % 12 + 1,
      f = i[Qs(n)](),
      p = i[Js(n)](),
      d = i[tl(n)](),
      g = r instanceof N_ ? r : Fs(r || j_) || Ns(),
      v = g.getModel("time"),
      y = v.get("month"),
      m = v.get("monthAbbr"),
      _ = v.get("dayOfWeek"),
      x = v.get("dayOfWeekAbbr");
    return (e || "").replace(/{yyyy}/g, o + "").replace(/{yy}/g, o % 100 + "").replace(/{Q}/g, s + "").replace(/{MMMM}/g, y[a - 1]).replace(/{MMM}/g, m[a - 1]).replace(/{MM}/g, Hs(a, 2)).replace(/{M}/g, a + "").replace(/{dd}/g, Hs(l, 2)).replace(/{d}/g, l + "").replace(/{eeee}/g, _[u]).replace(/{ee}/g, x[u]).replace(/{e}/g, u + "").replace(/{HH}/g, Hs(h, 2)).replace(/{H}/g, h + "").replace(/{hh}/g, Hs(c + "", 2)).replace(/{h}/g, c + "").replace(/{mm}/g, Hs(f, 2)).replace(/{m}/g, f + "").replace(/{ss}/g, Hs(p, 2)).replace(/{s}/g, p + "").replace(/{SSS}/g, Hs(d, 3)).replace(/{S}/g, d + "");
  }
  function qs(t, e, n, r, i) {
    var o = null;
    if ("string" == typeof n) o = n;else if ("function" == typeof n) o = n(t.value, e, {
      level: t.level
    });else {
      var a = h({}, tx);
      if (t.level > 0) for (var s = 0; s < rx.length; ++s) {
        a[rx[s]] = "{primary|" + a[rx[s]] + "}";
      }
      var l = n ? n.inherit === !1 ? n : c(n, a) : a,
        u = Xs(t.value, i);
      if (l[u]) o = l[u];else if (l.inherit) {
        for (var f = ix.indexOf(u), s = f - 1; s >= 0; --s) {
          if (l[u]) {
            o = l[u];
            break;
          }
        }
        o = o || a.none;
      }
      if (M(o)) {
        var p = null == t.level ? 0 : t.level >= 0 ? t.level : o.length + t.level;
        p = Math.min(p, o.length - 1), o = o[p];
      }
    }
    return Us(new Date(t.value), o, i, r);
  }
  function Xs(t, e) {
    var n = fo(t),
      r = n[Zs(e)]() + 1,
      i = n[Ks(e)](),
      o = n[$s(e)](),
      a = n[Qs(e)](),
      s = n[Js(e)](),
      l = n[tl(e)](),
      u = 0 === l,
      h = u && 0 === s,
      c = h && 0 === a,
      f = c && 0 === o,
      p = f && 1 === i,
      d = p && 1 === r;
    return d ? "year" : p ? "month" : f ? "day" : c ? "hour" : h ? "minute" : u ? "second" : "millisecond";
  }
  function Ys(t, e, n) {
    var r = "number" == typeof t ? fo(t) : t;
    switch (e = e || Xs(t, n)) {
      case "year":
        return r[js(n)]();
      case "half-year":
        return r[Zs(n)]() >= 6 ? 1 : 0;
      case "quarter":
        return Math.floor((r[Zs(n)]() + 1) / 4);
      case "month":
        return r[Zs(n)]();
      case "day":
        return r[Ks(n)]();
      case "half-day":
        return r[$s(n)]() / 24;
      case "hour":
        return r[$s(n)]();
      case "minute":
        return r[Qs(n)]();
      case "second":
        return r[Js(n)]();
      case "millisecond":
        return r[tl(n)]();
    }
  }
  function js(t) {
    return t ? "getUTCFullYear" : "getFullYear";
  }
  function Zs(t) {
    return t ? "getUTCMonth" : "getMonth";
  }
  function Ks(t) {
    return t ? "getUTCDate" : "getDate";
  }
  function $s(t) {
    return t ? "getUTCHours" : "getHours";
  }
  function Qs(t) {
    return t ? "getUTCMinutes" : "getMinutes";
  }
  function Js(t) {
    return t ? "getUTCSeconds" : "getSeconds";
  }
  function tl(t) {
    return t ? "getUTCSeconds" : "getSeconds";
  }
  function el(t) {
    return t ? "setUTCFullYear" : "setFullYear";
  }
  function nl(t) {
    return t ? "setUTCMonth" : "setMonth";
  }
  function rl(t) {
    return t ? "setUTCDate" : "setDate";
  }
  function il(t) {
    return t ? "setUTCHours" : "setHours";
  }
  function ol(t) {
    return t ? "setUTCMinutes" : "setMinutes";
  }
  function al(t) {
    return t ? "setUTCSeconds" : "setSeconds";
  }
  function sl(t) {
    return t ? "setUTCSeconds" : "setSeconds";
  }
  function ll(t, e, n, r, i, o, a, s) {
    var l = new Xy({
      style: {
        text: t,
        font: e,
        align: n,
        verticalAlign: r,
        padding: i,
        rich: o,
        overflow: a ? "truncate" : null,
        lineHeight: s
      }
    });
    return l.getBoundingRect();
  }
  function ul(t) {
    if (!xo(t)) return C(t) ? t : "-";
    var e = (t + "").split(".");
    return e[0].replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g, "$1,") + (e.length > 1 ? "." + e[1] : "");
  }
  function hl(t, e) {
    return t = (t || "").toLowerCase().replace(/-(.)/g, function (t, e) {
      return e.toUpperCase();
    }), e && t && (t = t.charAt(0).toUpperCase() + t.slice(1)), t;
  }
  function cl(t) {
    return null == t ? "" : (t + "").replace(ax, function (t, e) {
      return sx[e];
    });
  }
  function fl(t, e, n) {
    function r(t) {
      return t && G(t) ? t : "-";
    }
    function i(t) {
      return !(null == t || isNaN(t) || !isFinite(t));
    }
    var o = "yyyy-MM-dd hh:mm:ss",
      a = "time" === e,
      s = t instanceof Date;
    if (a || s) {
      var l = a ? fo(t) : t;
      if (!isNaN(+l)) return Us(l, o, n);
      if (s) return "-";
    }
    if ("ordinal" === e) return k(t) ? r(t) : D(t) && i(t) ? t + "" : "-";
    var u = _o(t);
    return i(u) ? ul(u) : k(t) ? r(t) : "-";
  }
  function pl(t, e, n) {
    M(e) || (e = [e]);
    var r = e.length;
    if (!r) return "";
    for (var i = e[0].$vars || [], o = 0; o < i.length; o++) {
      var a = lx[o];
      t = t.replace(ux(a), ux(a, 0));
    }
    for (var s = 0; r > s; s++) {
      for (var l = 0; l < i.length; l++) {
        var u = e[s][i[l]];
        t = t.replace(ux(lx[l], s), n ? cl(u) : u);
      }
    }
    return t;
  }
  function dl(t, e, n) {
    return v(e, function (e, r) {
      t = t.replace("{" + r + "}", n ? cl(e) : e);
    }), t;
  }
  function gl(t, e) {
    var n = C(t) ? {
        color: t,
        extraCssText: e
      } : t || {},
      r = n.color,
      i = n.type;
    e = n.extraCssText;
    var o = n.renderMode || "html";
    if (!r) return "";
    if ("html" === o) return "subItem" === i ? '<span style="display:inline-block;vertical-align:middle;margin-right:8px;margin-left:3px;border-radius:4px;width:4px;height:4px;background-color:' + cl(r) + ";" + (e || "") + '"></span>' : '<span style="display:inline-block;margin-right:4px;border-radius:10px;width:10px;height:10px;background-color:' + cl(r) + ";" + (e || "") + '"></span>';
    var a = n.markerId || "markerX";
    return {
      renderMode: o,
      content: "{" + a + "|}  ",
      style: "subItem" === i ? {
        width: 4,
        height: 4,
        borderRadius: 2,
        backgroundColor: r
      } : {
        width: 10,
        height: 10,
        borderRadius: 5,
        backgroundColor: r
      }
    };
  }
  function vl(t, e, n) {
    ("week" === t || "month" === t || "quarter" === t || "half-year" === t || "year" === t) && (t = "MM-dd\nyyyy");
    var r = fo(e),
      i = n ? "UTC" : "",
      o = r["get" + i + "FullYear"](),
      a = r["get" + i + "Month"]() + 1,
      s = r["get" + i + "Date"](),
      l = r["get" + i + "Hours"](),
      u = r["get" + i + "Minutes"](),
      h = r["get" + i + "Seconds"](),
      c = r["get" + i + "Milliseconds"]();
    return t = t.replace("MM", Hs(a, 2)).replace("M", a).replace("yyyy", o).replace("yy", o % 100 + "").replace("dd", Hs(s, 2)).replace("d", s).replace("hh", Hs(l, 2)).replace("h", l).replace("mm", Hs(u, 2)).replace("m", u).replace("ss", Hs(h, 2)).replace("s", h).replace("SSS", Hs(c, 3));
  }
  function yl(t) {
    return t ? t.charAt(0).toUpperCase() + t.substr(1) : t;
  }
  function ml(t, e) {
    return e = e || "transparent", C(t) ? t : I(t) ? t.colorStops && (t.colorStops[0] || {}).color || e : e;
  }
  function _l(t, e) {
    if ("_blank" === e || "blank" === e) {
      var n = window.open();
      n.opener = null, n.location.href = t;
    } else window.open(t, e);
  }
  function xl(t, e, n, r, i) {
    var o = 0,
      a = 0;
    null == r && (r = 1 / 0), null == i && (i = 1 / 0);
    var s = 0;
    e.eachChild(function (l, u) {
      var h,
        c,
        f = l.getBoundingRect(),
        p = e.childAt(u + 1),
        d = p && p.getBoundingRect();
      if ("horizontal" === t) {
        var g = f.width + (d ? -d.x + f.x : 0);
        h = o + g, h > r || l.newline ? (o = 0, h = g, a += s + n, s = f.height) : s = Math.max(s, f.height);
      } else {
        var v = f.height + (d ? -d.y + f.y : 0);
        c = a + v, c > i || l.newline ? (o += s + n, a = 0, c = v, s = f.width) : s = Math.max(s, f.width);
      }
      l.newline || (l.x = o, l.y = a, l.markRedraw(), "horizontal" === t ? o = h + n : a = c + n);
    });
  }
  function wl(t, e, n) {
    n = ox(n || 0);
    var r = e.width,
      i = e.height,
      o = ro(t.left, r),
      a = ro(t.top, i),
      s = ro(t.right, r),
      l = ro(t.bottom, i),
      u = ro(t.width, r),
      h = ro(t.height, i),
      c = n[2] + n[0],
      f = n[1] + n[3],
      p = t.aspect;
    switch (isNaN(u) && (u = r - s - f - o), isNaN(h) && (h = i - l - c - a), null != p && (isNaN(u) && isNaN(h) && (p > r / i ? u = .8 * r : h = .8 * i), isNaN(u) && (u = p * h), isNaN(h) && (h = u / p)), isNaN(o) && (o = r - s - u - f), isNaN(a) && (a = i - l - h - c), t.left || t.right) {
      case "center":
        o = r / 2 - u / 2 - n[3];
        break;
      case "right":
        o = r - u - f;
    }
    switch (t.top || t.bottom) {
      case "middle":
      case "center":
        a = i / 2 - h / 2 - n[0];
        break;
      case "bottom":
        a = i - h - c;
    }
    o = o || 0, a = a || 0, isNaN(u) && (u = r - f - o - (s || 0)), isNaN(h) && (h = i - c - a - (l || 0));
    var d = new tg(o + n[3], a + n[0], u, h);
    return d.margin = n, d;
  }
  function bl(t) {
    var e = t.layoutMode || t.constructor.layoutMode;
    return I(e) ? e : e ? {
      type: e
    } : null;
  }
  function Sl(t, e, n) {
    function r(n, r) {
      var a = {},
        l = 0,
        u = {},
        h = 0,
        c = 2;
      if (cx(n, function (e) {
        u[e] = t[e];
      }), cx(n, function (t) {
        i(e, t) && (a[t] = u[t] = e[t]), o(a, t) && l++, o(u, t) && h++;
      }), s[r]) return o(e, n[1]) ? u[n[2]] = null : o(e, n[2]) && (u[n[1]] = null), u;
      if (h !== c && l) {
        if (l >= c) return a;
        for (var f = 0; f < n.length; f++) {
          var p = n[f];
          if (!i(a, p) && i(t, p)) {
            a[p] = t[p];
            break;
          }
        }
        return a;
      }
      return u;
    }
    function i(t, e) {
      return t.hasOwnProperty(e);
    }
    function o(t, e) {
      return null != t[e] && "auto" !== t[e];
    }
    function a(t, e, n) {
      cx(t, function (t) {
        e[t] = n[t];
      });
    }
    var s = n && n.ignoreSize;
    !M(s) && (s = [s, s]);
    var l = r(px[0], 0),
      u = r(px[1], 1);
    a(px[0], t, l), a(px[1], t, u);
  }
  function Ml(t) {
    return Tl({}, t);
  }
  function Tl(t, e) {
    return e && t && cx(fx, function (n) {
      e.hasOwnProperty(n) && (t[n] = e[n]);
    }), t;
  }
  function Cl(t) {
    var e = [];
    return v(gx.getClassesByMainType(t), function (t) {
      e = e.concat(t.dependencies || t.prototype.dependencies || []);
    }), e = y(e, function (t) {
      return Ko(t).main;
    }), "dataset" !== t && f(e, "dataset") <= 0 && e.unshift("dataset"), e;
  }
  function kl(t) {
    Lx(t).datasetMap = X();
  }
  function Dl(t, e) {
    var n = t ? t.metaRawOption : null,
      r = F(e.seriesLayoutBy, n ? n.seriesLayoutBy : null),
      i = F(e.sourceHeader, t ? t.startIndex : null),
      o = F(e.dimensions, t ? t.dimensionsDefine : null);
    return {
      seriesLayoutBy: r,
      sourceHeader: i,
      dimensions: o
    };
  }
  function Il(t, e, n) {
    function r(t, e, n) {
      for (var r = 0; n > r; r++) {
        t.push(e + r);
      }
    }
    function i(t) {
      var e = t.dimsDef;
      return e ? e.length : 1;
    }
    var o = {},
      a = Al(e);
    if (!a || !t) return o;
    var s,
      l,
      u = [],
      h = [],
      c = e.ecModel,
      f = Lx(c).datasetMap,
      p = a.uid + "_" + n.seriesLayoutBy;
    t = t.slice(), v(t, function (e, n) {
      var r = I(e) ? e : t[n] = {
        name: e
      };
      "ordinal" === r.type && null == s && (s = n, l = i(r)), o[r.name] = [];
    });
    var d = f.get(p) || f.set(p, {
      categoryWayDim: l,
      valueWayDim: 0
    });
    return v(t, function (t, e) {
      var n = t.name,
        a = i(t);
      if (null == s) {
        var l = d.valueWayDim;
        r(o[n], l, a), r(h, l, a), d.valueWayDim += a;
      } else if (s === e) r(o[n], 0, a), r(u, 0, a);else {
        var l = d.categoryWayDim;
        r(o[n], l, a), r(h, l, a), d.categoryWayDim += a;
      }
    }), u.length && (o.itemName = u), h.length && (o.seriesName = h), o;
  }
  function Al(t) {
    var e = t.get("data", !0);
    return e ? void 0 : Xo(t.ecModel, "dataset", {
      index: t.get("datasetIndex", !0),
      id: t.get("datasetId", !0)
    }, qm).models[0];
  }
  function Pl(t) {
    return t.get("transform", !0) || t.get("fromTransformResult", !0) ? Xo(t.ecModel, "dataset", {
      index: t.get("fromDatasetIndex", !0),
      id: t.get("fromDatasetId", !0)
    }, qm).models : [];
  }
  function Ll(t, e) {
    return Ol(t.data, t.sourceFormat, t.seriesLayoutBy, t.dimensionsDefine, t.startIndex, e);
  }
  function Ol(t, e, n, r, i, o) {
    function a(t) {
      var e = C(t);
      return null != t && isFinite(t) && "" !== t ? e ? Px.Might : Px.Not : e && "-" !== t ? Px.Must : void 0;
    }
    var s,
      l = 5;
    if (P(t)) return Px.Not;
    var u, h;
    if (r) {
      var c = r[o];
      I(c) ? (u = c.name, h = c.type) : C(c) && (u = c);
    }
    if (null != h) return "ordinal" === h ? Px.Must : Px.Not;
    if (e === Mx) {
      var f = t;
      if (n === Ax) {
        for (var p = f[o], d = 0; d < (p || []).length && l > d; d++) {
          if (null != (s = a(p[i + d]))) return s;
        }
      } else for (var d = 0; d < f.length && l > d; d++) {
        var g = f[i + d];
        if (g && null != (s = a(g[o]))) return s;
      }
    } else if (e === Tx) {
      var v = t;
      if (!u) return Px.Not;
      for (var d = 0; d < v.length && l > d; d++) {
        var y = v[d];
        if (y && null != (s = a(y[u]))) return s;
      }
    } else if (e === Cx) {
      var m = t;
      if (!u) return Px.Not;
      var p = m[u];
      if (!p || P(p)) return Px.Not;
      for (var d = 0; d < p.length && l > d; d++) {
        if (null != (s = a(p[d]))) return s;
      }
    } else if (e === Sx) for (var _ = t, d = 0; d < _.length && l > d; d++) {
      var y = _[d],
        x = ko(y);
      if (!M(x)) return Px.Not;
      if (null != (s = a(x[o]))) return s;
    }
    return Px.Not;
  }
  function Rl(t, e, n) {
    var r = Ox.get(e);
    if (!r) return n;
    var i = r(t);
    return i ? n.concat(i) : n;
  }
  function El(t, e) {
    for (var n = t.length, r = 0; n > r; r++) {
      if (t[r].length > e) return t[r];
    }
    return t[n - 1];
  }
  function Bl(t, e, n, r, i, o, a) {
    o = o || t;
    var s = e(o),
      l = s.paletteIdx || 0,
      u = s.paletteNameMap = s.paletteNameMap || {};
    if (u.hasOwnProperty(i)) return u[i];
    var h = null != a && r ? El(r, a) : n;
    if (h = h || n, h && h.length) {
      var c = h[l];
      return i && (u[i] = c), s.paletteIdx = (l + 1) % h.length, c;
    }
  }
  function zl(t, e) {
    e(t).paletteIdx = 0, e(t).paletteNameMap = {};
  }
  function Fl(t, e) {
    if (e) {
      var n = e.seriesIndex,
        r = e.seriesId,
        i = e.seriesName;
      return null != n && t.componentIndex !== n || null != r && t.id !== r || null != i && t.name !== i;
    }
  }
  function Nl(t, e) {
    var n = t.color && !t.colorLayer;
    v(e, function (e, r) {
      "colorLayer" === r && n || gx.hasClass(r) || ("object" == _typeof(e) ? t[r] = t[r] ? l(t[r], e, !1) : s(e) : null == t[r] && (t[r] = e));
    });
  }
  function Hl(t, e, n) {
    if (M(e)) {
      var r = X();
      return v(e, function (t) {
        if (null != t) {
          var e = Fo(t, null);
          null != e && r.set(t, !0);
        }
      }), _(n, function (e) {
        return e && r.get(e[t]);
      });
    }
    var i = Fo(e, null);
    return _(n, function (e) {
      return e && null != i && e[t] === i;
    });
  }
  function Vl(t, e) {
    return e.hasOwnProperty("subType") ? _(t, function (t) {
      return t && t.subType === e.subType;
    }) : t;
  }
  function Wl(t) {
    var e = X();
    return t && v(To(t.replaceMerge), function (t) {
      e.set(t, !0);
    }), {
      replaceMergeMainTypeMap: e
    };
  }
  function Gl(t, e, n) {
    function r(t) {
      v(e, function (e) {
        e(t, n);
      });
    }
    var i,
      o,
      a = [],
      s = t.baseOption,
      l = t.timeline,
      u = t.options,
      h = t.media,
      c = !!t.media,
      f = !!(u || l || s && s.timeline);
    return s ? (o = s, o.timeline || (o.timeline = l)) : ((f || c) && (t.options = t.media = null), o = t), c && M(h) && v(h, function (t) {
      t && t.option && (t.query ? a.push(t) : i || (i = t));
    }), r(o), v(u, function (t) {
      return r(t);
    }), v(a, function (t) {
      return r(t.option);
    }), {
      baseOption: o,
      timelineOptions: u || [],
      mediaDefault: i,
      mediaList: a
    };
  }
  function Ul(t, e, n) {
    var r = {
        width: e,
        height: n,
        aspectratio: e / n
      },
      i = !0;
    return v(t, function (t, e) {
      var n = e.match(Yx);
      if (n && n[1] && n[2]) {
        var o = n[1],
          a = n[2].toLowerCase();
        ql(r[a], t, o) || (i = !1);
      }
    }), i;
  }
  function ql(t, e, n) {
    return "min" === n ? t >= e : "max" === n ? e >= t : t === e;
  }
  function Xl(t, e) {
    return t.join(",") === e.join(",");
  }
  function Yl(t) {
    var e = t && t.itemStyle;
    if (e) for (var n = 0, r = $x.length; r > n; n++) {
      var i = $x[n],
        o = e.normal,
        a = e.emphasis;
      o && o[i] && (t[i] = t[i] || {}, t[i].normal ? l(t[i].normal, o[i]) : t[i].normal = o[i], o[i] = null), a && a[i] && (t[i] = t[i] || {}, t[i].emphasis ? l(t[i].emphasis, a[i]) : t[i].emphasis = a[i], a[i] = null);
    }
  }
  function jl(t, e, n) {
    if (t && t[e] && (t[e].normal || t[e].emphasis)) {
      var r = t[e].normal,
        i = t[e].emphasis;
      r && (n ? (t[e].normal = t[e].emphasis = null, c(t[e], r)) : t[e] = r), i && (t.emphasis = t.emphasis || {}, t.emphasis[e] = i, i.focus && (t.emphasis.focus = i.focus), i.blurScope && (t.emphasis.blurScope = i.blurScope));
    }
  }
  function Zl(t) {
    jl(t, "itemStyle"), jl(t, "lineStyle"), jl(t, "areaStyle"), jl(t, "label"), jl(t, "labelLine"), jl(t, "upperLabel"), jl(t, "edgeLabel");
  }
  function Kl(t, e) {
    var n = Kx(t) && t[e],
      r = Kx(n) && n.textStyle;
    if (r) for (var i = 0, o = Gm.length; o > i; i++) {
      var a = Gm[i];
      r.hasOwnProperty(a) && (n[a] = r[a]);
    }
  }
  function $l(t) {
    t && (Zl(t), Kl(t, "label"), t.emphasis && Kl(t.emphasis, "label"));
  }
  function Ql(t) {
    if (Kx(t)) {
      Yl(t), Zl(t), Kl(t, "label"), Kl(t, "upperLabel"), Kl(t, "edgeLabel"), t.emphasis && (Kl(t.emphasis, "label"), Kl(t.emphasis, "upperLabel"), Kl(t.emphasis, "edgeLabel"));
      var e = t.markPoint;
      e && (Yl(e), $l(e));
      var n = t.markLine;
      n && (Yl(n), $l(n));
      var r = t.markArea;
      r && $l(r);
      var i = t.data;
      if ("graph" === t.type) {
        i = i || t.nodes;
        var o = t.links || t.edges;
        if (o && !P(o)) for (var a = 0; a < o.length; a++) {
          $l(o[a]);
        }
        v(t.categories, function (t) {
          Zl(t);
        });
      }
      if (i && !P(i)) for (var a = 0; a < i.length; a++) {
        $l(i[a]);
      }
      if (e = t.markPoint, e && e.data) for (var s = e.data, a = 0; a < s.length; a++) {
        $l(s[a]);
      }
      if (n = t.markLine, n && n.data) for (var l = n.data, a = 0; a < l.length; a++) {
        M(l[a]) ? ($l(l[a][0]), $l(l[a][1])) : $l(l[a]);
      }
      "gauge" === t.type ? (Kl(t, "axisLabel"), Kl(t, "title"), Kl(t, "detail")) : "treemap" === t.type ? (jl(t.breadcrumb, "itemStyle"), v(t.levels, function (t) {
        Zl(t);
      })) : "tree" === t.type && Zl(t.leaves);
    }
  }
  function Jl(t) {
    return M(t) ? t : t ? [t] : [];
  }
  function tu(t) {
    return (M(t) ? t[0] : t) || {};
  }
  function eu(t, e) {
    Zx(Jl(t.series), function (t) {
      Kx(t) && Ql(t);
    });
    var n = ["xAxis", "yAxis", "radiusAxis", "angleAxis", "singleAxis", "parallelAxis", "radar"];
    e && n.push("valueAxis", "categoryAxis", "logAxis", "timeAxis"), Zx(n, function (e) {
      Zx(Jl(t[e]), function (t) {
        t && (Kl(t, "axisLabel"), Kl(t.axisPointer, "label"));
      });
    }), Zx(Jl(t.parallel), function (t) {
      var e = t && t.parallelAxisDefault;
      Kl(e, "axisLabel"), Kl(e && e.axisPointer, "label");
    }), Zx(Jl(t.calendar), function (t) {
      jl(t, "itemStyle"), Kl(t, "dayLabel"), Kl(t, "monthLabel"), Kl(t, "yearLabel");
    }), Zx(Jl(t.radar), function (t) {
      Kl(t, "name"), t.name && null == t.axisName && (t.axisName = t.name, delete t.name), null != t.nameGap && null == t.axisNameGap && (t.axisNameGap = t.nameGap, delete t.nameGap);
    }), Zx(Jl(t.geo), function (t) {
      Kx(t) && ($l(t), Zx(Jl(t.regions), function (t) {
        $l(t);
      }));
    }), Zx(Jl(t.timeline), function (t) {
      $l(t), jl(t, "label"), jl(t, "itemStyle"), jl(t, "controlStyle", !0);
      var e = t.data;
      M(e) && v(e, function (t) {
        I(t) && (jl(t, "label"), jl(t, "itemStyle"));
      });
    }), Zx(Jl(t.toolbox), function (t) {
      jl(t, "iconStyle"), Zx(t.feature, function (t) {
        jl(t, "iconStyle");
      });
    }), Kl(tu(t.axisPointer), "label"), Kl(tu(t.tooltip).axisPointer, "label");
  }
  function nu(t, e) {
    for (var n = e.split(","), r = t, i = 0; i < n.length && (r = r && r[n[i]], null != r); i++) {
      ;
    }
    return r;
  }
  function ru(t, e, n, r) {
    for (var i, o = e.split(","), a = t, s = 0; s < o.length - 1; s++) {
      i = o[s], null == a[i] && (a[i] = {}), a = a[i];
    }
    (r || null == a[o[s]]) && (a[o[s]] = n);
  }
  function iu(t) {
    t && v(Qx, function (e) {
      e[0] in t && !(e[1] in t) && (t[e[1]] = t[e[0]]);
    });
  }
  function ou(t) {
    var e = t && t.itemStyle;
    if (e) for (var n = 0; n < tw.length; n++) {
      var r = tw[n][1],
        i = tw[n][0];
      null != e[r] && (e[i] = e[r]);
    }
  }
  function au(t) {
    t && "edge" === t.alignTo && null != t.margin && null == t.edgeDistance && (t.edgeDistance = t.margin);
  }
  function su(t) {
    t && t.downplay && !t.blur && (t.blur = t.downplay);
  }
  function lu(t) {
    t && null != t.focusNodeAdjacency && (t.emphasis = t.emphasis || {}, null == t.emphasis.focus && (t.emphasis.focus = "adjacency"));
  }
  function uu(t, e) {
    if (t) for (var n = 0; n < t.length; n++) {
      e(t[n]), t[n] && uu(t[n].children, e);
    }
  }
  function hu(t, e) {
    eu(t, e), t.series = To(t.series), v(t.series, function (t) {
      if (I(t)) {
        var e = t.type;
        if ("line" === e) null != t.clipOverflow && (t.clip = t.clipOverflow);else if ("pie" === e || "gauge" === e) {
          null != t.clockWise && (t.clockwise = t.clockWise), au(t.label);
          var n = t.data;
          if (n && !P(n)) for (var r = 0; r < n.length; r++) {
            au(n[r]);
          }
          null != t.hoverOffset && (t.emphasis = t.emphasis || {}, (t.emphasis.scaleSize = null) && (t.emphasis.scaleSize = t.hoverOffset));
        } else if ("gauge" === e) {
          var i = nu(t, "pointer.color");
          null != i && ru(t, "itemStyle.color", i);
        } else if ("bar" === e) {
          ou(t), ou(t.backgroundStyle), ou(t.emphasis);
          var n = t.data;
          if (n && !P(n)) for (var r = 0; r < n.length; r++) {
            "object" == _typeof(n[r]) && (ou(n[r]), ou(n[r] && n[r].emphasis));
          }
        } else if ("sunburst" === e) {
          var o = t.highlightPolicy;
          o && (t.emphasis = t.emphasis || {}, t.emphasis.focus || (t.emphasis.focus = o)), su(t), uu(t.data, su);
        } else "graph" === e || "sankey" === e ? lu(t) : "map" === e && (t.mapType && !t.map && (t.map = t.mapType), t.mapLocation && c(t, t.mapLocation));
        null != t.hoverAnimation && (t.emphasis = t.emphasis || {}, t.emphasis && null == t.emphasis.scale && (t.emphasis.scale = t.hoverAnimation)), iu(t);
      }
    }), t.dataRange && (t.visualMap = t.dataRange), v(Jx, function (e) {
      var n = t[e];
      n && (M(n) || (n = [n]), v(n, function (t) {
        iu(t);
      }));
    });
  }
  function cu(t) {
    var e = X();
    t.eachSeries(function (t) {
      var n = t.get("stack");
      if (n) {
        var r = e.get(n) || e.set(n, []),
          i = t.getData(),
          o = {
            stackResultDimension: i.getCalculationInfo("stackResultDimension"),
            stackedOverDimension: i.getCalculationInfo("stackedOverDimension"),
            stackedDimension: i.getCalculationInfo("stackedDimension"),
            stackedByDimension: i.getCalculationInfo("stackedByDimension"),
            isStackedByIndex: i.getCalculationInfo("isStackedByIndex"),
            data: i,
            seriesModel: t
          };
        if (!o.stackedDimension || !o.isStackedByIndex && !o.stackedByDimension) return;
        r.length && i.setCalculationInfo("stackedOnSeries", r[r.length - 1].seriesModel), r.push(o);
      }
    }), e.each(fu);
  }
  function fu(t) {
    v(t, function (e, n) {
      var r = [],
        i = [0 / 0, 0 / 0],
        o = [e.stackResultDimension, e.stackedOverDimension],
        a = e.data,
        s = e.isStackedByIndex,
        l = a.map(o, function (o, l, u) {
          var h = a.get(e.stackedDimension, u);
          if (isNaN(h)) return i;
          var c, f;
          s ? f = a.getRawIndex(u) : c = a.get(e.stackedByDimension, u);
          for (var p = 0 / 0, d = n - 1; d >= 0; d--) {
            var g = t[d];
            if (s || (f = g.data.rawIndexOf(g.stackedByDimension, c)), f >= 0) {
              var v = g.data.getByRawIndex(g.stackResultDimension, f);
              if (h >= 0 && v > 0 || 0 >= h && 0 > v) {
                h += v, p = v;
                break;
              }
            }
          }
          return r[0] = h, r[1] = p, r;
        });
      a.hostModel.setData(l), e.data = l;
    });
  }
  function pu(t) {
    return t instanceof ew;
  }
  function du(t, e, n, r) {
    n = n || mu(t);
    var i = e.seriesLayoutBy,
      o = _u(t, n, i, e.sourceHeader, e.dimensions),
      a = new ew({
        data: t,
        sourceFormat: n,
        seriesLayoutBy: i,
        dimensionsDefine: o.dimensionsDefine,
        startIndex: o.startIndex,
        dimensionsDetectedCount: o.dimensionsDetectedCount,
        encodeDefine: yu(r),
        metaRawOption: s(e)
      });
    return a;
  }
  function gu(t) {
    return new ew({
      data: t,
      sourceFormat: P(t) ? kx : Sx
    });
  }
  function vu(t) {
    return new ew({
      data: t.data,
      sourceFormat: t.sourceFormat,
      seriesLayoutBy: t.seriesLayoutBy,
      dimensionsDefine: s(t.dimensionsDefine),
      startIndex: t.startIndex,
      dimensionsDetectedCount: t.dimensionsDetectedCount,
      encodeDefine: yu(t.encodeDefine)
    });
  }
  function yu(t) {
    return t ? X(t) : null;
  }
  function mu(t) {
    var e = Dx;
    if (P(t)) e = kx;else if (M(t)) {
      0 === t.length && (e = Mx);
      for (var n = 0, r = t.length; r > n; n++) {
        var i = t[n];
        if (null != i) {
          if (M(i)) {
            e = Mx;
            break;
          }
          if (I(i)) {
            e = Tx;
            break;
          }
        }
      }
    } else if (I(t)) {
      for (var o in t) {
        if (Z(t, o) && g(t[o])) {
          e = Cx;
          break;
        }
      }
    } else if (null != t) throw new Error("Invalid data");
    return e;
  }
  function _u(t, e, n, r, i) {
    var o, a;
    if (!t) return {
      dimensionsDefine: wu(i),
      startIndex: a,
      dimensionsDetectedCount: o
    };
    if (e === Mx) {
      var s = t;
      "auto" === r || null == r ? bu(function (t) {
        null != t && "-" !== t && (C(t) ? null == a && (a = 1) : a = 0);
      }, n, s, 10) : a = D(r) ? r : r ? 1 : 0, i || 1 !== a || (i = [], bu(function (t, e) {
        i[e] = null != t ? t + "" : "";
      }, n, s, 1 / 0)), o = i ? i.length : n === Ax ? s.length : s[0] ? s[0].length : null;
    } else if (e === Tx) i || (i = xu(t));else if (e === Cx) i || (i = [], v(t, function (t, e) {
      i.push(e);
    }));else if (e === Sx) {
      var l = ko(t[0]);
      o = M(l) && l.length || 1;
    }
    return {
      startIndex: a,
      dimensionsDefine: wu(i),
      dimensionsDetectedCount: o
    };
  }
  function xu(t) {
    for (var e, n = 0; n < t.length && !(e = t[n++]);) {
      ;
    }
    if (e) {
      var r = [];
      return v(e, function (t, e) {
        r.push(e);
      }), r;
    }
  }
  function wu(t) {
    if (t) {
      var e = X();
      return y(t, function (t) {
        t = I(t) ? t : {
          name: t
        };
        var n = {
          name: t.name,
          displayName: t.displayName,
          type: t.type
        };
        if (null == name) return n;
        n.name += "", null == n.displayName && (n.displayName = n.name);
        var r = e.get(n.name);
        return r ? n.name += "-" + r.count++ : e.set(n.name, {
          count: 1
        }), n;
      });
    }
  }
  function bu(t, e, n, r) {
    if (e === Ax) for (var i = 0; i < n.length && r > i; i++) {
      t(n[i] ? n[i][0] : null, i);
    } else for (var o = n[0] || [], i = 0; i < o.length && r > i; i++) {
      t(o[i], i);
    }
  }
  function Su(t, e) {
    var n = iw[Cu(t, e)];
    return n;
  }
  function Mu(t, e) {
    var n = aw[Cu(t, e)];
    return n;
  }
  function Tu(t) {
    var e = lw[t];
    return e;
  }
  function Cu(t, e) {
    return t === Mx ? t + "_" + e : t;
  }
  function ku(t, e, n) {
    if (t) {
      var r = t.getRawDataItem(e);
      if (null != r) {
        var i,
          o,
          a = t.getProvider().getSource().sourceFormat,
          s = t.getDimensionInfo(n);
        return s && (i = s.name, o = s.index), Tu(a)(r, o, i);
      }
    }
  }
  function Du(t) {
    return new cw(t);
  }
  function Iu(t, e) {
    var n = e && e.type;
    if ("ordinal" === n) {
      var r = e && e.ordinalMeta;
      return r ? r.parseAndCollect(t) : t;
    }
    return "time" === n && "number" != typeof t && null != t && "-" !== t && (t = +fo(t)), null == t || "" === t ? 0 / 0 : +t;
  }
  function Au(t, e) {
    var n = new dw(),
      r = t.data,
      i = n.sourceFormat = t.sourceFormat,
      o = t.startIndex,
      a = [],
      s = {},
      l = t.dimensionsDefine;
    if (l) v(l, function (t, e) {
      var n = t.name,
        r = {
          index: e,
          name: n,
          displayName: t.displayName
        };
      if (a.push(r), null != n) {
        var i = "";
        Z(s, n) && Mo(i), s[n] = r;
      }
    });else for (var u = 0; u < t.dimensionsDetectedCount; u++) {
      a.push({
        index: u
      });
    }
    var h = Su(i, Ix);
    e.__isBuiltIn && (n.getRawDataItem = function (t) {
      return h(r, o, a, t);
    }, n.getRawData = Zp(Pu, null, t)), n.cloneRawData = Zp(Lu, null, t);
    var c = Mu(i, Ix);
    n.count = Zp(c, null, r, o, a);
    var f = Tu(i);
    n.retrieveValue = function (t, e) {
      var n = h(r, o, a, t);
      return p(n, e);
    };
    var p = n.retrieveValueFromItem = function (t, e) {
      if (null != t) {
        var n = a[e];
        return n ? f(t, e, n.name) : void 0;
      }
    };
    return n.getDimensionInfo = Zp(Ou, null, a, s), n.cloneAllDimensionInfo = Zp(Ru, null, a), n;
  }
  function Pu(t) {
    var e = t.sourceFormat,
      n = t.data;
    if (e === Mx || e === Tx || !n || M(n) && !n.length) return t.data;
    var r = "";
    Mo(r);
  }
  function Lu(t) {
    var e = t.sourceFormat,
      n = t.data;
    if (!n) return n;
    if (M(n) && !n.length) return [];
    if (e === Mx) {
      for (var r = [], i = 0, o = n.length; o > i; i++) {
        r.push(n[i].slice());
      }
      return r;
    }
    if (e === Tx) {
      for (var r = [], i = 0, o = n.length; o > i; i++) {
        r.push(h({}, n[i]));
      }
      return r;
    }
  }
  function Ou(t, e, n) {
    return null != n ? "number" == typeof n || !isNaN(n) && !Z(e, n) ? t[n] : Z(e, n) ? e[n] : void 0 : void 0;
  }
  function Ru(t) {
    return s(t);
  }
  function Eu(t) {
    t = s(t);
    var e = t.type,
      n = "";
    e || Mo(n);
    var r = e.split(":");
    2 !== r.length && Mo(n);
    var i = !1;
    "echarts" === r[0] && (e = r[1], i = !0), t.__isBuiltIn = i, gw.set(e, t);
  }
  function Bu(t, e, n) {
    var r = To(t),
      i = r.length,
      o = "";
    i || Mo(o);
    for (var a = 0, s = i; s > a; a++) {
      var l = r[a];
      e = zu(l, e, n, 1 === i ? null : a), a !== s - 1 && (e.length = Math.max(e.length, 1));
    }
    return e;
  }
  function zu(t, e) {
    var n = "";
    e.length || Mo(n), I(t) || Mo(n);
    var r = t.type,
      i = gw.get(r);
    i || Mo(n);
    var o = y(e, function (t) {
        return Au(t, i);
      }),
      a = To(i.transform({
        upstream: o[0],
        upstreamList: o,
        config: s(t.config)
      }));
    return y(a, function (t) {
      var n = "";
      I(t) || Mo(n);
      var r = t.data;
      null != r ? I(r) || g(r) || Mo(n) : r = e[0].data;
      var i = Dl(e[0], {
        seriesLayoutBy: Ix,
        sourceHeader: 0,
        dimensions: t.dimensions
      });
      return du(r, i, null, null);
    });
  }
  function Fu(t) {
    var e = t.option.transform;
    e && U(t.option.transform);
  }
  function Nu(t) {
    return "series" === t.mainType;
  }
  function Hu(t) {
    throw new Error(t);
  }
  function Vu(t, e) {
    return e.type = t, e;
  }
  function Wu(t, e) {
    var n = t.getData().getItemVisual(e, "style"),
      r = n[t.visualDrawType];
    return ml(r);
  }
  function Gu(t) {
    var e,
      n,
      r,
      i,
      o = t.series,
      a = t.dataIndex,
      s = t.multipleSeries,
      l = o.getData(),
      u = l.mapDimensionsAll("defaultedTooltip"),
      h = u.length,
      c = o.getRawValue(a),
      f = M(c),
      p = Wu(o, a);
    if (h > 1 || f && !h) {
      var d = Uu(c, o, a, u, p);
      e = d.inlineValues, n = d.inlineValueTypes, r = d.blocks, i = d.inlineValues[0];
    } else if (h) {
      var g = l.getDimensionInfo(u[0]);
      i = e = ku(l, a, u[0]), n = g.type;
    } else i = e = f ? c[0] : c;
    var v = No(o),
      y = v && o.name || "",
      m = l.getName(a),
      _ = s ? y : m;
    return Vu("section", {
      header: y,
      noHeader: s || !v,
      sortParam: i,
      blocks: [Vu("nameValue", {
        markerType: "item",
        markerColor: p,
        name: _,
        noName: !G(_),
        value: e,
        valueType: n
      })].concat(r || [])
    });
  }
  function Uu(t, e, n, r, i) {
    function o(t, e) {
      var n = a.getDimensionInfo(e);
      n && n.otherDims.tooltip !== !1 && (s ? h.push(Vu("nameValue", {
        markerType: "subItem",
        markerColor: i,
        name: n.displayName,
        value: t,
        valueType: n.type
      })) : (l.push(t), u.push(n.type)));
    }
    var a = e.getData(),
      s = m(t, function (t, e, n) {
        var r = a.getDimensionInfo(n);
        return t = t || r && r.tooltip !== !1 && null != r.displayName;
      }, !1),
      l = [],
      u = [],
      h = [];
    return r.length ? v(r, function (t) {
      o(ku(a, n, t), t);
    }) : v(t, o), {
      inlineValues: l,
      inlineValueTypes: u,
      blocks: h
    };
  }
  function qu(t, e) {
    return t.getName(e) || t.getId(e);
  }
  function Xu(t) {
    var e = t.name;
    No(t) || (t.name = Yu(t) || e);
  }
  function Yu(t) {
    var e = t.getRawData(),
      n = e.mapDimensionsAll("seriesName"),
      r = [];
    return v(n, function (t) {
      var n = e.getDimensionInfo(t);
      n.displayName && r.push(n.displayName);
    }), r.join(" ");
  }
  function ju(t) {
    return t.model.getRawData().count();
  }
  function Zu(t) {
    var e = t.model;
    return e.setData(e.getRawData().cloneShallow()), Ku;
  }
  function Ku(t, e) {
    e.outputData && t.end > e.outputData.count() && e.model.getRawData().cloneShallow(e.outputData);
  }
  function $u(t, e) {
    v(n(t.CHANGABLE_METHODS, t.DOWNSAMPLE_METHODS), function (n) {
      t.wrapMethod(n, S(Qu, e));
    });
  }
  function Qu(t, e) {
    var n = Ju(t);
    return n && n.setOutputEnd((e || this).count()), e;
  }
  function Ju(t) {
    var e = (t.ecModel || {}).scheduler,
      n = e && e.getPipeline(t.uid);
    if (n) {
      var r = n.currentTask;
      if (r) {
        var i = r.agentStubMap;
        i && (r = i.get(t.uid));
      }
      return r;
    }
  }
  function th() {
    var t = Uo();
    return function (e) {
      var n = t(e),
        r = e.pipelineContext,
        i = !!n.large,
        o = !!n.progressiveRender,
        a = n.large = !(!r || !r.large),
        s = n.progressiveRender = !(!r || !r.progressiveRender);
      return !(i === a && o === s) && "reset";
    };
  }
  function eh(t, e, n) {
    t && ("emphasis" === e ? Ca : ka)(t, n);
  }
  function nh(t, e, n) {
    var r = Go(t, e),
      i = e && null != e.highlightKey ? Ua(e.highlightKey) : null;
    null != r ? v(To(r), function (e) {
      eh(t.getItemGraphicEl(e), n, i);
    }) : t.eachItemGraphicEl(function (t) {
      eh(t, n, i);
    });
  }
  function rh(t) {
    return ww(t.model);
  }
  function ih(t) {
    var e = t.model,
      n = t.ecModel,
      r = t.api,
      i = t.payload,
      o = e.pipelineContext.progressiveRender,
      a = t.view,
      s = i && xw(i).updateMethod,
      l = o ? "incrementalPrepareRender" : s && a[s] ? s : "render";
    return "render" !== l && a[l](e, n, r, i), Mw[l];
  }
  function oh(t, e, n) {
    function r() {
      h = new Date().getTime(), c = null, t.apply(a, s || []);
    }
    var i,
      o,
      a,
      s,
      l,
      u = 0,
      h = 0,
      c = null;
    e = e || 0;
    var f = function f() {
      for (var t = [], f = 0; f < arguments.length; f++) {
        t[f] = arguments[f];
      }
      i = new Date().getTime(), a = this, s = t;
      var p = l || e,
        d = l || n;
      l = null, o = i - (d ? u : h) - p, clearTimeout(c), d ? c = setTimeout(r, p) : o >= 0 ? r() : c = setTimeout(r, -o), u = i;
    };
    return f.clear = function () {
      c && (clearTimeout(c), c = null);
    }, f.debounceNextCall = function (t) {
      l = t;
    }, f;
  }
  function ah(t, e) {
    var n = t.visualStyleMapper || Cw[e];
    return n ? n : (console.warn("Unkown style type '" + e + "'."), Cw.itemStyle);
  }
  function sh(t, e) {
    var n = t.visualDrawType || kw[e];
    return n ? n : (console.warn("Unkown style type '" + e + "'."), "fill");
  }
  function lh(t, e) {
    e = e || {}, c(e, {
      text: "loading",
      textColor: "#000",
      fontSize: "12px",
      maskColor: "rgba(255, 255, 255, 0.8)",
      showSpinner: !0,
      color: "#5470c6",
      spinnerRadius: 10,
      lineWidth: 5,
      zlevel: 0
    });
    var n = new qv(),
      r = new ty({
        style: {
          fill: e.maskColor
        },
        zlevel: e.zlevel,
        z: 1e4
      });
    n.add(r);
    var i = e.fontSize + " sans-serif",
      o = new ty({
        style: {
          fill: "none"
        },
        textContent: new Xy({
          style: {
            text: e.text,
            fill: e.textColor,
            font: i
          }
        }),
        textConfig: {
          position: "right",
          distance: 10
        },
        zlevel: e.zlevel,
        z: 10001
      });
    n.add(o);
    var a;
    return e.showSpinner && (a = new Ky({
      shape: {
        startAngle: -Lw / 2,
        endAngle: -Lw / 2 + .1,
        r: e.spinnerRadius
      },
      style: {
        stroke: e.color,
        lineCap: "round",
        lineWidth: e.lineWidth
      },
      zlevel: e.zlevel,
      z: 10001
    }), a.animateShape(!0).when(1e3, {
      endAngle: 3 * Lw / 2
    }).start("circularInOut"), a.animateShape(!0).when(1e3, {
      startAngle: 3 * Lw / 2
    }).delay(300).start("circularInOut"), n.add(a)), n.resize = function () {
      var n = Pn(e.text, i),
        s = e.showSpinner ? e.spinnerRadius : 0,
        l = (t.getWidth() - 2 * s - (e.showSpinner && n ? 10 : 0) - n) / 2 - (e.showSpinner ? 0 : n / 2),
        u = t.getHeight() / 2;
      e.showSpinner && a.setShape({
        cx: l,
        cy: u
      }), o.setShape({
        x: l - s,
        y: u - s,
        width: 2 * s,
        height: 2 * s
      }), r.setShape({
        x: 0,
        y: 0,
        width: t.getWidth(),
        height: t.getHeight()
      });
    }, n.resize(), n;
  }
  function uh(t) {
    t.overallReset(t.ecModel, t.api, t.payload);
  }
  function hh(t) {
    return t.overallProgress && ch;
  }
  function ch() {
    this.agent.dirty(), this.getDownstream().dirty();
  }
  function fh() {
    this.agent && this.agent.dirty();
  }
  function ph(t) {
    return t.plan ? t.plan(t.model, t.ecModel, t.api, t.payload) : null;
  }
  function dh(t) {
    t.useClearVisual && t.data.clearAllVisual();
    var e = t.resetDefines = To(t.reset(t.model, t.ecModel, t.api, t.payload));
    return e.length > 1 ? y(e, function (t, e) {
      return gh(e);
    }) : Rw;
  }
  function gh(t) {
    return function (e, n) {
      var r = n.data,
        i = n.resetDefines[t];
      if (i && i.dataEach) for (var o = e.start; o < e.end; o++) {
        i.dataEach(r, o);
      } else i && i.progress && i.progress(e, r);
    };
  }
  function vh(t) {
    return t.data.count();
  }
  function yh(t) {
    Sw = null;
    try {
      t(Ew, Bw);
    } catch (e) {}
    return Sw;
  }
  function mh(t, e) {
    for (var n in e.prototype) {
      t[n] = K;
    }
  }
  function _h(t, e, n) {
    switch (n) {
      case "color":
        var r = t.getItemVisual(e, "style");
        return r[t.getVisual("drawType")];
      case "opacity":
        return t.getItemVisual(e, "style").opacity;
      case "symbol":
      case "symbolSize":
      case "liftZ":
        return t.getItemVisual(e, n);
    }
  }
  function xh(t, e) {
    switch (e) {
      case "color":
        var n = t.getVisual("style");
        return n[t.getVisual("drawType")];
      case "opacity":
        return t.getVisual("style").opacity;
      case "symbol":
      case "symbolSize":
      case "liftZ":
        return t.getVisual(e);
    }
  }
  function wh(t, e, n, r, i) {
    var o = n.width,
      a = n.height;
    switch (t) {
      case "top":
        r.set(n.x + o / 2, n.y - e), i.set(0, -1);
        break;
      case "bottom":
        r.set(n.x + o / 2, n.y + a + e), i.set(0, 1);
        break;
      case "left":
        r.set(n.x - e, n.y + a / 2), i.set(-1, 0);
        break;
      case "right":
        r.set(n.x + o + e, n.y + a / 2), i.set(1, 0);
    }
  }
  function bh(t, e, n, r, i, o, a, s, l) {
    a -= t, s -= e;
    var u = Math.sqrt(a * a + s * s);
    a /= u, s /= u;
    var h = a * n + t,
      c = s * n + e;
    if (Math.abs(r - i) % Qw < 1e-4) return l[0] = h, l[1] = c, u - n;
    if (o) {
      var f = r;
      r = Br(i), i = Br(f);
    } else r = Br(r), i = Br(i);
    r > i && (i += Qw);
    var p = Math.atan2(s, a);
    if (0 > p && (p += Qw), p >= r && i >= p || p + Qw >= r && i >= p + Qw) return l[0] = h, l[1] = c, u - n;
    var d = n * Math.cos(r) + t,
      g = n * Math.sin(r) + e,
      v = n * Math.cos(i) + t,
      y = n * Math.sin(i) + e,
      m = (d - a) * (d - a) + (g - s) * (g - s),
      _ = (v - a) * (v - a) + (y - s) * (y - s);
    return _ > m ? (l[0] = d, l[1] = g, Math.sqrt(m)) : (l[0] = v, l[1] = y, Math.sqrt(_));
  }
  function Sh(t, e, n, r, i, o, a, s) {
    var l = i - t,
      u = o - e,
      h = n - t,
      c = r - e,
      f = Math.sqrt(h * h + c * c);
    h /= f, c /= f;
    var p = l * h + u * c,
      d = p / f;
    s && (d = Math.min(Math.max(d, 0), 1)), d *= f;
    var g = a[0] = t + d * h,
      v = a[1] = e + d * c;
    return Math.sqrt((g - i) * (g - i) + (v - o) * (v - o));
  }
  function Mh(t, e, n, r, i, o, a) {
    0 > n && (t += n, n = -n), 0 > r && (e += r, r = -r);
    var s = t + n,
      l = e + r,
      u = a[0] = Math.min(Math.max(i, t), s),
      h = a[1] = Math.min(Math.max(o, e), l);
    return Math.sqrt((u - i) * (u - i) + (h - o) * (h - o));
  }
  function Th(t, e, n) {
    var r = Mh(e.x, e.y, e.width, e.height, t.x, t.y, eb);
    return n.set(eb[0], eb[1]), r;
  }
  function Ch(t, e, n) {
    for (var r, i, o = 0, a = 0, s = 0, l = 0, u = 1 / 0, h = e.data, c = t.x, f = t.y, p = 0; p < h.length;) {
      var d = h[p++];
      1 === p && (o = h[p], a = h[p + 1], s = o, l = a);
      var g = u;
      switch (d) {
        case Jw.M:
          s = h[p++], l = h[p++], o = s, a = l;
          break;
        case Jw.L:
          g = Sh(o, a, h[p], h[p + 1], c, f, eb, !0), o = h[p++], a = h[p++];
          break;
        case Jw.C:
          g = yr(o, a, h[p++], h[p++], h[p++], h[p++], h[p], h[p + 1], c, f, eb), o = h[p++], a = h[p++];
          break;
        case Jw.Q:
          g = Mr(o, a, h[p++], h[p++], h[p], h[p + 1], c, f, eb), o = h[p++], a = h[p++];
          break;
        case Jw.A:
          var v = h[p++],
            y = h[p++],
            m = h[p++],
            _ = h[p++],
            x = h[p++],
            w = h[p++];
          p += 1;
          var b = !!(1 - h[p++]);
          r = Math.cos(x) * m + v, i = Math.sin(x) * _ + y, 1 >= p && (s = r, l = i);
          var S = (c - v) * _ / m + v;
          g = bh(v, y, _, x, x + w, b, S, f, eb), o = Math.cos(x + w) * m + v, a = Math.sin(x + w) * _ + y;
          break;
        case Jw.R:
          s = o = h[p++], l = a = h[p++];
          var M = h[p++],
            T = h[p++];
          g = Mh(s, l, M, T, c, f, eb);
          break;
        case Jw.Z:
          g = Sh(o, a, s, l, c, f, eb, !0), o = s, a = l;
      }
      u > g && (u = g, n.set(eb[0], eb[1]));
    }
    return u;
  }
  function kh(t, e) {
    if (t) {
      var n = t.getTextGuideLine(),
        r = t.getTextContent();
      if (r && n) {
        var i = t.textGuideLineConfig || {},
          o = [[0, 0], [0, 0], [0, 0]],
          a = i.candidates || tb,
          s = r.getBoundingRect().clone();
        s.applyTransform(r.getComputedTransform());
        var l = 1 / 0,
          u = i.anchor,
          h = t.getComputedTransform(),
          c = h && Ue([], h),
          f = e.get("length2") || 0;
        u && ib.copy(u);
        for (var p = 0; p < a.length; p++) {
          var d = a[p];
          wh(d, 0, s, nb, ob), qd.scaleAndAdd(rb, nb, ob, f), rb.transform(c);
          var g = t.getBoundingRect(),
            v = u ? u.distance(rb) : t instanceof Ov ? Ch(rb, t.path, ib) : Th(rb, g, ib);
          l > v && (l = v, rb.transform(h), ib.transform(h), ib.toArray(o[0]), rb.toArray(o[1]), nb.toArray(o[2]));
        }
        Dh(o, e.get("minTurnAngle")), n.setShape({
          points: o
        });
      }
    }
  }
  function Dh(t, e) {
    if (180 >= e && e > 0) {
      e = e / 180 * Math.PI, nb.fromArray(t[0]), rb.fromArray(t[1]), ib.fromArray(t[2]), qd.sub(ob, nb, rb), qd.sub(ab, ib, rb);
      var n = ob.len(),
        r = ab.len();
      if (!(.001 > n || .001 > r)) {
        ob.scale(1 / n), ab.scale(1 / r);
        var i = ob.dot(ab),
          o = Math.cos(e);
        if (i > o) {
          var a = Sh(rb.x, rb.y, ib.x, ib.y, nb.x, nb.y, sb, !1);
          lb.fromArray(sb), lb.scaleAndAdd(ab, a / Math.tan(Math.PI - e));
          var s = ib.x !== rb.x ? (lb.x - rb.x) / (ib.x - rb.x) : (lb.y - rb.y) / (ib.y - rb.y);
          if (isNaN(s)) return;
          0 > s ? qd.copy(lb, rb) : s > 1 && qd.copy(lb, ib), lb.toArray(t[1]);
        }
      }
    }
  }
  function Ih(t, e, n, r) {
    var i = "normal" === n,
      o = i ? t : t.ensureState(n);
    o.ignore = e;
    var a = r.get("smooth");
    a && a === !0 && (a = .3), o.shape = o.shape || {}, a > 0 && (o.shape.smooth = a);
    var s = r.getModel("lineStyle").getLineStyle();
    i ? t.useStyle(s) : o.style = s;
  }
  function Ah(t, e) {
    var n = e.smooth,
      r = e.points;
    if (r) if (t.moveTo(r[0][0], r[0][1]), n > 0 && r.length >= 3) {
      var i = ed(r[0], r[1]),
        o = ed(r[1], r[2]);
      if (!i || !o) return t.lineTo(r[1][0], r[1][1]), void t.lineTo(r[2][0], r[2][1]);
      var a = Math.min(i, o) * n,
        s = de([], r[1], r[0], a / i),
        l = de([], r[1], r[2], a / o),
        u = de([], s, l, .5);
      t.bezierCurveTo(s[0], s[1], s[0], s[1], u[0], u[1]), t.bezierCurveTo(l[0], l[1], l[0], l[1], r[2][0], r[2][1]);
    } else for (var h = 1; h < r.length; h++) {
      t.lineTo(r[h][0], r[h][1]);
    }
  }
  function Ph(t, e, n) {
    var r = t.getTextGuideLine(),
      i = t.getTextContent();
    if (!i) return void (r && t.removeTextGuideLine());
    for (var o = e.normal, a = o.get("show"), s = i.ignore, l = 0; l < s_.length; l++) {
      var u = s_[l],
        h = e[u],
        f = "normal" === u;
      if (h) {
        var p = h.get("show"),
          d = f ? s : F(i.states[u] && i.states[u].ignore, s);
        if (d || !F(p, a)) {
          var g = f ? r : r && r.states.normal;
          g && (g.ignore = !0);
          continue;
        }
        r || (r = new uy(), t.setTextGuideLine(r), f || !s && a || Ih(r, !0, "normal", e.normal), t.stateProxy && (r.stateProxy = t.stateProxy)), Ih(r, !1, u, h);
      }
    }
    if (r) {
      c(r.style, n), r.style.fill = null;
      var v = o.get("showAbove"),
        y = t.textGuideLineConfig = t.textGuideLineConfig || {};
      y.showAbove = v || !1, r.buildPath = Ah;
    }
  }
  function Lh(t, e) {
    e = e || "labelLine";
    for (var n = {
        normal: t.getModel(e)
      }, r = 0; r < a_.length; r++) {
      var i = a_[r];
      n[i] = t.getModel([i, e]);
    }
    return n;
  }
  function Oh(t) {
    for (var e = [], n = 0; n < t.length; n++) {
      var r = t[n];
      if (!r.defaultAttr.ignore) {
        var i = r.label,
          o = i.getComputedTransform(),
          a = i.getBoundingRect(),
          s = !o || o[1] < 1e-5 && o[2] < 1e-5,
          l = i.style.margin || 0,
          u = a.clone();
        u.applyTransform(o), u.x -= l / 2, u.y -= l / 2, u.width += l, u.height += l;
        var h = s ? new Am(a, o) : null;
        e.push({
          label: i,
          labelLine: r.labelLine,
          rect: u,
          localRect: a,
          obb: h,
          priority: r.priority,
          defaultAttr: r.defaultAttr,
          layoutOption: r.computedLayoutOption,
          axisAligned: s,
          transform: o
        });
      }
    }
    return e;
  }
  function Rh(t, e, n, r, i, o) {
    function a() {
      w = S.rect[e] - r, b = i - M.rect[e] - M.rect[n];
    }
    function s(t, e, n) {
      if (0 > t) {
        var r = Math.min(e, -t);
        if (r > 0) {
          l(r * n, 0, c);
          var i = r + t;
          0 > i && u(-i * n, 1);
        } else u(-t * n, 1);
      }
    }
    function l(n, r, i) {
      0 !== n && (d = !0);
      for (var o = r; i > o; o++) {
        var a = t[o],
          s = a.rect;
        s[e] += n, a.label[e] += n;
      }
    }
    function u(r, i) {
      for (var o = [], a = 0, s = 1; c > s; s++) {
        var u = t[s - 1].rect,
          h = Math.max(t[s].rect[e] - u[e] - u[n], 0);
        o.push(h), a += h;
      }
      if (a) {
        var f = Math.min(Math.abs(r) / a, i);
        if (r > 0) for (var s = 0; c - 1 > s; s++) {
          var p = o[s] * f;
          l(p, 0, s + 1);
        } else for (var s = c - 1; s > 0; s--) {
          var p = o[s - 1] * f;
          l(-p, s, c);
        }
      }
    }
    function h(t) {
      var e = 0 > t ? -1 : 1;
      t = Math.abs(t);
      for (var n = Math.ceil(t / (c - 1)), r = 0; c - 1 > r; r++) {
        if (e > 0 ? l(n, 0, r + 1) : l(-n, c - r - 1, c), t -= n, 0 >= t) return;
      }
    }
    var c = t.length;
    if (!(2 > c)) {
      t.sort(function (t, n) {
        return t.rect[e] - n.rect[e];
      });
      for (var f, p = 0, d = !1, g = [], v = 0, y = 0; c > y; y++) {
        var m = t[y],
          _ = m.rect;
        f = _[e] - p, 0 > f && (_[e] -= f, m.label[e] -= f, d = !0);
        var x = Math.max(-f, 0);
        g.push(x), v += x, p = _[e] + _[n];
      }
      v > 0 && o && l(-v / c, 0, c);
      var w,
        b,
        S = t[0],
        M = t[c - 1];
      return a(), 0 > w && u(-w, .8), 0 > b && u(b, .8), a(), s(w, b, 1), s(b, w, -1), a(), 0 > w && h(-w), 0 > b && h(b), d;
    }
  }
  function Eh(t, e, n, r) {
    return Rh(t, "x", "width", e, n, r);
  }
  function Bh(t, e, n, r) {
    return Rh(t, "y", "height", e, n, r);
  }
  function zh(t) {
    function e(t) {
      if (!t.ignore) {
        var e = t.ensureState("emphasis");
        null == e.ignore && (e.ignore = !1);
      }
      t.ignore = !0;
    }
    var n = [];
    t.sort(function (t, e) {
      return e.priority - t.priority;
    });
    for (var r = new tg(0, 0, 0, 0), i = 0; i < t.length; i++) {
      var o = t[i],
        a = o.axisAligned,
        s = o.localRect,
        l = o.transform,
        u = o.label,
        h = o.labelLine;
      r.copy(o.rect), r.width -= .1, r.height -= .1, r.x += .05, r.y += .05;
      for (var c = o.obb, f = !1, p = 0; p < n.length; p++) {
        var d = n[p];
        if (r.intersect(d.rect)) {
          if (a && d.axisAligned) {
            f = !0;
            break;
          }
          if (d.obb || (d.obb = new Am(d.localRect, d.transform)), c || (c = new Am(s, l)), c.intersect(d.obb)) {
            f = !0;
            break;
          }
        }
      }
      f ? (e(u), h && e(h)) : (u.attr("ignore", o.defaultAttr.ignore), h && h.attr("ignore", o.defaultAttr.labelGuideIgnore), n.push(o));
    }
  }
  function Fh(t) {
    if (t) {
      for (var e = [], n = 0; n < t.length; n++) {
        e.push(t[n].slice());
      }
      return e;
    }
  }
  function Nh(t, e) {
    var n = t.label,
      r = e && e.getTextGuideLine();
    return {
      dataIndex: t.dataIndex,
      dataType: t.dataType,
      seriesIndex: t.seriesModel.seriesIndex,
      text: t.label.style.text,
      rect: t.hostRect,
      labelRect: t.rect,
      align: n.style.align,
      verticalAlign: n.style.verticalAlign,
      labelLinePoints: Fh(r && r.shape.points)
    };
  }
  function Hh(t, e, n) {
    for (var r = 0; r < n.length; r++) {
      var i = n[r];
      null != e[i] && (t[i] = e[i]);
    }
  }
  function Vh(t, e, n, r, i) {
    var o = t + e;
    n.isSilent(o) || r.eachComponent({
      mainType: "series",
      subType: "pie"
    }, function (t) {
      for (var e = t.seriesIndex, r = i.selected, a = 0; a < r.length; a++) {
        if (r[a].seriesIndex === e) {
          var s = t.getData(),
            l = Go(s, i.fromActionPayload);
          n.trigger(o, {
            type: o,
            seriesId: t.id,
            name: s.getName(M(l) ? l[0] : l),
            selected: h({}, t.option.selectedMap)
          });
        }
      }
    });
  }
  function Wh(t, e, n) {
    t.on("selectchanged", function (t) {
      t.isFromClick ? (Vh("map", "selectchanged", e, n, t), Vh("pie", "selectchanged", e, n, t)) : "select" === t.fromAction ? (Vh("map", "selected", e, n, t), Vh("pie", "selected", e, n, t)) : "unselect" === t.fromAction && (Vh("map", "unselected", e, n, t), Vh("pie", "unselected", e, n, t));
    });
  }
  function Gh(t, e, n) {
    var r = null == e.x ? 0 : e.x,
      i = null == e.x2 ? 1 : e.x2,
      o = null == e.y ? 0 : e.y,
      a = null == e.y2 ? 0 : e.y2;
    e.global || (r = r * n.width + n.x, i = i * n.width + n.x, o = o * n.height + n.y, a = a * n.height + n.y), r = isNaN(r) ? 0 : r, i = isNaN(i) ? 1 : i, o = isNaN(o) ? 0 : o, a = isNaN(a) ? 0 : a;
    var s = t.createLinearGradient(r, o, i, a);
    return s;
  }
  function Uh(t, e, n) {
    var r = n.width,
      i = n.height,
      o = Math.min(r, i),
      a = null == e.x ? .5 : e.x,
      s = null == e.y ? .5 : e.y,
      l = null == e.r ? .5 : e.r;
    e.global || (a = a * r + n.x, s = s * i + n.y, l *= o);
    var u = t.createRadialGradient(a, s, 0, a, s, l);
    return u;
  }
  function qh(t, e, n) {
    for (var r = "radial" === e.type ? Uh(t, e, n) : Gh(t, e, n), i = e.colorStops, o = 0; o < i.length; o++) {
      r.addColorStop(i[o].offset, i[o].color);
    }
    return r;
  }
  function Xh(t, e) {
    if (t === e || !t && !e) return !1;
    if (!t || !e || t.length !== e.length) return !0;
    for (var n = 0; n < t.length; n++) {
      if (t[n] !== e[n]) return !0;
    }
    return !1;
  }
  function Yh(t, e) {
    return t && "solid" !== t && e > 0 ? (e = e || 1, "dashed" === t ? [4 * e, 2 * e] : "dotted" === t ? [e] : D(t) ? [t] : M(t) ? t : null) : null;
  }
  function jh(t) {
    var e = t.stroke;
    return !(null == e || "none" === e || !(t.lineWidth > 0));
  }
  function Zh(t) {
    var e = t.fill;
    return null != e && "none" !== e;
  }
  function Kh(t, e) {
    if (null != e.fillOpacity && 1 !== e.fillOpacity) {
      var n = t.globalAlpha;
      t.globalAlpha = e.fillOpacity * e.opacity, t.fill(), t.globalAlpha = n;
    } else t.fill();
  }
  function $h(t, e) {
    if (null != e.strokeOpacity && 1 !== e.strokeOpacity) {
      var n = t.globalAlpha;
      t.globalAlpha = e.strokeOpacity * e.opacity, t.stroke(), t.globalAlpha = n;
    } else t.stroke();
  }
  function Qh(t, e, n) {
    var r = Ci(e.image, e.__image, n);
    if (Di(r)) {
      var i = t.createPattern(r, e.repeat || "repeat");
      if ("function" == typeof DOMMatrix) {
        var o = new DOMMatrix();
        o.rotateSelf(0, 0, (e.rotation || 0) / Math.PI * 180), o.scaleSelf(e.scaleX || 1, e.scaleY || 1), o.translateSelf(e.x || 0, e.y || 0), i.setTransform(o);
      }
      return i;
    }
  }
  function Jh(t, e, n, r) {
    var i = jh(n),
      o = Zh(n),
      a = n.strokePercent,
      s = 1 > a,
      l = !e.path;
    e.silent && !s || !l || e.createPathProxy();
    var u = e.path || gb;
    if (!r) {
      var h = n.fill,
        c = n.stroke,
        f = o && !!h.colorStops,
        p = i && !!c.colorStops,
        d = o && !!h.image,
        g = i && !!c.image,
        v = void 0,
        m = void 0,
        _ = void 0,
        x = void 0,
        w = void 0;
      (f || p) && (w = e.getBoundingRect()), f && (v = e.__dirty ? qh(t, h, w) : e.__canvasFillGradient, e.__canvasFillGradient = v), p && (m = e.__dirty ? qh(t, c, w) : e.__canvasStrokeGradient, e.__canvasStrokeGradient = m), d && (_ = e.__dirty || !e.__canvasFillPattern ? Qh(t, h, e) : e.__canvasFillPattern, e.__canvasFillPattern = _), g && (x = e.__dirty || !e.__canvasStrokePattern ? Qh(t, c, e) : e.__canvasStrokePattern, e.__canvasStrokePattern = _), f ? t.fillStyle = v : d && (_ ? t.fillStyle = _ : o = !1), p ? t.strokeStyle = m : g && (x ? t.strokeStyle = x : i = !1);
    }
    var b = n.lineDash && n.lineWidth > 0 && Yh(n.lineDash, n.lineWidth),
      S = n.lineDashOffset,
      M = !!t.setLineDash,
      T = e.getGlobalScale();
    if (u.setScale(T[0], T[1], e.segmentIgnoreThreshold), b) {
      var C = n.strokeNoScale && e.getLineScale ? e.getLineScale() : 1;
      C && 1 !== C && (b = y(b, function (t) {
        return t / C;
      }), S /= C);
    }
    var k = !0;
    (l || e.__dirty & Ov.SHAPE_CHANGED_BIT || b && !M && i) && (u.setDPR(t.dpr), s ? u.setContext(null) : (u.setContext(t), k = !1), u.reset(), b && !M && (u.setLineDash(b), u.setLineDashOffset(S)), e.buildPath(u, e.shape, r), u.toStatic(), e.pathUpdated()), k && u.rebuildPath(t, s ? a : 1), b && M && (t.setLineDash(b), t.lineDashOffset = S), r || (n.strokeFirst ? (i && $h(t, n), o && Kh(t, n)) : (o && Kh(t, n), i && $h(t, n))), b && M && t.setLineDash([]);
  }
  function tc(t, e, n) {
    var r = e.__image = Ci(n.image, e.__image, e, e.onload);
    if (r && Di(r)) {
      var i = n.x || 0,
        o = n.y || 0,
        a = e.getWidth(),
        s = e.getHeight(),
        l = r.width / r.height;
      if (null == a && null != s ? a = s * l : null == s && null != a ? s = a / l : null == a && null == s && (a = r.width, s = r.height), n.sWidth && n.sHeight) {
        var u = n.sx || 0,
          h = n.sy || 0;
        t.drawImage(r, u, h, n.sWidth, n.sHeight, i, o, a, s);
      } else if (n.sx && n.sy) {
        var u = n.sx,
          h = n.sy,
          c = a - u,
          f = s - h;
        t.drawImage(r, u, h, c, f, i, o, a, s);
      } else t.drawImage(r, i, o, a, s);
    }
  }
  function ec(t, e, n) {
    var r = n.text;
    if (null != r && (r += ""), r) {
      t.font = n.font || ng, t.textAlign = n.textAlign, t.textBaseline = n.textBaseline;
      var i = void 0;
      if (t.setLineDash) {
        var o = n.lineDash && n.lineWidth > 0 && Yh(n.lineDash, n.lineWidth),
          a = n.lineDashOffset;
        if (o) {
          var s = n.strokeNoScale && e.getLineScale ? e.getLineScale() : 1;
          s && 1 !== s && (o = y(o, function (t) {
            return t / s;
          }), a /= s), t.setLineDash(o), t.lineDashOffset = a, i = !0;
        }
      }
      n.strokeFirst ? (jh(n) && t.strokeText(r, n.x, n.y), Zh(n) && t.fillText(r, n.x, n.y)) : (Zh(n) && t.fillText(r, n.x, n.y), jh(n) && t.strokeText(r, n.x, n.y)), i && t.setLineDash([]);
    }
  }
  function nc(t, e, n, r, i) {
    var o = !1;
    if (!r && (n = n || {}, e === n)) return !1;
    (r || e.opacity !== n.opacity) && (o || (uc(t, i), o = !0), t.globalAlpha = null == e.opacity ? Rg.opacity : e.opacity), (r || e.blend !== n.blend) && (o || (uc(t, i), o = !0), t.globalCompositeOperation = e.blend || Rg.blend);
    for (var a = 0; a < vb.length; a++) {
      var s = vb[a];
      (r || e[s] !== n[s]) && (o || (uc(t, i), o = !0), t[s] = t.dpr * (e[s] || 0));
    }
    return (r || e.shadowColor !== n.shadowColor) && (o || (uc(t, i), o = !0), t.shadowColor = e.shadowColor || Rg.shadowColor), o;
  }
  function rc(t, e, n, r, i) {
    var o = hc(e, i.inHover),
      a = r ? null : n && hc(n, i.inHover) || {};
    if (o === a) return !1;
    var s = nc(t, o, a, r, i);
    if ((r || o.fill !== a.fill) && (s || (uc(t, i), s = !0), t.fillStyle = o.fill), (r || o.stroke !== a.stroke) && (s || (uc(t, i), s = !0), t.strokeStyle = o.stroke), (r || o.opacity !== a.opacity) && (s || (uc(t, i), s = !0), t.globalAlpha = null == o.opacity ? 1 : o.opacity), e.hasStroke()) {
      var l = o.lineWidth,
        u = l / (o.strokeNoScale && e && e.getLineScale ? e.getLineScale() : 1);
      t.lineWidth !== u && (s || (uc(t, i), s = !0), t.lineWidth = u);
    }
    for (var h = 0; h < yb.length; h++) {
      var c = yb[h],
        f = c[0];
      (r || o[f] !== a[f]) && (s || (uc(t, i), s = !0), t[f] = o[f] || c[1]);
    }
    return s;
  }
  function ic(t, e, n, r, i) {
    return nc(t, hc(e, i.inHover), n && hc(n, i.inHover), r, i);
  }
  function oc(t, e) {
    var n = e.transform,
      r = t.dpr || 1;
    n ? t.setTransform(r * n[0], r * n[1], r * n[2], r * n[3], r * n[4], r * n[5]) : t.setTransform(r, 0, 0, r, 0, 0);
  }
  function ac(t, e, n) {
    for (var r = !1, i = 0; i < t.length; i++) {
      var o = t[i];
      r = r || o.isZeroArea(), oc(e, o), e.beginPath(), o.buildPath(e, o.shape), e.clip();
    }
    n.allClipped = r;
  }
  function sc(t, e) {
    return t && e ? t[0] !== e[0] || t[1] !== e[1] || t[2] !== e[2] || t[3] !== e[3] || t[4] !== e[4] || t[5] !== e[5] : t || e ? !0 : !1;
  }
  function lc(t) {
    var e = Zh(t),
      n = jh(t);
    return !(t.lineDash || !(+e ^ +n) || e && "string" != typeof t.fill || n && "string" != typeof t.stroke || t.strokePercent < 1 || t.strokeOpacity < 1 || t.fillOpacity < 1);
  }
  function uc(t, e) {
    e.batchFill && t.fill(), e.batchStroke && t.stroke(), e.batchFill = "", e.batchStroke = "";
  }
  function hc(t, e) {
    return e ? t.__hoverStyle || t.style : t.style;
  }
  function cc(t, e) {
    fc(t, e, {
      inHover: !1,
      viewWidth: 0,
      viewHeight: 0
    }, !0);
  }
  function fc(t, e, n, r) {
    var i = e.transform;
    if (!e.shouldBePainted(n.viewWidth, n.viewHeight, !1, !1)) return e.__dirty &= ~gg.REDARAW_BIT, void (e.__isRendered = !1);
    var o = e.__clipPaths,
      a = n.prevElClipPaths,
      s = !1,
      l = !1;
    if ((!a || Xh(o, a)) && (a && a.length && (uc(t, n), t.restore(), l = s = !0, n.prevElClipPaths = null, n.allClipped = !1, n.prevEl = null), o && o.length && (uc(t, n), t.save(), ac(o, t, n), s = !0), n.prevElClipPaths = o), n.allClipped) return void (e.__isRendered = !1);
    e.beforeBrush && e.beforeBrush(), e.innerBeforeBrush();
    var u = n.prevEl;
    u || (l = s = !0);
    var h = e instanceof Ov && e.autoBatch && lc(e.style);
    s || sc(i, u.transform) ? (uc(t, n), oc(t, e)) : h || uc(t, n);
    var c = hc(e, n.inHover);
    e instanceof Ov ? (n.lastDrawType !== mb && (l = !0, n.lastDrawType = mb), rc(t, e, u, l, n), h && (n.batchFill || n.batchStroke) || t.beginPath(), Jh(t, e, c, h), h && (n.batchFill = c.fill || "", n.batchStroke = c.stroke || "")) : e instanceof py ? (n.lastDrawType !== xb && (l = !0, n.lastDrawType = xb), rc(t, e, u, l, n), ec(t, e, c)) : e instanceof jv ? (n.lastDrawType !== _b && (l = !0, n.lastDrawType = _b), ic(t, e, u, l, n), tc(t, e, c)) : e instanceof By && (n.lastDrawType !== wb && (l = !0, n.lastDrawType = wb), pc(t, e, n)), h && r && uc(t, n), e.innerAfterBrush(), e.afterBrush && e.afterBrush(), n.prevEl = e, e.__dirty = 0, e.__isRendered = !0;
  }
  function pc(t, e, n) {
    var r = e.getDisplayables(),
      i = e.getTemporalDisplayables();
    t.save();
    var o,
      a,
      s = {
        prevElClipPaths: null,
        prevEl: null,
        allClipped: !1,
        viewWidth: n.viewWidth,
        viewHeight: n.viewHeight,
        inHover: n.inHover
      };
    for (o = e.getCursor(), a = r.length; a > o; o++) {
      var l = r[o];
      l.beforeBrush && l.beforeBrush(), l.innerBeforeBrush(), fc(t, l, s, o === a - 1), l.innerAfterBrush(), l.afterBrush && l.afterBrush(), s.prevEl = l;
    }
    for (var u = 0, h = i.length; h > u; u++) {
      var l = i[u];
      l.beforeBrush && l.beforeBrush(), l.innerBeforeBrush(), fc(t, l, s, u === h - 1), l.innerAfterBrush(), l.afterBrush && l.afterBrush(), s.prevEl = l;
    }
    e.clearTemporalDisplayables(), e.notClear = !0, t.restore();
  }
  function dc() {
    return !1;
  }
  function gc(t, e, n) {
    var r = jp(),
      i = e.getWidth(),
      o = e.getHeight(),
      a = r.style;
    return a && (a.position = "absolute", a.left = "0", a.top = "0", a.width = i + "px", a.height = o + "px", r.setAttribute("data-zr-dom-id", t)), r.width = i * n, r.height = o * n, r;
  }
  function vc(t) {
    return parseInt(t, 10);
  }
  function yc(t) {
    return t ? t.__builtin__ ? !0 : "function" != typeof t.resize || "function" != typeof t.refresh ? !1 : !0 : !1;
  }
  function mc(t, e) {
    var n = document.createElement("div");
    return n.style.cssText = ["position:relative", "width:" + t + "px", "height:" + e + "px", "padding:0", "margin:0", "border-width:0"].join(";") + ";", n;
  }
  function _c(t, e, n) {
    for (var r; t && (!e(t) || (r = t, !n));) {
      t = t.__hostTarget || t.parent;
    }
    return r;
  }
  function xc(t, e) {
    if ("image" !== this.type) {
      var n = this.style;
      this.__isEmptyBrush ? (n.stroke = t, n.fill = e || "#fff", n.lineWidth = 2) : n.fill = t, this.markRedraw();
    }
  }
  function wc(t, e, n, r, i, o, a) {
    var s = 0 === t.indexOf("empty");
    s && (t = t.substr(5, 1).toLowerCase() + t.substr(6));
    var l;
    return l = 0 === t.indexOf("image://") ? Ja(t.slice(8), new tg(e, n, r, i), a ? "center" : "cover") : 0 === t.indexOf("path://") ? Qa(t.slice(7), {}, new tg(e, n, r, i), a ? "center" : "cover") : new zb({
      shape: {
        symbolType: t,
        x: e,
        y: n,
        width: r,
        height: i
      }
    }), l.__isEmptyBrush = s, l.setColor = xc, o && l.setColor(o), l;
  }
  function bc(t, e) {
    function n(t) {
      function e() {
        for (var t = 1, e = 0, n = m.length; n > e; ++e) {
          t = So(t, m[e]);
        }
        for (var r = 1, e = 0, n = y.length; n > e; ++e) {
          r = So(r, y[e].length);
        }
        t *= r;
        var i = _ * m.length * y.length;
        return {
          width: Math.max(1, Math.min(t, s.maxTileWidth)),
          height: Math.max(1, Math.min(i, s.maxTileHeight))
        };
      }
      function n() {
        function t(t, e, n, a, l) {
          var u = o ? 1 : r,
            h = wc(l, t * u, e * u, n * u, a * u, s.color, s.symbolKeepAspect);
          o ? w.appendChild(i.painter.paintOne(h)) : cc(d, h);
        }
        d && (d.clearRect(0, 0, x.width, x.height), s.backgroundColor && (d.fillStyle = s.backgroundColor, d.fillRect(0, 0, x.width, x.height)));
        for (var e = 0, n = 0; n < v.length; ++n) {
          e += v[n];
        }
        if (!(0 >= e)) for (var a = -_, l = 0, u = 0, h = 0; a < b.height;) {
          if (l % 2 === 0) {
            for (var c = u / 2 % y.length, f = 0, p = 0, m = 0; f < 2 * b.width;) {
              for (var S = 0, n = 0; n < g[h].length; ++n) {
                S += g[h][n];
              }
              if (0 >= S) break;
              if (p % 2 === 0) {
                var M = .5 * (1 - s.symbolSize),
                  T = f + g[h][p] * M,
                  C = a + v[l] * M,
                  k = g[h][p] * s.symbolSize,
                  D = v[l] * s.symbolSize,
                  I = m / 2 % y[c].length;
                t(T, C, k, D, y[c][I]);
              }
              f += g[h][p], ++m, ++p, p === g[h].length && (p = 0);
            }
            ++h, h === g.length && (h = 0);
          }
          a += v[l], ++u, ++l, l === v.length && (l = 0);
        }
      }
      for (var a = [r], l = !0, u = 0; u < Hb.length; ++u) {
        var h = s[Hb[u]],
          c = _typeof(h);
        if (null != h && !M(h) && "string" !== c && "number" !== c && "boolean" !== c) {
          l = !1;
          break;
        }
        a.push(h);
      }
      var f;
      if (l) {
        f = a.join(",") + (o ? "-svg" : "");
        var p = Nb.get(f);
        p && (o ? t.svgElement = p : t.image = p);
      }
      var d,
        g = Mc(s.dashArrayX),
        v = Tc(s.dashArrayY),
        y = Sc(s.symbol),
        m = Cc(g),
        _ = kc(v),
        x = !o && jp(),
        w = o && i.painter.createSVGElement("g"),
        b = e();
      x && (x.width = b.width * r, x.height = b.height * r, d = x.getContext("2d")), n(), l && Nb.put(f, x || w), t.image = x, t.svgElement = w, t.svgWidth = b.width, t.svgHeight = b.height;
    }
    if ("none" === t) return null;
    var r = e.getDevicePixelRatio(),
      i = e.getZr(),
      o = "svg" === i.painter.type;
    t.dirty && Fb["delete"](t);
    var a = Fb.get(t);
    if (a) return a;
    var s = c(t, {
      symbol: "rect",
      symbolSize: 1,
      symbolKeepAspect: !0,
      color: "rgba(0, 0, 0, 0.2)",
      backgroundColor: null,
      dashArrayX: 5,
      dashArrayY: 5,
      dashLineOffset: 0,
      rotation: 0,
      maxTileWidth: 512,
      maxTileHeight: 512
    });
    "none" === s.backgroundColor && (s.backgroundColor = null);
    var l = {
      repeat: "repeat"
    };
    return n(l), l.rotation = s.rotation, l.scaleX = l.scaleY = o ? 1 : 1 / r, Fb.set(t, l), t.dirty = !1, l;
  }
  function Sc(t) {
    if (!t || 0 === t.length) return [["rect"]];
    if ("string" == typeof t) return [[t]];
    for (var e = !0, n = 0; n < t.length; ++n) {
      if ("string" != typeof t[n]) {
        e = !1;
        break;
      }
    }
    if (e) return Sc([t]);
    for (var r = [], n = 0; n < t.length; ++n) {
      r.push("string" == typeof t[n] ? [t[n]] : t[n]);
    }
    return r;
  }
  function Mc(t) {
    if (!t || 0 === t.length) return [[0, 0]];
    if ("number" == typeof t) {
      var e = Math.ceil(t);
      return [[e, e]];
    }
    for (var n = !0, r = 0; r < t.length; ++r) {
      if ("number" != typeof t[r]) {
        n = !1;
        break;
      }
    }
    if (n) return Mc([t]);
    for (var i = [], r = 0; r < t.length; ++r) {
      if ("number" == typeof t[r]) {
        var e = Math.ceil(t[r]);
        i.push([e, e]);
      } else {
        var e = y(t[r], function (t) {
          return Math.ceil(t);
        });
        i.push(e.length % 2 === 1 ? e.concat(e) : e);
      }
    }
    return i;
  }
  function Tc(t) {
    if (!t || "object" == _typeof(t) && 0 === t.length) return [0, 0];
    if ("number" == typeof t) {
      var e = Math.ceil(t);
      return [e, e];
    }
    var n = y(t, function (t) {
      return Math.ceil(t);
    });
    return t.length % 2 ? n.concat(n) : n;
  }
  function Cc(t) {
    return y(t, function (t) {
      return kc(t);
    });
  }
  function kc(t) {
    for (var e = 0, n = 0; n < t.length; ++n) {
      e += t[n];
    }
    return t.length % 2 === 1 ? 2 * e : e;
  }
  function Dc(t, e) {
    t.eachRawSeries(function (n) {
      if (!t.isSeriesFiltered(n)) {
        var r = n.getData();
        r.hasItemVisual() && r.each(function (t) {
          var n = r.getItemVisual(t, "decal");
          if (n) {
            var i = r.ensureUniqueItemVisual(t, "style");
            i.decal = bc(n, e);
          }
        });
        var i = r.getVisual("decal");
        if (i) {
          var o = r.getVisual("style");
          o.decal = bc(i, e);
        }
      }
    });
  }
  function Ic(t) {
    return function () {
      for (var e = [], n = 0; n < arguments.length; n++) {
        e[n] = arguments[n];
      }
      return this.isDisposed() ? void 0 : Pc(this, t, e);
    };
  }
  function Ac(t) {
    return function () {
      for (var e = [], n = 0; n < arguments.length; n++) {
        e[n] = arguments[n];
      }
      return Pc(this, t, e);
    };
  }
  function Pc(t, e, n) {
    return n[0] = n[0] && n[0].toLowerCase(), ad.prototype[e].apply(t, n);
  }
  function Lc(t, e, n) {
    var r = Bc(t);
    if (r) return r;
    var i = new VS(t, e, n);
    return i.id = "ec_" + eM++, JS[i.id] = i, Yo(t, rM, i.id), zS(i), Wb(jS, function (t) {
      t(i);
    }), i;
  }
  function Oc(t) {
    if (M(t)) {
      var e = t;
      t = null, Wb(e, function (e) {
        null != e.group && (t = e.group);
      }), t = t || "g_" + nM++, Wb(e, function (e) {
        e.group = t;
      });
    }
    return tM[t] = !0, t;
  }
  function Rc(t) {
    tM[t] = !1;
  }
  function Ec(t) {
    "string" == typeof t ? t = JS[t] : t instanceof VS || (t = Bc(t)), t instanceof VS && !t.isDisposed() && t.dispose();
  }
  function Bc(t) {
    return JS[jo(t, rM)];
  }
  function zc(t) {
    return JS[t];
  }
  function Fc(t, e) {
    $S[t] = e;
  }
  function Nc(t) {
    YS.push(t);
  }
  function Hc(t, e) {
    jc(XS, t, e, $b);
  }
  function Vc(t) {
    t && jS.push(t);
  }
  function Wc(t) {
    t && ZS.push(t);
  }
  function Gc(t, e, n) {
    "function" == typeof e && (n = e, e = "");
    var r = Ub(t) ? t.type : [t, t = {
      event: e
    }][0];
    t.event = (t.event || r).toLowerCase(), e = t.event, Vb(pS.test(r) && pS.test(e)), US[r] || (US[r] = {
      action: n,
      actionInfo: t
    }), qS[e] = r;
  }
  function Uc(t, e) {
    Xx.register(t, e);
  }
  function qc(t) {
    var e = Xx.get(t);
    return e ? e.getDimensionsInfo ? e.getDimensionsInfo() : e.dimensions.slice() : void 0;
  }
  function Xc(t, e) {
    jc(KS, t, e, Jb, "layout");
  }
  function Yc(t, e) {
    jc(KS, t, e, nS, "visual");
  }
  function jc(t, e, n, r, i) {
    (Gb(e) || Ub(e)) && (n = e, e = r);
    var o = Ow.wrapStageHandler(n, i);
    o.__prio = e, o.__raw = n, t.push(o);
  }
  function Zc(t, e) {
    QS[t] = e;
  }
  function Kc(t) {
    return gx.extend(t);
  }
  function $c(t) {
    return _w.extend(t);
  }
  function Qc(t) {
    return mw.extend(t);
  }
  function Jc(t) {
    return bw.extend(t);
  }
  function tf(t) {
    i("createCanvas", t);
  }
  function ef(t, e, n) {
    Yw.registerMap(t, e, n);
  }
  function nf(t) {
    var e = Yw.retrieveMap(t);
    return e && e[0] && {
      geoJson: e[0].geoJSON,
      specialAreas: e[0].specialAreas
    };
  }
  function rf(t) {
    return null == t ? 0 : t.length || 1;
  }
  function of(t) {
    return t;
  }
  function af(t) {
    var e = {},
      n = e.encode = {},
      r = X(),
      i = [],
      o = [],
      a = e.userOutput = {
        dimensionNames: t.dimensions.slice(),
        encode: {}
      };
    v(t.dimensions, function (e) {
      var s = t.getDimensionInfo(e),
        l = s.coordDim;
      if (l) {
        var u = s.coordDimIndex;
        sf(n, l)[u] = e, s.isExtraCoord || (r.set(l, 1), uf(s.type) && (i[0] = e), sf(a.encode, l)[u] = s.index), s.defaultTooltip && o.push(e);
      }
      bx.each(function (t, e) {
        var r = sf(n, e),
          i = s.otherDims[e];
        null != i && i !== !1 && (r[i] = s.name);
      });
    });
    var s = [],
      l = {};
    r.each(function (t, e) {
      var r = n[e];
      l[e] = r[0], s = s.concat(r);
    }), e.dataDimsOnCoord = s, e.encodeFirstDimNotExtra = l;
    var u = n.label;
    u && u.length && (i = u.slice());
    var h = n.tooltip;
    return h && h.length ? o = h.slice() : o.length || (o = i.slice()), n.defaultedLabel = i, n.defaultedTooltip = o, e;
  }
  function sf(t, e) {
    return t.hasOwnProperty(e) || (t[e] = []), t[e];
  }
  function lf(t) {
    return "category" === t ? "ordinal" : "time" === t ? "time" : "float";
  }
  function uf(t) {
    return !("ordinal" === t || "time" === t);
  }
  function hf(t, e, n) {
    function r(t, e, n) {
      null != bx.get(e) ? t.otherDims[e] = n : (t.coordDim = e, t.coordDimIndex = n, a.set(e, !0));
    }
    pu(e) || (e = gu(e)), n = n || {}, t = (t || []).slice();
    for (var i = (n.dimsDef || []).slice(), o = X(), a = X(), l = [], u = cf(e, t, i, n.dimCount), f = 0; u > f; f++) {
      var p = i[f],
        d = i[f] = h({}, I(p) ? p : {
          name: p
        }),
        g = d.name,
        y = l[f] = new bM();
      null != g && null == o.get(g) && (y.name = y.displayName = g, o.set(g, f)), null != d.type && (y.type = d.type), null != d.displayName && (y.displayName = d.displayName);
    }
    var m = n.encodeDef;
    !m && n.encodeDefaulter && (m = n.encodeDefaulter(e, u));
    var _ = X(m);
    _.each(function (t, e) {
      var n = To(t).slice();
      if (1 === n.length && !C(n[0]) && n[0] < 0) return void _.set(e, !1);
      var i = _.set(e, []);
      v(n, function (t, n) {
        var a = C(t) ? o.get(t) : t;
        null != a && u > a && (i[n] = a, r(l[a], e, n));
      });
    });
    var x = 0;
    v(t, function (t) {
      var e, n, i, o;
      if (C(t)) e = t, o = {};else {
        o = t, e = o.name;
        var a = o.ordinalMeta;
        o.ordinalMeta = null, o = s(o), o.ordinalMeta = a, n = o.dimsDef, i = o.otherDims, o.name = o.coordDim = o.coordDimIndex = o.dimsDef = o.otherDims = null;
      }
      var u = _.get(e);
      if (u !== !1) {
        if (u = To(u), !u.length) for (var h = 0; h < (n && n.length || 1); h++) {
          for (; x < l.length && null != l[x].coordDim;) {
            x++;
          }
          x < l.length && u.push(x++);
        }
        v(u, function (t, a) {
          var s = l[t];
          if (r(c(s, o), e, a), null == s.name && n) {
            var u = n[a];
            !I(u) && (u = {
              name: u
            }), s.name = s.displayName = u.name, s.defaultTooltip = u.defaultTooltip;
          }
          i && c(s.otherDims, i);
        });
      }
    });
    var w = n.generateCoord,
      b = n.generateCoordCount,
      S = null != b;
    b = w ? b || 1 : 0;
    for (var M = w || "value", T = 0; u > T; T++) {
      var y = l[T] = l[T] || new bM(),
        k = y.coordDim;
      null == k && (y.coordDim = ff(M, a, S), y.coordDimIndex = 0, (!w || 0 >= b) && (y.isExtraCoord = !0), b--), null == y.name && (y.name = ff(y.coordDim, o, !1)), null != y.type || Ll(e, T) !== Px.Must && (!y.isExtraCoord || null == y.otherDims.itemName && null == y.otherDims.seriesName) || (y.type = "ordinal");
    }
    return l;
  }
  function cf(t, e, n, r) {
    var i = Math.max(t.dimensionsDetectedCount || 1, e.length, n.length, r || 0);
    return v(e, function (t) {
      var e;
      I(t) && (e = t.dimsDef) && (i = Math.max(i, e.length));
    }), i;
  }
  function ff(t, e, n) {
    if (n || null != e.get(t)) {
      for (var r = 0; null != e.get(t + r);) {
        r++;
      }
      t += r;
    }
    return e.set(t, !0), t;
  }
  function pf(t, e) {
    return e = e || {}, hf(e.coordDimensions || [], t, {
      dimsDef: e.dimensionsDefine || t.dimensionsDefine,
      encodeDef: e.encodeDefine || t.encodeDefine,
      dimCount: e.dimensionsCount,
      encodeDefaulter: e.encodeDefaulter,
      generateCoord: e.generateCoord,
      generateCoordCount: e.generateCoordCount
    });
  }
  function df(t) {
    var e = t.get("coordinateSystem"),
      n = new BM(e),
      r = zM[e];
    return r ? (r(t, n, n.axisMap, n.categoryAxisMap), n) : void 0;
  }
  function gf(t) {
    return "category" === t.get("type");
  }
  function vf(t, e, n) {
    n = n || {};
    var r,
      i,
      o,
      a,
      s = n.byIndex,
      l = n.stackedCoordDimension,
      u = !(!t || !t.get("stack"));
    if (v(e, function (t, n) {
      C(t) && (e[n] = t = {
        name: t
      }), u && !t.isExtraCoord && (s || r || !t.ordinalMeta || (r = t), i || "ordinal" === t.type || "time" === t.type || l && l !== t.coordDim || (i = t));
    }), !i || s || r || (s = !0), i) {
      o = "__\x00ecstackresult", a = "__\x00ecstackedover", r && (r.createInvertedIndices = !0);
      var h = i.coordDim,
        c = i.type,
        f = 0;
      v(e, function (t) {
        t.coordDim === h && f++;
      }), e.push({
        name: o,
        coordDim: h,
        coordDimIndex: f,
        type: c,
        isExtraCoord: !0,
        isCalculationCoord: !0
      }), f++, e.push({
        name: a,
        coordDim: a,
        coordDimIndex: f,
        type: c,
        isExtraCoord: !0,
        isCalculationCoord: !0
      });
    }
    return {
      stackedDimension: i && i.name,
      stackedByDimension: r && r.name,
      isStackedByIndex: s,
      stackedOverDimension: a,
      stackResultDimension: o
    };
  }
  function yf(t, e) {
    return !!e && e === t.getCalculationInfo("stackedDimension");
  }
  function mf(t, e) {
    return yf(t, e) ? t.getCalculationInfo("stackResultDimension") : e;
  }
  function _f(t, e, n) {
    n = n || {}, pu(t) || (t = gu(t));
    var r,
      i = e.get("coordinateSystem"),
      o = Xx.get(i),
      a = df(e);
    a && a.coordSysDims && (r = y(a.coordSysDims, function (t) {
      var e = {
          name: t
        },
        n = a.axisMap.get(t);
      if (n) {
        var r = n.get("type");
        e.type = lf(r);
      }
      return e;
    })), r || (r = o && (o.getDimensionsInfo ? o.getDimensionsInfo() : o.dimensions.slice()) || ["x", "y"]);
    var s,
      l,
      u = n.useEncodeDefaulter,
      h = pf(t, {
        coordDimensions: r,
        generateCoord: n.generateCoord,
        encodeDefaulter: T(u) ? u : u ? S(Il, r, e) : null
      });
    a && v(h, function (t, e) {
      var n = t.coordDim,
        r = a.categoryAxisMap.get(n);
      r && (null == s && (s = e), t.ordinalMeta = r.getOrdinalMeta()), null != t.otherDims.itemName && (l = !0);
    }), l || null == s || (h[s].otherDims.itemName = 0);
    var c = vf(e, h),
      f = new EM(h, e);
    f.setCalculationInfo(c);
    var p = null != s && xf(t) ? function (t, e, n, r) {
      return r === s ? n : this.defaultDimValueGetter(t, e, n, r);
    } : null;
    return f.hasItemOption = !1, f.initData(t, null, p), f;
  }
  function xf(t) {
    if (t.sourceFormat === Sx) {
      var e = wf(t.data || []);
      return null != e && !M(ko(e));
    }
  }
  function wf(t) {
    for (var e = 0; e < t.length && null == t[e];) {
      e++;
    }
    return t[e];
  }
  function bf(t) {
    return I(t) && null != t.value ? t.value : t + "";
  }
  function Sf(t, e, n, r) {
    var i = {},
      o = t[1] - t[0],
      a = i.interval = vo(o / e, !0);
    null != n && n > a && (a = i.interval = n), null != r && a > r && (a = i.interval = r);
    var s = i.intervalPrecision = Mf(a),
      l = i.niceTickExtent = [HM(Math.ceil(t[0] / a) * a, s), HM(Math.floor(t[1] / a) * a, s)];
    return Cf(l, t), i;
  }
  function Mf(t) {
    return so(t) + 2;
  }
  function Tf(t, e, n) {
    t[e] = Math.max(Math.min(t[e], n[1]), n[0]);
  }
  function Cf(t, e) {
    !isFinite(t[0]) && (t[0] = e[0]), !isFinite(t[1]) && (t[1] = e[1]), Tf(t, 0, e), Tf(t, 1, e), t[0] > t[1] && (t[0] = t[1]);
  }
  function kf(t, e) {
    return t >= e[0] && t <= e[1];
  }
  function Df(t, e) {
    return e[1] === e[0] ? .5 : (t - e[0]) / (e[1] - e[0]);
  }
  function If(t, e) {
    return t * (e[1] - e[0]) + e[0];
  }
  function Af(t) {
    return t.get("stack") || UM + t.seriesIndex;
  }
  function Pf(t) {
    return t.dim + t.index;
  }
  function Lf(t, e) {
    var n = [];
    return e.eachSeriesByType(t, function (t) {
      zf(t) && !Ff(t) && n.push(t);
    }), n;
  }
  function Of(t) {
    var e = {};
    v(t, function (t) {
      var n = t.coordinateSystem,
        r = n.getBaseAxis();
      if ("time" === r.type || "value" === r.type) for (var i = t.getData(), o = r.dim + "_" + r.index, a = i.mapDimension(r.dim), s = 0, l = i.count(); l > s; ++s) {
        var u = i.get(a, s);
        e[o] ? e[o].push(u) : e[o] = [u];
      }
    });
    var n = {};
    for (var r in e) {
      if (e.hasOwnProperty(r)) {
        var i = e[r];
        if (i) {
          i.sort(function (t, e) {
            return t - e;
          });
          for (var o = null, a = 1; a < i.length; ++a) {
            var s = i[a] - i[a - 1];
            s > 0 && (o = null === o ? s : Math.min(o, s));
          }
          n[r] = o;
        }
      }
    }
    return n;
  }
  function Rf(t) {
    var e = Of(t),
      n = [];
    return v(t, function (t) {
      var r,
        i = t.coordinateSystem,
        o = i.getBaseAxis(),
        a = o.getExtent();
      if ("category" === o.type) r = o.getBandWidth();else if ("value" === o.type || "time" === o.type) {
        var s = o.dim + "_" + o.index,
          l = e[s],
          u = Math.abs(a[1] - a[0]),
          h = o.scale.getExtent(),
          c = Math.abs(h[1] - h[0]);
        r = l ? u / c * l : u;
      } else {
        var f = t.getData();
        r = Math.abs(a[1] - a[0]) / f.count();
      }
      var p = ro(t.get("barWidth"), r),
        d = ro(t.get("barMaxWidth"), r),
        g = ro(t.get("barMinWidth") || 1, r),
        v = t.get("barGap"),
        y = t.get("barCategoryGap");
      n.push({
        bandWidth: r,
        barWidth: p,
        barMaxWidth: d,
        barMinWidth: g,
        barGap: v,
        barCategoryGap: y,
        axisKey: Pf(o),
        stackId: Af(t)
      });
    }), Ef(n);
  }
  function Ef(t) {
    var e = {};
    v(t, function (t) {
      var n = t.axisKey,
        r = t.bandWidth,
        i = e[n] || {
          bandWidth: r,
          remainedWidth: r,
          autoWidthCount: 0,
          categoryGap: null,
          gap: "20%",
          stacks: {}
        },
        o = i.stacks;
      e[n] = i;
      var a = t.stackId;
      o[a] || i.autoWidthCount++, o[a] = o[a] || {
        width: 0,
        maxWidth: 0
      };
      var s = t.barWidth;
      s && !o[a].width && (o[a].width = s, s = Math.min(i.remainedWidth, s), i.remainedWidth -= s);
      var l = t.barMaxWidth;
      l && (o[a].maxWidth = l);
      var u = t.barMinWidth;
      u && (o[a].minWidth = u);
      var h = t.barGap;
      null != h && (i.gap = h);
      var c = t.barCategoryGap;
      null != c && (i.categoryGap = c);
    });
    var n = {};
    return v(e, function (t, e) {
      n[e] = {};
      var r = t.stacks,
        i = t.bandWidth,
        o = t.categoryGap;
      if (null == o) {
        var a = w(r).length;
        o = Math.max(35 - 4 * a, 15) + "%";
      }
      var s = ro(o, i),
        l = ro(t.gap, 1),
        u = t.remainedWidth,
        h = t.autoWidthCount,
        c = (u - s) / (h + (h - 1) * l);
      c = Math.max(c, 0), v(r, function (t) {
        var e = t.maxWidth,
          n = t.minWidth;
        if (t.width) {
          var r = t.width;
          e && (r = Math.min(r, e)), n && (r = Math.max(r, n)), t.width = r, u -= r + l * r, h--;
        } else {
          var r = c;
          e && r > e && (r = Math.min(e, u)), n && n > r && (r = n), r !== c && (t.width = r, u -= r + l * r, h--);
        }
      }), c = (u - s) / (h + (h - 1) * l), c = Math.max(c, 0);
      var f,
        p = 0;
      v(r, function (t) {
        t.width || (t.width = c), f = t, p += t.width * (1 + l);
      }), f && (p -= f.width * l);
      var d = -p / 2;
      v(r, function (t, r) {
        n[e][r] = n[e][r] || {
          bandWidth: i,
          offset: d,
          width: t.width
        }, d += t.width * (1 + l);
      });
    }), n;
  }
  function Bf(t, e, n) {
    if (t && e) {
      var r = t[Pf(e)];
      return null != r && null != n ? r[Af(n)] : r;
    }
  }
  function zf(t) {
    return t.coordinateSystem && "cartesian2d" === t.coordinateSystem.type;
  }
  function Ff(t) {
    return t.pipelineContext && t.pipelineContext.large;
  }
  function Nf(t, e) {
    return e.toGlobalCoord(e.dataToCoord("log" === e.type ? 1 : 0));
  }
  function Hf(t, e, n, r) {
    var i = fo(e),
      o = fo(n),
      a = function a(t) {
        return Ys(i, t, r) === Ys(o, t, r);
      },
      s = function s() {
        return a("year");
      },
      l = function l() {
        return s() && a("month");
      },
      u = function u() {
        return l() && a("day");
      },
      h = function h() {
        return u() && a("hour");
      },
      c = function c() {
        return h() && a("minute");
      },
      f = function f() {
        return c() && a("second");
      },
      p = function p() {
        return f() && a("millisecond");
      };
    switch (t) {
      case "year":
        return s();
      case "month":
        return l();
      case "day":
        return u();
      case "hour":
        return h();
      case "minute":
        return c();
      case "second":
        return f();
      case "millisecond":
        return p();
    }
  }
  function Vf(t) {
    return t /= Q_, t > 16 ? 16 : t > 7.5 ? 7 : t > 3.5 ? 4 : t > 1.5 ? 2 : 1;
  }
  function Wf(t) {
    var e = 30 * Q_;
    return t /= e, t > 6 ? 6 : t > 3 ? 3 : t > 2 ? 2 : 1;
  }
  function Gf(t) {
    return t /= $_, t > 12 ? 12 : t > 6 ? 6 : t > 3.5 ? 4 : t > 2 ? 2 : 1;
  }
  function Uf(t, e) {
    return t /= e ? K_ : Z_, t > 30 ? 30 : t > 20 ? 20 : t > 15 ? 15 : t > 10 ? 10 : t > 5 ? 5 : t > 2 ? 2 : 1;
  }
  function qf(t) {
    return vo(t, !0);
  }
  function Xf(t, e, n) {
    var r = new Date(t);
    switch (Vs(e)) {
      case "year":
      case "month":
        r[nl(n)](0);
      case "day":
        r[rl(n)](1);
      case "hour":
        r[il(n)](0);
      case "minute":
        r[ol(n)](0);
      case "second":
        r[al(n)](0), r[sl(n)](0);
    }
    return r.getTime();
  }
  function Yf(t, e, n, r) {
    function i(t, e, n, i, o, a, s) {
      for (var l = new Date(e), u = e, h = l[i](); n > u && u <= r[1];) {
        s.push({
          value: u
        }), h += t, l[o](h), u = l.getTime();
      }
      s.push({
        value: u,
        notAdd: !0
      });
    }
    function o(t, o, a) {
      var s = [],
        l = !o.length;
      if (!Hf(Vs(t), r[0], r[1], n)) {
        l && (o = [{
          value: Xf(new Date(r[0]), t, n)
        }, {
          value: r[1]
        }]);
        for (var u = 0; u < o.length - 1; u++) {
          var h = o[u].value,
            c = o[u + 1].value;
          if (h !== c) {
            var f = void 0,
              p = void 0,
              d = void 0,
              g = !1;
            switch (t) {
              case "year":
                f = Math.max(1, Math.round(e / Q_ / 365)), p = js(n), d = el(n);
                break;
              case "half-year":
              case "quarter":
              case "month":
                f = Wf(e), p = Zs(n), d = nl(n);
                break;
              case "week":
              case "half-week":
              case "day":
                f = Vf(e, 31), p = Ks(n), d = rl(n), g = !0;
                break;
              case "half-day":
              case "quarter-day":
              case "hour":
                f = Gf(e), p = $s(n), d = il(n);
                break;
              case "minute":
                f = Uf(e, !0), p = Qs(n), d = ol(n);
                break;
              case "second":
                f = Uf(e, !1), p = Js(n), d = al(n);
                break;
              case "millisecond":
                f = qf(e), p = tl(n), d = sl(n);
            }
            i(f, h, c, p, d, g, s), "year" === t && a.length > 1 && 0 === u && a.unshift({
              value: a[0].value - f
            });
          }
        }
        for (var u = 0; u < s.length; u++) {
          a.push(s[u]);
        }
        return s;
      }
    }
    for (var a = 1e4, s = ix, l = 0, u = [], h = [], c = 0, f = 0, p = 0; p < s.length && l++ < a; ++p) {
      var d = Vs(s[p]);
      if (Ws(s[p])) {
        o(s[p], u[u.length - 1] || [], h);
        var g = s[p + 1] ? Vs(s[p + 1]) : null;
        if (d !== g) {
          if (h.length) {
            f = c, h.sort(function (t, e) {
              return t.value - e.value;
            });
            for (var v = [], m = 0; m < h.length; ++m) {
              var x = h[m].value;
              (0 === m || h[m - 1].value !== x) && (v.push(h[m]), x >= r[0] && x <= r[1] && c++);
            }
            var w = (r[1] - r[0]) / e;
            if (c > 1.5 * w && f > w / 1.5) break;
            if (u.push(v), c > w || t === s[p]) break;
          }
          h = [];
        }
      }
    }
    for (var b = _(y(u, function (t) {
        return _(t, function (t) {
          return t.value >= r[0] && t.value <= r[1] && !t.notAdd;
        });
      }), function (t) {
        return t.length > 0;
      }), S = [], M = b.length - 1, p = 0; p < b.length; ++p) {
      for (var T = b[p], C = 0; C < T.length; ++C) {
        S.push({
          value: T[C].value,
          level: M - p
        });
      }
    }
    S.sort(function (t, e) {
      return t.value - e.value;
    });
    for (var k = [], p = 0; p < S.length; ++p) {
      (0 === p || S[p].value !== S[p - 1].value) && k.push(S[p]);
    }
    return k;
  }
  function jf(t, e) {
    return JM(t, QM(e));
  }
  function Zf(t, e, n) {
    var r = t.rawExtentInfo;
    return r ? r : (r = new aT(t, e, n), t.rawExtentInfo = r, r);
  }
  function Kf(t, e) {
    return null == e ? null : B(e) ? 0 / 0 : t.parse(e);
  }
  function $f(t, e) {
    var n = t.type,
      r = Zf(t, e, t.getExtent()).calculate();
    t.setBlank(r.isBlank);
    var i = r.min,
      o = r.max,
      a = e.ecModel;
    if (a && "time" === n) {
      var s = Lf("bar", a),
        l = !1;
      if (v(s, function (t) {
        l = l || t.getBaseAxis() === e.axis;
      }), l) {
        var u = Rf(s),
          h = Qf(i, o, e, u);
        i = h.min, o = h.max;
      }
    }
    return {
      extent: [i, o],
      fixMin: r.minFixed,
      fixMax: r.maxFixed
    };
  }
  function Qf(t, e, n, r) {
    var i = n.axis.getExtent(),
      o = i[1] - i[0],
      a = Bf(r, n.axis);
    if (void 0 === a) return {
      min: t,
      max: e
    };
    var s = 1 / 0;
    v(a, function (t) {
      s = Math.min(t.offset, s);
    });
    var l = -1 / 0;
    v(a, function (t) {
      l = Math.max(t.offset + t.width, l);
    }), s = Math.abs(s), l = Math.abs(l);
    var u = s + l,
      h = e - t,
      c = 1 - (s + l) / o,
      f = h / c - h;
    return e += f * (l / u), t -= f * (s / u), {
      min: t,
      max: e
    };
  }
  function Jf(t, e) {
    var n = $f(t, e),
      r = n.extent,
      i = e.get("splitNumber");
    t instanceof iT && (t.base = e.get("logBase"));
    var o = t.type;
    t.setExtent(r[0], r[1]), t.niceExtent({
      splitNumber: i,
      fixMin: n.fixMin,
      fixMax: n.fixMax,
      minInterval: "interval" === o || "time" === o ? e.get("minInterval") : null,
      maxInterval: "interval" === o || "time" === o ? e.get("maxInterval") : null
    });
    var a = e.get("interval");
    null != a && t.setInterval && t.setInterval(a);
  }
  function tp(t, e) {
    if (e = e || t.get("type")) switch (e) {
      case "category":
        return new VM({
          ordinalMeta: t.getOrdinalMeta ? t.getOrdinalMeta() : t.getCategories(),
          extent: [1 / 0, -1 / 0]
        });
      case "time":
        return new jM({
          locale: t.ecModel.getLocaleModel(),
          useUTC: t.ecModel.get("useUTC")
        });
      default:
        return new (FM.getClass(e) || GM)();
    }
  }
  function ep(t) {
    var e = t.getLabelModel().get("formatter"),
      n = "category" === t.type ? t.scale.getExtent()[0] : null;
    return "time" === t.scale.type ? function (e) {
      return function (n, r) {
        return t.scale.getFormattedLabel(n, r, e);
      };
    }(e) : "string" == typeof e ? function (e) {
      return function (n) {
        var r = t.scale.getLabel(n),
          i = e.replace("{value}", null != r ? r : "");
        return i;
      };
    }(e) : "function" == typeof e ? function (e) {
      return function (r, i) {
        return null != n && (i = r.value - n), e(np(t, r), i, null != r.level ? {
          level: r.level
        } : null);
      };
    }(e) : function (e) {
      return t.scale.getLabel(e);
    };
  }
  function np(t, e) {
    return "category" === t.type ? t.scale.getLabel(e) : e.value;
  }
  function rp(t) {
    var e = t.get("interval");
    return null == e ? "auto" : e;
  }
  function ip(t) {
    return "category" === t.type && 0 === rp(t.getLabelModel());
  }
  function op(t) {
    return _f(t.getSource(), t);
  }
  function ap(t, e) {
    var n = e;
    e instanceof N_ || (n = new N_(e));
    var r = tp(n);
    return r.setExtent(t[0], t[1]), Jf(r, n), r;
  }
  function sp(t) {
    d(t, uT);
  }
  function lp(t, e) {
    return Math.abs(t - e) < fT;
  }
  function up(t, e, n) {
    var r = 0,
      i = t[0];
    if (!i) return !1;
    for (var o = 1; o < t.length; o++) {
      var a = t[o];
      r += Fr(i[0], i[1], a[0], a[1], e, n), i = a;
    }
    var s = t[0];
    return lp(i[0], s[0]) && lp(i[1], s[1]) || (r += Fr(i[0], i[1], s[0], s[1], e, n)), 0 !== r;
  }
  function hp(t) {
    if (!t.UTF8Encoding) return t;
    var e = t,
      n = e.UTF8Scale;
    null == n && (n = 1024);
    for (var r = e.features, i = 0; i < r.length; i++) {
      var o = r[i],
        a = o.geometry;
      if ("Polygon" === a.type) for (var s = a.coordinates, l = 0; l < s.length; l++) {
        s[l] = cp(s[l], a.encodeOffsets[l], n);
      } else if ("MultiPolygon" === a.type) for (var s = a.coordinates, l = 0; l < s.length; l++) {
        for (var u = s[l], h = 0; h < u.length; h++) {
          u[h] = cp(u[h], a.encodeOffsets[l][h], n);
        }
      }
    }
    return e.UTF8Encoding = !1, e;
  }
  function cp(t, e, n) {
    for (var r = [], i = e[0], o = e[1], a = 0; a < t.length; a += 2) {
      var s = t.charCodeAt(a) - 64,
        l = t.charCodeAt(a + 1) - 64;
      s = s >> 1 ^ -(1 & s), l = l >> 1 ^ -(1 & l), s += i, l += o, i = s, o = l, r.push([s / n, l / n]);
    }
    return r;
  }
  function fp(t, e) {
    return t = hp(t), y(_(t.features, function (t) {
      return t.geometry && t.properties && t.geometry.coordinates.length > 0;
    }), function (t) {
      var n = t.properties,
        r = t.geometry,
        i = [];
      if ("Polygon" === r.type) {
        var o = r.coordinates;
        i.push({
          type: "polygon",
          exterior: o[0],
          interiors: o.slice(1)
        });
      }
      if ("MultiPolygon" === r.type) {
        var o = r.coordinates;
        v(o, function (t) {
          t[0] && i.push({
            type: "polygon",
            exterior: t[0],
            interiors: t.slice(1)
          });
        });
      }
      var a = new pT(n[e || "name"], i, n.cp);
      return a.properties = n, a;
    });
  }
  function pp(t) {
    return "category" === t.type ? gp(t) : mp(t);
  }
  function dp(t, e) {
    return "category" === t.type ? yp(t, e) : {
      ticks: y(t.scale.getTicks(), function (t) {
        return t.value;
      })
    };
  }
  function gp(t) {
    var e = t.getLabelModel(),
      n = vp(t, e);
    return !e.get("show") || t.scale.isBlank() ? {
      labels: [],
      labelCategoryInterval: n.labelCategoryInterval
    } : n;
  }
  function vp(t, e) {
    var n = _p(t, "labels"),
      r = rp(e),
      i = xp(n, r);
    if (i) return i;
    var o, a;
    return T(r) ? o = Cp(t, r) : (a = "auto" === r ? bp(t) : r, o = Tp(t, a)), wp(n, r, {
      labels: o,
      labelCategoryInterval: a
    });
  }
  function yp(t, e) {
    var n = _p(t, "ticks"),
      r = rp(e),
      i = xp(n, r);
    if (i) return i;
    var o, a;
    if ((!e.get("show") || t.scale.isBlank()) && (o = []), T(r)) o = Cp(t, r, !0);else if ("auto" === r) {
      var s = vp(t, t.getLabelModel());
      a = s.labelCategoryInterval, o = y(s.labels, function (t) {
        return t.tickValue;
      });
    } else a = r, o = Tp(t, a, !0);
    return wp(n, r, {
      ticks: o,
      tickCategoryInterval: a
    });
  }
  function mp(t) {
    var e = t.scale.getTicks(),
      n = ep(t);
    return {
      labels: y(e, function (e, r) {
        return {
          formattedLabel: n(e, r),
          rawLabel: t.scale.getLabel(e),
          tickValue: e.value
        };
      })
    };
  }
  function _p(t, e) {
    return dT(t)[e] || (dT(t)[e] = []);
  }
  function xp(t, e) {
    for (var n = 0; n < t.length; n++) {
      if (t[n].key === e) return t[n].value;
    }
  }
  function wp(t, e, n) {
    return t.push({
      key: e,
      value: n
    }), n;
  }
  function bp(t) {
    var e = dT(t).autoInterval;
    return null != e ? e : dT(t).autoInterval = t.calculateCategoryInterval();
  }
  function Sp(t) {
    var e = Mp(t),
      n = ep(t),
      r = (e.axisRotate - e.labelRotate) / 180 * Math.PI,
      i = t.scale,
      o = i.getExtent(),
      a = i.count();
    if (o[1] - o[0] < 1) return 0;
    var s = 1;
    a > 40 && (s = Math.max(1, Math.floor(a / 40)));
    for (var l = o[0], u = t.dataToCoord(l + 1) - t.dataToCoord(l), h = Math.abs(u * Math.cos(r)), c = Math.abs(u * Math.sin(r)), f = 0, p = 0; l <= o[1]; l += s) {
      var d = 0,
        g = 0,
        v = On(n({
          value: l
        }), e.font, "center", "top");
      d = 1.3 * v.width, g = 1.3 * v.height, f = Math.max(f, d, 7), p = Math.max(p, g, 7);
    }
    var y = f / h,
      m = p / c;
    isNaN(y) && (y = 1 / 0), isNaN(m) && (m = 1 / 0);
    var _ = Math.max(0, Math.floor(Math.min(y, m))),
      x = dT(t.model),
      w = t.getExtent(),
      b = x.lastAutoInterval,
      S = x.lastTickCount;
    return null != b && null != S && Math.abs(b - _) <= 1 && Math.abs(S - a) <= 1 && b > _ && x.axisExtent0 === w[0] && x.axisExtent1 === w[1] ? _ = b : (x.lastTickCount = a, x.lastAutoInterval = _, x.axisExtent0 = w[0], x.axisExtent1 = w[1]), _;
  }
  function Mp(t) {
    var e = t.getLabelModel();
    return {
      axisRotate: t.getRotate ? t.getRotate() : t.isHorizontal && !t.isHorizontal() ? 90 : 0,
      labelRotate: e.get("rotate") || 0,
      font: e.getFont()
    };
  }
  function Tp(t, e, n) {
    function r(t) {
      var e = {
        value: t
      };
      l.push(n ? t : {
        formattedLabel: i(e),
        rawLabel: o.getLabel(e),
        tickValue: t
      });
    }
    var i = ep(t),
      o = t.scale,
      a = o.getExtent(),
      s = t.getLabelModel(),
      l = [],
      u = Math.max((e || 0) + 1, 1),
      h = a[0],
      c = o.count();
    0 !== h && u > 1 && c / u > 2 && (h = Math.round(Math.ceil(h / u) * u));
    var f = ip(t),
      p = s.get("showMinLabel") || f,
      d = s.get("showMaxLabel") || f;
    p && h !== a[0] && r(a[0]);
    for (var g = h; g <= a[1]; g += u) {
      r(g);
    }
    return d && g - u !== a[1] && r(a[1]), l;
  }
  function Cp(t, e, n) {
    var r = t.scale,
      i = ep(t),
      o = [];
    return v(r.getTicks(), function (t) {
      var a = r.getLabel(t),
        s = t.value;
      e(t.value, a) && o.push(n ? s : {
        formattedLabel: i(t),
        rawLabel: a,
        tickValue: s
      });
    }), o;
  }
  function kp(t, e) {
    var n = t[1] - t[0],
      r = e,
      i = n / r / 2;
    t[0] += i, t[1] -= i;
  }
  function Dp(t, e, n, r) {
    function i(t, e) {
      return t = io(t), e = io(e), f ? t > e : e > t;
    }
    var o = e.length;
    if (t.onBand && !n && o) {
      var a,
        s,
        l = t.getExtent();
      if (1 === o) e[0].coord = l[0], a = e[1] = {
        coord: l[0]
      };else {
        var u = e[o - 1].tickValue - e[0].tickValue,
          h = (e[o - 1].coord - e[0].coord) / u;
        v(e, function (t) {
          t.coord -= h / 2;
        });
        var c = t.scale.getExtent();
        s = 1 + c[1] - e[o - 1].tickValue, a = {
          coord: e[o - 1].coord + h * s
        }, e.push(a);
      }
      var f = l[0] > l[1];
      i(e[0].coord, l[0]) && (r ? e[0].coord = l[0] : e.shift()), r && i(l[0], e[0].coord) && e.unshift({
        coord: l[0]
      }), i(l[1], a.coord) && (r ? a.coord = l[1] : e.pop()), r && i(a.coord, l[1]) && e.push({
        coord: l[1]
      });
    }
  }
  function Ip(t, e, n) {
    e = M(e) && {
      coordDimensions: e
    } || h({}, e);
    var r = t.getSource(),
      i = pf(r, e),
      o = new EM(i, t);
    return o.initData(r, n), o;
  }
  function Ap(t, e) {
    var n = t.get("center"),
      r = e.getWidth(),
      i = e.getHeight(),
      o = Math.min(r, i),
      a = ro(n[0], e.getWidth()),
      s = ro(n[1], e.getHeight()),
      l = ro(t.get("radius"), o / 2);
    return {
      cx: a,
      cy: s,
      r: l
    };
  }
  function Pp(t, e) {
    var n = null == t ? "" : t + "";
    return e && ("string" == typeof e ? n = e.replace("{value}", n) : "function" == typeof e && (n = e(t))), n;
  }
  var _Lp = function Lp(t, e) {
      return (_Lp = Object.setPrototypeOf || {
        __proto__: []
      } instanceof Array && function (t, e) {
        t.__proto__ = e;
      } || function (t, e) {
        for (var n in e) {
          e.hasOwnProperty(n) && (t[n] = e[n]);
        }
      })(t, e);
    },
    Op = function () {
      function t() {
        this.firefox = !1, this.ie = !1, this.edge = !1, this.weChat = !1;
      }
      return t;
    }(),
    Rp = function () {
      function t() {
        this.browser = new Op(), this.node = !1, this.wxa = !1, this.worker = !1, this.canvasSupported = !1, this.svgSupported = !1, this.touchEventsSupported = !1, this.pointerEventsSupported = !1, this.domSupported = !1;
      }
      return t;
    }(),
    Ep = new Rp();
  "object" == (typeof wx === "undefined" ? "undefined" : _typeof(wx)) && "function" == typeof wx.getSystemInfoSync ? (Ep.wxa = !0, Ep.canvasSupported = !0, Ep.touchEventsSupported = !0) : "undefined" == typeof document && "undefined" != typeof self ? (Ep.worker = !0, Ep.canvasSupported = !0) : "undefined" == typeof navigator ? (Ep.node = !0, Ep.canvasSupported = !0, Ep.svgSupported = !0) : r(navigator.userAgent, Ep);
  var Bp = {
      "[object Function]": !0,
      "[object RegExp]": !0,
      "[object Date]": !0,
      "[object Error]": !0,
      "[object CanvasGradient]": !0,
      "[object CanvasPattern]": !0,
      "[object Image]": !0,
      "[object Canvas]": !0
    },
    zp = {
      "[object Int8Array]": !0,
      "[object Uint8Array]": !0,
      "[object Uint8ClampedArray]": !0,
      "[object Int16Array]": !0,
      "[object Uint16Array]": !0,
      "[object Int32Array]": !0,
      "[object Uint32Array]": !0,
      "[object Float32Array]": !0,
      "[object Float64Array]": !0
    },
    Fp = Object.prototype.toString,
    Np = Array.prototype,
    Hp = Np.forEach,
    Vp = Np.filter,
    Wp = Np.slice,
    Gp = Np.map,
    Up = function () {}.constructor,
    qp = Up ? Up.prototype : null,
    Xp = {},
    Yp = 2311,
    jp = function jp() {
      return Xp.createCanvas();
    };
  Xp.createCanvas = function () {
    return document.createElement("canvas");
  };
  var Zp = qp && T(qp.bind) ? qp.call.bind(qp.bind) : b,
    Kp = "__ec_primitive__",
    $p = function () {
      function t(e) {
        function n(t, e) {
          r ? i.set(t, e) : i.set(e, t);
        }
        this.data = {};
        var r = M(e);
        this.data = {};
        var i = this;
        e instanceof t ? e.each(n) : e && v(e, n);
      }
      return t.prototype.get = function (t) {
        return this.data.hasOwnProperty(t) ? this.data[t] : null;
      }, t.prototype.set = function (t, e) {
        return this.data[t] = e;
      }, t.prototype.each = function (t, e) {
        for (var n in this.data) {
          this.data.hasOwnProperty(n) && t.call(e, this.data[n], n);
        }
      }, t.prototype.keys = function () {
        return w(this.data);
      }, t.prototype.removeKey = function (t) {
        delete this.data[t];
      }, t;
    }(),
    Qp = (Object.freeze || Object)({
      $override: i,
      guid: o,
      logError: a,
      clone: s,
      merge: l,
      mergeAll: u,
      extend: h,
      defaults: c,
      createCanvas: jp,
      indexOf: f,
      inherits: p,
      mixin: d,
      isArrayLike: g,
      each: v,
      map: y,
      reduce: m,
      filter: _,
      find: x,
      keys: w,
      bind: Zp,
      curry: S,
      isArray: M,
      isFunction: T,
      isString: C,
      isStringSafe: k,
      isNumber: D,
      isObject: I,
      isBuiltInObject: A,
      isTypedArray: P,
      isDom: L,
      isGradientObject: O,
      isPatternObject: R,
      isRegExp: E,
      eqNaN: B,
      retrieve: z,
      retrieve2: F,
      retrieve3: N,
      slice: H,
      normalizeCssArray: V,
      assert: W,
      trim: G,
      setAsPrimitive: U,
      isPrimitive: q,
      HashMap: $p,
      createHashMap: X,
      concatArray: Y,
      createObject: j,
      hasOwn: Z,
      noop: K
    }),
    Jp = ie,
    td = oe,
    ed = ce,
    nd = fe,
    rd = (Object.freeze || Object)({
      create: $,
      copy: Q,
      clone: J,
      set: te,
      add: ee,
      scaleAndAdd: ne,
      sub: re,
      len: ie,
      length: Jp,
      lenSquare: oe,
      lengthSquare: td,
      mul: ae,
      div: se,
      dot: le,
      scale: ue,
      normalize: he,
      distance: ce,
      dist: ed,
      distanceSquare: fe,
      distSquare: nd,
      negate: pe,
      lerp: de,
      applyTransform: ge,
      min: ve,
      max: ye
    }),
    id = function () {
      function t(t, e) {
        this.target = t, this.topTarget = e && e.topTarget;
      }
      return t;
    }(),
    od = function () {
      function t(t) {
        this.handler = t, t.on("mousedown", this._dragStart, this), t.on("mousemove", this._drag, this), t.on("mouseup", this._dragEnd, this);
      }
      return t.prototype._dragStart = function (t) {
        for (var e = t.target; e && !e.draggable;) {
          e = e.parent;
        }
        e && (this._draggingTarget = e, e.dragging = !0, this._x = t.offsetX, this._y = t.offsetY, this.handler.dispatchToElement(new id(e, t), "dragstart", t.event));
      }, t.prototype._drag = function (t) {
        var e = this._draggingTarget;
        if (e) {
          var n = t.offsetX,
            r = t.offsetY,
            i = n - this._x,
            o = r - this._y;
          this._x = n, this._y = r, e.drift(i, o, t), this.handler.dispatchToElement(new id(e, t), "drag", t.event);
          var a = this.handler.findHover(n, r, e).target,
            s = this._dropTarget;
          this._dropTarget = a, e !== a && (s && a !== s && this.handler.dispatchToElement(new id(s, t), "dragleave", t.event), a && a !== s && this.handler.dispatchToElement(new id(a, t), "dragenter", t.event));
        }
      }, t.prototype._dragEnd = function (t) {
        var e = this._draggingTarget;
        e && (e.dragging = !1), this.handler.dispatchToElement(new id(e, t), "dragend", t.event), this._dropTarget && this.handler.dispatchToElement(new id(this._dropTarget, t), "drop", t.event), this._draggingTarget = null, this._dropTarget = null;
      }, t;
    }(),
    ad = function () {
      function t(t) {
        t && (this._$eventProcessor = t);
      }
      return t.prototype.on = function (t, e, n, r) {
        this._$handlers || (this._$handlers = {});
        var i = this._$handlers;
        if ("function" == typeof e && (r = n, n = e, e = null), !n || !t) return this;
        var o = this._$eventProcessor;
        null != e && o && o.normalizeQuery && (e = o.normalizeQuery(e)), i[t] || (i[t] = []);
        for (var a = 0; a < i[t].length; a++) {
          if (i[t][a].h === n) return this;
        }
        var s = {
            h: n,
            query: e,
            ctx: r || this,
            callAtLast: n.zrEventfulCallAtLast
          },
          l = i[t].length - 1,
          u = i[t][l];
        return u && u.callAtLast ? i[t].splice(l, 0, s) : i[t].push(s), this;
      }, t.prototype.isSilent = function (t) {
        var e = this._$handlers;
        return !e || !e[t] || !e[t].length;
      }, t.prototype.off = function (t, e) {
        var n = this._$handlers;
        if (!n) return this;
        if (!t) return this._$handlers = {}, this;
        if (e) {
          if (n[t]) {
            for (var r = [], i = 0, o = n[t].length; o > i; i++) {
              n[t][i].h !== e && r.push(n[t][i]);
            }
            n[t] = r;
          }
          n[t] && 0 === n[t].length && delete n[t];
        } else delete n[t];
        return this;
      }, t.prototype.trigger = function (t) {
        for (var e = [], n = 1; n < arguments.length; n++) {
          e[n - 1] = arguments[n];
        }
        if (!this._$handlers) return this;
        var r = this._$handlers[t],
          i = this._$eventProcessor;
        if (r) for (var o = e.length, a = r.length, s = 0; a > s; s++) {
          var l = r[s];
          if (!i || !i.filter || null == l.query || i.filter(t, l.query)) switch (o) {
            case 0:
              l.h.call(l.ctx);
              break;
            case 1:
              l.h.call(l.ctx, e[0]);
              break;
            case 2:
              l.h.call(l.ctx, e[0], e[1]);
              break;
            default:
              l.h.apply(l.ctx, e);
          }
        }
        return i && i.afterTrigger && i.afterTrigger(t), this;
      }, t.prototype.triggerWithContext = function (t) {
        if (!this._$handlers) return this;
        var e = this._$handlers[t],
          n = this._$eventProcessor;
        if (e) for (var r = arguments, i = r.length, o = r[i - 1], a = e.length, s = 0; a > s; s++) {
          var l = e[s];
          if (!n || !n.filter || null == l.query || n.filter(t, l.query)) switch (i) {
            case 0:
              l.h.call(o);
              break;
            case 1:
              l.h.call(o, r[0]);
              break;
            case 2:
              l.h.call(o, r[0], r[1]);
              break;
            default:
              l.h.apply(o, r.slice(1, i - 1));
          }
        }
        return n && n.afterTrigger && n.afterTrigger(t), this;
      }, t;
    }(),
    sd = Math.log(2),
    ld = "___zrEVENTSAVED",
    ud = "undefined" != typeof window && !!window.addEventListener,
    hd = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
    cd = [],
    fd = ud ? function (t) {
      t.preventDefault(), t.stopPropagation(), t.cancelBubble = !0;
    } : function (t) {
      t.returnValue = !1, t.cancelBubble = !0;
    },
    pd = function () {
      function t() {
        this._track = [];
      }
      return t.prototype.recognize = function (t, e, n) {
        return this._doTrack(t, e, n), this._recognize(t);
      }, t.prototype.clear = function () {
        return this._track.length = 0, this;
      }, t.prototype._doTrack = function (t, e, n) {
        var r = t.touches;
        if (r) {
          for (var i = {
              points: [],
              touches: [],
              target: e,
              event: t
            }, o = 0, a = r.length; a > o; o++) {
            var s = r[o],
              l = Me(n, s, {});
            i.points.push([l.zrX, l.zrY]), i.touches.push(s);
          }
          this._track.push(i);
        }
      }, t.prototype._recognize = function (t) {
        for (var e in dd) {
          if (dd.hasOwnProperty(e)) {
            var n = dd[e](this._track, t);
            if (n) return n;
          }
        }
      }, t;
    }(),
    dd = {
      pinch: function pinch(t, e) {
        var n = t.length;
        if (n) {
          var r = (t[n - 1] || {}).points,
            i = (t[n - 2] || {}).points || r;
          if (i && i.length > 1 && r && r.length > 1) {
            var o = Pe(r) / Pe(i);
            !isFinite(o) && (o = 1), e.pinchScale = o;
            var a = Le(r);
            return e.pinchX = a[0], e.pinchY = a[1], {
              type: "pinch",
              target: t[0].target,
              event: e
            };
          }
        }
      }
    },
    gd = "silent",
    vd = function (t) {
      function n() {
        var e = null !== t && t.apply(this, arguments) || this;
        return e.handler = null, e;
      }
      return e(n, t), n.prototype.dispose = function () {}, n.prototype.setCursor = function () {}, n;
    }(ad),
    yd = function () {
      function t(t, e) {
        this.x = t, this.y = e;
      }
      return t;
    }(),
    md = ["click", "dblclick", "mousewheel", "mouseout", "mouseup", "mousedown", "mousemove", "contextmenu"],
    _d = function (t) {
      function n(e, n, r, i) {
        var o = t.call(this) || this;
        return o._hovered = new yd(0, 0), o.storage = e, o.painter = n, o.painterRoot = i, r = r || new vd(), o.proxy = null, o.setHandlerProxy(r), o._draggingMgr = new od(o), o;
      }
      return e(n, t), n.prototype.setHandlerProxy = function (t) {
        this.proxy && this.proxy.dispose(), t && (v(md, function (e) {
          t.on && t.on(e, this[e], this);
        }, this), t.handler = this), this.proxy = t;
      }, n.prototype.mousemove = function (t) {
        var e = t.zrX,
          n = t.zrY,
          r = Be(this, e, n),
          i = this._hovered,
          o = i.target;
        o && !o.__zr && (i = this.findHover(i.x, i.y), o = i.target);
        var a = this._hovered = r ? new yd(e, n) : this.findHover(e, n),
          s = a.target,
          l = this.proxy;
        l.setCursor && l.setCursor(s ? s.cursor : "default"), o && s !== o && this.dispatchToElement(i, "mouseout", t), this.dispatchToElement(a, "mousemove", t), s && s !== o && this.dispatchToElement(a, "mouseover", t);
      }, n.prototype.mouseout = function (t) {
        var e = t.zrEventControl,
          n = t.zrIsToLocalDOM;
        "only_globalout" !== e && this.dispatchToElement(this._hovered, "mouseout", t), "no_globalout" !== e && !n && this.trigger("globalout", {
          type: "globalout",
          event: t
        });
      }, n.prototype.resize = function () {
        this._hovered = new yd(0, 0);
      }, n.prototype.dispatch = function (t, e) {
        var n = this[t];
        n && n.call(this, e);
      }, n.prototype.dispose = function () {
        this.proxy.dispose(), this.storage = null, this.proxy = null, this.painter = null;
      }, n.prototype.setCursorStyle = function (t) {
        var e = this.proxy;
        e.setCursor && e.setCursor(t);
      }, n.prototype.dispatchToElement = function (t, e, n) {
        t = t || {};
        var r = t.target;
        if (!r || !r.silent) {
          for (var i = "on" + e, o = Oe(e, t, n); r && (r[i] && (o.cancelBubble = !!r[i].call(r, o)), r.trigger(e, o), r = r.__hostTarget ? r.__hostTarget : r.parent, !o.cancelBubble);) {
            ;
          }
          o.cancelBubble || (this.trigger(e, o), this.painter && this.painter.eachOtherLayer && this.painter.eachOtherLayer(function (t) {
            "function" == typeof t[i] && t[i].call(t, o), t.trigger && t.trigger(e, o);
          }));
        }
      }, n.prototype.findHover = function (t, e, n) {
        for (var r = this.storage.getDisplayList(), i = new yd(t, e), o = r.length - 1; o >= 0; o--) {
          var a = void 0;
          if (r[o] !== n && !r[o].ignore && (a = Ee(r[o], t, e)) && (!i.topTarget && (i.topTarget = r[o]), a !== gd)) {
            i.target = r[o];
            break;
          }
        }
        return i;
      }, n.prototype.processGesture = function (t, e) {
        this._gestureMgr || (this._gestureMgr = new pd());
        var n = this._gestureMgr;
        "start" === e && n.clear();
        var r = n.recognize(t, this.findHover(t.zrX, t.zrY, null).target, this.proxy.dom);
        if ("end" === e && n.clear(), r) {
          var i = r.type;
          t.gestureEvent = i;
          var o = new yd();
          o.target = r.target, this.dispatchToElement(o, i, r.event);
        }
      }, n;
    }(ad);
  v(["click", "mousedown", "mouseup", "mousewheel", "dblclick", "contextmenu"], function (t) {
    _d.prototype[t] = function (e) {
      var n,
        r,
        i = e.zrX,
        o = e.zrY,
        a = Be(this, i, o);
      if ("mouseup" === t && a || (n = this.findHover(i, o), r = n.target), "mousedown" === t) this._downEl = r, this._downPoint = [e.zrX, e.zrY], this._upEl = r;else if ("mouseup" === t) this._upEl = r;else if ("click" === t) {
        if (this._downEl !== this._upEl || !this._downPoint || ed(this._downPoint, [e.zrX, e.zrY]) > 4) return;
        this._downPoint = null;
      }
      this.dispatchToElement(n, t, e);
    };
  });
  var xd,
    wd,
    bd = (Object.freeze || Object)({
      create: ze,
      identity: Fe,
      copy: Ne,
      mul: He,
      translate: Ve,
      rotate: We,
      scale: Ge,
      invert: Ue,
      clone: qe
    }),
    Sd = Fe,
    Md = 5e-5,
    Td = [],
    Cd = [],
    kd = ze(),
    Dd = Math.abs,
    Id = function () {
      function t() {}
      return t.prototype.setPosition = function (t) {
        this.x = t[0], this.y = t[1];
      }, t.prototype.setScale = function (t) {
        this.scaleX = t[0], this.scaleY = t[1];
      }, t.prototype.setOrigin = function (t) {
        this.originX = t[0], this.originY = t[1];
      }, t.prototype.needLocalTransform = function () {
        return Xe(this.rotation) || Xe(this.x) || Xe(this.y) || Xe(this.scaleX - 1) || Xe(this.scaleY - 1);
      }, t.prototype.updateTransform = function () {
        var t = this.parent,
          e = t && t.transform,
          n = this.needLocalTransform(),
          r = this.transform;
        return n || e ? (r = r || ze(), n ? this.getLocalTransform(r) : Sd(r), e && (n ? He(r, t.transform, r) : Ne(r, t.transform)), this.transform = r, void this._resolveGlobalScaleRatio(r)) : void (r && Sd(r));
      }, t.prototype._resolveGlobalScaleRatio = function (t) {
        var e = this.globalScaleRatio;
        if (null != e && 1 !== e) {
          this.getGlobalScale(Td);
          var n = Td[0] < 0 ? -1 : 1,
            r = Td[1] < 0 ? -1 : 1,
            i = ((Td[0] - n) * e + n) / Td[0] || 0,
            o = ((Td[1] - r) * e + r) / Td[1] || 0;
          t[0] *= i, t[1] *= i, t[2] *= o, t[3] *= o;
        }
        this.invTransform = this.invTransform || ze(), Ue(this.invTransform, t);
      }, t.prototype.getLocalTransform = function (e) {
        return t.getLocalTransform(this, e);
      }, t.prototype.getComputedTransform = function () {
        for (var t = this, e = []; t;) {
          e.push(t), t = t.parent;
        }
        for (; t = e.pop();) {
          t.updateTransform();
        }
        return this.transform;
      }, t.prototype.setLocalTransform = function (t) {
        if (t) {
          var e = t[0] * t[0] + t[1] * t[1],
            n = t[2] * t[2] + t[3] * t[3];
          Xe(e - 1) && (e = Math.sqrt(e)), Xe(n - 1) && (n = Math.sqrt(n)), t[0] < 0 && (e = -e), t[3] < 0 && (n = -n), this.rotation = Math.atan2(-t[1] / n, t[0] / e), 0 > e && 0 > n && (this.rotation += Math.PI, e = -e, n = -n), this.x = t[4], this.y = t[5], this.scaleX = e, this.scaleY = n;
        }
      }, t.prototype.decomposeTransform = function () {
        if (this.transform) {
          var t = this.parent,
            e = this.transform;
          t && t.transform && (He(Cd, t.invTransform, e), e = Cd);
          var n = this.originX,
            r = this.originY;
          (n || r) && (kd[4] = n, kd[5] = r, He(Cd, e, kd), Cd[4] -= n, Cd[5] -= r, e = Cd), this.setLocalTransform(e);
        }
      }, t.prototype.getGlobalScale = function (t) {
        var e = this.transform;
        return t = t || [], e ? (t[0] = Math.sqrt(e[0] * e[0] + e[1] * e[1]), t[1] = Math.sqrt(e[2] * e[2] + e[3] * e[3]), e[0] < 0 && (t[0] = -t[0]), e[3] < 0 && (t[1] = -t[1]), t) : (t[0] = 1, t[1] = 1, t);
      }, t.prototype.transformCoordToLocal = function (t, e) {
        var n = [t, e],
          r = this.invTransform;
        return r && ge(n, n, r), n;
      }, t.prototype.transformCoordToGlobal = function (t, e) {
        var n = [t, e],
          r = this.transform;
        return r && ge(n, n, r), n;
      }, t.prototype.getLineScale = function () {
        var t = this.transform;
        return t && Dd(t[0] - 1) > 1e-10 && Dd(t[3] - 1) > 1e-10 ? Math.sqrt(Dd(t[0] * t[3] - t[2] * t[1])) : 1;
      }, t.getLocalTransform = function (t, e) {
        e = e || [], Sd(e);
        var n = t.originX || 0,
          r = t.originY || 0,
          i = t.scaleX,
          o = t.scaleY,
          a = t.rotation || 0,
          s = t.x,
          l = t.y;
        return e[4] -= n, e[5] -= r, e[0] *= i, e[1] *= o, e[2] *= i, e[3] *= o, e[4] *= i, e[5] *= o, a && We(e, e, a), e[4] += n, e[5] += r, e[4] += s, e[5] += l, e;
      }, t.initDefaultProps = function () {
        var e = t.prototype;
        e.x = 0, e.y = 0, e.scaleX = 1, e.scaleY = 1, e.originX = 0, e.originY = 0, e.rotation = 0, e.globalScaleRatio = 1;
      }(), t;
    }(),
    Ad = {
      linear: function linear(t) {
        return t;
      },
      quadraticIn: function quadraticIn(t) {
        return t * t;
      },
      quadraticOut: function quadraticOut(t) {
        return t * (2 - t);
      },
      quadraticInOut: function quadraticInOut(t) {
        return (t *= 2) < 1 ? .5 * t * t : -.5 * (--t * (t - 2) - 1);
      },
      cubicIn: function cubicIn(t) {
        return t * t * t;
      },
      cubicOut: function cubicOut(t) {
        return --t * t * t + 1;
      },
      cubicInOut: function cubicInOut(t) {
        return (t *= 2) < 1 ? .5 * t * t * t : .5 * ((t -= 2) * t * t + 2);
      },
      quarticIn: function quarticIn(t) {
        return t * t * t * t;
      },
      quarticOut: function quarticOut(t) {
        return 1 - --t * t * t * t;
      },
      quarticInOut: function quarticInOut(t) {
        return (t *= 2) < 1 ? .5 * t * t * t * t : -.5 * ((t -= 2) * t * t * t - 2);
      },
      quinticIn: function quinticIn(t) {
        return t * t * t * t * t;
      },
      quinticOut: function quinticOut(t) {
        return --t * t * t * t * t + 1;
      },
      quinticInOut: function quinticInOut(t) {
        return (t *= 2) < 1 ? .5 * t * t * t * t * t : .5 * ((t -= 2) * t * t * t * t + 2);
      },
      sinusoidalIn: function sinusoidalIn(t) {
        return 1 - Math.cos(t * Math.PI / 2);
      },
      sinusoidalOut: function sinusoidalOut(t) {
        return Math.sin(t * Math.PI / 2);
      },
      sinusoidalInOut: function sinusoidalInOut(t) {
        return .5 * (1 - Math.cos(Math.PI * t));
      },
      exponentialIn: function exponentialIn(t) {
        return 0 === t ? 0 : Math.pow(1024, t - 1);
      },
      exponentialOut: function exponentialOut(t) {
        return 1 === t ? 1 : 1 - Math.pow(2, -10 * t);
      },
      exponentialInOut: function exponentialInOut(t) {
        return 0 === t ? 0 : 1 === t ? 1 : (t *= 2) < 1 ? .5 * Math.pow(1024, t - 1) : .5 * (-Math.pow(2, -10 * (t - 1)) + 2);
      },
      circularIn: function circularIn(t) {
        return 1 - Math.sqrt(1 - t * t);
      },
      circularOut: function circularOut(t) {
        return Math.sqrt(1 - --t * t);
      },
      circularInOut: function circularInOut(t) {
        return (t *= 2) < 1 ? -.5 * (Math.sqrt(1 - t * t) - 1) : .5 * (Math.sqrt(1 - (t -= 2) * t) + 1);
      },
      elasticIn: function elasticIn(t) {
        var e,
          n = .1,
          r = .4;
        return 0 === t ? 0 : 1 === t ? 1 : (!n || 1 > n ? (n = 1, e = r / 4) : e = r * Math.asin(1 / n) / (2 * Math.PI), -(n * Math.pow(2, 10 * (t -= 1)) * Math.sin(2 * (t - e) * Math.PI / r)));
      },
      elasticOut: function elasticOut(t) {
        var e,
          n = .1,
          r = .4;
        return 0 === t ? 0 : 1 === t ? 1 : (!n || 1 > n ? (n = 1, e = r / 4) : e = r * Math.asin(1 / n) / (2 * Math.PI), n * Math.pow(2, -10 * t) * Math.sin(2 * (t - e) * Math.PI / r) + 1);
      },
      elasticInOut: function elasticInOut(t) {
        var e,
          n = .1,
          r = .4;
        return 0 === t ? 0 : 1 === t ? 1 : (!n || 1 > n ? (n = 1, e = r / 4) : e = r * Math.asin(1 / n) / (2 * Math.PI), (t *= 2) < 1 ? -.5 * n * Math.pow(2, 10 * (t -= 1)) * Math.sin(2 * (t - e) * Math.PI / r) : n * Math.pow(2, -10 * (t -= 1)) * Math.sin(2 * (t - e) * Math.PI / r) * .5 + 1);
      },
      backIn: function backIn(t) {
        var e = 1.70158;
        return t * t * ((e + 1) * t - e);
      },
      backOut: function backOut(t) {
        var e = 1.70158;
        return --t * t * ((e + 1) * t + e) + 1;
      },
      backInOut: function backInOut(t) {
        var e = 2.5949095;
        return (t *= 2) < 1 ? .5 * t * t * ((e + 1) * t - e) : .5 * ((t -= 2) * t * ((e + 1) * t + e) + 2);
      },
      bounceIn: function bounceIn(t) {
        return 1 - Ad.bounceOut(1 - t);
      },
      bounceOut: function bounceOut(t) {
        return 1 / 2.75 > t ? 7.5625 * t * t : 2 / 2.75 > t ? 7.5625 * (t -= 1.5 / 2.75) * t + .75 : 2.5 / 2.75 > t ? 7.5625 * (t -= 2.25 / 2.75) * t + .9375 : 7.5625 * (t -= 2.625 / 2.75) * t + .984375;
      },
      bounceInOut: function bounceInOut(t) {
        return .5 > t ? .5 * Ad.bounceIn(2 * t) : .5 * Ad.bounceOut(2 * t - 1) + .5;
      }
    },
    Pd = function () {
      function t(t) {
        this._initialized = !1, this._startTime = 0, this._pausedTime = 0, this._paused = !1, this._life = t.life || 1e3, this._delay = t.delay || 0, this.loop = null == t.loop ? !1 : t.loop, this.gap = t.gap || 0, this.easing = t.easing || "linear", this.onframe = t.onframe, this.ondestroy = t.ondestroy, this.onrestart = t.onrestart;
      }
      return t.prototype.step = function (t, e) {
        if (this._initialized || (this._startTime = t + this._delay, this._initialized = !0), this._paused) return void (this._pausedTime += e);
        var n = (t - this._startTime - this._pausedTime) / this._life;
        0 > n && (n = 0), n = Math.min(n, 1);
        var r = this.easing,
          i = "string" == typeof r ? Ad[r] : r,
          o = "function" == typeof i ? i(n) : n;
        if (this.onframe && this.onframe(o), 1 === n) {
          if (!this.loop) return !0;
          this._restart(t), this.onrestart && this.onrestart();
        }
        return !1;
      }, t.prototype._restart = function (t) {
        var e = (t - this._startTime - this._pausedTime) % this._life;
        this._startTime = t - e + this.gap, this._pausedTime = 0;
      }, t.prototype.pause = function () {
        this._paused = !0;
      }, t.prototype.resume = function () {
        this._paused = !1;
      }, t;
    }(),
    Ld = function () {
      function t(t) {
        this.value = t;
      }
      return t;
    }(),
    Od = function () {
      function t() {
        this._len = 0;
      }
      return t.prototype.insert = function (t) {
        var e = new Ld(t);
        return this.insertEntry(e), e;
      }, t.prototype.insertEntry = function (t) {
        this.head ? (this.tail.next = t, t.prev = this.tail, t.next = null, this.tail = t) : this.head = this.tail = t, this._len++;
      }, t.prototype.remove = function (t) {
        var e = t.prev,
          n = t.next;
        e ? e.next = n : this.head = n, n ? n.prev = e : this.tail = e, t.next = t.prev = null, this._len--;
      }, t.prototype.len = function () {
        return this._len;
      }, t.prototype.clear = function () {
        this.head = this.tail = null, this._len = 0;
      }, t;
    }(),
    Rd = function () {
      function t(t) {
        this._list = new Od(), this._maxSize = 10, this._map = {}, this._maxSize = t;
      }
      return t.prototype.put = function (t, e) {
        var n = this._list,
          r = this._map,
          i = null;
        if (null == r[t]) {
          var o = n.len(),
            a = this._lastRemovedEntry;
          if (o >= this._maxSize && o > 0) {
            var s = n.head;
            n.remove(s), delete r[s.key], i = s.value, this._lastRemovedEntry = s;
          }
          a ? a.value = e : a = new Ld(e), a.key = t, n.insertEntry(a), r[t] = a;
        }
        return i;
      }, t.prototype.get = function (t) {
        var e = this._map[t],
          n = this._list;
        return null != e ? (e !== n.tail && (n.remove(e), n.insertEntry(e)), e.value) : void 0;
      }, t.prototype.clear = function () {
        this._list.clear(), this._map = {};
      }, t.prototype.len = function () {
        return this._list.len();
      }, t;
    }(),
    Ed = {
      transparent: [0, 0, 0, 0],
      aliceblue: [240, 248, 255, 1],
      antiquewhite: [250, 235, 215, 1],
      aqua: [0, 255, 255, 1],
      aquamarine: [127, 255, 212, 1],
      azure: [240, 255, 255, 1],
      beige: [245, 245, 220, 1],
      bisque: [255, 228, 196, 1],
      black: [0, 0, 0, 1],
      blanchedalmond: [255, 235, 205, 1],
      blue: [0, 0, 255, 1],
      blueviolet: [138, 43, 226, 1],
      brown: [165, 42, 42, 1],
      burlywood: [222, 184, 135, 1],
      cadetblue: [95, 158, 160, 1],
      chartreuse: [127, 255, 0, 1],
      chocolate: [210, 105, 30, 1],
      coral: [255, 127, 80, 1],
      cornflowerblue: [100, 149, 237, 1],
      cornsilk: [255, 248, 220, 1],
      crimson: [220, 20, 60, 1],
      cyan: [0, 255, 255, 1],
      darkblue: [0, 0, 139, 1],
      darkcyan: [0, 139, 139, 1],
      darkgoldenrod: [184, 134, 11, 1],
      darkgray: [169, 169, 169, 1],
      darkgreen: [0, 100, 0, 1],
      darkgrey: [169, 169, 169, 1],
      darkkhaki: [189, 183, 107, 1],
      darkmagenta: [139, 0, 139, 1],
      darkolivegreen: [85, 107, 47, 1],
      darkorange: [255, 140, 0, 1],
      darkorchid: [153, 50, 204, 1],
      darkred: [139, 0, 0, 1],
      darksalmon: [233, 150, 122, 1],
      darkseagreen: [143, 188, 143, 1],
      darkslateblue: [72, 61, 139, 1],
      darkslategray: [47, 79, 79, 1],
      darkslategrey: [47, 79, 79, 1],
      darkturquoise: [0, 206, 209, 1],
      darkviolet: [148, 0, 211, 1],
      deeppink: [255, 20, 147, 1],
      deepskyblue: [0, 191, 255, 1],
      dimgray: [105, 105, 105, 1],
      dimgrey: [105, 105, 105, 1],
      dodgerblue: [30, 144, 255, 1],
      firebrick: [178, 34, 34, 1],
      floralwhite: [255, 250, 240, 1],
      forestgreen: [34, 139, 34, 1],
      fuchsia: [255, 0, 255, 1],
      gainsboro: [220, 220, 220, 1],
      ghostwhite: [248, 248, 255, 1],
      gold: [255, 215, 0, 1],
      goldenrod: [218, 165, 32, 1],
      gray: [128, 128, 128, 1],
      green: [0, 128, 0, 1],
      greenyellow: [173, 255, 47, 1],
      grey: [128, 128, 128, 1],
      honeydew: [240, 255, 240, 1],
      hotpink: [255, 105, 180, 1],
      indianred: [205, 92, 92, 1],
      indigo: [75, 0, 130, 1],
      ivory: [255, 255, 240, 1],
      khaki: [240, 230, 140, 1],
      lavender: [230, 230, 250, 1],
      lavenderblush: [255, 240, 245, 1],
      lawngreen: [124, 252, 0, 1],
      lemonchiffon: [255, 250, 205, 1],
      lightblue: [173, 216, 230, 1],
      lightcoral: [240, 128, 128, 1],
      lightcyan: [224, 255, 255, 1],
      lightgoldenrodyellow: [250, 250, 210, 1],
      lightgray: [211, 211, 211, 1],
      lightgreen: [144, 238, 144, 1],
      lightgrey: [211, 211, 211, 1],
      lightpink: [255, 182, 193, 1],
      lightsalmon: [255, 160, 122, 1],
      lightseagreen: [32, 178, 170, 1],
      lightskyblue: [135, 206, 250, 1],
      lightslategray: [119, 136, 153, 1],
      lightslategrey: [119, 136, 153, 1],
      lightsteelblue: [176, 196, 222, 1],
      lightyellow: [255, 255, 224, 1],
      lime: [0, 255, 0, 1],
      limegreen: [50, 205, 50, 1],
      linen: [250, 240, 230, 1],
      magenta: [255, 0, 255, 1],
      maroon: [128, 0, 0, 1],
      mediumaquamarine: [102, 205, 170, 1],
      mediumblue: [0, 0, 205, 1],
      mediumorchid: [186, 85, 211, 1],
      mediumpurple: [147, 112, 219, 1],
      mediumseagreen: [60, 179, 113, 1],
      mediumslateblue: [123, 104, 238, 1],
      mediumspringgreen: [0, 250, 154, 1],
      mediumturquoise: [72, 209, 204, 1],
      mediumvioletred: [199, 21, 133, 1],
      midnightblue: [25, 25, 112, 1],
      mintcream: [245, 255, 250, 1],
      mistyrose: [255, 228, 225, 1],
      moccasin: [255, 228, 181, 1],
      navajowhite: [255, 222, 173, 1],
      navy: [0, 0, 128, 1],
      oldlace: [253, 245, 230, 1],
      olive: [128, 128, 0, 1],
      olivedrab: [107, 142, 35, 1],
      orange: [255, 165, 0, 1],
      orangered: [255, 69, 0, 1],
      orchid: [218, 112, 214, 1],
      palegoldenrod: [238, 232, 170, 1],
      palegreen: [152, 251, 152, 1],
      paleturquoise: [175, 238, 238, 1],
      palevioletred: [219, 112, 147, 1],
      papayawhip: [255, 239, 213, 1],
      peachpuff: [255, 218, 185, 1],
      peru: [205, 133, 63, 1],
      pink: [255, 192, 203, 1],
      plum: [221, 160, 221, 1],
      powderblue: [176, 224, 230, 1],
      purple: [128, 0, 128, 1],
      red: [255, 0, 0, 1],
      rosybrown: [188, 143, 143, 1],
      royalblue: [65, 105, 225, 1],
      saddlebrown: [139, 69, 19, 1],
      salmon: [250, 128, 114, 1],
      sandybrown: [244, 164, 96, 1],
      seagreen: [46, 139, 87, 1],
      seashell: [255, 245, 238, 1],
      sienna: [160, 82, 45, 1],
      silver: [192, 192, 192, 1],
      skyblue: [135, 206, 235, 1],
      slateblue: [106, 90, 205, 1],
      slategray: [112, 128, 144, 1],
      slategrey: [112, 128, 144, 1],
      snow: [255, 250, 250, 1],
      springgreen: [0, 255, 127, 1],
      steelblue: [70, 130, 180, 1],
      tan: [210, 180, 140, 1],
      teal: [0, 128, 128, 1],
      thistle: [216, 191, 216, 1],
      tomato: [255, 99, 71, 1],
      turquoise: [64, 224, 208, 1],
      violet: [238, 130, 238, 1],
      wheat: [245, 222, 179, 1],
      white: [255, 255, 255, 1],
      whitesmoke: [245, 245, 245, 1],
      yellow: [255, 255, 0, 1],
      yellowgreen: [154, 205, 50, 1]
    },
    Bd = new Rd(20),
    zd = null,
    Fd = un,
    Nd = hn,
    Hd = (Object.freeze || Object)({
      parse: rn,
      lift: sn,
      toHex: ln,
      fastLerp: un,
      fastMapToColor: Fd,
      lerp: hn,
      mapToColor: Nd,
      modifyHSL: cn,
      modifyAlpha: fn,
      stringify: pn,
      lum: dn,
      random: gn
    }),
    Vd = Array.prototype.slice,
    Wd = [0, 0, 0, 0],
    Gd = function () {
      function t(t) {
        this.keyframes = [], this.maxTime = 0, this.arrDim = 0, this.interpolable = !0, this._needsSort = !1, this._isAllValueEqual = !0, this._lastFrame = 0, this._lastFramePercent = 0, this.propName = t;
      }
      return t.prototype.isFinished = function () {
        return this._finished;
      }, t.prototype.setFinished = function () {
        this._finished = !0, this._additiveTrack && this._additiveTrack.setFinished();
      }, t.prototype.needsAnimate = function () {
        return !this._isAllValueEqual && this.keyframes.length >= 2 && this.interpolable;
      }, t.prototype.getAdditiveTrack = function () {
        return this._additiveTrack;
      }, t.prototype.addKeyframe = function (t, e) {
        t >= this.maxTime ? this.maxTime = t : this._needsSort = !0;
        var n = this.keyframes,
          r = n.length;
        if (this.interpolable) if (g(e)) {
          var i = In(e);
          if (r > 0 && this.arrDim !== i) return void (this.interpolable = !1);
          if (1 === i && "number" != typeof e[0] || 2 === i && "number" != typeof e[0][0]) return void (this.interpolable = !1);
          if (r > 0) {
            var o = n[r - 1];
            this._isAllValueEqual && (1 === i ? Sn(e, o.value) || (this._isAllValueEqual = !1) : this._isAllValueEqual = !1);
          }
          this.arrDim = i;
        } else {
          if (this.arrDim > 0) return void (this.interpolable = !1);
          if ("string" == typeof e) {
            var a = rn(e);
            a ? (e = a, this.isValueColor = !0) : this.interpolable = !1;
          } else if ("number" != typeof e) return void (this.interpolable = !1);
          if (this._isAllValueEqual && r > 0) {
            var o = n[r - 1];
            this.isValueColor && !Sn(o.value, e) ? this._isAllValueEqual = !1 : o.value !== e && (this._isAllValueEqual = !1);
          }
        }
        var s = {
          time: t,
          value: e,
          percent: 0
        };
        return this.keyframes.push(s), s;
      }, t.prototype.prepare = function (t) {
        var e = this.keyframes;
        this._needsSort && e.sort(function (t, e) {
          return t.time - e.time;
        });
        for (var n = this.arrDim, r = e.length, i = e[r - 1], o = 0; r > o; o++) {
          e[o].percent = e[o].time / this.maxTime, n > 0 && o !== r - 1 && bn(e[o].value, i.value, n);
        }
        if (t && this.needsAnimate() && t.needsAnimate() && n === t.arrDim && this.isValueColor === t.isValueColor && !t._finished) {
          this._additiveTrack = t;
          for (var a = e[0].value, o = 0; r > o; o++) {
            0 === n ? e[o].additiveValue = this.isValueColor ? xn([], e[o].value, a, -1) : e[o].value - a : 1 === n ? e[o].additiveValue = xn([], e[o].value, a, -1) : 2 === n && (e[o].additiveValue = wn([], e[o].value, a, -1));
          }
        }
      }, t.prototype.step = function (t, e) {
        if (!this._finished) {
          this._additiveTrack && this._additiveTrack._finished && (this._additiveTrack = null);
          var n,
            r = null != this._additiveTrack,
            i = r ? "additiveValue" : "value",
            o = this.keyframes,
            a = this.keyframes.length,
            s = this.propName,
            l = this.arrDim,
            u = this.isValueColor;
          if (0 > e) n = 0;else if (e < this._lastFramePercent) {
            var h = Math.min(this._lastFrame + 1, a - 1);
            for (n = h; n >= 0 && !(o[n].percent <= e); n--) {
              ;
            }
            n = Math.min(n, a - 2);
          } else {
            for (n = this._lastFrame; a > n && !(o[n].percent > e); n++) {
              ;
            }
            n = Math.min(n - 1, a - 2);
          }
          var c = o[n + 1],
            f = o[n];
          if (f && c) {
            this._lastFrame = n, this._lastFramePercent = e;
            var p = c.percent - f.percent;
            if (0 !== p) {
              var d = (e - f.percent) / p,
                g = r ? this._additiveValue : u ? Wd : t[s];
              if ((l > 0 || u) && !g && (g = this._additiveValue = []), this.useSpline) {
                var v = o[n][i],
                  y = o[0 === n ? n : n - 1][i],
                  m = o[n > a - 2 ? a - 1 : n + 1][i],
                  _ = o[n > a - 3 ? a - 1 : n + 2][i];
                if (l > 0) 1 === l ? Tn(g, y, v, m, _, d, d * d, d * d * d) : Cn(g, y, v, m, _, d, d * d, d * d * d);else if (u) Tn(g, y, v, m, _, d, d * d, d * d * d), r || (t[s] = Dn(g));else {
                  var x = void 0;
                  x = this.interpolable ? Mn(y, v, m, _, d, d * d, d * d * d) : m, r ? this._additiveValue = x : t[s] = x;
                }
              } else if (l > 0) 1 === l ? mn(g, f[i], c[i], d) : _n(g, f[i], c[i], d);else if (u) mn(g, f[i], c[i], d), r || (t[s] = Dn(g));else {
                var x = void 0;
                x = this.interpolable ? vn(f[i], c[i], d) : yn(f[i], c[i], d), r ? this._additiveValue = x : t[s] = x;
              }
              r && this._addToTarget(t);
            }
          }
        }
      }, t.prototype._addToTarget = function (t) {
        var e = this.arrDim,
          n = this.propName,
          r = this._additiveValue;
        0 === e ? this.isValueColor ? (rn(t[n], Wd), xn(Wd, Wd, r, 1), t[n] = Dn(Wd)) : t[n] = t[n] + r : 1 === e ? xn(t[n], t[n], r, 1) : 2 === e && wn(t[n], t[n], r, 1);
      }, t;
    }(),
    Ud = function () {
      function t(t, e, n) {
        return this._tracks = {}, this._trackKeys = [], this._delay = 0, this._maxTime = 0, this._paused = !1, this._started = 0, this._clip = null, this._target = t, this._loop = e, e && n ? void a("Can' use additive animation on looped animation.") : void (this._additiveAnimators = n);
      }
      return t.prototype.getTarget = function () {
        return this._target;
      }, t.prototype.changeTarget = function (t) {
        this._target = t;
      }, t.prototype.when = function (t, e) {
        return this.whenWithKeys(t, e, w(e));
      }, t.prototype.whenWithKeys = function (t, e, n) {
        for (var r = this._tracks, i = 0; i < n.length; i++) {
          var o = n[i],
            a = r[o];
          if (!a) {
            a = r[o] = new Gd(o);
            var s = void 0,
              l = this._getAdditiveTrack(o);
            if (l) {
              var u = l.keyframes[l.keyframes.length - 1];
              s = u && u.value, l.isValueColor && s && (s = Dn(s));
            } else s = this._target[o];
            if (null == s) continue;
            0 !== t && a.addKeyframe(0, kn(s)), this._trackKeys.push(o);
          }
          a.addKeyframe(t, kn(e[o]));
        }
        return this._maxTime = Math.max(this._maxTime, t), this;
      }, t.prototype.pause = function () {
        this._clip.pause(), this._paused = !0;
      }, t.prototype.resume = function () {
        this._clip.resume(), this._paused = !1;
      }, t.prototype.isPaused = function () {
        return !!this._paused;
      }, t.prototype._doneCallback = function () {
        this._setTracksFinished(), this._clip = null;
        var t = this._doneList;
        if (t) for (var e = t.length, n = 0; e > n; n++) {
          t[n].call(this);
        }
      }, t.prototype._abortedCallback = function () {
        this._setTracksFinished();
        var t = this.animation,
          e = this._abortedList;
        if (t && t.removeClip(this._clip), this._clip = null, e) for (var n = 0; n < e.length; n++) {
          e[n].call(this);
        }
      }, t.prototype._setTracksFinished = function () {
        for (var t = this._tracks, e = this._trackKeys, n = 0; n < e.length; n++) {
          t[e[n]].setFinished();
        }
      }, t.prototype._getAdditiveTrack = function (t) {
        var e,
          n = this._additiveAnimators;
        if (n) for (var r = 0; r < n.length; r++) {
          var i = n[r].getTrack(t);
          i && (e = i);
        }
        return e;
      }, t.prototype.start = function (t, e) {
        if (!(this._started > 0)) {
          this._started = 1;
          for (var n = this, r = [], i = 0; i < this._trackKeys.length; i++) {
            var o = this._trackKeys[i],
              a = this._tracks[o],
              s = this._getAdditiveTrack(o),
              l = a.keyframes;
            if (a.prepare(s), a.needsAnimate()) r.push(a);else if (!a.interpolable) {
              var u = l[l.length - 1];
              u && (n._target[a.propName] = u.value);
            }
          }
          if (r.length || e) {
            var h = new Pd({
              life: this._maxTime,
              loop: this._loop,
              delay: this._delay,
              onframe: function onframe(t) {
                n._started = 2;
                var e = n._additiveAnimators;
                if (e) {
                  for (var i = !1, o = 0; o < e.length; o++) {
                    if (e[o]._clip) {
                      i = !0;
                      break;
                    }
                  }
                  i || (n._additiveAnimators = null);
                }
                for (var o = 0; o < r.length; o++) {
                  r[o].step(n._target, t);
                }
                var a = n._onframeList;
                if (a) for (var o = 0; o < a.length; o++) {
                  a[o](n._target, t);
                }
              },
              ondestroy: function ondestroy() {
                n._doneCallback();
              }
            });
            this._clip = h, this.animation && this.animation.addClip(h), t && "spline" !== t && (h.easing = t);
          } else this._doneCallback();
          return this;
        }
      }, t.prototype.stop = function (t) {
        if (this._clip) {
          var e = this._clip;
          t && e.onframe(1), this._abortedCallback();
        }
      }, t.prototype.delay = function (t) {
        return this._delay = t, this;
      }, t.prototype.during = function (t) {
        return t && (this._onframeList || (this._onframeList = []), this._onframeList.push(t)), this;
      }, t.prototype.done = function (t) {
        return t && (this._doneList || (this._doneList = []), this._doneList.push(t)), this;
      }, t.prototype.aborted = function (t) {
        return t && (this._abortedList || (this._abortedList = []), this._abortedList.push(t)), this;
      }, t.prototype.getClip = function () {
        return this._clip;
      }, t.prototype.getTrack = function (t) {
        return this._tracks[t];
      }, t.prototype.stopTracks = function (t, e) {
        if (!t.length || !this._clip) return !0;
        for (var n = this._tracks, r = this._trackKeys, i = 0; i < t.length; i++) {
          var o = n[t[i]];
          o && (e ? o.step(this._target, 1) : 1 === this._started && o.step(this._target, 0), o.setFinished());
        }
        for (var a = !0, i = 0; i < r.length; i++) {
          if (!n[r[i]].isFinished()) {
            a = !1;
            break;
          }
        }
        return a && this._abortedCallback(), a;
      }, t.prototype.saveFinalToTarget = function (t, e) {
        if (t) {
          e = e || this._trackKeys;
          for (var n = 0; n < e.length; n++) {
            var r = e[n],
              i = this._tracks[r];
            if (i && !i.isFinished()) {
              var o = i.keyframes,
                a = o[o.length - 1];
              if (a) {
                var s = kn(a.value);
                i.isValueColor && (s = Dn(s)), t[r] = s;
              }
            }
          }
        }
      }, t.prototype.__changeFinalValue = function (t, e) {
        e = e || w(t);
        for (var n = 0; n < e.length; n++) {
          var r = e[n],
            i = this._tracks[r];
          if (i) {
            var o = i.keyframes;
            if (o.length > 1) {
              var a = o.pop();
              i.addKeyframe(a.time, t[r]), i.prepare(i.getAdditiveTrack());
            }
          }
        }
      }, t;
    }(),
    qd = function () {
      function t(t, e) {
        this.x = t || 0, this.y = e || 0;
      }
      return t.prototype.copy = function (t) {
        return this.x = t.x, this.y = t.y, this;
      }, t.prototype.clone = function () {
        return new t(this.x, this.y);
      }, t.prototype.set = function (t, e) {
        return this.x = t, this.y = e, this;
      }, t.prototype.equal = function (t) {
        return t.x === this.x && t.y === this.y;
      }, t.prototype.add = function (t) {
        return this.x += t.x, this.y += t.y, this;
      }, t.prototype.scale = function (t) {
        this.x *= t, this.y *= t;
      }, t.prototype.scaleAndAdd = function (t, e) {
        this.x += t.x * e, this.y += t.y * e;
      }, t.prototype.sub = function (t) {
        return this.x -= t.x, this.y -= t.y, this;
      }, t.prototype.dot = function (t) {
        return this.x * t.x + this.y * t.y;
      }, t.prototype.len = function () {
        return Math.sqrt(this.x * this.x + this.y * this.y);
      }, t.prototype.lenSquare = function () {
        return this.x * this.x + this.y * this.y;
      }, t.prototype.normalize = function () {
        var t = this.len();
        return this.x /= t, this.y /= t, this;
      }, t.prototype.distance = function (t) {
        var e = this.x - t.x,
          n = this.y - t.y;
        return Math.sqrt(e * e + n * n);
      }, t.prototype.distanceSquare = function (t) {
        var e = this.x - t.x,
          n = this.y - t.y;
        return e * e + n * n;
      }, t.prototype.negate = function () {
        return this.x = -this.x, this.y = -this.y, this;
      }, t.prototype.transform = function (t) {
        if (t) {
          var e = this.x,
            n = this.y;
          return this.x = t[0] * e + t[2] * n + t[4], this.y = t[1] * e + t[3] * n + t[5], this;
        }
      }, t.prototype.toArray = function (t) {
        return t[0] = this.x, t[1] = this.y, t;
      }, t.prototype.fromArray = function (t) {
        this.x = t[0], this.y = t[1];
      }, t.set = function (t, e, n) {
        t.x = e, t.y = n;
      }, t.copy = function (t, e) {
        t.x = e.x, t.y = e.y;
      }, t.len = function (t) {
        return Math.sqrt(t.x * t.x + t.y * t.y);
      }, t.lenSquare = function (t) {
        return t.x * t.x + t.y * t.y;
      }, t.dot = function (t, e) {
        return t.x * e.x + t.y * e.y;
      }, t.add = function (t, e, n) {
        t.x = e.x + n.x, t.y = e.y + n.y;
      }, t.sub = function (t, e, n) {
        t.x = e.x - n.x, t.y = e.y - n.y;
      }, t.scale = function (t, e, n) {
        t.x = e.x * n, t.y = e.y * n;
      }, t.scaleAndAdd = function (t, e, n, r) {
        t.x = e.x + n.x * r, t.y = e.y + n.y * r;
      }, t.lerp = function (t, e, n, r) {
        var i = 1 - r;
        t.x = i * e.x + r * n.x, t.y = i * e.y + r * n.y;
      }, t;
    }(),
    Xd = Math.min,
    Yd = Math.max,
    jd = new qd(),
    Zd = new qd(),
    Kd = new qd(),
    $d = new qd(),
    Qd = new qd(),
    Jd = new qd(),
    tg = function () {
      function t(t, e, n, r) {
        0 > n && isFinite(n) && (t += n, n = -n), 0 > r && isFinite(r) && (e += r, r = -r), this.x = t, this.y = e, this.width = n, this.height = r;
      }
      return t.prototype.union = function (t) {
        var e = Xd(t.x, this.x),
          n = Xd(t.y, this.y);
        this.width = isFinite(this.x) && isFinite(this.width) ? Yd(t.x + t.width, this.x + this.width) - e : t.width, this.height = isFinite(this.y) && isFinite(this.height) ? Yd(t.y + t.height, this.y + this.height) - n : t.height, this.x = e, this.y = n;
      }, t.prototype.applyTransform = function (e) {
        t.applyTransform(this, this, e);
      }, t.prototype.calculateTransform = function (t) {
        var e = this,
          n = t.width / e.width,
          r = t.height / e.height,
          i = ze();
        return Ve(i, i, [-e.x, -e.y]), Ge(i, i, [n, r]), Ve(i, i, [t.x, t.y]), i;
      }, t.prototype.intersect = function (e, n) {
        if (!e) return !1;
        e instanceof t || (e = t.create(e));
        var r = this,
          i = r.x,
          o = r.x + r.width,
          a = r.y,
          s = r.y + r.height,
          l = e.x,
          u = e.x + e.width,
          h = e.y,
          c = e.y + e.height,
          f = !(l > o || i > u || h > s || a > c);
        if (n) {
          var p = 1 / 0,
            d = 0,
            g = Math.abs(o - l),
            v = Math.abs(u - i),
            y = Math.abs(s - h),
            m = Math.abs(c - a),
            _ = Math.min(g, v),
            x = Math.min(y, m);
          l > o || i > u ? _ > d && (d = _, v > g ? qd.set(Jd, -g, 0) : qd.set(Jd, v, 0)) : p > _ && (p = _, v > g ? qd.set(Qd, g, 0) : qd.set(Qd, -v, 0)), h > s || a > c ? x > d && (d = x, m > y ? qd.set(Jd, 0, -y) : qd.set(Jd, 0, m)) : p > _ && (p = _, m > y ? qd.set(Qd, 0, y) : qd.set(Qd, 0, -m));
        }
        return n && qd.copy(n, f ? Qd : Jd), f;
      }, t.prototype.contain = function (t, e) {
        var n = this;
        return t >= n.x && t <= n.x + n.width && e >= n.y && e <= n.y + n.height;
      }, t.prototype.clone = function () {
        return new t(this.x, this.y, this.width, this.height);
      }, t.prototype.copy = function (e) {
        t.copy(this, e);
      }, t.prototype.plain = function () {
        return {
          x: this.x,
          y: this.y,
          width: this.width,
          height: this.height
        };
      }, t.prototype.isFinite = function () {
        return isFinite(this.x) && isFinite(this.y) && isFinite(this.width) && isFinite(this.height);
      }, t.prototype.isZero = function () {
        return 0 === this.width || 0 === this.height;
      }, t.create = function (e) {
        return new t(e.x, e.y, e.width, e.height);
      }, t.copy = function (t, e) {
        t.x = e.x, t.y = e.y, t.width = e.width, t.height = e.height;
      }, t.applyTransform = function (e, n, r) {
        if (!r) return void (e !== n && t.copy(e, n));
        if (r[1] < 1e-5 && r[1] > -1e-5 && r[2] < 1e-5 && r[2] > -1e-5) {
          var i = r[0],
            o = r[3],
            a = r[4],
            s = r[5];
          return e.x = n.x * i + a, e.y = n.y * o + s, e.width = n.width * i, e.height = n.height * o, e.width < 0 && (e.x += e.width, e.width = -e.width), void (e.height < 0 && (e.y += e.height, e.height = -e.height));
        }
        jd.x = Kd.x = n.x, jd.y = $d.y = n.y, Zd.x = $d.x = n.x + n.width, Zd.y = Kd.y = n.y + n.height, jd.transform(r), $d.transform(r), Zd.transform(r), Kd.transform(r), e.x = Xd(jd.x, Zd.x, Kd.x, $d.x), e.y = Xd(jd.y, Zd.y, Kd.y, $d.y);
        var l = Yd(jd.x, Zd.x, Kd.x, $d.x),
          u = Yd(jd.y, Zd.y, Kd.y, $d.y);
        e.width = l - e.x, e.height = u - e.y;
      }, t;
    }(),
    eg = {},
    ng = "12px sans-serif",
    rg = {
      measureText: An
    },
    ig = 1;
  "undefined" != typeof window && (ig = Math.max(window.devicePixelRatio || window.screen.deviceXDPI / window.screen.logicalXDPI || 1, 1));
  var og = ig,
    ag = .4,
    sg = "#333",
    lg = "#ccc",
    ug = "#eee",
    hg = "__zr_normal__",
    cg = ["x", "y", "scaleX", "scaleY", "originX", "originY", "rotation", "ignore"],
    fg = {
      x: !0,
      y: !0,
      scaleX: !0,
      scaleY: !0,
      originX: !0,
      originY: !0,
      rotation: !0,
      ignore: !1
    },
    pg = {},
    dg = new tg(0, 0, 0, 0),
    gg = function () {
      function t(t) {
        this.id = o(), this.animators = [], this.currentStates = [], this.states = {}, this._init(t);
      }
      return t.prototype._init = function (t) {
        this.attr(t);
      }, t.prototype.drift = function (t, e) {
        switch (this.draggable) {
          case "horizontal":
            e = 0;
            break;
          case "vertical":
            t = 0;
        }
        var n = this.transform;
        n || (n = this.transform = [1, 0, 0, 1, 0, 0]), n[4] += t, n[5] += e, this.decomposeTransform(), this.markRedraw();
      }, t.prototype.beforeUpdate = function () {}, t.prototype.afterUpdate = function () {}, t.prototype.update = function () {
        this.updateTransform(), this.__dirty && this.updateInnerText();
      }, t.prototype.updateInnerText = function (t) {
        var e = this._textContent;
        if (e && (!e.ignore || t)) {
          this.textConfig || (this.textConfig = {});
          var n = this.textConfig,
            r = n.local,
            i = e.attachedTransform,
            o = void 0,
            a = void 0,
            s = !1;
          i.parent = r ? this : null;
          var l = !1;
          if (i.x = e.x, i.y = e.y, i.originX = e.originX, i.originY = e.originY, i.rotation = e.rotation, i.scaleX = e.scaleX, i.scaleY = e.scaleY, null != n.position) {
            var u = dg;
            u.copy(n.layoutRect ? n.layoutRect : this.getBoundingRect()), r || u.applyTransform(this.transform), this.calculateTextPosition ? this.calculateTextPosition(pg, n, u) : Fn(pg, n, u), i.x = pg.x, i.y = pg.y, o = pg.align, a = pg.verticalAlign;
            var h = n.origin;
            if (h && null != n.rotation) {
              var c = void 0,
                f = void 0;
              "center" === h ? (c = .5 * u.width, f = .5 * u.height) : (c = zn(h[0], u.width), f = zn(h[1], u.height)), l = !0, i.originX = -i.x + c + (r ? 0 : u.x), i.originY = -i.y + f + (r ? 0 : u.y);
            }
          }
          null != n.rotation && (i.rotation = n.rotation);
          var p = n.offset;
          p && (i.x += p[0], i.y += p[1], l || (i.originX = -p[0], i.originY = -p[1]));
          var d = null == n.inside ? "string" == typeof n.position && n.position.indexOf("inside") >= 0 : n.inside,
            g = this._innerTextDefaultStyle || (this._innerTextDefaultStyle = {}),
            v = void 0,
            y = void 0,
            m = void 0;
          d && this.canBeInsideText() ? (v = n.insideFill, y = n.insideStroke, (null == v || "auto" === v) && (v = this.getInsideTextFill()), (null == y || "auto" === y) && (y = this.getInsideTextStroke(v), m = !0)) : (v = n.outsideFill, y = n.outsideStroke, (null == v || "auto" === v) && (v = this.getOutsideFill()), (null == y || "auto" === y) && (y = this.getOutsideStroke(v), m = !0)), v = v || "#000", (v !== g.fill || y !== g.stroke || m !== g.autoStroke || o !== g.align || a !== g.verticalAlign) && (s = !0, g.fill = v, g.stroke = y, g.autoStroke = m, g.align = o, g.verticalAlign = a, e.setDefaultTextStyle(g)), s && e.dirtyStyle(), e.markRedraw();
        }
      }, t.prototype.canBeInsideText = function () {
        return !0;
      }, t.prototype.getInsideTextFill = function () {
        return "#fff";
      }, t.prototype.getInsideTextStroke = function () {
        return "#000";
      }, t.prototype.getOutsideFill = function () {
        return this.__zr && this.__zr.isDarkMode() ? lg : sg;
      }, t.prototype.getOutsideStroke = function () {
        var t = this.__zr && this.__zr.getBackgroundColor(),
          e = "string" == typeof t && rn(t);
        e || (e = [255, 255, 255, 1]);
        for (var n = e[3], r = this.__zr.isDarkMode(), i = 0; 3 > i; i++) {
          e[i] = e[i] * n + (r ? 0 : 255) * (1 - n);
        }
        return e[3] = 1, pn(e, "rgba");
      }, t.prototype.traverse = function () {}, t.prototype.attrKV = function (t, e) {
        "textConfig" === t ? this.setTextConfig(e) : "textContent" === t ? this.setTextContent(e) : "clipPath" === t ? this.setClipPath(e) : "extra" === t ? (this.extra = this.extra || {}, h(this.extra, e)) : this[t] = e;
      }, t.prototype.hide = function () {
        this.ignore = !0, this.markRedraw();
      }, t.prototype.show = function () {
        this.ignore = !1, this.markRedraw();
      }, t.prototype.attr = function (t, e) {
        if ("string" == typeof t) this.attrKV(t, e);else if (I(t)) for (var n = t, r = w(n), i = 0; i < r.length; i++) {
          var o = r[i];
          this.attrKV(o, t[o]);
        }
        return this.markRedraw(), this;
      }, t.prototype.saveCurrentToNormalState = function (t) {
        this._innerSaveToNormal(t);
        for (var e = this._normalState, n = 0; n < this.animators.length; n++) {
          var r = this.animators[n],
            i = r.__fromStateTransition;
          if (!i || i === hg) {
            var o = r.targetName,
              a = o ? e[o] : e;
            r.saveFinalToTarget(a);
          }
        }
      }, t.prototype._innerSaveToNormal = function (t) {
        var e = this._normalState;
        e || (e = this._normalState = {}), t.textConfig && !e.textConfig && (e.textConfig = this.textConfig), this._savePrimaryToNormal(t, e, cg);
      }, t.prototype._savePrimaryToNormal = function (t, e, n) {
        for (var r = 0; r < n.length; r++) {
          var i = n[r];
          null == t[i] || i in e || (e[i] = this[i]);
        }
      }, t.prototype.hasState = function () {
        return this.currentStates.length > 0;
      }, t.prototype.getState = function (t) {
        return this.states[t];
      }, t.prototype.ensureState = function (t) {
        var e = this.states;
        return e[t] || (e[t] = {}), e[t];
      }, t.prototype.clearStates = function (t) {
        this.useState(hg, !1, t);
      }, t.prototype.useState = function (e, n, r) {
        var i = e === hg,
          o = this.hasState();
        if (o || !i) {
          var s = this.currentStates,
            l = this.stateTransition;
          if (!(f(s, e) >= 0) || !n && 1 !== s.length) {
            var u;
            if (this.stateProxy && !i && (u = this.stateProxy(e)), u || (u = this.states && this.states[e]), !u && !i) return void a("State " + e + " not exists.");
            i || this.saveCurrentToNormalState(u);
            var h = !(!u || !u.hoverLayer);
            return h && this._toggleHoverLayerFlag(!0), this._applyStateObj(e, u, this._normalState, n, !r && !this.__inHover && l && l.duration > 0, l), this._textContent && this._textContent.useState(e, n), this._textGuide && this._textGuide.useState(e, n), i ? (this.currentStates = [], this._normalState = {}) : n ? this.currentStates.push(e) : this.currentStates = [e], this._updateAnimationTargets(), this.markRedraw(), !h && this.__inHover && (this._toggleHoverLayerFlag(!1), this.__dirty &= ~t.REDARAW_BIT), u;
          }
        }
      }, t.prototype.useStates = function (e, n) {
        if (e.length) {
          var r = [],
            i = this.currentStates,
            o = e.length,
            a = o === i.length;
          if (a) for (var s = 0; o > s; s++) {
            if (e[s] !== i[s]) {
              a = !1;
              break;
            }
          }
          if (a) return;
          for (var s = 0; o > s; s++) {
            var l = e[s],
              u = void 0;
            this.stateProxy && (u = this.stateProxy(l, e)), u || (u = this.states[l]), u && r.push(u);
          }
          var h = !(!r[o - 1] || !r[o - 1].hoverLayer);
          h && this._toggleHoverLayerFlag(!0);
          var c = this._mergeStates(r),
            f = this.stateTransition;
          this.saveCurrentToNormalState(c), this._applyStateObj(e.join(","), c, this._normalState, !1, !n && !this.__inHover && f && f.duration > 0, f), this._textContent && this._textContent.useStates(e), this._textGuide && this._textGuide.useStates(e), this._updateAnimationTargets(), this.currentStates = e.slice(), this.markRedraw(), !h && this.__inHover && (this._toggleHoverLayerFlag(!1), this.__dirty &= ~t.REDARAW_BIT);
        } else this.clearStates();
      }, t.prototype._updateAnimationTargets = function () {
        for (var t = 0; t < this.animators.length; t++) {
          var e = this.animators[t];
          e.targetName && e.changeTarget(this[e.targetName]);
        }
      }, t.prototype.removeState = function (t) {
        var e = f(this.currentStates, t);
        if (e >= 0) {
          var n = this.currentStates.slice();
          n.splice(e, 1), this.useStates(n);
        }
      }, t.prototype.replaceState = function (t, e, n) {
        var r = this.currentStates.slice(),
          i = f(r, t),
          o = f(r, e) >= 0;
        i >= 0 ? o ? r.splice(i, 1) : r[i] = e : n && !o && r.push(e), this.useStates(r);
      }, t.prototype.toggleState = function (t, e) {
        e ? this.useState(t, !0) : this.removeState(t);
      }, t.prototype._mergeStates = function (t) {
        for (var e, n = {}, r = 0; r < t.length; r++) {
          var i = t[r];
          h(n, i), i.textConfig && (e = e || {}, h(e, i.textConfig));
        }
        return e && (n.textConfig = e), n;
      }, t.prototype._applyStateObj = function (t, e, n, r, i, o) {
        var a = !(e && r);
        e && e.textConfig ? (this.textConfig = h({}, r ? this.textConfig : n.textConfig), h(this.textConfig, e.textConfig)) : a && n.textConfig && (this.textConfig = n.textConfig);
        for (var s = {}, l = !1, u = 0; u < cg.length; u++) {
          var c = cg[u],
            f = i && fg[c];
          e && null != e[c] ? f ? (l = !0, s[c] = e[c]) : this[c] = e[c] : a && null != n[c] && (f ? (l = !0, s[c] = n[c]) : this[c] = n[c]);
        }
        if (!i) for (var u = 0; u < this.animators.length; u++) {
          var p = this.animators[u],
            d = p.targetName;
          p.__changeFinalValue(d ? (e || n)[d] : e || n);
        }
        l && this._transitionState(t, s, o);
      }, t.prototype._attachComponent = function (t) {
        if (t.__zr && !t.__hostTarget) throw new Error("Text element has been added to zrender.");
        if (t === this) throw new Error("Recursive component attachment.");
        var e = this.__zr;
        e && t.addSelfToZr(e), t.__zr = e, t.__hostTarget = this;
      }, t.prototype._detachComponent = function (t) {
        t.__zr && t.removeSelfFromZr(t.__zr), t.__zr = null, t.__hostTarget = null;
      }, t.prototype.getClipPath = function () {
        return this._clipPath;
      }, t.prototype.setClipPath = function (t) {
        this._clipPath && this._clipPath !== t && this.removeClipPath(), this._attachComponent(t), this._clipPath = t, this.markRedraw();
      }, t.prototype.removeClipPath = function () {
        var t = this._clipPath;
        t && (this._detachComponent(t), this._clipPath = null, this.markRedraw());
      }, t.prototype.getTextContent = function () {
        return this._textContent;
      }, t.prototype.setTextContent = function (t) {
        var e = this._textContent;
        if (e !== t) {
          if (e && e !== t && this.removeTextContent(), t.__zr && !t.__hostTarget) throw new Error("Text element has been added to zrender.");
          t.attachedTransform = new Id(), this._attachComponent(t), this._textContent = t, this.markRedraw();
        }
      }, t.prototype.setTextConfig = function (t) {
        this.textConfig || (this.textConfig = {}), h(this.textConfig, t), this.markRedraw();
      }, t.prototype.removeTextContent = function () {
        var t = this._textContent;
        t && (t.attachedTransform = null, this._detachComponent(t), this._textContent = null, this._innerTextDefaultStyle = null, this.markRedraw());
      }, t.prototype.getTextGuideLine = function () {
        return this._textGuide;
      }, t.prototype.setTextGuideLine = function (t) {
        this._textGuide && this._textGuide !== t && this.removeTextGuideLine(), this._attachComponent(t), this._textGuide = t, this.markRedraw();
      }, t.prototype.removeTextGuideLine = function () {
        var t = this._textGuide;
        t && (this._detachComponent(t), this._textGuide = null, this.markRedraw());
      }, t.prototype.markRedraw = function () {
        this.__dirty |= t.REDARAW_BIT;
        var e = this.__zr;
        e && (this.__inHover ? e.refreshHover() : e.refresh()), this.__hostTarget && this.__hostTarget.markRedraw();
      }, t.prototype.dirty = function () {
        this.markRedraw();
      }, t.prototype._toggleHoverLayerFlag = function (t) {
        this.__inHover = t;
        var e = this._textContent,
          n = this._textGuide;
        e && (e.__inHover = t), n && (n.__inHover = t);
      }, t.prototype.addSelfToZr = function (t) {
        this.__zr = t;
        var e = this.animators;
        if (e) for (var n = 0; n < e.length; n++) {
          t.animation.addAnimator(e[n]);
        }
        this._clipPath && this._clipPath.addSelfToZr(t), this._textContent && this._textContent.addSelfToZr(t), this._textGuide && this._textGuide.addSelfToZr(t);
      }, t.prototype.removeSelfFromZr = function (t) {
        this.__zr = null;
        var e = this.animators;
        if (e) for (var n = 0; n < e.length; n++) {
          t.animation.removeAnimator(e[n]);
        }
        this._clipPath && this._clipPath.removeSelfFromZr(t), this._textContent && this._textContent.removeSelfFromZr(t), this._textGuide && this._textGuide.removeSelfFromZr(t);
      }, t.prototype.animate = function (t, e) {
        var n = t ? this[t] : this;
        if (!n) return void a('Property "' + t + '" is not existed in element ' + this.id);
        var r = new Ud(n, e);
        return this.addAnimator(r, t), r;
      }, t.prototype.addAnimator = function (t, e) {
        var n = this.__zr,
          r = this;
        t.during(function () {
          r.updateDuringAnimation(e);
        }).done(function () {
          var e = r.animators,
            n = f(e, t);
          n >= 0 && e.splice(n, 1);
        }), this.animators.push(t), n && n.animation.addAnimator(t), n && n.wakeUp();
      }, t.prototype.updateDuringAnimation = function () {
        this.markRedraw();
      }, t.prototype.stopAnimation = function (t, e) {
        for (var n = this.animators, r = n.length, i = [], o = 0; r > o; o++) {
          var a = n[o];
          t && t !== a.scope ? i.push(a) : a.stop(e);
        }
        return this.animators = i, this;
      }, t.prototype.animateTo = function (t, e, n) {
        Nn(this, t, e, n);
      }, t.prototype.animateFrom = function (t, e, n) {
        Nn(this, t, e, n, !0);
      }, t.prototype._transitionState = function (t, e, n, r) {
        for (var i = Nn(this, e, n, r), o = 0; o < i.length; o++) {
          i[o].__fromStateTransition = t;
        }
      }, t.prototype.getBoundingRect = function () {
        return null;
      }, t.prototype.getPaintRect = function () {
        return null;
      }, t.REDARAW_BIT = 1, t.initDefaultProps = function () {
        function e(t, e, n) {
          i[t + e + n] || (console.warn("DEPRECATED: '" + t + "' has been deprecated. use '" + e + "', '" + n + "' instead"), i[t + e + n] = !0);
        }
        function n(t, n, i, o) {
          function a(t, e) {
            Object.defineProperty(e, 0, {
              get: function get() {
                return t[i];
              },
              set: function set(e) {
                t[i] = e;
              }
            }), Object.defineProperty(e, 1, {
              get: function get() {
                return t[o];
              },
              set: function set(e) {
                t[o] = e;
              }
            });
          }
          Object.defineProperty(r, t, {
            get: function get() {
              if (e(t, i, o), !this[n]) {
                var r = this[n] = [];
                a(this, r);
              }
              return this[n];
            },
            set: function set(r) {
              e(t, i, o), this[i] = r[0], this[o] = r[1], this[n] = r, a(this, r);
            }
          });
        }
        var r = t.prototype;
        r.type = "element", r.name = "", r.ignore = !1, r.silent = !1, r.isGroup = !1, r.draggable = !1, r.dragging = !1, r.ignoreClip = !1, r.__inHover = !1, r.__dirty = t.REDARAW_BIT;
        var i = {};
        Object.defineProperty && (!Ep.browser.ie || Ep.browser.version > 8) && (n("position", "_legacyPos", "x", "y"), n("scale", "_legacyScale", "scaleX", "scaleY"), n("origin", "_legacyOrigin", "originX", "originY"));
      }(), t;
    }();
  d(gg, ad), d(gg, Id);
  var vg,
    yg = 32,
    mg = 7,
    _g = !1,
    xg = function () {
      function t() {
        this._roots = [], this._displayList = [], this._displayListLen = 0, this.displayableSortFunc = Jn;
      }
      return t.prototype.traverse = function (t, e) {
        for (var n = 0; n < this._roots.length; n++) {
          this._roots[n].traverse(t, e);
        }
      }, t.prototype.getDisplayList = function (t, e) {
        e = e || !1;
        var n = this._displayList;
        return (t || !n.length) && this.updateDisplayList(e), n;
      }, t.prototype.updateDisplayList = function (t) {
        this._displayListLen = 0;
        for (var e = this._roots, n = this._displayList, r = 0, i = e.length; i > r; r++) {
          this._updateAndAddDisplayable(e[r], null, t);
        }
        n.length = this._displayListLen, Ep.canvasSupported && $n(n, Jn);
      }, t.prototype._updateAndAddDisplayable = function (t, e, n) {
        if (!t.ignore || n) {
          t.beforeUpdate(), t.update(), t.afterUpdate();
          var r = t.getClipPath();
          if (t.ignoreClip) e = null;else if (r) {
            e = e ? e.slice() : [];
            for (var i = r, o = t; i;) {
              i.parent = o, i.updateTransform(), e.push(i), o = i, i = i.getClipPath();
            }
          }
          if (t.childrenRef) {
            for (var a = t.childrenRef(), s = 0; s < a.length; s++) {
              var l = a[s];
              t.__dirty && (l.__dirty |= gg.REDARAW_BIT), this._updateAndAddDisplayable(l, e, n);
            }
            t.__dirty = 0;
          } else {
            var u = t;
            e && e.length ? u.__clipPaths = e : u.__clipPaths && u.__clipPaths.length > 0 && (u.__clipPaths = []), isNaN(u.z) && (Qn(), u.z = 0), isNaN(u.z2) && (Qn(), u.z2 = 0), isNaN(u.zlevel) && (Qn(), u.zlevel = 0), this._displayList[this._displayListLen++] = u;
          }
          var h = t.getDecalElement && t.getDecalElement();
          h && this._updateAndAddDisplayable(h, e, n);
          var c = t.getTextGuideLine();
          c && this._updateAndAddDisplayable(c, e, n);
          var f = t.getTextContent();
          f && this._updateAndAddDisplayable(f, e, n);
        }
      }, t.prototype.addRoot = function (t) {
        t.__zr && t.__zr.storage === this || this._roots.push(t);
      }, t.prototype.delRoot = function (t) {
        if (t instanceof Array) for (var e = 0, n = t.length; n > e; e++) {
          this.delRoot(t[e]);
        } else {
          var r = f(this._roots, t);
          r >= 0 && this._roots.splice(r, 1);
        }
      }, t.prototype.delAllRoots = function () {
        this._roots = [], this._displayList = [], this._displayListLen = 0;
      }, t.prototype.getRoots = function () {
        return this._roots;
      }, t.prototype.dispose = function () {
        this._displayList = null, this._roots = null;
      }, t;
    }();
  vg = "undefined" != typeof window && (window.requestAnimationFrame && window.requestAnimationFrame.bind(window) || window.msRequestAnimationFrame && window.msRequestAnimationFrame.bind(window) || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame) || function (t) {
    return setTimeout(t, 16);
  };
  var wg = vg,
    bg = function (t) {
      function n(e) {
        var n = t.call(this) || this;
        return n._running = !1, n._time = 0, n._pausedTime = 0, n._pauseStart = 0, n._paused = !1, e = e || {}, n.stage = e.stage || {}, n.onframe = e.onframe || function () {}, n;
      }
      return e(n, t), n.prototype.addClip = function (t) {
        t.animation && this.removeClip(t), this._clipsHead ? (this._clipsTail.next = t, t.prev = this._clipsTail, t.next = null, this._clipsTail = t) : this._clipsHead = this._clipsTail = t, t.animation = this;
      }, n.prototype.addAnimator = function (t) {
        t.animation = this;
        var e = t.getClip();
        e && this.addClip(e);
      }, n.prototype.removeClip = function (t) {
        if (t.animation) {
          var e = t.prev,
            n = t.next;
          e ? e.next = n : this._clipsHead = n, n ? n.prev = e : this._clipsTail = e, t.next = t.prev = t.animation = null;
        }
      }, n.prototype.removeAnimator = function (t) {
        var e = t.getClip();
        e && this.removeClip(e), t.animation = null;
      }, n.prototype.update = function (t) {
        for (var e = new Date().getTime() - this._pausedTime, n = e - this._time, r = this._clipsHead; r;) {
          var i = r.next,
            o = r.step(e, n);
          o ? (r.ondestroy && r.ondestroy(), this.removeClip(r), r = i) : r = i;
        }
        this._time = e, t || (this.onframe(n), this.trigger("frame", n), this.stage.update && this.stage.update());
      }, n.prototype._startLoop = function () {
        function t() {
          e._running && (wg(t), !e._paused && e.update());
        }
        var e = this;
        this._running = !0, wg(t);
      }, n.prototype.start = function () {
        this._running || (this._time = new Date().getTime(), this._pausedTime = 0, this._startLoop());
      }, n.prototype.stop = function () {
        this._running = !1;
      }, n.prototype.pause = function () {
        this._paused || (this._pauseStart = new Date().getTime(), this._paused = !0);
      }, n.prototype.resume = function () {
        this._paused && (this._pausedTime += new Date().getTime() - this._pauseStart, this._paused = !1);
      }, n.prototype.clear = function () {
        for (var t = this._clipsHead; t;) {
          var e = t.next;
          t.prev = t.next = t.animation = null, t = e;
        }
        this._clipsHead = this._clipsTail = null;
      }, n.prototype.isFinished = function () {
        return null == this._clipsHead;
      }, n.prototype.animate = function (t, e) {
        e = e || {}, this.start();
        var n = new Ud(t, e.loop);
        return this.addAnimator(n), n;
      }, n;
    }(ad),
    Sg = 300,
    Mg = Ep.domSupported,
    Tg = function () {
      var t = ["click", "dblclick", "mousewheel", "wheel", "mouseout", "mouseup", "mousedown", "mousemove", "contextmenu"],
        e = ["touchstart", "touchend", "touchmove"],
        n = {
          pointerdown: 1,
          pointerup: 1,
          pointermove: 1,
          pointerout: 1
        },
        r = y(t, function (t) {
          var e = t.replace("mouse", "pointer");
          return n.hasOwnProperty(e) ? e : t;
        });
      return {
        mouse: t,
        touch: e,
        pointer: r
      };
    }(),
    Cg = {
      mouse: ["mousemove", "mouseup"],
      pointer: ["pointermove", "pointerup"]
    },
    kg = !1,
    Dg = function () {
      function t(t, e) {
        this.stopPropagation = K, this.stopImmediatePropagation = K, this.preventDefault = K, this.type = e.type, this.target = this.currentTarget = t.dom, this.pointerType = e.pointerType, this.clientX = e.clientX, this.clientY = e.clientY;
      }
      return t;
    }(),
    Ig = {
      mousedown: function mousedown(t) {
        t = ke(this.dom, t), this.__mayPointerCapture = [t.zrX, t.zrY], this.trigger("mousedown", t);
      },
      mousemove: function mousemove(t) {
        t = ke(this.dom, t);
        var e = this.__mayPointerCapture;
        !e || t.zrX === e[0] && t.zrY === e[1] || this.__togglePointerCapture(!0), this.trigger("mousemove", t);
      },
      mouseup: function mouseup(t) {
        t = ke(this.dom, t), this.__togglePointerCapture(!1), this.trigger("mouseup", t);
      },
      mouseout: function mouseout(t) {
        if (t.target === this.dom) {
          t = ke(this.dom, t), this.__pointerCapturing && (t.zrEventControl = "no_globalout");
          var e = t.toElement || t.relatedTarget;
          t.zrIsToLocalDOM = ir(this, e), this.trigger("mouseout", t);
        }
      },
      wheel: function wheel(t) {
        kg = !0, t = ke(this.dom, t), this.trigger("mousewheel", t);
      },
      mousewheel: function mousewheel(t) {
        kg || (t = ke(this.dom, t), this.trigger("mousewheel", t));
      },
      touchstart: function touchstart(t) {
        t = ke(this.dom, t), nr(t), this.__lastTouchMoment = new Date(), this.handler.processGesture(t, "start"), Ig.mousemove.call(this, t), Ig.mousedown.call(this, t);
      },
      touchmove: function touchmove(t) {
        t = ke(this.dom, t), nr(t), this.handler.processGesture(t, "change"), Ig.mousemove.call(this, t);
      },
      touchend: function touchend(t) {
        t = ke(this.dom, t), nr(t), this.handler.processGesture(t, "end"), Ig.mouseup.call(this, t), +new Date() - +this.__lastTouchMoment < Sg && Ig.click.call(this, t);
      },
      pointerdown: function pointerdown(t) {
        Ig.mousedown.call(this, t);
      },
      pointermove: function pointermove(t) {
        tr(t) || Ig.mousemove.call(this, t);
      },
      pointerup: function pointerup(t) {
        Ig.mouseup.call(this, t);
      },
      pointerout: function pointerout(t) {
        tr(t) || Ig.mouseout.call(this, t);
      }
    };
  v(["click", "dblclick", "contextmenu"], function (t) {
    Ig[t] = function (e) {
      e = ke(this.dom, e), this.trigger(t, e);
    };
  });
  var Ag = {
      pointermove: function pointermove(t) {
        tr(t) || Ag.mousemove.call(this, t);
      },
      pointerup: function pointerup(t) {
        Ag.mouseup.call(this, t);
      },
      mousemove: function mousemove(t) {
        this.trigger("mousemove", t);
      },
      mouseup: function mouseup(t) {
        var e = this.__pointerCapturing;
        this.__togglePointerCapture(!1), this.trigger("mouseup", t), e && (t.zrEventControl = "only_globalout", this.trigger("mouseout", t));
      }
    },
    Pg = function () {
      function t(t, e) {
        this.mounted = {}, this.listenerOpts = {}, this.touching = !1, this.domTarget = t, this.domHandlers = e;
      }
      return t;
    }(),
    Lg = function (t) {
      function n(e, n) {
        var r = t.call(this) || this;
        return r.__pointerCapturing = !1, r.dom = e, r.painterRoot = n, r._localHandlerScope = new Pg(e, Ig), Mg && (r._globalHandlerScope = new Pg(document, Ag)), or(r, r._localHandlerScope), r;
      }
      return e(n, t), n.prototype.dispose = function () {
        lr(this._localHandlerScope), Mg && lr(this._globalHandlerScope);
      }, n.prototype.setCursor = function (t) {
        this.dom.style && (this.dom.style.cursor = t || "default");
      }, n.prototype.__togglePointerCapture = function (t) {
        if (this.__mayPointerCapture = null, Mg && +this.__pointerCapturing ^ +t) {
          this.__pointerCapturing = t;
          var e = this._globalHandlerScope;
          t ? ar(this, e) : lr(e);
        }
      }, n;
    }(ad),
    Og = "__zr_style_" + Math.round(10 * Math.random()),
    Rg = {
      shadowBlur: 0,
      shadowOffsetX: 0,
      shadowOffsetY: 0,
      shadowColor: "#000",
      opacity: 1,
      blend: "source-over"
    },
    Eg = {
      style: {
        shadowBlur: !0,
        shadowOffsetX: !0,
        shadowOffsetY: !0,
        shadowColor: !0,
        opacity: !0
      }
    };
  Rg[Og] = !0;
  var Bg = ["z", "z2", "invisible"],
    zg = function (t) {
      function n(e) {
        return t.call(this, e) || this;
      }
      return e(n, t), n.prototype._init = function (e) {
        for (var n = w(e), r = 0; r < n.length; r++) {
          var i = n[r];
          "style" === i ? this.useStyle(e[i]) : t.prototype.attrKV.call(this, i, e[i]);
        }
        this.style || this.useStyle({});
      }, n.prototype.beforeBrush = function () {}, n.prototype.afterBrush = function () {}, n.prototype.innerBeforeBrush = function () {}, n.prototype.innerAfterBrush = function () {}, n.prototype.shouldBePainted = function (t, e, n, r) {
        var i = this.transform;
        if (this.ignore || this.invisible || 0 === this.style.opacity || this.culling && ur(this, t, e) || i && !i[0] && !i[3]) return !1;
        if (n && this.__clipPaths) for (var o = 0; o < this.__clipPaths.length; ++o) {
          if (this.__clipPaths[o].isZeroArea()) return !1;
        }
        if (r && this.parent) for (var a = this.parent; a;) {
          if (a.ignore) return !1;
          a = a.parent;
        }
        return !0;
      }, n.prototype.contain = function (t, e) {
        return this.rectContain(t, e);
      }, n.prototype.traverse = function (t, e) {
        t.call(e, this);
      }, n.prototype.rectContain = function (t, e) {
        var n = this.transformCoordToLocal(t, e),
          r = this.getBoundingRect();
        return r.contain(n[0], n[1]);
      }, n.prototype.getPaintRect = function () {
        var t = this._paintRect;
        if (!this._paintRect || this.__dirty) {
          var e = this.transform,
            n = this.getBoundingRect(),
            r = this.style,
            i = r.shadowBlur || 0,
            o = r.shadowOffsetX || 0,
            a = r.shadowOffsetY || 0;
          t = this._paintRect || (this._paintRect = new tg(0, 0, 0, 0)), e ? tg.applyTransform(t, n, e) : t.copy(n), (i || o || a) && (t.width += 2 * i + Math.abs(o), t.height += 2 * i + Math.abs(a), t.x = Math.min(t.x, t.x + o - i), t.y = Math.min(t.y, t.y + a - i));
          var s = this.dirtyRectTolerance;
          t.isZero() || (t.x = Math.floor(t.x - s), t.y = Math.floor(t.y - s), t.width = Math.ceil(t.width + 1 + 2 * s), t.height = Math.ceil(t.height + 1 + 2 * s));
        }
        return t;
      }, n.prototype.setPrevPaintRect = function (t) {
        t ? (this._prevPaintRect = this._prevPaintRect || new tg(0, 0, 0, 0), this._prevPaintRect.copy(t)) : this._prevPaintRect = null;
      }, n.prototype.getPrevPaintRect = function () {
        return this._prevPaintRect;
      }, n.prototype.animateStyle = function (t) {
        return this.animate("style", t);
      }, n.prototype.updateDuringAnimation = function (t) {
        "style" === t ? this.dirtyStyle() : this.markRedraw();
      }, n.prototype.attrKV = function (e, n) {
        "style" !== e ? t.prototype.attrKV.call(this, e, n) : this.style ? this.setStyle(n) : this.useStyle(n);
      }, n.prototype.setStyle = function (t, e) {
        return "string" == typeof t ? this.style[t] = e : h(this.style, t), this.dirtyStyle(), this;
      }, n.prototype.dirtyStyle = function () {
        this.markRedraw(), this.__dirty |= n.STYLE_CHANGED_BIT, this._rect && (this._rect = null);
      }, n.prototype.dirty = function () {
        this.dirtyStyle();
      }, n.prototype.styleChanged = function () {
        return !!(this.__dirty & n.STYLE_CHANGED_BIT);
      }, n.prototype.styleUpdated = function () {
        this.__dirty &= ~n.STYLE_CHANGED_BIT;
      }, n.prototype.createStyle = function (t) {
        return j(Rg, t);
      }, n.prototype.useStyle = function (t) {
        t[Og] || (t = this.createStyle(t)), this.__inHover ? this.__hoverStyle = t : this.style = t, this.dirtyStyle();
      }, n.prototype.isStyleObject = function (t) {
        return t[Og];
      }, n.prototype._innerSaveToNormal = function (e) {
        t.prototype._innerSaveToNormal.call(this, e);
        var n = this._normalState;
        e.style && !n.style && (n.style = this._mergeStyle(this.createStyle(), this.style)), this._savePrimaryToNormal(e, n, Bg);
      }, n.prototype._applyStateObj = function (e, n, r, i, o, a) {
        t.prototype._applyStateObj.call(this, e, n, r, i, o, a);
        var s,
          l = !(n && i);
        if (n && n.style ? o ? i ? s = n.style : (s = this._mergeStyle(this.createStyle(), r.style), this._mergeStyle(s, n.style)) : (s = this._mergeStyle(this.createStyle(), i ? this.style : r.style), this._mergeStyle(s, n.style)) : l && (s = r.style), s) if (o) {
          var u = this.style;
          if (this.style = this.createStyle(l ? {} : u), l) for (var h = w(u), c = 0; c < h.length; c++) {
            var f = h[c];
            f in s && (s[f] = s[f], this.style[f] = u[f]);
          }
          for (var p = w(s), c = 0; c < p.length; c++) {
            var f = p[c];
            this.style[f] = this.style[f];
          }
          this._transitionState(e, {
            style: s
          }, a, this.getAnimationStyleProps());
        } else this.useStyle(s);
        for (var c = 0; c < Bg.length; c++) {
          var f = Bg[c];
          n && null != n[f] ? this[f] = n[f] : l && null != r[f] && (this[f] = r[f]);
        }
      }, n.prototype._mergeStates = function (e) {
        for (var n, r = t.prototype._mergeStates.call(this, e), i = 0; i < e.length; i++) {
          var o = e[i];
          o.style && (n = n || {}, this._mergeStyle(n, o.style));
        }
        return n && (r.style = n), r;
      }, n.prototype._mergeStyle = function (t, e) {
        return h(t, e), t;
      }, n.prototype.getAnimationStyleProps = function () {
        return Eg;
      }, n.STYLE_CHANGED_BIT = 2, n.initDefaultProps = function () {
        var t = n.prototype;
        t.type = "displayable", t.invisible = !1, t.z = 0, t.z2 = 0, t.zlevel = 0, t.culling = !1, t.cursor = "pointer", t.rectHover = !1, t.incremental = !1, t._rect = null, t.dirtyRectTolerance = 0, t.__dirty = gg.REDARAW_BIT | n.STYLE_CHANGED_BIT;
      }(), n;
    }(gg),
    Fg = new tg(0, 0, 0, 0),
    Ng = new tg(0, 0, 0, 0),
    Hg = Math.pow,
    Vg = Math.sqrt,
    Wg = 1e-8,
    Gg = 1e-4,
    Ug = Vg(3),
    qg = 1 / 3,
    Xg = $(),
    Yg = $(),
    jg = $(),
    Zg = Math.min,
    Kg = Math.max,
    $g = Math.sin,
    Qg = Math.cos,
    Jg = 2 * Math.PI,
    tv = $(),
    ev = $(),
    nv = $(),
    rv = [],
    iv = [],
    ov = {
      M: 1,
      L: 2,
      C: 3,
      Q: 4,
      A: 5,
      Z: 6,
      R: 7
    },
    av = [],
    sv = [],
    lv = [],
    uv = [],
    hv = [],
    cv = [],
    fv = Math.min,
    pv = Math.max,
    dv = Math.cos,
    gv = Math.sin,
    vv = Math.sqrt,
    yv = Math.abs,
    mv = Math.PI,
    _v = 2 * mv,
    xv = "undefined" != typeof Float32Array,
    wv = [],
    bv = function () {
      function t(t) {
        this.dpr = 1, this._version = 0, this._xi = 0, this._yi = 0, this._x0 = 0, this._y0 = 0, this._len = 0, t && (this._saveData = !1), this._saveData && (this.data = []);
      }
      return t.prototype.increaseVersion = function () {
        this._version++;
      }, t.prototype.getVersion = function () {
        return this._version;
      }, t.prototype.setScale = function (t, e, n) {
        n = n || 0, n > 0 && (this._ux = yv(n / og / t) || 0, this._uy = yv(n / og / e) || 0);
      }, t.prototype.setDPR = function (t) {
        this.dpr = t;
      }, t.prototype.setContext = function (t) {
        this._ctx = t;
      }, t.prototype.getContext = function () {
        return this._ctx;
      }, t.prototype.beginPath = function () {
        return this._ctx && this._ctx.beginPath(), this.reset(), this;
      }, t.prototype.reset = function () {
        this._saveData && (this._len = 0), this._lineDash && (this._lineDash = null, this._dashOffset = 0), this._pathSegLen && (this._pathSegLen = null, this._pathLen = 0), this._version++;
      }, t.prototype.moveTo = function (t, e) {
        return this.addData(ov.M, t, e), this._ctx && this._ctx.moveTo(t, e), this._x0 = t, this._y0 = e, this._xi = t, this._yi = e, this;
      }, t.prototype.lineTo = function (t, e) {
        var n = yv(t - this._xi) > this._ux || yv(e - this._yi) > this._uy || this._len < 5;
        return this.addData(ov.L, t, e), this._ctx && n && (this._needsDash ? this._dashedLineTo(t, e) : this._ctx.lineTo(t, e)), n && (this._xi = t, this._yi = e), this;
      }, t.prototype.bezierCurveTo = function (t, e, n, r, i, o) {
        return this.addData(ov.C, t, e, n, r, i, o), this._ctx && (this._needsDash ? this._dashedBezierTo(t, e, n, r, i, o) : this._ctx.bezierCurveTo(t, e, n, r, i, o)), this._xi = i, this._yi = o, this;
      }, t.prototype.quadraticCurveTo = function (t, e, n, r) {
        return this.addData(ov.Q, t, e, n, r), this._ctx && (this._needsDash ? this._dashedQuadraticTo(t, e, n, r) : this._ctx.quadraticCurveTo(t, e, n, r)), this._xi = n, this._yi = r, this;
      }, t.prototype.arc = function (t, e, n, r, i, o) {
        wv[0] = r, wv[1] = i, Lr(wv, o), r = wv[0], i = wv[1];
        var a = i - r;
        return this.addData(ov.A, t, e, n, n, r, a, 0, o ? 0 : 1), this._ctx && this._ctx.arc(t, e, n, r, i, o), this._xi = dv(i) * n + t, this._yi = gv(i) * n + e, this;
      }, t.prototype.arcTo = function (t, e, n, r, i) {
        return this._ctx && this._ctx.arcTo(t, e, n, r, i), this;
      }, t.prototype.rect = function (t, e, n, r) {
        return this._ctx && this._ctx.rect(t, e, n, r), this.addData(ov.R, t, e, n, r), this;
      }, t.prototype.closePath = function () {
        this.addData(ov.Z);
        var t = this._ctx,
          e = this._x0,
          n = this._y0;
        return t && (this._needsDash && this._dashedLineTo(e, n), t.closePath()), this._xi = e, this._yi = n, this;
      }, t.prototype.fill = function (t) {
        t && t.fill(), this.toStatic();
      }, t.prototype.stroke = function (t) {
        t && t.stroke(), this.toStatic();
      }, t.prototype.setLineDash = function (t) {
        if (t instanceof Array) {
          this._lineDash = t, this._dashIdx = 0;
          for (var e = 0, n = 0; n < t.length; n++) {
            e += t[n];
          }
          this._dashSum = e, this._needsDash = !0;
        } else this._lineDash = null, this._needsDash = !1;
        return this;
      }, t.prototype.setLineDashOffset = function (t) {
        return this._dashOffset = t, this;
      }, t.prototype.len = function () {
        return this._len;
      }, t.prototype.setData = function (t) {
        var e = t.length;
        this.data && this.data.length === e || !xv || (this.data = new Float32Array(e));
        for (var n = 0; e > n; n++) {
          this.data[n] = t[n];
        }
        this._len = e;
      }, t.prototype.appendPath = function (t) {
        t instanceof Array || (t = [t]);
        for (var e = t.length, n = 0, r = this._len, i = 0; e > i; i++) {
          n += t[i].len();
        }
        xv && this.data instanceof Float32Array && (this.data = new Float32Array(r + n));
        for (var i = 0; e > i; i++) {
          for (var o = t[i].data, a = 0; a < o.length; a++) {
            this.data[r++] = o[a];
          }
        }
        this._len = r;
      }, t.prototype.addData = function () {
        if (this._saveData) {
          var t = this.data;
          this._len + arguments.length > t.length && (this._expandData(), t = this.data);
          for (var e = 0; e < arguments.length; e++) {
            t[this._len++] = arguments[e];
          }
        }
      }, t.prototype._expandData = function () {
        if (!(this.data instanceof Array)) {
          for (var t = [], e = 0; e < this._len; e++) {
            t[e] = this.data[e];
          }
          this.data = t;
        }
      }, t.prototype._dashedLineTo = function (t, e) {
        var n,
          r,
          i = this._dashSum,
          o = this._lineDash,
          a = this._ctx,
          s = this._dashOffset,
          l = this._xi,
          u = this._yi,
          h = t - l,
          c = e - u,
          f = vv(h * h + c * c),
          p = l,
          d = u,
          g = o.length;
        for (h /= f, c /= f, 0 > s && (s = i + s), s %= i, p -= s * h, d -= s * c; h > 0 && t >= p || 0 > h && p >= t || 0 === h && (c > 0 && e >= d || 0 > c && d >= e);) {
          r = this._dashIdx, n = o[r], p += h * n, d += c * n, this._dashIdx = (r + 1) % g, h > 0 && l > p || 0 > h && p > l || c > 0 && u > d || 0 > c && d > u || a[r % 2 ? "moveTo" : "lineTo"](h >= 0 ? fv(p, t) : pv(p, t), c >= 0 ? fv(d, e) : pv(d, e));
        }
        h = p - t, c = d - e, this._dashOffset = -vv(h * h + c * c);
      }, t.prototype._dashedBezierTo = function (t, e, n, r, i, o) {
        var a,
          s,
          l,
          u,
          h,
          c = this._ctx,
          f = this._dashSum,
          p = this._dashOffset,
          d = this._lineDash,
          g = this._xi,
          v = this._yi,
          y = 0,
          m = this._dashIdx,
          _ = d.length,
          x = 0;
        for (0 > p && (p = f + p), p %= f, a = 0; 1 > a; a += .1) {
          s = fr(g, t, n, i, a + .1) - fr(g, t, n, i, a), l = fr(v, e, r, o, a + .1) - fr(v, e, r, o, a), y += vv(s * s + l * l);
        }
        for (; _ > m && (x += d[m], !(x > p)); m++) {
          ;
        }
        for (a = (x - p) / y; 1 >= a;) {
          u = fr(g, t, n, i, a), h = fr(v, e, r, o, a), m % 2 ? c.moveTo(u, h) : c.lineTo(u, h), a += d[m] / y, m = (m + 1) % _;
        }
        m % 2 !== 0 && c.lineTo(i, o), s = i - u, l = o - h, this._dashOffset = -vv(s * s + l * l);
      }, t.prototype._dashedQuadraticTo = function (t, e, n, r) {
        var i = n,
          o = r;
        n = (n + 2 * t) / 3, r = (r + 2 * e) / 3, t = (this._xi + 2 * t) / 3, e = (this._yi + 2 * e) / 3, this._dashedBezierTo(t, e, n, r, i, o);
      }, t.prototype.toStatic = function () {
        if (this._saveData) {
          var t = this.data;
          t instanceof Array && (t.length = this._len, xv && this._len > 11 && (this.data = new Float32Array(t)));
        }
      }, t.prototype.getBoundingRect = function () {
        lv[0] = lv[1] = hv[0] = hv[1] = Number.MAX_VALUE, uv[0] = uv[1] = cv[0] = cv[1] = -Number.MAX_VALUE;
        var t,
          e = this.data,
          n = 0,
          r = 0,
          i = 0,
          o = 0;
        for (t = 0; t < this._len;) {
          var a = e[t++],
            s = 1 === t;
          switch (s && (n = e[t], r = e[t + 1], i = n, o = r), a) {
            case ov.M:
              n = i = e[t++], r = o = e[t++], hv[0] = i, hv[1] = o, cv[0] = i, cv[1] = o;
              break;
            case ov.L:
              kr(n, r, e[t], e[t + 1], hv, cv), n = e[t++], r = e[t++];
              break;
            case ov.C:
              Dr(n, r, e[t++], e[t++], e[t++], e[t++], e[t], e[t + 1], hv, cv), n = e[t++], r = e[t++];
              break;
            case ov.Q:
              Ir(n, r, e[t++], e[t++], e[t], e[t + 1], hv, cv), n = e[t++], r = e[t++];
              break;
            case ov.A:
              var l = e[t++],
                u = e[t++],
                h = e[t++],
                c = e[t++],
                f = e[t++],
                p = e[t++] + f;
              t += 1;
              var d = !e[t++];
              s && (i = dv(f) * h + l, o = gv(f) * c + u), Ar(l, u, h, c, f, p, d, hv, cv), n = dv(p) * h + l, r = gv(p) * c + u;
              break;
            case ov.R:
              i = n = e[t++], o = r = e[t++];
              var g = e[t++],
                v = e[t++];
              kr(i, o, i + g, o + v, hv, cv);
              break;
            case ov.Z:
              n = i, r = o;
          }
          ve(lv, lv, hv), ye(uv, uv, cv);
        }
        return 0 === t && (lv[0] = lv[1] = uv[0] = uv[1] = 0), new tg(lv[0], lv[1], uv[0] - lv[0], uv[1] - lv[1]);
      }, t.prototype._calculateLength = function () {
        var t = this.data,
          e = this._len,
          n = this._ux,
          r = this._uy,
          i = 0,
          o = 0,
          a = 0,
          s = 0;
        this._pathSegLen || (this._pathSegLen = []);
        for (var l = this._pathSegLen, u = 0, h = 0, c = 0; e > c;) {
          var f = t[c++],
            p = 1 === c;
          p && (i = t[c], o = t[c + 1], a = i, s = o);
          var d = -1;
          switch (f) {
            case ov.M:
              i = a = t[c++], o = s = t[c++];
              break;
            case ov.L:
              var g = t[c++],
                v = t[c++],
                y = g - i,
                m = v - o;
              (yv(y) > n || yv(m) > r || c === e - 1) && (d = Math.sqrt(y * y + m * m), i = g, o = v);
              break;
            case ov.C:
              var _ = t[c++],
                x = t[c++],
                g = t[c++],
                v = t[c++],
                w = t[c++],
                b = t[c++];
              d = mr(i, o, _, x, g, v, w, b, 10), i = w, o = b;
              break;
            case ov.Q:
              var _ = t[c++],
                x = t[c++],
                g = t[c++],
                v = t[c++];
              d = Tr(i, o, _, x, g, v, 10), i = g, o = v;
              break;
            case ov.A:
              var S = t[c++],
                M = t[c++],
                T = t[c++],
                C = t[c++],
                k = t[c++],
                D = t[c++],
                I = D + k;
              c += 1;
              {
                !t[c++];
              }
              p && (a = dv(k) * T + S, s = gv(k) * C + M), d = pv(T, C) * fv(_v, Math.abs(D)), i = dv(I) * T + S, o = gv(I) * C + M;
              break;
            case ov.R:
              a = i = t[c++], s = o = t[c++];
              var A = t[c++],
                P = t[c++];
              d = 2 * A + 2 * P;
              break;
            case ov.Z:
              var y = a - i,
                m = s - o;
              d = Math.sqrt(y * y + m * m), i = a, o = s;
          }
          d >= 0 && (l[h++] = d, u += d);
        }
        return this._pathLen = u, u;
      }, t.prototype.rebuildPath = function (t, e) {
        var n,
          r,
          i,
          o,
          a,
          s,
          l,
          u,
          h,
          c = this.data,
          f = this._ux,
          p = this._uy,
          d = this._len,
          g = 1 > e,
          v = 0,
          y = 0;
        if (!g || (this._pathSegLen || this._calculateLength(), l = this._pathSegLen, u = this._pathLen, h = e * u)) t: for (var m = 0; d > m;) {
          var _ = c[m++],
            x = 1 === m;
          switch (x && (i = c[m], o = c[m + 1], n = i, r = o), _) {
            case ov.M:
              n = i = c[m++], r = o = c[m++], t.moveTo(i, o);
              break;
            case ov.L:
              if (a = c[m++], s = c[m++], yv(a - i) > f || yv(s - o) > p || m === d - 1) {
                if (g) {
                  var w = l[y++];
                  if (v + w > h) {
                    var b = (h - v) / w;
                    t.lineTo(i * (1 - b) + a * b, o * (1 - b) + s * b);
                    break t;
                  }
                  v += w;
                }
                t.lineTo(a, s), i = a, o = s;
              }
              break;
            case ov.C:
              var S = c[m++],
                M = c[m++],
                T = c[m++],
                C = c[m++],
                k = c[m++],
                D = c[m++];
              if (g) {
                var w = l[y++];
                if (v + w > h) {
                  var b = (h - v) / w;
                  vr(i, S, T, k, b, av), vr(o, M, C, D, b, sv), t.bezierCurveTo(av[1], sv[1], av[2], sv[2], av[3], sv[3]);
                  break t;
                }
                v += w;
              }
              t.bezierCurveTo(S, M, T, C, k, D), i = k, o = D;
              break;
            case ov.Q:
              var S = c[m++],
                M = c[m++],
                T = c[m++],
                C = c[m++];
              if (g) {
                var w = l[y++];
                if (v + w > h) {
                  var b = (h - v) / w;
                  Sr(i, S, T, b, av), Sr(o, M, C, b, sv), t.quadraticCurveTo(av[1], sv[1], av[2], sv[2]);
                  break t;
                }
                v += w;
              }
              t.quadraticCurveTo(S, M, T, C), i = T, o = C;
              break;
            case ov.A:
              var I = c[m++],
                A = c[m++],
                P = c[m++],
                L = c[m++],
                O = c[m++],
                R = c[m++],
                E = c[m++],
                B = !c[m++],
                z = P > L ? P : L,
                F = yv(P - L) > .001,
                N = O + R,
                H = !1;
              if (g) {
                var w = l[y++];
                v + w > h && (N = O + R * (h - v) / w, H = !0), v += w;
              }
              if (F && t.ellipse ? t.ellipse(I, A, P, L, E, O, N, B) : t.arc(I, A, z, O, N, B), H) break t;
              x && (n = dv(O) * P + I, r = gv(O) * L + A), i = dv(N) * P + I, o = gv(N) * L + A;
              break;
            case ov.R:
              n = i = c[m], r = o = c[m + 1], a = c[m++], s = c[m++];
              var V = c[m++],
                W = c[m++];
              if (g) {
                var w = l[y++];
                if (v + w > h) {
                  var G = h - v;
                  t.moveTo(a, s), t.lineTo(a + fv(G, V), s), G -= V, G > 0 && t.lineTo(a + V, s + fv(G, W)), G -= W, G > 0 && t.lineTo(a + pv(V - G, 0), s + W), G -= V, G > 0 && t.lineTo(a, s + pv(W - G, 0));
                  break t;
                }
                v += w;
              }
              t.rect(a, s, V, W);
              break;
            case ov.Z:
              if (g) {
                var w = l[y++];
                if (v + w > h) {
                  var b = (h - v) / w;
                  t.lineTo(i * (1 - b) + n * b, o * (1 - b) + r * b);
                  break t;
                }
                v += w;
              }
              t.closePath(), i = n, o = r;
          }
        }
      }, t.CMD = ov, t.initDefaultProps = function () {
        var e = t.prototype;
        e._saveData = !0, e._needsDash = !1, e._dashOffset = 0, e._dashIdx = 0, e._dashSum = 0, e._ux = 0, e._uy = 0;
      }(), t;
    }(),
    Sv = 2 * Math.PI,
    Mv = 2 * Math.PI,
    Tv = bv.CMD,
    Cv = 2 * Math.PI,
    kv = 1e-4,
    Dv = [-1, -1, -1],
    Iv = [-1, -1],
    Av = c({
      fill: "#000",
      stroke: null,
      strokePercent: 1,
      fillOpacity: 1,
      strokeOpacity: 1,
      lineDashOffset: 0,
      lineWidth: 1,
      lineCap: "butt",
      miterLimit: 10,
      strokeNoScale: !1,
      strokeFirst: !1
    }, Rg),
    Pv = {
      style: c({
        fill: !0,
        stroke: !0,
        strokePercent: !0,
        fillOpacity: !0,
        strokeOpacity: !0,
        lineDashOffset: !0,
        lineWidth: !0,
        miterLimit: !0
      }, Eg.style)
    },
    Lv = ["x", "y", "rotation", "scaleX", "scaleY", "originX", "originY", "invisible", "culling", "z", "z2", "zlevel", "parent"],
    Ov = function (t) {
      function n(e) {
        return t.call(this, e) || this;
      }
      return e(n, t), n.prototype.update = function () {
        var e = this;
        t.prototype.update.call(this);
        var r = this.style;
        if (r.decal) {
          var i = this._decalEl = this._decalEl || new n();
          i.buildPath === n.prototype.buildPath && (i.buildPath = function (t) {
            e.buildPath(t, e.shape);
          }), i.silent = !0;
          var o = i.style;
          for (var a in r) {
            o[a] !== r[a] && (o[a] = r[a]);
          }
          o.fill = r.fill ? r.decal : null, o.decal = null, o.shadowColor = null, r.strokeFirst && (o.stroke = null);
          for (var s = 0; s < Lv.length; ++s) {
            i[Lv[s]] = this[Lv[s]];
          }
          i.__dirty |= gg.REDARAW_BIT;
        } else this._decalEl && (this._decalEl = null);
      }, n.prototype.getDecalElement = function () {
        return this._decalEl;
      }, n.prototype._init = function (e) {
        var n = w(e);
        this.shape = this.getDefaultShape();
        var r = this.getDefaultStyle();
        r && this.useStyle(r);
        for (var i = 0; i < n.length; i++) {
          var o = n[i],
            a = e[o];
          "style" === o ? this.style ? h(this.style, a) : this.useStyle(a) : "shape" === o ? h(this.shape, a) : t.prototype.attrKV.call(this, o, a);
        }
        this.style || this.useStyle({});
      }, n.prototype.getDefaultStyle = function () {
        return null;
      }, n.prototype.getDefaultShape = function () {
        return {};
      }, n.prototype.canBeInsideText = function () {
        return this.hasFill();
      }, n.prototype.getInsideTextFill = function () {
        var t = this.style.fill;
        if ("none" !== t) {
          if (C(t)) {
            var e = dn(t, 0);
            return e > .5 ? sg : e > .2 ? ug : lg;
          }
          if (t) return lg;
        }
        return sg;
      }, n.prototype.getInsideTextStroke = function (t) {
        var e = this.style.fill;
        if (C(e)) {
          var n = this.__zr,
            r = !(!n || !n.isDarkMode()),
            i = dn(t, 0) < ag;
          if (r === i) return e;
        }
      }, n.prototype.buildPath = function () {}, n.prototype.pathUpdated = function () {
        this.__dirty &= ~n.SHAPE_CHANGED_BIT;
      }, n.prototype.createPathProxy = function () {
        this.path = new bv(!1);
      }, n.prototype.hasStroke = function () {
        var t = this.style,
          e = t.stroke;
        return !(null == e || "none" === e || !(t.lineWidth > 0));
      }, n.prototype.hasFill = function () {
        var t = this.style,
          e = t.fill;
        return null != e && "none" !== e;
      }, n.prototype.getBoundingRect = function () {
        var t = this._rect,
          e = this.style,
          r = !t;
        if (r) {
          var i = !1;
          this.path || (i = !0, this.createPathProxy());
          var o = this.path;
          (i || this.__dirty & n.SHAPE_CHANGED_BIT) && (o.beginPath(), this.buildPath(o, this.shape, !1), this.pathUpdated()), t = o.getBoundingRect();
        }
        if (this._rect = t, this.hasStroke() && this.path && this.path.len() > 0) {
          var a = this._rectWithStroke || (this._rectWithStroke = t.clone());
          if (this.__dirty || r) {
            a.copy(t);
            var s = e.strokeNoScale ? this.getLineScale() : 1,
              l = e.lineWidth;
            if (!this.hasFill()) {
              var u = this.strokeContainThreshold;
              l = Math.max(l, null == u ? 4 : u);
            }
            s > 1e-10 && (a.width += l / s, a.height += l / s, a.x -= l / s / 2, a.y -= l / s / 2);
          }
          return a;
        }
        return t;
      }, n.prototype.contain = function (t, e) {
        var n = this.transformCoordToLocal(t, e),
          r = this.getBoundingRect(),
          i = this.style;
        if (t = n[0], e = n[1], r.contain(t, e)) {
          var o = this.path;
          if (this.hasStroke()) {
            var a = i.lineWidth,
              s = i.strokeNoScale ? this.getLineScale() : 1;
            if (s > 1e-10 && (this.hasFill() || (a = Math.max(a, this.strokeContainThreshold)), Xr(o, a / s, t, e))) return !0;
          }
          if (this.hasFill()) return qr(o, t, e);
        }
        return !1;
      }, n.prototype.dirtyShape = function () {
        this.__dirty |= n.SHAPE_CHANGED_BIT, this._rect && (this._rect = null), this._decalEl && this._decalEl.dirtyShape(), this.markRedraw();
      }, n.prototype.dirty = function () {
        this.dirtyStyle(), this.dirtyShape();
      }, n.prototype.animateShape = function (t) {
        return this.animate("shape", t);
      }, n.prototype.updateDuringAnimation = function (t) {
        "style" === t ? this.dirtyStyle() : "shape" === t ? this.dirtyShape() : this.markRedraw();
      }, n.prototype.attrKV = function (e, n) {
        "shape" === e ? this.setShape(n) : t.prototype.attrKV.call(this, e, n);
      }, n.prototype.setShape = function (t, e) {
        var n = this.shape;
        return n || (n = this.shape = {}), "string" == typeof t ? n[t] = e : h(n, t), this.dirtyShape(), this;
      }, n.prototype.shapeChanged = function () {
        return !!(this.__dirty & n.SHAPE_CHANGED_BIT);
      }, n.prototype.createStyle = function (t) {
        return j(Av, t);
      }, n.prototype._innerSaveToNormal = function (e) {
        t.prototype._innerSaveToNormal.call(this, e);
        var n = this._normalState;
        e.shape && !n.shape && (n.shape = h({}, this.shape));
      }, n.prototype._applyStateObj = function (e, n, r, i, o, a) {
        t.prototype._applyStateObj.call(this, e, n, r, i, o, a);
        var s,
          l = !(n && i);
        if (n && n.shape ? o ? i ? s = n.shape : (s = h({}, r.shape), h(s, n.shape)) : (s = h({}, i ? this.shape : r.shape), h(s, n.shape)) : l && (s = r.shape), s) if (o) {
          this.shape = h({}, this.shape);
          for (var u = {}, c = w(s), f = 0; f < c.length; f++) {
            var p = c[f];
            "object" == _typeof(s[p]) ? this.shape[p] = s[p] : u[p] = s[p];
          }
          this._transitionState(e, {
            shape: u
          }, a);
        } else this.shape = s, this.dirtyShape();
      }, n.prototype._mergeStates = function (e) {
        for (var n, r = t.prototype._mergeStates.call(this, e), i = 0; i < e.length; i++) {
          var o = e[i];
          o.shape && (n = n || {}, this._mergeStyle(n, o.shape));
        }
        return n && (r.shape = n), r;
      }, n.prototype.getAnimationStyleProps = function () {
        return Pv;
      }, n.prototype.isZeroArea = function () {
        return !1;
      }, n.extend = function (t) {
        var r = function (n) {
          function r(e) {
            var r = n.call(this, e) || this;
            return t.init && t.init.call(r, e), r;
          }
          return e(r, n), r.prototype.getDefaultStyle = function () {
            return s(t.style);
          }, r.prototype.getDefaultShape = function () {
            return s(t.shape);
          }, r;
        }(n);
        for (var i in t) {
          "function" == typeof t[i] && (r.prototype[i] = t[i]);
        }
        return r;
      }, n.SHAPE_CHANGED_BIT = 4, n.initDefaultProps = function () {
        var t = n.prototype;
        t.type = "path", t.strokeContainThreshold = 5, t.segmentIgnoreThreshold = 0, t.subPixelOptimize = !1, t.autoBatch = !1, t.__dirty = gg.REDARAW_BIT | zg.STYLE_CHANGED_BIT | n.SHAPE_CHANGED_BIT;
      }(), n;
    }(zg),
    Rv = bv.CMD,
    Ev = [[], [], []],
    Bv = Math.sqrt,
    zv = Math.atan2,
    Fv = Math.sqrt,
    Nv = Math.sin,
    Hv = Math.cos,
    Vv = Math.PI,
    Wv = /([mlvhzcqtsa])([^mlvhzcqtsa]*)/gi,
    Gv = /-?([0-9]*\.)?[0-9]+([eE]-?[0-9]+)?/g,
    Uv = function (t) {
      function n() {
        return null !== t && t.apply(this, arguments) || this;
      }
      return e(n, t), n.prototype.applyTransform = function () {}, n;
    }(Ov),
    qv = function (t) {
      function n(e) {
        var n = t.call(this) || this;
        return n.isGroup = !0, n._children = [], n.attr(e), n;
      }
      return e(n, t), n.prototype.childrenRef = function () {
        return this._children;
      }, n.prototype.children = function () {
        return this._children.slice();
      }, n.prototype.childAt = function (t) {
        return this._children[t];
      }, n.prototype.childOfName = function (t) {
        for (var e = this._children, n = 0; n < e.length; n++) {
          if (e[n].name === t) return e[n];
        }
      }, n.prototype.childCount = function () {
        return this._children.length;
      }, n.prototype.add = function (t) {
        if (t && (t !== this && t.parent !== this && (this._children.push(t), this._doAdd(t)), t.__hostTarget)) throw "This elemenet has been used as an attachment";
        return this;
      }, n.prototype.addBefore = function (t, e) {
        if (t && t !== this && t.parent !== this && e && e.parent === this) {
          var n = this._children,
            r = n.indexOf(e);
          r >= 0 && (n.splice(r, 0, t), this._doAdd(t));
        }
        return this;
      }, n.prototype.replaceAt = function (t, e) {
        var n = this._children,
          r = n[e];
        if (t && t !== this && t.parent !== this && t !== r) {
          n[e] = t, r.parent = null;
          var i = this.__zr;
          i && r.removeSelfFromZr(i), this._doAdd(t);
        }
        return this;
      }, n.prototype._doAdd = function (t) {
        t.parent && t.parent.remove(t), t.parent = this;
        var e = this.__zr;
        e && e !== t.__zr && t.addSelfToZr(e), e && e.refresh();
      }, n.prototype.remove = function (t) {
        var e = this.__zr,
          n = this._children,
          r = f(n, t);
        return 0 > r ? this : (n.splice(r, 1), t.parent = null, e && t.removeSelfFromZr(e), e && e.refresh(), this);
      }, n.prototype.removeAll = function () {
        for (var t = this._children, e = this.__zr, n = 0; n < t.length; n++) {
          var r = t[n];
          e && r.removeSelfFromZr(e), r.parent = null;
        }
        return t.length = 0, this;
      }, n.prototype.eachChild = function (t, e) {
        for (var n = this._children, r = 0; r < n.length; r++) {
          var i = n[r];
          t.call(e, i, r);
        }
        return this;
      }, n.prototype.traverse = function (t, e) {
        for (var n = 0; n < this._children.length; n++) {
          var r = this._children[n],
            i = t.call(e, r);
          r.isGroup && !i && r.traverse(t, e);
        }
        return this;
      }, n.prototype.addSelfToZr = function (e) {
        t.prototype.addSelfToZr.call(this, e);
        for (var n = 0; n < this._children.length; n++) {
          var r = this._children[n];
          r.addSelfToZr(e);
        }
      }, n.prototype.removeSelfFromZr = function (e) {
        t.prototype.removeSelfFromZr.call(this, e);
        for (var n = 0; n < this._children.length; n++) {
          var r = this._children[n];
          r.removeSelfFromZr(e);
        }
      }, n.prototype.getBoundingRect = function (t) {
        for (var e = new tg(0, 0, 0, 0), n = t || this._children, r = [], i = null, o = 0; o < n.length; o++) {
          var a = n[o];
          if (!a.ignore && !a.invisible) {
            var s = a.getBoundingRect(),
              l = a.getLocalTransform(r);
            l ? (tg.applyTransform(e, s, l), i = i || e.clone(), i.union(e)) : (i = i || s.clone(), i.union(s));
          }
        }
        return i || e;
      }, n;
    }(gg);
  qv.prototype.type = "group";
  var Xv = c({
      x: 0,
      y: 0
    }, Rg),
    Yv = {
      style: c({
        x: !0,
        y: !0,
        width: !0,
        height: !0,
        sx: !0,
        sy: !0,
        sWidth: !0,
        sHeight: !0
      }, Eg.style)
    },
    jv = function (t) {
      function n() {
        return null !== t && t.apply(this, arguments) || this;
      }
      return e(n, t), n.prototype.createStyle = function (t) {
        return j(Xv, t);
      }, n.prototype._getSize = function (t) {
        var e = this.style,
          n = e[t];
        if (null != n) return n;
        var r = ii(e.image) ? e.image : this.__image;
        if (!r) return 0;
        var i = "width" === t ? "height" : "width",
          o = e[i];
        return null == o ? r[t] : r[t] / r[i] * o;
      }, n.prototype.getWidth = function () {
        return this._getSize("width");
      }, n.prototype.getHeight = function () {
        return this._getSize("height");
      }, n.prototype.getAnimationStyleProps = function () {
        return Yv;
      }, n.prototype.getBoundingRect = function () {
        var t = this.style;
        return this._rect || (this._rect = new tg(t.x || 0, t.y || 0, this.getWidth(), this.getHeight())), this._rect;
      }, n;
    }(zg);
  jv.prototype.type = "image";
  var Zv = function () {
      function t() {
        this.cx = 0, this.cy = 0, this.r = 0;
      }
      return t;
    }(),
    Kv = function (t) {
      function n(e) {
        return t.call(this, e) || this;
      }
      return e(n, t), n.prototype.getDefaultShape = function () {
        return new Zv();
      }, n.prototype.buildPath = function (t, e, n) {
        n && t.moveTo(e.cx + e.r, e.cy), t.arc(e.cx, e.cy, e.r, 0, 2 * Math.PI);
      }, n;
    }(Ov);
  Kv.prototype.type = "circle";
  var $v = Math.round,
    Qv = function () {
      function t() {
        this.x = 0, this.y = 0, this.width = 0, this.height = 0;
      }
      return t;
    }(),
    Jv = {},
    ty = function (t) {
      function n(e) {
        return t.call(this, e) || this;
      }
      return e(n, t), n.prototype.getDefaultShape = function () {
        return new Qv();
      }, n.prototype.buildPath = function (t, e) {
        var n, r, i, o;
        if (this.subPixelOptimize) {
          var a = si(Jv, e, this.style);
          n = a.x, r = a.y, i = a.width, o = a.height, a.r = e.r, e = a;
        } else n = e.x, r = e.y, i = e.width, o = e.height;
        e.r ? oi(t, e) : t.rect(n, r, i, o);
      }, n.prototype.isZeroArea = function () {
        return !this.shape.width || !this.shape.height;
      }, n;
    }(Ov);
  ty.prototype.type = "rect";
  var ey = function () {
      function t() {
        this.cx = 0, this.cy = 0, this.rx = 0, this.ry = 0;
      }
      return t;
    }(),
    ny = function (t) {
      function n(e) {
        return t.call(this, e) || this;
      }
      return e(n, t), n.prototype.getDefaultShape = function () {
        return new ey();
      }, n.prototype.buildPath = function (t, e) {
        var n = .5522848,
          r = e.cx,
          i = e.cy,
          o = e.rx,
          a = e.ry,
          s = o * n,
          l = a * n;
        t.moveTo(r - o, i), t.bezierCurveTo(r - o, i - l, r - s, i - a, r, i - a), t.bezierCurveTo(r + s, i - a, r + o, i - l, r + o, i), t.bezierCurveTo(r + o, i + l, r + s, i + a, r, i + a), t.bezierCurveTo(r - s, i + a, r - o, i + l, r - o, i), t.closePath();
      }, n;
    }(Ov);
  ny.prototype.type = "ellipse";
  var ry = {},
    iy = function () {
      function t() {
        this.x1 = 0, this.y1 = 0, this.x2 = 0, this.y2 = 0, this.percent = 1;
      }
      return t;
    }(),
    oy = function (t) {
      function n(e) {
        return t.call(this, e) || this;
      }
      return e(n, t), n.prototype.getDefaultStyle = function () {
        return {
          stroke: "#000",
          fill: null
        };
      }, n.prototype.getDefaultShape = function () {
        return new iy();
      }, n.prototype.buildPath = function (t, e) {
        var n, r, i, o;
        if (this.subPixelOptimize) {
          var a = ai(ry, e, this.style);
          n = a.x1, r = a.y1, i = a.x2, o = a.y2;
        } else n = e.x1, r = e.y1, i = e.x2, o = e.y2;
        var s = e.percent;
        0 !== s && (t.moveTo(n, r), 1 > s && (i = n * (1 - s) + i * s, o = r * (1 - s) + o * s), t.lineTo(i, o));
      }, n.prototype.pointAt = function (t) {
        var e = this.shape;
        return [e.x1 * (1 - t) + e.x2 * t, e.y1 * (1 - t) + e.y2 * t];
      }, n;
    }(Ov);
  oy.prototype.type = "line";
  var ay = function () {
      function t() {
        this.points = null, this.smooth = 0, this.smoothConstraint = null;
      }
      return t;
    }(),
    sy = function (t) {
      function n(e) {
        return t.call(this, e) || this;
      }
      return e(n, t), n.prototype.getDefaultShape = function () {
        return new ay();
      }, n.prototype.buildPath = function (t, e) {
        fi(t, e, !0);
      }, n;
    }(Ov);
  sy.prototype.type = "polygon";
  var ly = function () {
      function t() {
        this.points = null, this.percent = 1, this.smooth = 0, this.smoothConstraint = null;
      }
      return t;
    }(),
    uy = function (t) {
      function n(e) {
        return t.call(this, e) || this;
      }
      return e(n, t), n.prototype.getDefaultStyle = function () {
        return {
          stroke: "#000",
          fill: null
        };
      }, n.prototype.getDefaultShape = function () {
        return new ly();
      }, n.prototype.buildPath = function (t, e) {
        fi(t, e, !1);
      }, n;
    }(Ov);
  uy.prototype.type = "polyline";
  var hy = function () {
      function t(t) {
        this.colorStops = t || [];
      }
      return t.prototype.addColorStop = function (t, e) {
        this.colorStops.push({
          offset: t,
          color: e
        });
      }, t;
    }(),
    cy = function (t) {
      function n(e, n, r, i, o, a) {
        var s = t.call(this, o) || this;
        return s.x = null == e ? 0 : e, s.y = null == n ? 0 : n, s.x2 = null == r ? 1 : r, s.y2 = null == i ? 0 : i, s.type = "linear", s.global = a || !1, s;
      }
      return e(n, t), n;
    }(hy),
    fy = c({
      strokeFirst: !0,
      font: ng,
      x: 0,
      y: 0,
      textAlign: "left",
      textBaseline: "top",
      miterLimit: 2
    }, Av),
    py = function (t) {
      function n() {
        return null !== t && t.apply(this, arguments) || this;
      }
      return e(n, t), n.prototype.hasStroke = function () {
        var t = this.style,
          e = t.stroke;
        return null != e && "none" !== e && t.lineWidth > 0;
      }, n.prototype.hasFill = function () {
        var t = this.style,
          e = t.fill;
        return null != e && "none" !== e;
      }, n.prototype.createStyle = function (t) {
        return j(fy, t);
      }, n.prototype.setBoundingRect = function (t) {
        this._rect = t;
      }, n.prototype.getBoundingRect = function () {
        var t = this.style;
        if (!this._rect) {
          var e = t.text;
          null != e ? e += "" : e = "";
          var n = On(e, t.font, t.textAlign, t.textBaseline);
          if (n.x += t.x || 0, n.y += t.y || 0, this.hasStroke()) {
            var r = t.lineWidth;
            n.x -= r / 2, n.y -= r / 2, n.width += r, n.height += r;
          }
          this._rect = n;
        }
        return this._rect;
      }, n.initDefaultProps = function () {
        var t = n.prototype;
        t.dirtyRectTolerance = 10;
      }(), n;
    }(zg);
  py.prototype.type = "tspan";
  var dy,
    gy = /[\s,]+/,
    vy = (function () {
      function t() {
        this._defs = {}, this._root = null, this._isDefine = !1, this._isText = !1;
      }
      return t.prototype.parse = function (t, e) {
        e = e || {};
        var n = pi(t);
        if (!n) throw new Error("Illegal svg");
        var r = new qv();
        this._root = r;
        var i = n.getAttribute("viewBox") || "",
          o = parseFloat(n.getAttribute("width") || e.width),
          a = parseFloat(n.getAttribute("height") || e.height);
        isNaN(o) && (o = null), isNaN(a) && (a = null), yi(n, r, null, !0);
        for (var s = n.firstChild; s;) {
          this._parseNode(s, r), s = s.nextSibling;
        }
        var l, u;
        if (i) {
          var h = G(i).split(gy);
          h.length >= 4 && (l = {
            x: parseFloat(h[0] || 0),
            y: parseFloat(h[1] || 0),
            width: parseFloat(h[2]),
            height: parseFloat(h[3])
          });
        }
        if (l && null != o && null != a && (u = wi(l, o, a), !e.ignoreViewBox)) {
          var c = r;
          r = new qv(), r.add(c), c.scaleX = c.scaleY = u.scale, c.x = u.x, c.y = u.y;
        }
        return e.ignoreRootClip || null == o || null == a || r.setClipPath(new ty({
          shape: {
            x: 0,
            y: 0,
            width: o,
            height: a
          }
        })), {
          root: r,
          width: o,
          height: a,
          viewBoxRect: l,
          viewBoxTransform: u
        };
      }, t.prototype._parseNode = function (t, e) {
        var n = t.nodeName.toLowerCase();
        "defs" === n ? this._isDefine = !0 : "text" === n && (this._isText = !0);
        var r;
        if (this._isDefine) {
          var i = vy[n];
          if (i) {
            var o = i.call(this, t),
              a = t.getAttribute("id");
            a && (this._defs[a] = o);
          }
        } else {
          var i = dy[n];
          i && (r = i.call(this, t, e), e.add(r));
        }
        if (r) for (var s = t.firstChild; s;) {
          1 === s.nodeType && this._parseNode(s, r), 3 === s.nodeType && this._isText && this._parseText(s, r), s = s.nextSibling;
        }
        "defs" === n ? this._isDefine = !1 : "text" === n && (this._isText = !1);
      }, t.prototype._parseText = function (t, e) {
        if (1 === t.nodeType) {
          var n = t.getAttribute("dx") || 0,
            r = t.getAttribute("dy") || 0;
          this._textX += parseFloat(n), this._textY += parseFloat(r);
        }
        var i = new py({
          style: {
            text: t.textContent
          },
          x: this._textX || 0,
          y: this._textY || 0
        });
        gi(e, i), yi(t, i, this._defs);
        var o = i.style,
          a = o.fontSize;
        a && 9 > a && (o.fontSize = 9, i.scaleX *= a / 9, i.scaleY *= a / 9);
        var s = (o.fontSize || o.fontFamily) && [o.fontStyle, o.fontWeight, (o.fontSize || 12) + "px", o.fontFamily || "sans-serif"].join(" ");
        o.font = s;
        var l = i.getBoundingRect();
        return this._textX += l.width, e.add(i), i;
      }, t.internalField = function () {
        dy = {
          g: function g(t, e) {
            var n = new qv();
            return gi(e, n), yi(t, n, this._defs), n;
          },
          rect: function rect(t, e) {
            var n = new ty();
            return gi(e, n), yi(t, n, this._defs), n.setShape({
              x: parseFloat(t.getAttribute("x") || "0"),
              y: parseFloat(t.getAttribute("y") || "0"),
              width: parseFloat(t.getAttribute("width") || "0"),
              height: parseFloat(t.getAttribute("height") || "0")
            }), n;
          },
          circle: function circle(t, e) {
            var n = new Kv();
            return gi(e, n), yi(t, n, this._defs), n.setShape({
              cx: parseFloat(t.getAttribute("cx") || "0"),
              cy: parseFloat(t.getAttribute("cy") || "0"),
              r: parseFloat(t.getAttribute("r") || "0")
            }), n;
          },
          line: function line(t, e) {
            var n = new oy();
            return gi(e, n), yi(t, n, this._defs), n.setShape({
              x1: parseFloat(t.getAttribute("x1") || "0"),
              y1: parseFloat(t.getAttribute("y1") || "0"),
              x2: parseFloat(t.getAttribute("x2") || "0"),
              y2: parseFloat(t.getAttribute("y2") || "0")
            }), n;
          },
          ellipse: function ellipse(t, e) {
            var n = new ny();
            return gi(e, n), yi(t, n, this._defs), n.setShape({
              cx: parseFloat(t.getAttribute("cx") || "0"),
              cy: parseFloat(t.getAttribute("cy") || "0"),
              rx: parseFloat(t.getAttribute("rx") || "0"),
              ry: parseFloat(t.getAttribute("ry") || "0")
            }), n;
          },
          polygon: function polygon(t, e) {
            var n,
              r = t.getAttribute("points");
            r && (n = vi(r));
            var i = new sy({
              shape: {
                points: n || []
              }
            });
            return gi(e, i), yi(t, i, this._defs), i;
          },
          polyline: function polyline(t, e) {
            var n = new Ov();
            gi(e, n), yi(t, n, this._defs);
            var r,
              i = t.getAttribute("points");
            i && (r = vi(i));
            var o = new uy({
              shape: {
                points: r || []
              }
            });
            return o;
          },
          image: function image(t, e) {
            var n = new jv();
            return gi(e, n), yi(t, n, this._defs), n.setStyle({
              image: t.getAttribute("xlink:href"),
              x: +t.getAttribute("x"),
              y: +t.getAttribute("y"),
              width: +t.getAttribute("width"),
              height: +t.getAttribute("height")
            }), n;
          },
          text: function text(t, e) {
            var n = t.getAttribute("x") || "0",
              r = t.getAttribute("y") || "0",
              i = t.getAttribute("dx") || "0",
              o = t.getAttribute("dy") || "0";
            this._textX = parseFloat(n) + parseFloat(i), this._textY = parseFloat(r) + parseFloat(o);
            var a = new qv();
            return gi(e, a), yi(t, a, this._defs), a;
          },
          tspan: function tspan(t, e) {
            var n = t.getAttribute("x"),
              r = t.getAttribute("y");
            null != n && (this._textX = parseFloat(n)), null != r && (this._textY = parseFloat(r));
            var i = t.getAttribute("dx") || 0,
              o = t.getAttribute("dy") || 0,
              a = new qv();
            return gi(e, a), yi(t, a, this._defs), this._textX += i, this._textY += o, a;
          },
          path: function path(t, e) {
            var n = t.getAttribute("d") || "",
              r = ei(n);
            return gi(e, r), yi(t, r, this._defs), r;
          }
        };
      }(), t;
    }(), {
      lineargradient: function lineargradient(t) {
        var e = parseInt(t.getAttribute("x1") || "0", 10),
          n = parseInt(t.getAttribute("y1") || "0", 10),
          r = parseInt(t.getAttribute("x2") || "10", 10),
          i = parseInt(t.getAttribute("y2") || "0", 10),
          o = new cy(e, n, r, i);
        return di(t, o), o;
      }
    }),
    yy = {
      fill: "fill",
      stroke: "stroke",
      "stroke-width": "lineWidth",
      opacity: "opacity",
      "fill-opacity": "fillOpacity",
      "stroke-opacity": "strokeOpacity",
      "stroke-dasharray": "lineDash",
      "stroke-dashoffset": "lineDashOffset",
      "stroke-linecap": "lineCap",
      "stroke-linejoin": "lineJoin",
      "stroke-miterlimit": "miterLimit",
      "font-family": "fontFamily",
      "font-size": "fontSize",
      "font-style": "fontStyle",
      "font-weight": "fontWeight",
      "text-align": "textAlign",
      "alignment-baseline": "textBaseline"
    },
    my = /url\(\s*#(.*?)\)/,
    _y = /(translate|scale|rotate|skewX|skewY|matrix)\(([\-\s0-9\.e,]*)\)/g,
    xy = /([^\s:;]+)\s*:\s*([^:;]+)/g,
    wy = Math.PI,
    by = 2 * wy,
    Sy = Math.sin,
    My = Math.cos,
    Ty = Math.acos,
    Cy = Math.atan2,
    ky = Math.abs,
    Dy = Math.sqrt,
    Iy = Math.max,
    Ay = Math.min,
    Py = 1e-4,
    Ly = function () {
      function t() {
        this.cx = 0, this.cy = 0, this.r0 = 0, this.r = 0, this.startAngle = 0, this.endAngle = 2 * Math.PI, this.clockwise = !0, this.cornerRadius = 0, this.innerCornerRadius = 0;
      }
      return t;
    }(),
    Oy = function (t) {
      function n(e) {
        return t.call(this, e) || this;
      }
      return e(n, t), n.prototype.getDefaultShape = function () {
        return new Ly();
      }, n.prototype.buildPath = function (t, e) {
        Mi(t, e);
      }, n.prototype.isZeroArea = function () {
        return this.shape.startAngle === this.shape.endAngle || this.shape.r === this.shape.r0;
      }, n;
    }(Ov);
  Oy.prototype.type = "sector";
  var Ry = function (t) {
      function n() {
        var e = null !== t && t.apply(this, arguments) || this;
        return e.type = "compound", e;
      }
      return e(n, t), n.prototype._updatePathDirty = function () {
        for (var t = this.shape.paths, e = this.shapeChanged(), n = 0; n < t.length; n++) {
          e = e || t[n].shapeChanged();
        }
        e && this.dirtyShape();
      }, n.prototype.beforeBrush = function () {
        this._updatePathDirty();
        for (var t = this.shape.paths || [], e = this.getGlobalScale(), n = 0; n < t.length; n++) {
          t[n].path || t[n].createPathProxy(), t[n].path.setScale(e[0], e[1], t[n].segmentIgnoreThreshold);
        }
      }, n.prototype.buildPath = function (t, e) {
        for (var n = e.paths || [], r = 0; r < n.length; r++) {
          n[r].buildPath(t, n[r].shape, !0);
        }
      }, n.prototype.afterBrush = function () {
        for (var t = this.shape.paths || [], e = 0; e < t.length; e++) {
          t[e].pathUpdated();
        }
      }, n.prototype.getBoundingRect = function () {
        return this._updatePathDirty.call(this), Ov.prototype.getBoundingRect.call(this);
      }, n;
    }(Ov),
    Ey = [],
    By = function (t) {
      function n() {
        var e = null !== t && t.apply(this, arguments) || this;
        return e.notClear = !0, e.incremental = !0, e._displayables = [], e._temporaryDisplayables = [], e._cursor = 0, e;
      }
      return e(n, t), n.prototype.traverse = function (t, e) {
        t.call(e, this);
      }, n.prototype.useStyle = function () {
        this.style = {};
      }, n.prototype.getCursor = function () {
        return this._cursor;
      }, n.prototype.innerAfterBrush = function () {
        this._cursor = this._displayables.length;
      }, n.prototype.clearDisplaybles = function () {
        this._displayables = [], this._temporaryDisplayables = [], this._cursor = 0, this.markRedraw(), this.notClear = !1;
      }, n.prototype.clearTemporalDisplayables = function () {
        this._temporaryDisplayables = [];
      }, n.prototype.addDisplayable = function (t, e) {
        e ? this._temporaryDisplayables.push(t) : this._displayables.push(t), this.markRedraw();
      }, n.prototype.addDisplayables = function (t, e) {
        e = e || !1;
        for (var n = 0; n < t.length; n++) {
          this.addDisplayable(t[n], e);
        }
      }, n.prototype.getDisplayables = function () {
        return this._displayables;
      }, n.prototype.getTemporalDisplayables = function () {
        return this._temporaryDisplayables;
      }, n.prototype.eachPendingDisplayable = function (t) {
        for (var e = this._cursor; e < this._displayables.length; e++) {
          t && t(this._displayables[e]);
        }
        for (var e = 0; e < this._temporaryDisplayables.length; e++) {
          t && t(this._temporaryDisplayables[e]);
        }
      }, n.prototype.update = function () {
        this.updateTransform();
        for (var t = this._cursor; t < this._displayables.length; t++) {
          var e = this._displayables[t];
          e.parent = this, e.update(), e.parent = null;
        }
        for (var t = 0; t < this._temporaryDisplayables.length; t++) {
          var e = this._temporaryDisplayables[t];
          e.parent = this, e.update(), e.parent = null;
        }
      }, n.prototype.getBoundingRect = function () {
        if (!this._rect) {
          for (var t = new tg(1 / 0, 1 / 0, -1 / 0, -1 / 0), e = 0; e < this._displayables.length; e++) {
            var n = this._displayables[e],
              r = n.getBoundingRect().clone();
            n.needLocalTransform() && r.applyTransform(n.getLocalTransform(Ey)), t.union(r);
          }
          this._rect = t;
        }
        return this._rect;
      }, n.prototype.contain = function (t, e) {
        var n = this.transformCoordToLocal(t, e),
          r = this.getBoundingRect();
        if (r.contain(n[0], n[1])) for (var i = 0; i < this._displayables.length; i++) {
          var o = this._displayables[i];
          if (o.contain(t, e)) return !0;
        }
        return !1;
      }, n;
    }(zg),
    zy = new Rd(50),
    Fy = /\{([a-zA-Z0-9_]+)\|([^}]*)\}/g,
    Ny = function () {
      function t() {}
      return t;
    }(),
    Hy = function () {
      function t(t) {
        this.tokens = [], t && (this.tokens = t);
      }
      return t;
    }(),
    Vy = function () {
      function t() {
        this.width = 0, this.height = 0, this.contentWidth = 0, this.contentHeight = 0, this.outerWidth = 0, this.outerHeight = 0, this.lines = [];
      }
      return t;
    }(),
    Wy = m(",&?/;] ".split(""), function (t, e) {
      return t[e] = !0, t;
    }, {}),
    Gy = {
      fill: "#000"
    },
    Uy = 2,
    qy = {
      style: c({
        fill: !0,
        stroke: !0,
        fillOpacity: !0,
        strokeOpacity: !0,
        lineWidth: !0,
        fontSize: !0,
        lineHeight: !0,
        width: !0,
        height: !0,
        textShadowColor: !0,
        textShadowBlur: !0,
        textShadowOffsetX: !0,
        textShadowOffsetY: !0,
        backgroundColor: !0,
        padding: !0,
        borderColor: !0,
        borderWidth: !0,
        borderRadius: !0
      }, Eg.style)
    },
    Xy = function (t) {
      function n(e) {
        var n = t.call(this) || this;
        return n.type = "text", n._children = [], n._defaultStyle = Gy, n.attr(e), n;
      }
      return e(n, t), n.prototype.childrenRef = function () {
        return this._children;
      }, n.prototype.update = function () {
        this.styleChanged() && this._updateSubTexts();
        for (var e = 0; e < this._children.length; e++) {
          var n = this._children[e];
          n.zlevel = this.zlevel, n.z = this.z, n.z2 = this.z2, n.culling = this.culling, n.cursor = this.cursor, n.invisible = this.invisible;
        }
        var r = this.attachedTransform;
        if (r) {
          r.updateTransform();
          var i = r.transform;
          i ? (this.transform = this.transform || [], Ne(this.transform, i)) : this.transform = null;
        } else t.prototype.update.call(this);
      }, n.prototype.getComputedTransform = function () {
        return this.__hostTarget && (this.__hostTarget.getComputedTransform(), this.__hostTarget.updateInnerText(!0)), this.attachedTransform ? this.attachedTransform.getComputedTransform() : t.prototype.getComputedTransform.call(this);
      }, n.prototype._updateSubTexts = function () {
        this._childCursor = 0, Hi(this.style), this.style.rich ? this._updateRichTexts() : this._updatePlainTexts(), this._children.length = this._childCursor, this.styleUpdated();
      }, n.prototype.addSelfToZr = function (e) {
        t.prototype.addSelfToZr.call(this, e);
        for (var n = 0; n < this._children.length; n++) {
          this._children[n].__zr = e;
        }
      }, n.prototype.removeSelfFromZr = function (e) {
        t.prototype.removeSelfFromZr.call(this, e);
        for (var n = 0; n < this._children.length; n++) {
          this._children[n].__zr = null;
        }
      }, n.prototype.getBoundingRect = function () {
        if (this.styleChanged() && this._updateSubTexts(), !this._rect) {
          for (var t = new tg(0, 0, 0, 0), e = this._children, n = [], r = null, i = 0; i < e.length; i++) {
            var o = e[i],
              a = o.getBoundingRect(),
              s = o.getLocalTransform(n);
            s ? (t.copy(a), t.applyTransform(s), r = r || t.clone(), r.union(t)) : (r = r || a.clone(), r.union(a));
          }
          this._rect = r || t;
        }
        return this._rect;
      }, n.prototype.setDefaultTextStyle = function (t) {
        this._defaultStyle = t || Gy;
      }, n.prototype.setTextContent = function () {
        throw new Error("Can't attach text on another text");
      }, n.prototype._mergeStyle = function (t, e) {
        if (!e) return t;
        var n = e.rich,
          r = t.rich || n && {};
        return h(t, e), n && r ? (this._mergeRich(r, n), t.rich = r) : r && (t.rich = r), t;
      }, n.prototype._mergeRich = function (t, e) {
        for (var n = w(e), r = 0; r < n.length; r++) {
          var i = n[r];
          t[i] = t[i] || {}, h(t[i], e[i]);
        }
      }, n.prototype.getAnimationStyleProps = function () {
        return qy;
      }, n.prototype._getOrCreateChild = function (t) {
        var e = this._children[this._childCursor];
        return e && e instanceof t || (e = new t()), this._children[this._childCursor++] = e, e.__zr = this.__zr, e.parent = this, e;
      }, n.prototype._updatePlainTexts = function () {
        var t = this.style,
          e = t.font || ng,
          n = t.padding,
          r = qi(t),
          i = Oi(r, t),
          o = Xi(t),
          a = !!t.backgroundColor,
          s = i.outerHeight,
          l = i.lines,
          u = i.lineHeight,
          h = this._defaultStyle,
          c = t.x || 0,
          f = t.y || 0,
          p = t.align || h.align || "left",
          d = t.verticalAlign || h.verticalAlign || "top",
          g = c,
          v = En(f, i.contentHeight, d);
        if (o || n) {
          var y = i.width;
          n && (y += n[1] + n[3]);
          var m = Rn(c, y, p),
            _ = En(f, s, d);
          o && this._renderBackground(t, t, m, _, y, s);
        }
        v += u / 2, n && (g = Ui(c, p, n), "top" === d ? v += n[0] : "bottom" === d && (v -= n[2]));
        for (var x = 0, w = !1, b = Gi(("fill" in t) ? t.fill : (w = !0, h.fill)), S = Wi(("stroke" in t) ? t.stroke : a || h.autoStroke && !w ? null : (x = Uy, h.stroke)), M = t.textShadowBlur > 0, T = null != t.width && ("truncate" === t.overflow || "break" === t.overflow || "breakAll" === t.overflow), C = i.calculatedLineHeight, k = 0; k < l.length; k++) {
          var D = this._getOrCreateChild(py),
            I = D.createStyle();
          D.useStyle(I), I.text = l[k], I.x = g, I.y = v, p && (I.textAlign = p), I.textBaseline = "middle", I.opacity = t.opacity, I.strokeFirst = !0, M && (I.shadowBlur = t.textShadowBlur || 0, I.shadowColor = t.textShadowColor || "transparent", I.shadowOffsetX = t.textShadowOffsetX || 0, I.shadowOffsetY = t.textShadowOffsetY || 0), S && (I.stroke = S, I.lineWidth = t.lineWidth || x, I.lineDash = t.lineDash, I.lineDashOffset = t.lineDashOffset || 0), b && (I.fill = b), I.font = e, v += u, T && D.setBoundingRect(new tg(Rn(I.x, t.width, I.textAlign), En(I.y, C, I.textBaseline), t.width, C));
        }
      }, n.prototype._updateRichTexts = function () {
        var t = this.style,
          e = qi(t),
          n = Ri(e, t),
          r = n.width,
          i = n.outerWidth,
          o = n.outerHeight,
          a = t.padding,
          s = t.x || 0,
          l = t.y || 0,
          u = this._defaultStyle,
          h = t.align || u.align,
          c = t.verticalAlign || u.verticalAlign,
          f = Rn(s, i, h),
          p = En(l, o, c),
          d = f,
          g = p;
        a && (d += a[3], g += a[0]);
        var v = d + r;
        Xi(t) && this._renderBackground(t, t, f, p, i, o);
        for (var y = !!t.backgroundColor, m = 0; m < n.lines.length; m++) {
          for (var _ = n.lines[m], x = _.tokens, w = x.length, b = _.lineHeight, S = _.width, M = 0, T = d, C = v, k = w - 1, D = void 0; w > M && (D = x[M], !D.align || "left" === D.align);) {
            this._placeToken(D, t, b, g, T, "left", y), S -= D.width, T += D.width, M++;
          }
          for (; k >= 0 && (D = x[k], "right" === D.align);) {
            this._placeToken(D, t, b, g, C, "right", y), S -= D.width, C -= D.width, k--;
          }
          for (T += (r - (T - d) - (v - C) - S) / 2; k >= M;) {
            D = x[M], this._placeToken(D, t, b, g, T + D.width / 2, "center", y), T += D.width, M++;
          }
          g += b;
        }
      }, n.prototype._placeToken = function (t, e, n, r, i, o, a) {
        var s = e.rich[t.styleName] || {};
        s.text = t.text;
        var l = t.verticalAlign,
          u = r + n / 2;
        "top" === l ? u = r + t.height / 2 : "bottom" === l && (u = r + n - t.height / 2);
        var h = !t.isLineHolder && Xi(s);
        h && this._renderBackground(s, e, "right" === o ? i - t.width : "center" === o ? i - t.width / 2 : i, u - t.height / 2, t.width, t.height);
        var c = !!s.backgroundColor,
          f = t.textPadding;
        f && (i = Ui(i, o, f), u -= t.height / 2 - f[0] - t.innerHeight / 2);
        var p = this._getOrCreateChild(py),
          d = p.createStyle();
        p.useStyle(d);
        var g = this._defaultStyle,
          v = !1,
          y = 0,
          m = Wi("fill" in s ? s.fill : "fill" in e ? e.fill : (v = !0, g.fill)),
          _ = Wi("stroke" in s ? s.stroke : "stroke" in e ? e.stroke : c || a || g.autoStroke && !v ? null : (y = Uy, g.stroke)),
          x = s.textShadowBlur > 0 || e.textShadowBlur > 0;
        d.text = t.text, d.x = i, d.y = u, x && (d.shadowBlur = s.textShadowBlur || e.textShadowBlur || 0, d.shadowColor = s.textShadowColor || e.textShadowColor || "transparent", d.shadowOffsetX = s.textShadowOffsetX || e.textShadowOffsetX || 0, d.shadowOffsetY = s.textShadowOffsetY || e.textShadowOffsetY || 0), d.textAlign = o, d.textBaseline = "middle", d.font = t.font || ng, d.opacity = N(s.opacity, e.opacity, 1), _ && (d.lineWidth = N(s.lineWidth, e.lineWidth, y), d.lineDash = F(s.lineDash, e.lineDash), d.lineDashOffset = e.lineDashOffset || 0, d.stroke = _), m && (d.fill = m);
        var w = t.contentWidth,
          b = t.contentHeight;
        p.setBoundingRect(new tg(Rn(d.x, w, d.textAlign), En(d.y, b, d.textBaseline), w, b));
      }, n.prototype._renderBackground = function (t, e, n, r, i, o) {
        var a,
          s,
          l = t.backgroundColor,
          u = t.borderWidth,
          h = t.borderColor,
          c = C(l),
          f = t.borderRadius,
          p = this;
        if (c || u && h) {
          a = this._getOrCreateChild(ty), a.useStyle(a.createStyle()), a.style.fill = null;
          var d = a.shape;
          d.x = n, d.y = r, d.width = i, d.height = o, d.r = f, a.dirtyShape();
        }
        if (c) {
          var g = a.style;
          g.fill = l || null, g.fillOpacity = F(t.fillOpacity, 1);
        } else if (l && l.image) {
          s = this._getOrCreateChild(jv), s.onload = function () {
            p.dirtyStyle();
          };
          var v = s.style;
          v.image = l.image, v.x = n, v.y = r, v.width = i, v.height = o;
        }
        if (u && h) {
          var g = a.style;
          g.lineWidth = u, g.stroke = h, g.strokeOpacity = F(t.strokeOpacity, 1), g.lineDash = t.borderDash, g.lineDashOffset = t.borderDashOffset || 0, a.strokeContainThreshold = 0, a.hasFill() && a.hasStroke() && (g.strokeFirst = !0, g.lineWidth *= 2);
        }
        var y = (a || s).style;
        y.shadowBlur = t.shadowBlur || 0, y.shadowColor = t.shadowColor || "transparent", y.shadowOffsetX = t.shadowOffsetX || 0, y.shadowOffsetY = t.shadowOffsetY || 0, y.opacity = N(t.opacity, e.opacity, 1);
      }, n.makeFont = function (t) {
        var e = "";
        if (t.fontSize || t.fontFamily || t.fontWeight) {
          var n = "";
          n = "string" != typeof t.fontSize || -1 === t.fontSize.indexOf("px") && -1 === t.fontSize.indexOf("rem") && -1 === t.fontSize.indexOf("em") ? isNaN(+t.fontSize) ? "12px" : t.fontSize + "px" : t.fontSize, e = [t.fontStyle, t.fontWeight, n, t.fontFamily || "sans-serif"].join(" ");
        }
        return e && G(e) || t.textFont || t.font;
      }, n;
    }(zg),
    Yy = {
      left: !0,
      right: 1,
      center: 1
    },
    jy = {
      top: 1,
      bottom: 1,
      middle: 1
    },
    Zy = function () {
      function t() {
        this.cx = 0, this.cy = 0, this.r = 0, this.startAngle = 0, this.endAngle = 2 * Math.PI, this.clockwise = !0;
      }
      return t;
    }(),
    Ky = function (t) {
      function n(e) {
        return t.call(this, e) || this;
      }
      return e(n, t), n.prototype.getDefaultStyle = function () {
        return {
          stroke: "#000",
          fill: null
        };
      }, n.prototype.getDefaultShape = function () {
        return new Zy();
      }, n.prototype.buildPath = function (t, e) {
        var n = e.cx,
          r = e.cy,
          i = Math.max(e.r, 0),
          o = e.startAngle,
          a = e.endAngle,
          s = e.clockwise,
          l = Math.cos(o),
          u = Math.sin(o);
        t.moveTo(l * i + n, u * i + r), t.arc(n, r, i, o, a, !s);
      }, n;
    }(Ov);
  Ky.prototype.type = "arc";
  var $y = [],
    Qy = function () {
      function t() {
        this.x1 = 0, this.y1 = 0, this.x2 = 0, this.y2 = 0, this.cpx1 = 0, this.cpy1 = 0, this.percent = 1;
      }
      return t;
    }(),
    Jy = function (t) {
      function n(e) {
        return t.call(this, e) || this;
      }
      return e(n, t), n.prototype.getDefaultStyle = function () {
        return {
          stroke: "#000",
          fill: null
        };
      }, n.prototype.getDefaultShape = function () {
        return new Qy();
      }, n.prototype.buildPath = function (t, e) {
        var n = e.x1,
          r = e.y1,
          i = e.x2,
          o = e.y2,
          a = e.cpx1,
          s = e.cpy1,
          l = e.cpx2,
          u = e.cpy2,
          h = e.percent;
        0 !== h && (t.moveTo(n, r), null == l || null == u ? (1 > h && (Sr(n, a, i, h, $y), a = $y[1], i = $y[2], Sr(r, s, o, h, $y), s = $y[1], o = $y[2]), t.quadraticCurveTo(a, s, i, o)) : (1 > h && (vr(n, a, l, i, h, $y), a = $y[1], l = $y[2], i = $y[3], vr(r, s, u, o, h, $y), s = $y[1], u = $y[2], o = $y[3]), t.bezierCurveTo(a, s, l, u, i, o)));
      }, n.prototype.pointAt = function (t) {
        return Yi(this.shape, t, !1);
      }, n.prototype.tangentAt = function (t) {
        var e = Yi(this.shape, t, !0);
        return he(e, e);
      }, n;
    }(Ov);
  Jy.prototype.type = "bezier-curve";
  var tm = function () {
      function t() {
        this.cx = 0, this.cy = 0, this.width = 0, this.height = 0;
      }
      return t;
    }(),
    em = function (t) {
      function n(e) {
        return t.call(this, e) || this;
      }
      return e(n, t), n.prototype.getDefaultShape = function () {
        return new tm();
      }, n.prototype.buildPath = function (t, e) {
        var n = e.cx,
          r = e.cy,
          i = e.width,
          o = e.height;
        t.moveTo(n, r + i), t.bezierCurveTo(n + i, r + i, n + 3 * i / 2, r - i / 3, n, r - o), t.bezierCurveTo(n - 3 * i / 2, r - i / 3, n - i, r + i, n, r + i), t.closePath();
      }, n;
    }(Ov);
  em.prototype.type = "droplet";
  var nm = function () {
      function t() {
        this.cx = 0, this.cy = 0, this.width = 0, this.height = 0;
      }
      return t;
    }(),
    rm = function (t) {
      function n(e) {
        return t.call(this, e) || this;
      }
      return e(n, t), n.prototype.getDefaultShape = function () {
        return new nm();
      }, n.prototype.buildPath = function (t, e) {
        var n = e.cx,
          r = e.cy,
          i = e.width,
          o = e.height;
        t.moveTo(n, r), t.bezierCurveTo(n + i / 2, r - 2 * o / 3, n + 2 * i, r + o / 3, n, r + o), t.bezierCurveTo(n - 2 * i, r + o / 3, n - i / 2, r - 2 * o / 3, n, r);
      }, n;
    }(Ov);
  rm.prototype.type = "heart";
  var im = Math.PI,
    om = Math.sin,
    am = Math.cos,
    sm = function () {
      function t() {
        this.x = 0, this.y = 0, this.r = 0, this.n = 0;
      }
      return t;
    }(),
    lm = function (t) {
      function n(e) {
        return t.call(this, e) || this;
      }
      return e(n, t), n.prototype.getDefaultShape = function () {
        return new sm();
      }, n.prototype.buildPath = function (t, e) {
        var n = e.n;
        if (n && !(2 > n)) {
          var r = e.x,
            i = e.y,
            o = e.r,
            a = 2 * im / n,
            s = -im / 2;
          t.moveTo(r + o * am(s), i + o * om(s));
          for (var l = 0, u = n - 1; u > l; l++) {
            s += a, t.lineTo(r + o * am(s), i + o * om(s));
          }
          t.closePath();
        }
      }, n;
    }(Ov);
  lm.prototype.type = "isogon";
  var um = function () {
      function t() {
        this.cx = 0, this.cy = 0, this.r = 0, this.r0 = 0;
      }
      return t;
    }(),
    hm = function (t) {
      function n(e) {
        return t.call(this, e) || this;
      }
      return e(n, t), n.prototype.getDefaultShape = function () {
        return new um();
      }, n.prototype.buildPath = function (t, e) {
        var n = e.cx,
          r = e.cy,
          i = 2 * Math.PI;
        t.moveTo(n + e.r, r), t.arc(n, r, e.r, 0, i, !1), t.moveTo(n + e.r0, r), t.arc(n, r, e.r0, 0, i, !0);
      }, n;
    }(Ov);
  hm.prototype.type = "ring";
  var cm = Math.sin,
    fm = Math.cos,
    pm = Math.PI / 180,
    dm = function () {
      function t() {
        this.cx = 0, this.cy = 0, this.r = [], this.k = 0, this.n = 1;
      }
      return t;
    }(),
    gm = function (t) {
      function n(e) {
        return t.call(this, e) || this;
      }
      return e(n, t), n.prototype.getDefaultStyle = function () {
        return {
          stroke: "#000",
          fill: null
        };
      }, n.prototype.getDefaultShape = function () {
        return new dm();
      }, n.prototype.buildPath = function (t, e) {
        var n,
          r,
          i,
          o = e.r,
          a = e.k,
          s = e.n,
          l = e.cx,
          u = e.cy;
        t.moveTo(l, u);
        for (var h = 0, c = o.length; c > h; h++) {
          i = o[h];
          for (var f = 0; 360 * s >= f; f++) {
            n = i * cm(a / s * f % 360 * pm) * fm(f * pm) + l, r = i * cm(a / s * f % 360 * pm) * cm(f * pm) + u, t.lineTo(n, r);
          }
        }
      }, n;
    }(Ov);
  gm.prototype.type = "rose";
  var vm = Math.PI,
    ym = Math.cos,
    mm = Math.sin,
    _m = function () {
      function t() {
        this.cx = 0, this.cy = 0, this.n = 3, this.r = 0;
      }
      return t;
    }(),
    xm = function (t) {
      function n(e) {
        return t.call(this, e) || this;
      }
      return e(n, t), n.prototype.getDefaultShape = function () {
        return new _m();
      }, n.prototype.buildPath = function (t, e) {
        var n = e.n;
        if (n && !(2 > n)) {
          var r = e.cx,
            i = e.cy,
            o = e.r,
            a = e.r0;
          null == a && (a = n > 4 ? o * ym(2 * vm / n) / ym(vm / n) : o / 3);
          var s = vm / n,
            l = -vm / 2,
            u = r + o * ym(l),
            h = i + o * mm(l);
          l += s, t.moveTo(u, h);
          for (var c = 0, f = 2 * n - 1, p = void 0; f > c; c++) {
            p = c % 2 === 0 ? a : o, t.lineTo(r + p * ym(l), i + p * mm(l)), l += s;
          }
          t.closePath();
        }
      }, n;
    }(Ov);
  xm.prototype.type = "star";
  var wm = Math.cos,
    bm = Math.sin,
    Sm = function () {
      function t() {
        this.cx = 0, this.cy = 0, this.r = 0, this.r0 = 0, this.d = 0, this.location = "out";
      }
      return t;
    }(),
    Mm = function (t) {
      function n(e) {
        return t.call(this, e) || this;
      }
      return e(n, t), n.prototype.getDefaultStyle = function () {
        return {
          stroke: "#000",
          fill: null
        };
      }, n.prototype.getDefaultShape = function () {
        return new Sm();
      }, n.prototype.buildPath = function (t, e) {
        var n,
          r,
          i,
          o,
          a = e.r,
          s = e.r0,
          l = e.d,
          u = e.cx,
          h = e.cy,
          c = "out" === e.location ? 1 : -1;
        if (!(e.location && s >= a)) {
          var f,
            p = 0,
            d = 1;
          n = (a + c * s) * wm(0) - c * l * wm(0) + u, r = (a + c * s) * bm(0) - l * bm(0) + h, t.moveTo(n, r);
          do {
            p++;
          } while (s * p % (a + c * s) !== 0);
          do {
            f = Math.PI / 180 * d, i = (a + c * s) * wm(f) - c * l * wm((a / s + c) * f) + u, o = (a + c * s) * bm(f) - l * bm((a / s + c) * f) + h, t.lineTo(i, o), d++;
          } while (s * p / (a + c * s) * 360 >= d);
        }
      }, n;
    }(Ov);
  Mm.prototype.type = "trochoid";
  var Tm = function (t) {
      function n(e, n, r, i, o) {
        var a = t.call(this, i) || this;
        return a.x = null == e ? .5 : e, a.y = null == n ? .5 : n, a.r = null == r ? .5 : r, a.type = "radial", a.global = o || !1, a;
      }
      return e(n, t), n;
    }(hy),
    Cm = [0, 0],
    km = [0, 0],
    Dm = new qd(),
    Im = new qd(),
    Am = function () {
      function t(t, e) {
        this._corners = [], this._axes = [], this._origin = [0, 0];
        for (var n = 0; 4 > n; n++) {
          this._corners[n] = new qd();
        }
        for (var n = 0; 2 > n; n++) {
          this._axes[n] = new qd();
        }
        t && this.fromBoundingRect(t, e);
      }
      return t.prototype.fromBoundingRect = function (t, e) {
        var n = this._corners,
          r = this._axes,
          i = t.x,
          o = t.y,
          a = i + t.width,
          s = o + t.height;
        if (n[0].set(i, o), n[1].set(a, o), n[2].set(a, s), n[3].set(i, s), e) for (var l = 0; 4 > l; l++) {
          n[l].transform(e);
        }
        qd.sub(r[0], n[1], n[0]), qd.sub(r[1], n[3], n[0]), r[0].normalize(), r[1].normalize();
        for (var l = 0; 2 > l; l++) {
          this._origin[l] = r[l].dot(n[0]);
        }
      }, t.prototype.intersect = function (t, e) {
        var n = !0,
          r = !e;
        return Dm.set(1 / 0, 1 / 0), Im.set(0, 0), !this._intersectCheckOneSide(this, t, Dm, Im, r, 1) && (n = !1, r) ? n : !this._intersectCheckOneSide(t, this, Dm, Im, r, -1) && (n = !1, r) ? n : (r || qd.copy(e, n ? Dm : Im), n);
      }, t.prototype._intersectCheckOneSide = function (t, e, n, r, i, o) {
        for (var a = !0, s = 0; 2 > s; s++) {
          var l = this._axes[s];
          if (this._getProjMinMaxOnAxis(s, t._corners, Cm), this._getProjMinMaxOnAxis(s, e._corners, km), Cm[1] < km[0] || Cm[0] > km[1]) {
            if (a = !1, i) return a;
            var u = Math.abs(km[0] - Cm[1]),
              h = Math.abs(Cm[0] - km[1]);
            Math.min(u, h) > r.len() && (h > u ? qd.scale(r, l, -u * o) : qd.scale(r, l, h * o));
          } else if (n) {
            var u = Math.abs(km[0] - Cm[1]),
              h = Math.abs(Cm[0] - km[1]);
            Math.min(u, h) < n.len() && (h > u ? qd.scale(n, l, u * o) : qd.scale(n, l, -h * o));
          }
        }
        return a;
      }, t.prototype._getProjMinMaxOnAxis = function (t, e, n) {
        for (var r = this._axes[t], i = this._origin, o = e[0].dot(r) + i[t], a = o, s = o, l = 1; l < e.length; l++) {
          var u = e[l].dot(r) + i[t];
          a = Math.min(u, a), s = Math.max(u, s);
        }
        n[0] = a, n[1] = s;
      }, t;
    }(),
    Pm = (function () {
      function t(t) {
        var e = this.dom = document.createElement("div");
        e.className = "ec-debug-dirty-rect", t = Object.assign({}, t), Object.assign(t, {
          backgroundColor: "rgba(0, 0, 255, 0.2)",
          border: "1px solid #00f"
        }), e.style.cssText = "\nposition: absolute;\nopacity: 0;\ntransition: opacity 0.5s linear;\npointer-events: none;\n";
        for (var n in t) {
          t.hasOwnProperty(n) && (e.style[n] = t[n]);
        }
      }
      return t.prototype.update = function (t) {
        var e = this.dom.style;
        e.width = t.width + "px", e.height = t.height + "px", e.left = t.x + "px", e.top = t.y + "px";
      }, t.prototype.hide = function () {
        this.dom.style.opacity = "0";
      }, t.prototype.show = function (t) {
        var e = this;
        clearTimeout(this._hideTimeout), this.dom.style.opacity = "1", this._hideTimeout = setTimeout(function () {
          e.hide();
        }, t || 1e3);
      }, t;
    }(), !Ep.canvasSupported),
    Lm = {},
    Om = {},
    Rm = function () {
      function t(t, e, n) {
        var r = this;
        this._sleepAfterStill = 10, this._stillFrameAccum = 0, this._needsRefresh = !0, this._needsRefreshHover = !0, this._darkMode = !1, n = n || {}, this.dom = e, this.id = t;
        var i = new xg(),
          o = n.renderer;
        if (Pm) {
          if (!Lm.vml) throw new Error("You need to require 'zrender/vml/vml' to support IE8");
          o = "vml";
        } else o || (o = "canvas");
        if (!Lm[o]) throw new Error("Renderer '" + o + "' is not imported. Please import it first.");
        n.useDirtyRect = null == n.useDirtyRect ? !1 : n.useDirtyRect;
        var a = new Lm[o](e, i, n, t);
        this.storage = i, this.painter = a;
        var s = Ep.node || Ep.worker ? null : new Lg(a.getViewportRoot(), a.root);
        this.handler = new _d(i, a, s, a.root), this.animation = new bg({
          stage: {
            update: function update() {
              return r._flush(!0);
            }
          }
        }), this.animation.start();
      }
      return t.prototype.add = function (t) {
        t && (this.storage.addRoot(t), t.addSelfToZr(this), this.refresh());
      }, t.prototype.remove = function (t) {
        t && (this.storage.delRoot(t), t.removeSelfFromZr(this), this.refresh());
      }, t.prototype.configLayer = function (t, e) {
        this.painter.configLayer && this.painter.configLayer(t, e), this.refresh();
      }, t.prototype.setBackgroundColor = function (t) {
        this.painter.setBackgroundColor && this.painter.setBackgroundColor(t), this.refresh(), this._backgroundColor = t, this._darkMode = Zi(t);
      }, t.prototype.getBackgroundColor = function () {
        return this._backgroundColor;
      }, t.prototype.setDarkMode = function (t) {
        this._darkMode = t;
      }, t.prototype.isDarkMode = function () {
        return this._darkMode;
      }, t.prototype.refreshImmediately = function (t) {
        t || this.animation.update(!0), this._needsRefresh = !1, this.painter.refresh(), this._needsRefresh = !1;
      }, t.prototype.refresh = function () {
        this._needsRefresh = !0, this.animation.start();
      }, t.prototype.flush = function () {
        this._flush(!1);
      }, t.prototype._flush = function (t) {
        var e,
          n = new Date().getTime();
        this._needsRefresh && (e = !0, this.refreshImmediately(t)), this._needsRefreshHover && (e = !0, this.refreshHoverImmediately());
        var r = new Date().getTime();
        e ? (this._stillFrameAccum = 0, this.trigger("rendered", {
          elapsedTime: r - n
        })) : this._sleepAfterStill > 0 && (this._stillFrameAccum++, this._stillFrameAccum > this._sleepAfterStill && this.animation.stop());
      }, t.prototype.setSleepAfterStill = function (t) {
        this._sleepAfterStill = t;
      }, t.prototype.wakeUp = function () {
        this.animation.start(), this._stillFrameAccum = 0;
      }, t.prototype.addHover = function () {}, t.prototype.removeHover = function () {}, t.prototype.clearHover = function () {}, t.prototype.refreshHover = function () {
        this._needsRefreshHover = !0;
      }, t.prototype.refreshHoverImmediately = function () {
        this._needsRefreshHover = !1, this.painter.refreshHover && "canvas" === this.painter.getType() && this.painter.refreshHover();
      }, t.prototype.resize = function (t) {
        t = t || {}, this.painter.resize(t.width, t.height), this.handler.resize();
      }, t.prototype.clearAnimation = function () {
        this.animation.clear();
      }, t.prototype.getWidth = function () {
        return this.painter.getWidth();
      }, t.prototype.getHeight = function () {
        return this.painter.getHeight();
      }, t.prototype.pathToImage = function (t, e) {
        return this.painter.pathToImage ? this.painter.pathToImage(t, e) : void 0;
      }, t.prototype.setCursorStyle = function (t) {
        this.handler.setCursorStyle(t);
      }, t.prototype.findHover = function (t, e) {
        return this.handler.findHover(t, e);
      }, t.prototype.on = function (t, e, n) {
        return this.handler.on(t, e, n), this;
      }, t.prototype.off = function (t, e) {
        this.handler.off(t, e);
      }, t.prototype.trigger = function (t, e) {
        this.handler.trigger(t, e);
      }, t.prototype.clear = function () {
        for (var t = this.storage.getRoots(), e = 0; e < t.length; e++) {
          t[e] instanceof qv && t[e].removeSelfFromZr(this);
        }
        this.storage.delAllRoots(), this.painter.clear();
      }, t.prototype.dispose = function () {
        this.animation.stop(), this.clear(), this.storage.dispose(), this.painter.dispose(), this.handler.dispose(), this.animation = this.storage = this.painter = this.handler = null, ji(this.id);
      }, t;
    }(),
    Em = "5.0.1",
    Bm = (Object.freeze || Object)({
      init: Ki,
      dispose: $i,
      disposeAll: Qi,
      getInstance: Ji,
      registerPainter: to,
      version: Em
    }),
    zm = 1e-4,
    Fm = 9007199254740991,
    Nm = /^(?:(\d{4})(?:[-\/](\d{1,2})(?:[-\/](\d{1,2})(?:[T ](\d{1,2})(?::(\d{1,2})(?::(\d{1,2})(?:[.,](\d+))?)?)?(Z|[\+\-]\d\d:?\d\d)?)?)?)?)?$/,
    Hm = (Object.freeze || Object)({
      linearMap: no,
      parsePercent: ro,
      round: io,
      asc: oo,
      getPrecision: ao,
      getPrecisionSafe: so,
      getPixelPrecision: lo,
      getPercentWithPrecision: uo,
      MAX_SAFE_INTEGER: Fm,
      remRadian: ho,
      isRadianAroundZero: co,
      parseDate: fo,
      quantity: po,
      quantityExponent: go,
      nice: vo,
      quantile: yo,
      reformIntervals: mo,
      numericToNumber: _o,
      isNumeric: xo,
      getRandomIdBase: wo,
      getGreatestCommonDividor: bo,
      getLeastCommonMultiple: So
    }),
    Vm = "series\x00",
    Wm = "\x00_ec_\x00",
    Gm = ["fontStyle", "fontWeight", "fontSize", "fontFamily", "rich", "tag", "color", "textBorderColor", "textBorderWidth", "width", "height", "lineHeight", "align", "verticalAlign", "baseline", "shadowColor", "shadowBlur", "shadowOffsetX", "shadowOffsetY", "textShadowColor", "textShadowBlur", "textShadowOffsetX", "textShadowOffsetY", "backgroundColor", "borderColor", "borderWidth", "borderRadius", "padding"],
    Um = wo(),
    qm = {
      useDefault: !0,
      enableAll: !1,
      enableNone: !1
    },
    Xm = ".",
    Ym = "___EC__COMPONENT__CONTAINER___",
    jm = "___EC__EXTENDED_CLASS___",
    Zm = Math.round(10 * Math.random()),
    Km = [["fill", "color"], ["shadowBlur"], ["shadowOffsetX"], ["shadowOffsetY"], ["opacity"], ["shadowColor"]],
    $m = aa(Km),
    Qm = function () {
      function t() {}
      return t.prototype.getAreaStyle = function (t, e) {
        return $m(this, t, e);
      }, t;
    }(),
    Jm = Uo(),
    t_ = 1,
    e_ = {},
    n_ = Uo(),
    r_ = 0,
    i_ = 1,
    o_ = 2,
    a_ = ["emphasis", "blur", "select"],
    s_ = ["normal", "emphasis", "blur", "select"],
    l_ = 10,
    u_ = 9,
    h_ = "highlight",
    c_ = "downplay",
    f_ = "select",
    p_ = "unselect",
    d_ = "toggleSelect",
    g_ = new Rd(100),
    v_ = ["emphasis", "blur", "select"],
    y_ = {
      itemStyle: "getItemStyle",
      lineStyle: "getLineStyle",
      areaStyle: "getAreaStyle"
    },
    m_ = Math.max,
    __ = Math.min,
    x_ = {},
    w_ = ni,
    b_ = ri,
    S_ = li;
  Ka("circle", Kv), Ka("ellipse", ny), Ka("sector", Oy), Ka("ring", hm), Ka("polygon", sy), Ka("polyline", uy), Ka("rect", ty), Ka("line", oy), Ka("bezierCurve", Jy), Ka("arc", Ky);
  var M_ = (Object.freeze || Object)({
      extendShape: ja,
      extendPath: Za,
      registerShape: Ka,
      getShapeClass: $a,
      makePath: Qa,
      makeImage: Ja,
      mergePath: b_,
      resizePath: es,
      subPixelOptimizeLine: ns,
      subPixelOptimizeRect: rs,
      subPixelOptimize: S_,
      updateProps: os,
      initProps: as,
      removeElement: ss,
      removeElementWithFadeOut: us,
      isElementRemoved: hs,
      getTransform: cs,
      applyTransform: fs,
      transformDirection: ps,
      groupTransition: vs,
      clipPointsByRect: ys,
      clipRectByRect: ms,
      createIcon: _s,
      linePolygonIntersect: xs,
      lineLineIntersect: ws,
      Group: qv,
      Image: jv,
      Text: Xy,
      Circle: Kv,
      Ellipse: ny,
      Sector: Oy,
      Ring: hm,
      Polygon: sy,
      Polyline: uy,
      Rect: ty,
      Line: oy,
      BezierCurve: Jy,
      Arc: Ky,
      IncrementalDisplayable: By,
      CompoundPath: Ry,
      LinearGradient: cy,
      RadialGradient: Tm,
      BoundingRect: tg,
      OrientedBoundingRect: Am,
      Point: qd,
      Path: Ov
    }),
    T_ = {},
    C_ = ["fontStyle", "fontWeight", "fontSize", "fontFamily", "textShadowColor", "textShadowBlur", "textShadowOffsetX", "textShadowOffsetY"],
    k_ = ["align", "lineHeight", "width", "height", "tag", "verticalAlign"],
    D_ = ["padding", "borderWidth", "borderRadius", "borderDashOffset", "backgroundColor", "borderColor", "shadowColor", "shadowBlur", "shadowOffsetX", "shadowOffsetY"],
    I_ = Uo(),
    A_ = ["textStyle", "color"],
    P_ = new Xy(),
    L_ = function () {
      function t() {}
      return t.prototype.getTextColor = function (t) {
        var e = this.ecModel;
        return this.getShallow("color") || (!t && e ? e.get(A_) : null);
      }, t.prototype.getFont = function () {
        return As({
          fontStyle: this.getShallow("fontStyle"),
          fontWeight: this.getShallow("fontWeight"),
          fontSize: this.getShallow("fontSize"),
          fontFamily: this.getShallow("fontFamily")
        }, this.ecModel);
      }, t.prototype.getTextRect = function (t) {
        return P_.useStyle({
          text: t,
          fontStyle: this.getShallow("fontStyle"),
          fontWeight: this.getShallow("fontWeight"),
          fontSize: this.getShallow("fontSize"),
          fontFamily: this.getShallow("fontFamily"),
          verticalAlign: this.getShallow("verticalAlign") || this.getShallow("baseline"),
          padding: this.getShallow("padding"),
          lineHeight: this.getShallow("lineHeight"),
          rich: this.getShallow("rich")
        }), P_.update(), P_.getBoundingRect();
      }, t;
    }(),
    O_ = [["lineWidth", "width"], ["stroke", "color"], ["opacity"], ["shadowBlur"], ["shadowOffsetX"], ["shadowOffsetY"], ["shadowColor"], ["lineDash", "type"], ["lineDashOffset", "dashOffset"], ["lineCap", "cap"], ["lineJoin", "join"], ["miterLimit"]],
    R_ = aa(O_),
    E_ = function () {
      function t() {}
      return t.prototype.getLineStyle = function (t) {
        return R_(this, t);
      }, t;
    }(),
    B_ = [["fill", "color"], ["stroke", "borderColor"], ["lineWidth", "borderWidth"], ["opacity"], ["shadowBlur"], ["shadowOffsetX"], ["shadowOffsetY"], ["shadowColor"], ["lineDash", "borderType"], ["lineDashOffset", "borderDashOffset"], ["lineCap", "borderCap"], ["lineJoin", "borderJoin"], ["miterLimit", "borderMiterLimit"]],
    z_ = aa(B_),
    F_ = function () {
      function t() {}
      return t.prototype.getItemStyle = function (t, e) {
        return z_(this, t, e);
      }, t;
    }(),
    N_ = function () {
      function t(t, e, n) {
        this.parentModel = e, this.ecModel = n, this.option = t;
      }
      return t.prototype.init = function () {
        for (var t = [], e = 3; e < arguments.length; e++) {
          t[e - 3] = arguments[e];
        }
      }, t.prototype.mergeOption = function (t) {
        l(this.option, t, !0);
      }, t.prototype.get = function (t, e) {
        return null == t ? this.option : this._doGet(this.parsePath(t), !e && this.parentModel);
      }, t.prototype.getShallow = function (t, e) {
        var n = this.option,
          r = null == n ? n : n[t];
        if (null == r && !e) {
          var i = this.parentModel;
          i && (r = i.getShallow(t));
        }
        return r;
      }, t.prototype.getModel = function (e, n) {
        var r = null != e,
          i = r ? this.parsePath(e) : null,
          o = r ? this._doGet(i) : this.option;
        return n = n || this.parentModel && this.parentModel.getModel(this.resolveParentPath(i)), new t(o, n, this.ecModel);
      }, t.prototype.isEmpty = function () {
        return null == this.option;
      }, t.prototype.restoreData = function () {}, t.prototype.clone = function () {
        var t = this.constructor;
        return new t(s(this.option));
      }, t.prototype.parsePath = function (t) {
        return "string" == typeof t ? t.split(".") : t;
      }, t.prototype.resolveParentPath = function (t) {
        return t;
      }, t.prototype.isAnimationEnabled = function () {
        if (!Ep.node && this.option) {
          if (null != this.option.animation) return !!this.option.animation;
          if (this.parentModel) return this.parentModel.isAnimationEnabled();
        }
      }, t.prototype._doGet = function (t, e) {
        var n = this.option;
        if (!t) return n;
        for (var r = 0; r < t.length && (!t[r] || (n = n && "object" == _typeof(n) ? n[t[r]] : null, null != n)); r++) {
          ;
        }
        return null == n && e && (n = e._doGet(this.resolveParentPath(t), e.parentModel)), n;
      }, t;
    }();
  Jo(N_), na(N_), d(N_, E_), d(N_, F_), d(N_, Qm), d(N_, L_);
  var H_ = Math.round(10 * Math.random()),
    V_ = {
      time: {
        month: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        monthAbbr: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        dayOfWeek: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
        dayOfWeekAbbr: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
      },
      legend: {
        selector: {
          all: "All",
          inverse: "Inv"
        }
      },
      toolbox: {
        brush: {
          title: {
            rect: "Box Select",
            polygon: "Lasso Select",
            lineX: "Horizontally Select",
            lineY: "Vertically Select",
            keep: "Keep Selections",
            clear: "Clear Selections"
          }
        },
        dataView: {
          title: "Data View",
          lang: ["Data View", "Close", "Refresh"]
        },
        dataZoom: {
          title: {
            zoom: "Zoom",
            back: "Zoom Reset"
          }
        },
        magicType: {
          title: {
            line: "Switch to Line Chart",
            bar: "Switch to Bar Chart",
            stack: "Stack",
            tiled: "Tile"
          }
        },
        restore: {
          title: "Restore"
        },
        saveAsImage: {
          title: "Save as Image",
          lang: ["Right Click to Save Image"]
        }
      },
      series: {
        typeNames: {
          pie: "Pie chart",
          bar: "Bar chart",
          line: "Line chart",
          scatter: "Scatter plot",
          effectScatter: "Ripple scatter plot",
          radar: "Radar chart",
          tree: "Tree",
          treemap: "Treemap",
          boxplot: "Boxplot",
          candlestick: "Candlestick",
          k: "K line chart",
          heatmap: "Heat map",
          map: "Map",
          parallel: "Parallel coordinate map",
          lines: "Line graph",
          graph: "Relationship graph",
          sankey: "Sankey diagram",
          funnel: "Funnel chart",
          gauge: "Guage",
          pictorialBar: "Pictorial bar",
          themeRiver: "Theme River Map",
          sunburst: "Sunburst"
        }
      },
      aria: {
        general: {
          withTitle: 'This is a chart about "{title}"',
          withoutTitle: "This is a chart"
        },
        series: {
          single: {
            prefix: "",
            withName: " with type {seriesType} named {seriesName}.",
            withoutName: " with type {seriesType}."
          },
          multiple: {
            prefix: ". It consists of {seriesCount} series count.",
            withName: " The {seriesId} series is a {seriesType} representing {seriesName}.",
            withoutName: " The {seriesId} series is a {seriesType}.",
            separator: {
              middle: "",
              end: ""
            }
          }
        },
        data: {
          allData: "The data is as follows: ",
          partialData: "The first {displayCnt} items are: ",
          withName: "the data for {name} is {value}",
          withoutName: "{value}",
          separator: {
            middle: ", ",
            end: ". "
          }
        }
      }
    },
    W_ = {
      time: {
        month: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
        monthAbbr: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
        dayOfWeek: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"],
        dayOfWeekAbbr: ["日", "一", "二", "三", "四", "五", "六"]
      },
      legend: {
        selector: {
          all: "全选",
          inverse: "反选"
        }
      },
      toolbox: {
        brush: {
          title: {
            rect: "矩形选择",
            polygon: "圈选",
            lineX: "横向选择",
            lineY: "纵向选择",
            keep: "保持选择",
            clear: "清除选择"
          }
        },
        dataView: {
          title: "数据视图",
          lang: ["数据视图", "关闭", "刷新"]
        },
        dataZoom: {
          title: {
            zoom: "区域缩放",
            back: "区域缩放还原"
          }
        },
        magicType: {
          title: {
            line: "切换为折线图",
            bar: "切换为柱状图",
            stack: "切换为堆叠",
            tiled: "切换为平铺"
          }
        },
        restore: {
          title: "还原"
        },
        saveAsImage: {
          title: "保存为图片",
          lang: ["右键另存为图片"]
        }
      },
      series: {
        typeNames: {
          pie: "饼图",
          bar: "柱状图",
          line: "折线图",
          scatter: "散点图",
          effectScatter: "涟漪散点图",
          radar: "雷达图",
          tree: "树图",
          treemap: "矩形树图",
          boxplot: "箱型图",
          candlestick: "K线图",
          k: "K线图",
          heatmap: "热力图",
          map: "地图",
          parallel: "平行坐标图",
          lines: "线图",
          graph: "关系图",
          sankey: "桑基图",
          funnel: "漏斗图",
          gauge: "仪表盘图",
          pictorialBar: "象形柱图",
          themeRiver: "主题河流图",
          sunburst: "旭日图"
        }
      },
      aria: {
        general: {
          withTitle: "这是一个关于“{title}”的图表。",
          withoutTitle: "这是一个图表，"
        },
        series: {
          single: {
            prefix: "",
            withName: "图表类型是{seriesType}，表示{seriesName}。",
            withoutName: "图表类型是{seriesType}。"
          },
          multiple: {
            prefix: "它由{seriesCount}个图表系列组成。",
            withName: "第{seriesId}个系列是一个表示{seriesName}的{seriesType}，",
            withoutName: "第{seriesId}个系列是一个{seriesType}，",
            separator: {
              middle: "；",
              end: "。"
            }
          }
        },
        data: {
          allData: "其数据是——",
          partialData: "其中，前{displayCnt}项是——",
          withName: "{name}的数据是{value}",
          withoutName: "{value}",
          separator: {
            middle: "，",
            end: ""
          }
        }
      }
    },
    G_ = "ZH",
    U_ = "EN",
    q_ = U_,
    X_ = {},
    Y_ = {},
    j_ = Ep.domSupported ? function () {
      var t = (document.documentElement.lang || navigator.language || navigator.browserLanguage).toUpperCase();
      return t.indexOf(G_) > -1 ? G_ : q_;
    }() : q_;
  Bs(U_, V_), Bs(G_, W_);
  var Z_ = 1e3,
    K_ = 60 * Z_,
    $_ = 60 * K_,
    Q_ = 24 * $_,
    J_ = 365 * Q_,
    tx = {
      year: "{yyyy}",
      month: "{MMM}",
      day: "{d}",
      hour: "{HH}:{mm}",
      minute: "{HH}:{mm}",
      second: "{HH}:{mm}:{ss}",
      millisecond: "{hh}:{mm}:{ss} {SSS}",
      none: "{yyyy}-{MM}-{dd} {hh}:{mm}:{ss} {SSS}"
    },
    ex = "{yyyy}-{MM}-{dd}",
    nx = {
      year: "{yyyy}",
      month: "{yyyy}-{MM}",
      day: ex,
      hour: ex + " " + tx.hour,
      minute: ex + " " + tx.minute,
      second: ex + " " + tx.second,
      millisecond: tx.none
    },
    rx = ["year", "month", "day", "hour", "minute", "second", "millisecond"],
    ix = ["year", "half-year", "quarter", "month", "week", "half-week", "day", "half-day", "quarter-day", "hour", "minute", "second", "millisecond"],
    ox = V,
    ax = /([&<>"'])/g,
    sx = {
      "&": "&amp;",
      "<": "&lt;",
      ">": "&gt;",
      '"': "&quot;",
      "'": "&#39;"
    },
    lx = ["a", "b", "c", "d", "e", "f", "g"],
    ux = function ux(t, e) {
      return "{" + t + (null == e ? "" : e) + "}";
    },
    hx = (Object.freeze || Object)({
      addCommas: ul,
      toCamelCase: hl,
      normalizeCssArray: ox,
      encodeHTML: cl,
      makeValueReadable: fl,
      formatTpl: pl,
      formatTplSimple: dl,
      getTooltipMarker: gl,
      formatTime: vl,
      capitalFirst: yl,
      convertToColorString: ml,
      windowOpen: _l,
      truncateText: Ii,
      getTextRect: ll
    }),
    cx = v,
    fx = ["left", "right", "top", "bottom", "width", "height"],
    px = [["width", "left", "right"], ["height", "top", "bottom"]],
    dx = (S(xl, "vertical"), S(xl, "horizontal"), Uo()),
    gx = function (t) {
      function n(e, n, r) {
        var i = t.call(this, e, n, r) || this;
        return i.uid = Os("ec_cpt_model"), i;
      }
      return e(n, t), n.prototype.init = function (t, e, n) {
        this.mergeDefaultAndTheme(t, n);
      }, n.prototype.mergeDefaultAndTheme = function (t, e) {
        var n = bl(this),
          r = n ? Ml(t) : {},
          i = e.getTheme();
        l(t, i.get(this.mainType)), l(t, this.getDefaultOption()), n && Sl(t, r, n);
      }, n.prototype.mergeOption = function (t) {
        l(this.option, t, !0);
        var e = bl(this);
        e && Sl(this.option, t, e);
      }, n.prototype.optionUpdated = function () {}, n.prototype.getDefaultOption = function () {
        var t = this.constructor;
        if (!Qo(t)) return t.defaultOption;
        var e = dx(this);
        if (!e.defaultOption) {
          for (var n = [], r = t; r;) {
            var i = r.prototype.defaultOption;
            i && n.push(i), r = r.superClass;
          }
          for (var o = {}, a = n.length - 1; a >= 0; a--) {
            o = l(o, n[a], !0);
          }
          e.defaultOption = o;
        }
        return e.defaultOption;
      }, n.prototype.getReferringComponents = function (t, e) {
        var n = t + "Index",
          r = t + "Id";
        return Xo(this.ecModel, t, {
          index: this.get(n, !0),
          id: this.get(r, !0)
        }, e);
      }, n.prototype.getBoxLayoutParams = function () {
        var t = this;
        return {
          left: t.get("left"),
          top: t.get("top"),
          right: t.get("right"),
          bottom: t.get("bottom"),
          width: t.get("width"),
          height: t.get("height")
        };
      }, n.protoInitialize = function () {
        var t = n.prototype;
        t.type = "component", t.id = "", t.name = "", t.mainType = "", t.subType = "", t.componentIndex = 0;
      }(), n;
    }(N_);
  ea(gx, N_), oa(gx, {
    registerWhenExtend: !0
  }), Rs(gx), Es(gx, Cl);
  var vx = "";
  "undefined" != typeof navigator && (vx = navigator.platform || "");
  var yx,
    mx,
    _x = "rgba(0, 0, 0, 0.2)",
    xx = {
      darkMode: "auto",
      color: ["#5470c6", "#91cc75", "#fac858", "#ee6666", "#73c0de", "#3ba272", "#fc8452", "#9a60b4", "#ea7ccc"],
      gradientColor: ["#f6efa6", "#d88273", "#bf444c"],
      aria: {
        decal: {
          decals: [{
            color: _x,
            dashArrayX: [1, 0],
            dashArrayY: [2, 5],
            symbolSize: 1,
            rotation: Math.PI / 6
          }, {
            color: _x,
            symbol: "circle",
            dashArrayX: [[8, 8], [0, 8, 8, 0]],
            dashArrayY: [6, 0],
            symbolSize: .8
          }, {
            color: _x,
            dashArrayX: [1, 0],
            dashArrayY: [4, 3],
            dashLineOffset: 0,
            rotation: -Math.PI / 4
          }, {
            color: _x,
            dashArrayX: [[6, 6], [0, 6, 6, 0]],
            dashArrayY: [6, 0]
          }, {
            color: _x,
            dashArrayX: [[1, 0], [1, 6]],
            dashArrayY: [1, 0, 6, 0],
            rotation: Math.PI / 4
          }, {
            color: _x,
            symbol: "triangle",
            dashArrayX: [[9, 9], [0, 9, 9, 0]],
            dashArrayY: [7, 2],
            symbolSize: .75
          }]
        }
      },
      textStyle: {
        fontFamily: vx.match(/^Win/) ? "Microsoft YaHei" : "sans-serif",
        fontSize: 12,
        fontStyle: "normal",
        fontWeight: "normal"
      },
      blendMode: null,
      stateAnimation: {
        duration: 300,
        easing: "cubicOut"
      },
      animation: "auto",
      animationDuration: 1e3,
      animationDurationUpdate: 500,
      animationEasing: "cubicInOut",
      animationEasingUpdate: "cubicInOut",
      animationThreshold: 2e3,
      progressiveThreshold: 3e3,
      progressive: 400,
      hoverLayerThreshold: 3e3,
      useUTC: !1
    },
    bx = X(["tooltip", "label", "itemName", "itemId", "seriesName"]),
    Sx = "original",
    Mx = "arrayRows",
    Tx = "objectRows",
    Cx = "keyedColumns",
    kx = "typedArray",
    Dx = "unknown",
    Ix = "column",
    Ax = "row",
    Px = {
      Must: 1,
      Might: 2,
      Not: 3
    },
    Lx = Uo(),
    Ox = X(),
    Rx = Uo(),
    Ex = (Uo(), function () {
      function t() {}
      return t.prototype.getColorFromPalette = function (t, e, n) {
        var r = To(this.get("color", !0)),
          i = this.get("colorLayer", !0);
        return Bl(this, Rx, r, i, t, e, n);
      }, t.prototype.clearColorPalette = function () {
        zl(this, Rx);
      }, t;
    }()),
    Bx = "\x00_ec_inner",
    zx = function (t) {
      function n() {
        return null !== t && t.apply(this, arguments) || this;
      }
      return e(n, t), n.prototype.init = function (t, e, n, r, i, o) {
        r = r || {}, this.option = null, this._theme = new N_(r), this._locale = new N_(i), this._optionManager = o;
      }, n.prototype.setOption = function (t, e, n) {
        W(!(Bx in t), "please use chart.getOption()");
        var r = Wl(e);
        this._optionManager.setOption(t, n, r), this._resetOption(null, r);
      }, n.prototype.resetOption = function (t, e) {
        return this._resetOption(t, Wl(e));
      }, n.prototype._resetOption = function (t, e) {
        var n = !1,
          r = this._optionManager;
        if (!t || "recreate" === t) {
          var i = r.mountOption("recreate" === t);
          this.option && "recreate" !== t ? (this.restoreData(), this._mergeOption(i, e)) : mx(this, i), n = !0;
        }
        if (("timeline" === t || "media" === t) && this.restoreData(), !t || "recreate" === t || "timeline" === t) {
          var o = r.getTimelineOption(this);
          o && (n = !0, this._mergeOption(o, e));
        }
        if (!t || "recreate" === t || "media" === t) {
          var a = r.getMediaOption(this);
          a.length && v(a, function (t) {
            n = !0, this._mergeOption(t, e);
          }, this);
        }
        return n;
      }, n.prototype.mergeOption = function (t) {
        this._mergeOption(t, null);
      }, n.prototype._mergeOption = function (t, e) {
        function n(e) {
          var n = Rl(this, e, To(t[e])),
            a = i.get(e),
            s = a ? c && c.get(e) ? "replaceMerge" : "normalMerge" : "replaceAll",
            l = Io(a, n, s);
          Vo(l, e, gx), r[e] = null, i.set(e, null), o.set(e, 0);
          var u = [],
            f = [],
            p = 0;
          v(l, function (t, n) {
            var r = t.existing,
              i = t.newOption;
            if (i) {
              var o = gx.getClass(e, t.keyInfo.subType, !0);
              if (r && r.constructor === o) r.name = t.keyInfo.name, r.mergeOption(i, this), r.optionUpdated(i, !1);else {
                var a = h({
                  componentIndex: n
                }, t.keyInfo);
                r = new o(i, this, this, a), h(r, a), t.brandNew && (r.__requireNewView = !0), r.init(i, this, this), r.optionUpdated(null, !0);
              }
            } else r && (r.mergeOption({}, this), r.optionUpdated({}, !1));
            r ? (u.push(r.option), f.push(r), p++) : (u.push(void 0), f.push(void 0));
          }, this), r[e] = u, i.set(e, f), o.set(e, p), "series" === e && yx(this);
        }
        var r = this.option,
          i = this._componentsMap,
          o = this._componentsCount,
          a = [],
          u = X(),
          c = e && e.replaceMergeMainTypeMap;
        kl(this), v(t, function (t, e) {
          null != t && (gx.hasClass(e) ? e && (a.push(e), u.set(e, !0)) : r[e] = null == r[e] ? s(t) : l(r[e], t, !0));
        }), c && c.each(function (t, e) {
          gx.hasClass(e) && !u.get(e) && (a.push(e), u.set(e, !0));
        }), gx.topologicalTravel(a, gx.getAllClassMainTypes(), n, this), this._seriesIndices || yx(this);
      }, n.prototype.getOption = function () {
        var t = s(this.option);
        return v(t, function (e, n) {
          if (gx.hasClass(n)) {
            for (var r = To(e), i = r.length, o = !1, a = i - 1; a >= 0; a--) {
              r[a] && !Ho(r[a]) ? o = !0 : (r[a] = null, !o && i--);
            }
            r.length = i, t[n] = r;
          }
        }), delete t[Bx], t;
      }, n.prototype.getTheme = function () {
        return this._theme;
      }, n.prototype.getLocaleModel = function () {
        return this._locale;
      }, n.prototype.getLocale = function (t) {
        var e = this.getLocaleModel();
        return e.get(t);
      }, n.prototype.setUpdatePayload = function (t) {
        this._payload = t;
      }, n.prototype.getUpdatePayload = function () {
        return this._payload;
      }, n.prototype.getComponent = function (t, e) {
        var n = this._componentsMap.get(t);
        if (n) {
          var r = n[e || 0];
          if (r) return r;
          if (null == e) for (var i = 0; i < n.length; i++) {
            if (n[i]) return n[i];
          }
        }
      }, n.prototype.queryComponents = function (t) {
        var e = t.mainType;
        if (!e) return [];
        var n = t.index,
          r = t.id,
          i = t.name,
          o = this._componentsMap.get(e);
        if (!o || !o.length) return [];
        var a;
        return null != n ? (a = [], v(To(n), function (t) {
          o[t] && a.push(o[t]);
        })) : a = null != r ? Hl("id", r, o) : null != i ? Hl("name", i, o) : _(o, function (t) {
          return !!t;
        }), Vl(a, t);
      }, n.prototype.findComponents = function (t) {
        function e(t) {
          var e = i + "Index",
            n = i + "Id",
            r = i + "Name";
          return !t || null == t[e] && null == t[n] && null == t[r] ? null : {
            mainType: i,
            index: t[e],
            id: t[n],
            name: t[r]
          };
        }
        function n(e) {
          return t.filter ? _(e, t.filter) : e;
        }
        var r = t.query,
          i = t.mainType,
          o = e(r),
          a = o ? this.queryComponents(o) : _(this._componentsMap.get(i), function (t) {
            return !!t;
          });
        return n(Vl(a, t));
      }, n.prototype.eachComponent = function (t, e, n) {
        var r = this._componentsMap;
        if (T(t)) {
          var i = e,
            o = t;
          r.each(function (t, e) {
            for (var n = 0; t && n < t.length; n++) {
              var r = t[n];
              r && o.call(i, e, r, r.componentIndex);
            }
          });
        } else for (var a = C(t) ? r.get(t) : I(t) ? this.findComponents(t) : null, s = 0; a && s < a.length; s++) {
          var l = a[s];
          l && e.call(n, l, l.componentIndex);
        }
      }, n.prototype.getSeriesByName = function (t) {
        var e = Fo(t, null);
        return _(this._componentsMap.get("series"), function (t) {
          return !!t && null != e && t.name === e;
        });
      }, n.prototype.getSeriesByIndex = function (t) {
        return this._componentsMap.get("series")[t];
      }, n.prototype.getSeriesByType = function (t) {
        return _(this._componentsMap.get("series"), function (e) {
          return !!e && e.subType === t;
        });
      }, n.prototype.getSeries = function () {
        return _(this._componentsMap.get("series").slice(), function (t) {
          return !!t;
        });
      }, n.prototype.getSeriesCount = function () {
        return this._componentsCount.get("series");
      }, n.prototype.eachSeries = function (t, e) {
        v(this._seriesIndices, function (n) {
          var r = this._componentsMap.get("series")[n];
          t.call(e, r, n);
        }, this);
      }, n.prototype.eachRawSeries = function (t, e) {
        v(this._componentsMap.get("series"), function (n) {
          n && t.call(e, n, n.componentIndex);
        });
      }, n.prototype.eachSeriesByType = function (t, e, n) {
        v(this._seriesIndices, function (r) {
          var i = this._componentsMap.get("series")[r];
          i.subType === t && e.call(n, i, r);
        }, this);
      }, n.prototype.eachRawSeriesByType = function (t, e, n) {
        return v(this.getSeriesByType(t), e, n);
      }, n.prototype.isSeriesFiltered = function (t) {
        return null == this._seriesIndicesMap.get(t.componentIndex);
      }, n.prototype.getCurrentSeriesIndices = function () {
        return (this._seriesIndices || []).slice();
      }, n.prototype.filterSeries = function (t, e) {
        var n = [];
        v(this._seriesIndices, function (r) {
          var i = this._componentsMap.get("series")[r];
          t.call(e, i, r) && n.push(r);
        }, this), this._seriesIndices = n, this._seriesIndicesMap = X(n);
      }, n.prototype.restoreData = function (t) {
        yx(this);
        var e = this._componentsMap,
          n = [];
        e.each(function (t, e) {
          gx.hasClass(e) && n.push(e);
        }), gx.topologicalTravel(n, gx.getAllClassMainTypes(), function (n) {
          v(e.get(n), function (e) {
            !e || "series" === n && Fl(e, t) || e.restoreData();
          });
        });
      }, n.internalField = function () {
        yx = function yx(t) {
          var e = t._seriesIndices = [];
          v(t._componentsMap.get("series"), function (t) {
            t && e.push(t.componentIndex);
          }), t._seriesIndicesMap = X(e);
        }, mx = function mx(t, e) {
          t.option = {}, t.option[Bx] = 1, t._componentsMap = X({
            series: []
          }), t._componentsCount = X();
          var n = e.aria;
          I(n) && null == n.enabled && (n.enabled = !0), Nl(e, t._theme.option), l(e, xx, !1), t._mergeOption(e, null);
        };
      }(), n;
    }(N_);
  d(zx, Ex);
  var Fx,
    Nx,
    Hx,
    Vx,
    Wx,
    Gx = ["getDom", "getZr", "getWidth", "getHeight", "getDevicePixelRatio", "dispatchAction", "isDisposed", "on", "off", "getDataURL", "getConnectedDataURL", "getOption", "getId", "updateLabelLayout"],
    Ux = function () {
      function t(t) {
        v(Gx, function (e) {
          this[e] = Zp(t[e], t);
        }, this);
      }
      return t;
    }(),
    qx = {},
    Xx = function () {
      function t() {
        this._coordinateSystems = [];
      }
      return t.prototype.create = function (t, e) {
        var n = [];
        v(qx, function (r) {
          var i = r.create(t, e);
          n = n.concat(i || []);
        }), this._coordinateSystems = n;
      }, t.prototype.update = function (t, e) {
        v(this._coordinateSystems, function (n) {
          n.update && n.update(t, e);
        });
      }, t.prototype.getCoordinateSystems = function () {
        return this._coordinateSystems.slice();
      }, t.register = function (t, e) {
        qx[t] = e;
      }, t.get = function (t) {
        return qx[t];
      }, t;
    }(),
    Yx = /^(min|max)?(.+)$/,
    jx = function () {
      function t(t) {
        this._timelineOptions = [], this._mediaList = [], this._currentMediaIndices = [], this._api = t;
      }
      return t.prototype.setOption = function (t, e) {
        t && (v(To(t.series), function (t) {
          t && t.data && P(t.data) && U(t.data);
        }), v(To(t.dataset), function (t) {
          t && t.source && P(t.source) && U(t.source);
        })), t = s(t);
        var n = this._optionBackup,
          r = Gl(t, e, !n);
        this._newBaseOption = r.baseOption, n ? (r.timelineOptions.length && (n.timelineOptions = r.timelineOptions), r.mediaList.length && (n.mediaList = r.mediaList), r.mediaDefault && (n.mediaDefault = r.mediaDefault)) : this._optionBackup = r;
      }, t.prototype.mountOption = function (t) {
        var e = this._optionBackup;
        return this._timelineOptions = e.timelineOptions, this._mediaList = e.mediaList, this._mediaDefault = e.mediaDefault, this._currentMediaIndices = [], s(t ? e.baseOption : this._newBaseOption);
      }, t.prototype.getTimelineOption = function (t) {
        var e,
          n = this._timelineOptions;
        if (n.length) {
          var r = t.getComponent("timeline");
          r && (e = s(n[r.getCurrentIndex()]));
        }
        return e;
      }, t.prototype.getMediaOption = function () {
        var t = this._api.getWidth(),
          e = this._api.getHeight(),
          n = this._mediaList,
          r = this._mediaDefault,
          i = [],
          o = [];
        if (!n.length && !r) return o;
        for (var a = 0, l = n.length; l > a; a++) {
          Ul(n[a].query, t, e) && i.push(a);
        }
        return !i.length && r && (i = [-1]), i.length && !Xl(i, this._currentMediaIndices) && (o = y(i, function (t) {
          return s(-1 === t ? r.option : n[t].option);
        })), this._currentMediaIndices = i, o;
      }, t;
    }(),
    Zx = v,
    Kx = I,
    $x = ["areaStyle", "lineStyle", "nodeStyle", "linkStyle", "chordStyle", "label", "labelLine"],
    Qx = [["x", "left"], ["y", "top"], ["x2", "right"], ["y2", "bottom"]],
    Jx = ["grid", "geo", "parallel", "legend", "toolbox", "title", "visualMap", "dataZoom", "timeline"],
    tw = [["borderRadius", "barBorderRadius"], ["borderColor", "barBorderColor"], ["borderWidth", "barBorderWidth"]],
    ew = function () {
      function t(t) {
        this.data = t.data || (t.sourceFormat === Cx ? {} : []), this.sourceFormat = t.sourceFormat || Dx, this.seriesLayoutBy = t.seriesLayoutBy || Ix, this.startIndex = t.startIndex || 0, this.dimensionsDefine = t.dimensionsDefine, this.dimensionsDetectedCount = t.dimensionsDetectedCount, this.encodeDefine = t.encodeDefine, this.metaRawOption = t.metaRawOption;
      }
      return t;
    }(),
    nw = function () {
      function t(t, e) {
        var n = pu(t) ? t : gu(t);
        this._source = n;
        var r = this._data = n.data;
        n.sourceFormat === kx && (this._offset = 0, this._dimSize = e, this._data = r), Wx(this, r, n);
      }
      return t.prototype.getSource = function () {
        return this._source;
      }, t.prototype.count = function () {
        return 0;
      }, t.prototype.getItem = function () {}, t.prototype.appendData = function () {}, t.prototype.clean = function () {}, t.protoInitialize = function () {
        var e = t.prototype;
        e.pure = !1, e.persistent = !0;
      }(), t.internalField = function () {
        function t(t) {
          for (var e = 0; e < t.length; e++) {
            this._data.push(t[e]);
          }
        }
        var e;
        Wx = function Wx(t, e, o) {
          var a = o.sourceFormat,
            s = o.seriesLayoutBy,
            l = o.startIndex,
            u = o.dimensionsDefine,
            c = Vx[Cu(a, s)];
          if (h(t, c), a === kx) t.getItem = n, t.count = i, t.fillStorage = r;else {
            var f = Su(a, s);
            t.getItem = Zp(f, null, e, l, u);
            var p = Mu(a, s);
            t.count = Zp(p, null, e, l, u);
          }
        };
        var n = function n(t, e) {
            t -= this._offset, e = e || [];
            for (var n = this._data, r = this._dimSize, i = r * t, o = 0; r > o; o++) {
              e[o] = n[i + o];
            }
            return e;
          },
          r = function r(t, e, n, _r2) {
            for (var i = this._data, o = this._dimSize, a = 0; o > a; a++) {
              for (var s = _r2[a], l = null == s[0] ? 1 / 0 : s[0], u = null == s[1] ? -1 / 0 : s[1], h = e - t, c = n[a], f = 0; h > f; f++) {
                var p = i[(t + f) * o + a];
                c[t + f] = p, l > p && (l = p), p > u && (u = p);
              }
              s[0] = l, s[1] = u;
            }
          },
          i = function i() {
            return this._data ? this._data.length / this._dimSize : 0;
          };
        e = {}, e[Mx + "_" + Ix] = {
          pure: !0,
          appendData: t
        }, e[Mx + "_" + Ax] = {
          pure: !0,
          appendData: function appendData() {
            throw new Error('Do not support appendData when set seriesLayoutBy: "row".');
          }
        }, e[Tx] = {
          pure: !0,
          appendData: t
        }, e[Cx] = {
          pure: !0,
          appendData: function appendData(t) {
            var e = this._data;
            v(t, function (t, n) {
              for (var r = e[n] || (e[n] = []), i = 0; i < (t || []).length; i++) {
                r.push(t[i]);
              }
            });
          }
        }, e[Sx] = {
          appendData: t
        }, e[kx] = {
          persistent: !1,
          pure: !0,
          appendData: function appendData(t) {
            this._data = t;
          },
          clean: function clean() {
            this._offset += this.count(), this._data = null;
          }
        }, Vx = e;
      }(), t;
    }(),
    rw = function rw(t, e, n, r) {
      return t[r];
    },
    iw = (Fx = {}, Fx[Mx + "_" + Ix] = function (t, e, n, r) {
      return t[r + e];
    }, Fx[Mx + "_" + Ax] = function (t, e, n, r) {
      r += e;
      for (var i = [], o = t, a = 0; a < o.length; a++) {
        var s = o[a];
        i.push(s ? s[r] : null);
      }
      return i;
    }, Fx[Tx] = rw, Fx[Cx] = function (t, e, n, r) {
      for (var i = [], o = 0; o < n.length; o++) {
        var a = n[o].name,
          s = t[a];
        i.push(s ? s[r] : null);
      }
      return i;
    }, Fx[Sx] = rw, Fx),
    ow = function ow(t) {
      return t.length;
    },
    aw = (Nx = {}, Nx[Mx + "_" + Ix] = function (t, e) {
      return Math.max(0, t.length - e);
    }, Nx[Mx + "_" + Ax] = function (t, e) {
      var n = t[0];
      return n ? Math.max(0, n.length - e) : 0;
    }, Nx[Tx] = ow, Nx[Cx] = function (t, e, n) {
      var r = n[0].name,
        i = t[r];
      return i ? i.length : 0;
    }, Nx[Sx] = ow, Nx),
    sw = function sw(t, e) {
      return null != e ? t[e] : t;
    },
    lw = (Hx = {}, Hx[Mx] = sw, Hx[Tx] = function (t, e, n) {
      return null != e ? t[n] : t;
    }, Hx[Cx] = sw, Hx[Sx] = function (t, e) {
      var n = ko(t);
      return null != e && n instanceof Array ? n[e] : n;
    }, Hx[kx] = sw, Hx),
    uw = /\{@(.+?)\}/g,
    hw = function () {
      function t() {}
      return t.prototype.getDataParams = function (t, e) {
        var n = this.getData(e),
          r = this.getRawValue(t, e),
          i = n.getRawIndex(t),
          o = n.getName(t),
          a = n.getRawDataItem(t),
          s = n.getItemVisual(t, "style"),
          l = s && s[n.getItemVisual(t, "drawType") || "fill"],
          u = s && s.stroke,
          h = this.mainType,
          c = "series" === h,
          f = n.userOutput;
        return {
          componentType: h,
          componentSubType: this.subType,
          componentIndex: this.componentIndex,
          seriesType: c ? this.subType : null,
          seriesIndex: this.seriesIndex,
          seriesId: c ? this.id : null,
          seriesName: c ? this.name : null,
          name: o,
          dataIndex: i,
          data: a,
          dataType: e,
          value: r,
          color: l,
          borderColor: u,
          dimensionNames: f ? f.dimensionNames : null,
          encode: f ? f.encode : null,
          $vars: ["seriesName", "name", "value"]
        };
      }, t.prototype.getFormattedLabel = function (t, e, n, r, i, o) {
        e = e || "normal";
        var a = this.getData(n),
          s = this.getDataParams(t, n);
        if (o && h(s, o), null != r && s.value instanceof Array && (s.value = s.value[r]), !i) {
          var l = a.getItemModel(t);
          i = l.get("normal" === e ? ["label", "formatter"] : [e, "label", "formatter"]);
        }
        if ("function" == typeof i) return s.status = e, s.dimensionIndex = r, i(s);
        if ("string" == typeof i) {
          var u = pl(i, s);
          return u.replace(uw, function (e, n) {
            var r = n.length;
            return "[" === n.charAt(0) && "]" === n.charAt(r - 1) && (n = +n.slice(1, r - 1)), ku(a, t, n);
          });
        }
      }, t.prototype.getRawValue = function (t, e) {
        return ku(this.getData(e), t);
      }, t.prototype.formatTooltip = function () {}, t;
    }(),
    cw = function () {
      function t(t) {
        t = t || {}, this._reset = t.reset, this._plan = t.plan, this._count = t.count, this._onDirty = t.onDirty, this._dirty = !0;
      }
      return t.prototype.perform = function (t) {
        function e(t) {
          return !(t >= 1) && (t = 1), t;
        }
        var n = this._upstream,
          r = t && t.skip;
        if (this._dirty && n) {
          var i = this.context;
          i.data = i.outputData = n.context.outputData;
        }
        this.__pipeline && (this.__pipeline.currentTask = this);
        var o;
        this._plan && !r && (o = this._plan(this.context));
        var a = e(this._modBy),
          s = this._modDataCount || 0,
          l = e(t && t.modBy),
          u = t && t.modDataCount || 0;
        (a !== l || s !== u) && (o = "reset");
        var h;
        (this._dirty || "reset" === o) && (this._dirty = !1, h = this._doReset(r)), this._modBy = l, this._modDataCount = u;
        var c = t && t.step;
        if (this._dueEnd = n ? n._outputDueEnd : this._count ? this._count(this.context) : 1 / 0, this._progress) {
          var f = this._dueIndex,
            p = Math.min(null != c ? this._dueIndex + c : 1 / 0, this._dueEnd);
          if (!r && (h || p > f)) {
            var d = this._progress;
            if (M(d)) for (var g = 0; g < d.length; g++) {
              this._doProgress(d[g], f, p, l, u);
            } else this._doProgress(d, f, p, l, u);
          }
          this._dueIndex = p;
          var v = null != this._settedOutputEnd ? this._settedOutputEnd : p;
          this._outputDueEnd = v;
        } else this._dueIndex = this._outputDueEnd = null != this._settedOutputEnd ? this._settedOutputEnd : this._dueEnd;
        return this.unfinished();
      }, t.prototype.dirty = function () {
        this._dirty = !0, this._onDirty && this._onDirty(this.context);
      }, t.prototype._doProgress = function (t, e, n, r, i) {
        fw.reset(e, n, r, i), this._callingProgress = t, this._callingProgress({
          start: e,
          end: n,
          count: n - e,
          next: fw.next
        }, this.context);
      }, t.prototype._doReset = function (t) {
        this._dueIndex = this._outputDueEnd = this._dueEnd = 0, this._settedOutputEnd = null;
        var e, n;
        !t && this._reset && (e = this._reset(this.context), e && e.progress && (n = e.forceFirstProgress, e = e.progress), M(e) && !e.length && (e = null)), this._progress = e, this._modBy = this._modDataCount = null;
        var r = this._downstream;
        return r && r.dirty(), n;
      }, t.prototype.unfinished = function () {
        return this._progress && this._dueIndex < this._dueEnd;
      }, t.prototype.pipe = function (t) {
        (this._downstream !== t || this._dirty) && (this._downstream = t, t._upstream = this, t.dirty());
      }, t.prototype.dispose = function () {
        this._disposed || (this._upstream && (this._upstream._downstream = null), this._downstream && (this._downstream._upstream = null), this._dirty = !1, this._disposed = !0);
      }, t.prototype.getUpstream = function () {
        return this._upstream;
      }, t.prototype.getDownstream = function () {
        return this._downstream;
      }, t.prototype.setOutputEnd = function (t) {
        this._outputDueEnd = this._settedOutputEnd = t;
      }, t;
    }(),
    fw = function () {
      function t() {
        return n > r ? r++ : null;
      }
      function e() {
        var t = r % a * i + Math.ceil(r / a),
          e = r >= n ? null : o > t ? t : r;
        return r++, e;
      }
      var n,
        r,
        i,
        o,
        a,
        s = {
          reset: function reset(l, u, h, c) {
            r = l, n = u, i = h, o = c, a = Math.ceil(o / i), s.next = i > 1 && o > 0 ? e : t;
          }
        };
      return s;
    }(),
    pw = (X({
      number: function number(t) {
        return parseFloat(t);
      },
      time: function time(t) {
        return +fo(t);
      },
      trim: function trim(t) {
        return "string" == typeof t ? G(t) : t;
      }
    }), {
      lt: function lt(t, e) {
        return e > t;
      },
      lte: function lte(t, e) {
        return e >= t;
      },
      gt: function gt(t, e) {
        return t > e;
      },
      gte: function gte(t, e) {
        return t >= e;
      }
    }),
    dw = (function () {
      function t(t, e) {
        if ("number" != typeof e) {
          var n = "";
          Mo(n);
        }
        this._opFn = pw[t], this._rvalFloat = _o(e);
      }
      return t.prototype.evaluate = function (t) {
        return "number" == typeof t ? this._opFn(t, this._rvalFloat) : this._opFn(_o(t), this._rvalFloat);
      }, t;
    }(), function () {
      function t(t, e) {
        var n = "desc" === t;
        this._resultLT = n ? 1 : -1, null == e && (e = n ? "min" : "max"), this._incomparable = "min" === e ? -1 / 0 : 1 / 0;
      }
      return t.prototype.evaluate = function (t, e) {
        var n = _typeof(t),
          r = _typeof(e),
          i = "number" === n ? t : _o(t),
          o = "number" === r ? e : _o(e),
          a = isNaN(i),
          s = isNaN(o);
        if (a && (i = this._incomparable), s && (o = this._incomparable), a && s) {
          var l = "string" === n,
            u = "string" === r;
          l && (i = u ? t : 0), u && (o = l ? e : 0);
        }
        return o > i ? this._resultLT : i > o ? -this._resultLT : 0;
      }, t;
    }(), function () {
      function t(t, e) {
        this._rval = e, this._isEQ = t, this._rvalTypeof = _typeof(e), this._rvalFloat = _o(e);
      }
      return t.prototype.evaluate = function (t) {
        var e = t === this._rval;
        if (!e) {
          var n = _typeof(t);
          n === this._rvalTypeof || "number" !== n && "number" !== this._rvalTypeof || (e = _o(t) === this._rvalFloat);
        }
        return this._isEQ ? e : !e;
      }, t;
    }(), function () {
      function t() {}
      return t.prototype.getRawData = function () {
        throw new Error("not supported");
      }, t.prototype.getRawDataItem = function () {
        throw new Error("not supported");
      }, t.prototype.cloneRawData = function () {}, t.prototype.getDimensionInfo = function () {}, t.prototype.cloneAllDimensionInfo = function () {}, t.prototype.count = function () {}, t.prototype.retrieveValue = function () {}, t.prototype.retrieveValueFromItem = function () {}, t.prototype.convertValue = function (t, e) {
        return Iu(t, e);
      }, t;
    }()),
    gw = X(),
    vw = function () {
      function t(t) {
        this._sourceList = [], this._upstreamSignList = [], this._versionSignBase = 0, this._sourceHost = t;
      }
      return t.prototype.dirty = function () {
        this._setLocalSource([], []);
      }, t.prototype._setLocalSource = function (t, e) {
        this._sourceList = t, this._upstreamSignList = e, this._versionSignBase++, this._versionSignBase > 9e10 && (this._versionSignBase = 0);
      }, t.prototype._getVersionSign = function () {
        return this._sourceHost.uid + "_" + this._versionSignBase;
      }, t.prototype.prepareSource = function () {
        this._isDirty() && this._createSource();
      }, t.prototype._createSource = function () {
        this._setLocalSource([], []);
        var t,
          e,
          n = this._sourceHost,
          r = this._getUpstreamSourceManagers(),
          i = !!r.length;
        if (Nu(n)) {
          var o = n,
            a = void 0,
            s = void 0,
            l = void 0;
          if (i) {
            var u = r[0];
            u.prepareSource(), l = u.getSource(), a = l.data, s = l.sourceFormat, e = [u._getVersionSign()];
          } else a = o.get("data", !0), s = P(a) ? kx : Sx, e = [];
          var h = Dl(l, this._getSourceMetaRawOption());
          t = [du(a, h, s, o.get("encode", !0))];
        } else {
          var c = n;
          if (i) {
            var f = this._applyTransform(r);
            t = f.sourceList, e = f.upstreamSignList;
          } else {
            var p = c.get("source", !0);
            t = [du(p, this._getSourceMetaRawOption(), null, null)], e = [];
          }
        }
        this._setLocalSource(t, e);
      }, t.prototype._applyTransform = function (t) {
        var e = this._sourceHost,
          n = e.get("transform", !0),
          r = e.get("fromTransformResult", !0);
        if (null != r) {
          var i = "";
          1 !== t.length && Hu(i);
        }
        var o,
          a = [],
          s = [];
        return v(t, function (t) {
          t.prepareSource();
          var e = t.getSource(r || 0),
            n = "";
          null == r || e || Hu(n), a.push(e), s.push(t._getVersionSign());
        }), n ? o = Bu(n, a, {
          datasetIndex: e.componentIndex
        }) : null != r && (o = [vu(a[0])]), {
          sourceList: o,
          upstreamSignList: s
        };
      }, t.prototype._isDirty = function () {
        var t = this._sourceList;
        if (!t.length) return !0;
        for (var e = this._getUpstreamSourceManagers(), n = 0; n < e.length; n++) {
          var r = e[n];
          if (r._isDirty() || this._upstreamSignList[n] !== r._getVersionSign()) return !0;
        }
      }, t.prototype.getSource = function (t) {
        return this._sourceList[t || 0];
      }, t.prototype._getUpstreamSourceManagers = function () {
        var t = this._sourceHost;
        if (Nu(t)) {
          var e = Al(t);
          return e ? [e.getSourceManager()] : [];
        }
        return y(Pl(t), function (t) {
          return t.getSourceManager();
        });
      }, t.prototype._getSourceMetaRawOption = function () {
        var t,
          e,
          n,
          r = this._sourceHost;
        if (Nu(r)) t = r.get("seriesLayoutBy", !0), e = r.get("sourceHeader", !0), n = r.get("dimensions", !0);else if (!this._getUpstreamSourceManagers().length) {
          var i = r;
          t = i.get("seriesLayoutBy", !0), e = i.get("sourceHeader", !0), n = i.get("dimensions", !0);
        }
        return {
          seriesLayoutBy: t,
          sourceHeader: e,
          dimensions: n
        };
      }, t;
    }(),
    yw = (function () {
      function t() {
        this.richTextStyles = {}, this._nextStyleNameId = wo();
      }
      return t.prototype._generateStyleName = function () {
        return "__EC_aUTo_" + this._nextStyleNameId++;
      }, t.prototype.makeTooltipMarker = function (t, e, n) {
        var r = "richText" === n ? this._generateStyleName() : null,
          i = gl({
            color: e,
            type: t,
            renderMode: n,
            markerId: r
          });
        return C(i) ? i : (this.richTextStyles[r] = i.style, i.content);
      }, t.prototype.wrapRichTextStyle = function (t, e) {
        var n = {};
        M(e) ? v(e, function (t) {
          return h(n, t);
        }) : h(n, e);
        var r = this._generateStyleName();
        return this.richTextStyles[r] = n, "{" + r + "|" + t + "}";
      }, t;
    }(), Uo()),
    mw = function (t) {
      function n() {
        var e = null !== t && t.apply(this, arguments) || this;
        return e._selectedDataIndicesMap = {}, e;
      }
      return e(n, t), n.prototype.init = function (t, e, n) {
        this.seriesIndex = this.componentIndex, this.dataTask = Du({
          count: ju,
          reset: Zu
        }), this.dataTask.context = {
          model: this
        }, this.mergeDefaultAndTheme(t, n);
        var r = yw(this).sourceManager = new vw(this);
        r.prepareSource();
        var i = this.getInitialData(t, n);
        $u(i, this), this.dataTask.context.data = i, yw(this).dataBeforeProcessed = i, Xu(this), this._initSelectedMapFromData(i);
      }, n.prototype.mergeDefaultAndTheme = function (t, e) {
        var n = bl(this),
          r = n ? Ml(t) : {},
          i = this.subType;
        gx.hasClass(i) && (i += "Series"), l(t, e.getTheme().get(this.subType)), l(t, this.getDefaultOption()), Co(t, "label", ["show"]), this.fillDataTextStyle(t.data), n && Sl(t, r, n);
      }, n.prototype.mergeOption = function (t, e) {
        t = l(this.option, t, !0), this.fillDataTextStyle(t.data);
        var n = bl(this);
        n && Sl(this.option, t, n);
        var r = yw(this).sourceManager;
        r.dirty(), r.prepareSource();
        var i = this.getInitialData(t, e);
        $u(i, this), this.dataTask.dirty(), this.dataTask.context.data = i, yw(this).dataBeforeProcessed = i, Xu(this), this._initSelectedMapFromData(i);
      }, n.prototype.fillDataTextStyle = function (t) {
        if (t && !P(t)) for (var e = ["show"], n = 0; n < t.length; n++) {
          t[n] && t[n].label && Co(t[n], "label", e);
        }
      }, n.prototype.getInitialData = function () {}, n.prototype.appendData = function (t) {
        var e = this.getRawData();
        e.appendData(t.data);
      }, n.prototype.getData = function (t) {
        var e = Ju(this);
        if (e) {
          var n = e.context.data;
          return null == t ? n : n.getLinkedData(t);
        }
        return yw(this).data;
      }, n.prototype.getAllData = function () {
        var t = this.getData();
        return t && t.getLinkedDataAll ? t.getLinkedDataAll() : [{
          data: t
        }];
      }, n.prototype.setData = function (t) {
        var e = Ju(this);
        if (e) {
          var n = e.context;
          n.outputData = t, e !== this.dataTask && (n.data = t);
        }
        yw(this).data = t;
      }, n.prototype.getSource = function () {
        return yw(this).sourceManager.getSource();
      }, n.prototype.getRawData = function () {
        return yw(this).dataBeforeProcessed;
      }, n.prototype.getBaseAxis = function () {
        var t = this.coordinateSystem;
        return t && t.getBaseAxis && t.getBaseAxis();
      }, n.prototype.formatTooltip = function (t, e) {
        return Gu({
          series: this,
          dataIndex: t,
          multipleSeries: e
        });
      }, n.prototype.isAnimationEnabled = function () {
        if (Ep.node) return !1;
        var t = this.getShallow("animation");
        return t && this.getData().count() > this.getShallow("animationThreshold") && (t = !1), !!t;
      }, n.prototype.restoreData = function () {
        this.dataTask.dirty();
      }, n.prototype.getColorFromPalette = function (t, e, n) {
        var r = this.ecModel,
          i = Ex.prototype.getColorFromPalette.call(this, t, e, n);
        return i || (i = r.getColorFromPalette(t, e, n)), i;
      }, n.prototype.coordDimToDataDim = function (t) {
        return this.getRawData().mapDimensionsAll(t);
      }, n.prototype.getProgressive = function () {
        return this.get("progressive");
      }, n.prototype.getProgressiveThreshold = function () {
        return this.get("progressiveThreshold");
      }, n.prototype.select = function (t, e) {
        this._innerSelect(this.getData(e), t);
      }, n.prototype.unselect = function (t, e) {
        var n = this.option.selectedMap;
        if (n) for (var r = this.getData(e), i = 0; i < t.length; i++) {
          var o = t[i],
            a = qu(r, o);
          n[a] = !1, this._selectedDataIndicesMap[a] = -1;
        }
      }, n.prototype.toggleSelect = function (t, e) {
        for (var n = [], r = 0; r < t.length; r++) {
          n[0] = t[r], this.isSelected(t[r], e) ? this.unselect(n, e) : this.select(n, e);
        }
      }, n.prototype.getSelectedDataIndices = function () {
        for (var t = this._selectedDataIndicesMap, e = w(t), n = [], r = 0; r < e.length; r++) {
          var i = t[e[r]];
          i >= 0 && n.push(i);
        }
        return n;
      }, n.prototype.isSelected = function (t, e) {
        var n = this.option.selectedMap;
        if (!n) return !1;
        var r = this.getData(e),
          i = qu(r, t);
        return n[i] || !1;
      }, n.prototype._innerSelect = function (t, e) {
        var n,
          r,
          i = this.option.selectedMode,
          o = e.length;
        if (i && o) if ("multiple" === i) for (var a = this.option.selectedMap || (this.option.selectedMap = {}), s = 0; o > s; s++) {
          var l = e[s],
            u = qu(t, l);
          a[u] = !0, this._selectedDataIndicesMap[u] = t.getRawIndex(l);
        } else if ("single" === i || i === !0) {
          var h = e[o - 1],
            u = qu(t, h);
          this.option.selectedMap = (n = {}, n[u] = !0, n), this._selectedDataIndicesMap = (r = {}, r[u] = t.getRawIndex(h), r);
        }
      }, n.prototype._initSelectedMapFromData = function (t) {
        if (!this.option.selectedMap) {
          var e = [];
          t.hasItemOption && t.each(function (n) {
            var r = t.getRawDataItem(n);
            "object" == _typeof(r) && r.selected && e.push(n);
          }), e.length > 0 && this._innerSelect(t, e);
        }
      }, n.registerClass = function (t) {
        return gx.registerClass(t);
      }, n.protoInitialize = function () {
        var t = n.prototype;
        t.type = "series.__base__", t.seriesIndex = 0, t.useColorPaletteOnData = !1, t.ignoreStyleOnData = !1, t.hasSymbolVisual = !1, t.defaultSymbol = "circle", t.visualStyleAccessPath = "itemStyle", t.visualDrawType = "fill";
      }(), n;
    }(gx);
  d(mw, hw), d(mw, Ex), ea(mw, gx);
  var _w = function () {
    function t() {
      this.group = new qv(), this.uid = Os("viewComponent");
    }
    return t.prototype.init = function () {}, t.prototype.render = function () {}, t.prototype.dispose = function () {}, t.prototype.updateView = function () {}, t.prototype.updateLayout = function () {}, t.prototype.updateVisual = function () {}, t.prototype.blurSeries = function () {}, t;
  }();
  Jo(_w), oa(_w, {
    registerWhenExtend: !0
  });
  var xw = Uo(),
    ww = th(),
    bw = function () {
      function t() {
        this.group = new qv(), this.uid = Os("viewChart"), this.renderTask = Du({
          plan: rh,
          reset: ih
        }), this.renderTask.context = {
          view: this
        };
      }
      return t.prototype.init = function () {}, t.prototype.render = function () {}, t.prototype.highlight = function (t, e, n, r) {
        nh(t.getData(), r, "emphasis");
      }, t.prototype.downplay = function (t, e, n, r) {
        nh(t.getData(), r, "normal");
      }, t.prototype.remove = function () {
        this.group.removeAll();
      }, t.prototype.dispose = function () {}, t.prototype.updateView = function (t, e, n, r) {
        this.render(t, e, n, r);
      }, t.prototype.updateLayout = function (t, e, n, r) {
        this.render(t, e, n, r);
      }, t.prototype.updateVisual = function (t, e, n, r) {
        this.render(t, e, n, r);
      }, t.markUpdateMethod = function (t, e) {
        xw(t).updateMethod = e;
      }, t.protoInitialize = function () {
        var e = t.prototype;
        e.type = "chart";
      }(), t;
    }();
  Jo(bw, ["dispose"]), oa(bw, {
    registerWhenExtend: !0
  });
  var Sw,
    Mw = {
      incrementalPrepareRender: {
        progress: function progress(t, e) {
          e.view.incrementalRender(t, e.model, e.ecModel, e.api, e.payload);
        }
      },
      render: {
        forceFirstProgress: !0,
        progress: function progress(t, e) {
          e.view.render(e.model, e.ecModel, e.api, e.payload);
        }
      }
    },
    Tw = Uo(),
    Cw = {
      itemStyle: aa(B_, !0),
      lineStyle: aa(O_, !0)
    },
    kw = {
      lineStyle: "stroke",
      itemStyle: "fill"
    },
    Dw = {
      createOnAllSeries: !0,
      performRawSeries: !0,
      reset: function reset(t, e) {
        var n = t.getData(),
          r = t.visualStyleAccessPath || "itemStyle",
          i = t.getModel(r),
          o = ah(t, r),
          a = o(i),
          s = i.getShallow("decal");
        s && (n.setVisual("decal", s), s.dirty = !0);
        var l = sh(t, r),
          u = a[l],
          c = T(u) ? u : null;
        return (!a[l] || c) && (a[l] = t.getColorFromPalette(t.name, null, e.getSeriesCount()), n.setVisual("colorFromPalette", !0)), n.setVisual("style", a), n.setVisual("drawType", l), !e.isSeriesFiltered(t) && c ? (n.setVisual("colorFromPalette", !1), {
          dataEach: function dataEach(e, n) {
            var r = t.getDataParams(n),
              i = h({}, a);
            i[l] = c(r), e.setItemVisual(n, "style", i);
          }
        }) : void 0;
      }
    },
    Iw = new N_(),
    Aw = {
      createOnAllSeries: !0,
      performRawSeries: !0,
      reset: function reset(t, e) {
        if (!t.ignoreStyleOnData && !e.isSeriesFiltered(t)) {
          var n = t.getData(),
            r = t.visualStyleAccessPath || "itemStyle",
            i = ah(t, r),
            o = n.getVisual("drawType");
          return {
            dataEach: n.hasItemOption ? function (t, e) {
              var n = t.getRawDataItem(e);
              if (n && n[r]) {
                Iw.option = n[r];
                var a = i(Iw),
                  s = t.ensureUniqueItemVisual(e, "style");
                h(s, a), Iw.option.decal && (t.setItemVisual(e, "decal", Iw.option.decal), Iw.option.decal.dirty = !0), o in a && t.setItemVisual(e, "colorFromPalette", !1);
              }
            } : null
          };
        }
      }
    },
    Pw = {
      performRawSeries: !0,
      overallReset: function overallReset(t) {
        var e = X();
        t.eachSeries(function (t) {
          if (t.useColorPaletteOnData) {
            var n = e.get(t.type);
            n || (n = {}, e.set(t.type, n)), Tw(t).scope = n;
          }
        }), t.eachSeries(function (e) {
          if (e.useColorPaletteOnData && !t.isSeriesFiltered(e)) {
            var n = e.getRawData(),
              r = {},
              i = e.getData(),
              o = Tw(e).scope,
              a = e.visualStyleAccessPath || "itemStyle",
              s = sh(e, a);
            i.each(function (t) {
              var e = i.getRawIndex(t);
              r[e] = t;
            }), n.each(function (t) {
              var a = r[t],
                l = i.getItemVisual(a, "colorFromPalette");
              if (l) {
                var u = i.ensureUniqueItemVisual(a, "style"),
                  h = n.getName(t) || t + "",
                  c = n.count();
                u[s] = e.getColorFromPalette(h, o, c);
              }
            });
          }
        });
      }
    },
    Lw = Math.PI,
    Ow = function () {
      function t(t, e, n, r) {
        this._stageTaskMap = X(), this.ecInstance = t, this.api = e, n = this._dataProcessorHandlers = n.slice(), r = this._visualHandlers = r.slice(), this._allHandlers = n.concat(r);
      }
      return t.prototype.restoreData = function (t, e) {
        t.restoreData(e), this._stageTaskMap.each(function (t) {
          var e = t.overallTask;
          e && e.dirty();
        });
      }, t.prototype.getPerformArgs = function (t, e) {
        if (t.__pipeline) {
          var n = this._pipelineMap.get(t.__pipeline.id),
            r = n.context,
            i = !e && n.progressiveEnabled && (!r || r.progressiveRender) && t.__idxInPipeline > n.blockIndex,
            o = i ? n.step : null,
            a = r && r.modDataCount,
            s = null != a ? Math.ceil(a / o) : null;
          return {
            step: o,
            modBy: s,
            modDataCount: a
          };
        }
      }, t.prototype.getPipeline = function (t) {
        return this._pipelineMap.get(t);
      }, t.prototype.updateStreamModes = function (t, e) {
        var n = this._pipelineMap.get(t.uid),
          r = t.getData(),
          i = r.count(),
          o = n.progressiveEnabled && e.incrementalPrepareRender && i >= n.threshold,
          a = t.get("large") && i >= t.get("largeThreshold"),
          s = "mod" === t.get("progressiveChunkMode") ? i : null;
        t.pipelineContext = n.context = {
          progressiveRender: o,
          modDataCount: s,
          large: a
        };
      }, t.prototype.restorePipelines = function (t) {
        var e = this,
          n = e._pipelineMap = X();
        t.eachSeries(function (t) {
          var r = t.getProgressive(),
            i = t.uid;
          n.set(i, {
            id: i,
            head: null,
            tail: null,
            threshold: t.getProgressiveThreshold(),
            progressiveEnabled: r && !(t.preventIncremental && t.preventIncremental()),
            blockIndex: -1,
            step: Math.round(r || 700),
            count: 0
          }), e._pipe(t, t.dataTask);
        });
      }, t.prototype.prepareStageTasks = function () {
        var t = this._stageTaskMap,
          e = this.api.getModel(),
          n = this.api;
        v(this._allHandlers, function (r) {
          var i = t.get(r.uid) || t.set(r.uid, {}),
            o = "";
          W(!(r.reset && r.overallReset), o), r.reset && this._createSeriesStageTask(r, i, e, n), r.overallReset && this._createOverallStageTask(r, i, e, n);
        }, this);
      }, t.prototype.prepareView = function (t, e, n, r) {
        var i = t.renderTask,
          o = i.context;
        o.model = e, o.ecModel = n, o.api = r, i.__block = !t.incrementalPrepareRender, this._pipe(e, i);
      }, t.prototype.performDataProcessorTasks = function (t, e) {
        this._performStageTasks(this._dataProcessorHandlers, t, e, {
          block: !0
        });
      }, t.prototype.performVisualTasks = function (t, e, n) {
        this._performStageTasks(this._visualHandlers, t, e, n);
      }, t.prototype._performStageTasks = function (t, e, n, r) {
        function i(t, e) {
          return t.setDirty && (!t.dirtyMap || t.dirtyMap.get(e.__pipeline.id));
        }
        r = r || {};
        var o = !1,
          a = this;
        v(t, function (t) {
          if (!r.visualType || r.visualType === t.visualType) {
            var s = a._stageTaskMap.get(t.uid),
              l = s.seriesTaskMap,
              u = s.overallTask;
            if (u) {
              var h,
                c = u.agentStubMap;
              c.each(function (t) {
                i(r, t) && (t.dirty(), h = !0);
              }), h && u.dirty(), a.updatePayload(u, n);
              var f = a.getPerformArgs(u, r.block);
              c.each(function (t) {
                t.perform(f);
              }), u.perform(f) && (o = !0);
            } else l && l.each(function (s) {
              i(r, s) && s.dirty();
              var l = a.getPerformArgs(s, r.block);
              l.skip = !t.performRawSeries && e.isSeriesFiltered(s.context.model), a.updatePayload(s, n), s.perform(l) && (o = !0);
            });
          }
        }), this.unfinished = o || this.unfinished;
      }, t.prototype.performSeriesTasks = function (t) {
        var e;
        t.eachSeries(function (t) {
          e = t.dataTask.perform() || e;
        }), this.unfinished = e || this.unfinished;
      }, t.prototype.plan = function () {
        this._pipelineMap.each(function (t) {
          var e = t.tail;
          do {
            if (e.__block) {
              t.blockIndex = e.__idxInPipeline;
              break;
            }
            e = e.getUpstream();
          } while (e);
        });
      }, t.prototype.updatePayload = function (t, e) {
        "remain" !== e && (t.context.payload = e);
      }, t.prototype._createSeriesStageTask = function (t, e, n, r) {
        function i(e) {
          var i = e.uid,
            l = s.set(i, a && a.get(i) || Du({
              plan: ph,
              reset: dh,
              count: vh
            }));
          l.context = {
            model: e,
            ecModel: n,
            api: r,
            useClearVisual: t.isVisual && !t.isLayout,
            plan: t.plan,
            reset: t.reset,
            scheduler: o
          }, o._pipe(e, l);
        }
        var o = this,
          a = e.seriesTaskMap,
          s = e.seriesTaskMap = X(),
          l = t.seriesType,
          u = t.getTargetSeries;
        t.createOnAllSeries ? n.eachRawSeries(i) : l ? n.eachRawSeriesByType(l, i) : u && u(n, r).each(i);
      }, t.prototype._createOverallStageTask = function (t, e, n, r) {
        function i(t) {
          var e = t.uid,
            n = l.set(e, s && s.get(e) || (f = !0, Du({
              reset: hh,
              onDirty: fh
            })));
          n.context = {
            model: t,
            overallProgress: c
          }, n.agent = a, n.__block = c, o._pipe(t, n);
        }
        var o = this,
          a = e.overallTask = e.overallTask || Du({
            reset: uh
          });
        a.context = {
          ecModel: n,
          api: r,
          overallReset: t.overallReset,
          scheduler: o
        };
        var s = a.agentStubMap,
          l = a.agentStubMap = X(),
          u = t.seriesType,
          h = t.getTargetSeries,
          c = !0,
          f = !1,
          p = "";
        W(!t.createOnAllSeries, p), u ? n.eachRawSeriesByType(u, i) : h ? h(n, r).each(i) : (c = !1, v(n.getSeries(), i)), f && a.dirty();
      }, t.prototype._pipe = function (t, e) {
        var n = t.uid,
          r = this._pipelineMap.get(n);
        !r.head && (r.head = e), r.tail && r.tail.pipe(e), r.tail = e, e.__idxInPipeline = r.count++, e.__pipeline = r;
      }, t.wrapStageHandler = function (t, e) {
        return T(t) && (t = {
          overallReset: t,
          seriesType: yh(t)
        }), t.uid = Os("stageHandler"), e && (t.visualType = e), t;
      }, t;
    }(),
    Rw = gh(0),
    Ew = {},
    Bw = {};
  mh(Ew, zx), mh(Bw, Ux), Ew.eachSeriesByType = Ew.eachRawSeriesByType = function (t) {
    Sw = t;
  }, Ew.eachComponent = function (t) {
    "series" === t.mainType && t.subType && (Sw = t.subType);
  };
  var zw = ["#37A2DA", "#32C5E9", "#67E0E3", "#9FE6B8", "#FFDB5C", "#ff9f7f", "#fb7293", "#E062AE", "#E690D1", "#e7bcf3", "#9d96f5", "#8378EA", "#96BFFF"],
    Fw = {
      color: zw,
      colorLayer: [["#37A2DA", "#ffd85c", "#fd7b5f"], ["#37A2DA", "#67E0E3", "#FFDB5C", "#ff9f7f", "#E062AE", "#9d96f5"], ["#37A2DA", "#32C5E9", "#9FE6B8", "#FFDB5C", "#ff9f7f", "#fb7293", "#e7bcf3", "#8378EA", "#96BFFF"], zw]
    },
    Nw = "#B9B8CE",
    Hw = "#100C2A",
    Vw = function Vw() {
      return {
        axisLine: {
          lineStyle: {
            color: Nw
          }
        },
        splitLine: {
          lineStyle: {
            color: "#484753"
          }
        },
        splitArea: {
          areaStyle: {
            color: ["rgba(255,255,255,0.02)", "rgba(255,255,255,0.05)"]
          }
        },
        minorSplitLine: {
          lineStyle: {
            color: "#20203B"
          }
        }
      };
    },
    Ww = ["#4992ff", "#7cffb2", "#fddd60", "#ff6e76", "#58d9f9", "#05c091", "#ff8a45", "#8d48e3", "#dd79ff"],
    Gw = {
      darkMode: !0,
      color: Ww,
      backgroundColor: Hw,
      axisPointer: {
        lineStyle: {
          color: "#817f91"
        },
        crossStyle: {
          color: "#817f91"
        },
        label: {
          color: "#fff"
        }
      },
      legend: {
        textStyle: {
          color: Nw
        }
      },
      textStyle: {
        color: Nw
      },
      title: {
        textStyle: {
          color: "#EEF1FA"
        },
        subtextStyle: {
          color: "#B9B8CE"
        }
      },
      toolbox: {
        iconStyle: {
          borderColor: Nw
        }
      },
      dataZoom: {
        borderColor: "#71708A",
        textStyle: {
          color: Nw
        },
        brushStyle: {
          color: "rgba(135,163,206,0.3)"
        },
        handleStyle: {
          color: "#353450",
          borderColor: "#C5CBE3"
        },
        moveHandleStyle: {
          color: "#B0B6C3",
          opacity: .3
        },
        fillerColor: "rgba(135,163,206,0.2)",
        emphasis: {
          handleStyle: {
            borderColor: "#91B7F2",
            color: "#4D587D"
          },
          moveHandleStyle: {
            color: "#636D9A",
            opacity: .7
          }
        },
        dataBackground: {
          lineStyle: {
            color: "#71708A",
            width: 1
          },
          areaStyle: {
            color: "#71708A"
          }
        },
        selectedDataBackground: {
          lineStyle: {
            color: "#87A3CE"
          },
          areaStyle: {
            color: "#87A3CE"
          }
        }
      },
      visualMap: {
        textStyle: {
          color: Nw
        }
      },
      timeline: {
        lineStyle: {
          color: Nw
        },
        label: {
          color: Nw
        },
        controlStyle: {
          color: Nw,
          borderColor: Nw
        }
      },
      calendar: {
        itemStyle: {
          color: Hw
        },
        dayLabel: {
          color: Nw
        },
        monthLabel: {
          color: Nw
        },
        yearLabel: {
          color: Nw
        }
      },
      timeAxis: Vw(),
      logAxis: Vw(),
      valueAxis: Vw(),
      categoryAxis: Vw(),
      line: {
        symbol: "circle"
      },
      graph: {
        color: Ww
      },
      gauge: {
        title: {
          color: Nw
        },
        axisLine: {
          lineStyle: {
            color: [[1, "rgba(207,212,219,0.2)"]]
          }
        },
        axisLabel: {
          color: Nw
        },
        detail: {
          color: "#EEF1FA"
        }
      },
      candlestick: {
        itemStyle: {
          color: "#FD1050",
          color0: "#0CF49B",
          borderColor: "#FD1050",
          borderColor0: "#0CF49B"
        }
      }
    };
  Gw.categoryAxis.splitLine.show = !1;
  var Uw = function (t) {
    function n() {
      var e = null !== t && t.apply(this, arguments) || this;
      return e.type = "dataset", e;
    }
    return e(n, t), n.prototype.init = function (e, n, r) {
      t.prototype.init.call(this, e, n, r), this._sourceManager = new vw(this), Fu(this);
    }, n.prototype.mergeOption = function (e, n) {
      t.prototype.mergeOption.call(this, e, n), Fu(this);
    }, n.prototype.optionUpdated = function () {
      this._sourceManager.dirty();
    }, n.prototype.getSourceManager = function () {
      return this._sourceManager;
    }, n.type = "dataset", n.defaultOption = {
      seriesLayoutBy: Ix
    }, n;
  }(gx);
  gx.registerClass(Uw);
  var qw = function (t) {
    function n() {
      var e = null !== t && t.apply(this, arguments) || this;
      return e.type = "dataset", e;
    }
    return e(n, t), n.type = "dataset", n;
  }(_w);
  _w.registerClass(qw);
  var Xw = X(),
    Yw = {
      registerMap: function registerMap(t, e, n) {
        var r;
        if (M(e)) r = e;else if (e.svg) r = [{
          type: "svg",
          source: e.svg,
          specialAreas: e.specialAreas
        }];else {
          var i = e.geoJson || e.geoJSON;
          i && !e.features && (n = e.specialAreas, e = i), r = [{
            type: "geoJSON",
            source: e,
            specialAreas: n
          }];
        }
        return v(r, function (t) {
          var e = t.type;
          "geoJson" === e && (e = t.type = "geoJSON");
          var n = jw[e];
          n(t);
        }), Xw.set(t, r);
      },
      retrieveMap: function retrieveMap(t) {
        return Xw.get(t);
      }
    },
    jw = {
      geoJSON: function geoJSON(t) {
        var e = t.source;
        t.geoJSON = C(e) ? "undefined" != typeof JSON && JSON.parse ? JSON.parse(e) : new Function("return (" + e + ");")() : e;
      },
      svg: function svg(t) {
        t.svgXML = pi(t.source);
      }
    },
    Zw = function () {
      function t() {}
      return t.prototype.normalizeQuery = function (t) {
        var e = {},
          n = {},
          r = {};
        if (C(t)) {
          var i = Ko(t);
          e.mainType = i.main || null, e.subType = i.sub || null;
        } else {
          var o = ["Index", "Name", "Id"],
            a = {
              name: 1,
              dataIndex: 1,
              dataType: 1
            };
          v(t, function (t, i) {
            for (var s = !1, l = 0; l < o.length; l++) {
              var u = o[l],
                h = i.lastIndexOf(u);
              if (h > 0 && h === i.length - u.length) {
                var c = i.slice(0, h);
                "data" !== c && (e.mainType = c, e[u.toLowerCase()] = t, s = !0);
              }
            }
            a.hasOwnProperty(i) && (n[i] = t, s = !0), s || (r[i] = t);
          });
        }
        return {
          cptQuery: e,
          dataQuery: n,
          otherQuery: r
        };
      }, t.prototype.filter = function (t, e) {
        function n(t, e, n, r) {
          return null == t[n] || e[r || n] === t[n];
        }
        var r = this.eventInfo;
        if (!r) return !0;
        var i = r.targetEl,
          o = r.packedEvent,
          a = r.model,
          s = r.view;
        if (!a || !s) return !0;
        var l = e.cptQuery,
          u = e.dataQuery;
        return n(l, a, "mainType") && n(l, a, "subType") && n(l, a, "index", "componentIndex") && n(l, a, "name") && n(l, a, "id") && n(u, o, "name") && n(u, o, "dataIndex") && n(u, o, "dataType") && (!s.filterForExposedEvent || s.filterForExposedEvent(t, e.otherQuery, i, o));
      }, t.prototype.afterTrigger = function () {
        this.eventInfo = null;
      }, t;
    }(),
    Kw = {
      createOnAllSeries: !0,
      performRawSeries: !0,
      reset: function reset(t, e) {
        function n(e, n) {
          var r = t.getRawValue(n),
            a = t.getDataParams(n);
          l && e.setItemVisual(n, "symbol", i(r, a)), u && e.setItemVisual(n, "symbolSize", o(r, a)), h && e.setItemVisual(n, "symbolRotate", s(r, a));
        }
        var r = t.getData();
        if (t.legendSymbol && r.setVisual("legendSymbol", t.legendSymbol), t.hasSymbolVisual) {
          var i = t.get("symbol"),
            o = t.get("symbolSize"),
            a = t.get("symbolKeepAspect"),
            s = t.get("symbolRotate"),
            l = T(i),
            u = T(o),
            h = T(s),
            c = l || u || h,
            f = !l && i ? i : t.defaultSymbol,
            p = u ? null : o,
            d = h ? null : s;
          if (r.setVisual({
            legendSymbol: t.legendSymbol || f,
            symbol: f,
            symbolSize: p,
            symbolKeepAspect: a,
            symbolRotate: d
          }), !e.isSeriesFiltered(t)) return {
            dataEach: c ? n : null
          };
        }
      }
    },
    $w = {
      createOnAllSeries: !0,
      performRawSeries: !0,
      reset: function reset(t, e) {
        function n(t, e) {
          var n = t.getItemModel(e),
            r = n.getShallow("symbol", !0),
            i = n.getShallow("symbolSize", !0),
            o = n.getShallow("symbolRotate", !0),
            a = n.getShallow("symbolKeepAspect", !0);
          null != r && t.setItemVisual(e, "symbol", r), null != i && t.setItemVisual(e, "symbolSize", i), null != o && t.setItemVisual(e, "symbolRotate", o), null != a && t.setItemVisual(e, "symbolKeepAspect", a);
        }
        if (t.hasSymbolVisual && !e.isSeriesFiltered(t)) {
          var r = t.getData();
          return {
            dataEach: r.hasItemOption ? n : null
          };
        }
      }
    },
    Qw = 2 * Math.PI,
    Jw = bv.CMD,
    tb = ["top", "right", "bottom", "left"],
    eb = [],
    nb = new qd(),
    rb = new qd(),
    ib = new qd(),
    ob = new qd(),
    ab = new qd(),
    sb = [],
    lb = new qd(),
    ub = ["align", "verticalAlign", "width", "height", "fontSize"],
    hb = new Id(),
    cb = Uo(),
    fb = Uo(),
    pb = ["x", "y", "rotation"],
    db = function () {
      function t() {
        this._labelList = [], this._chartViewList = [];
      }
      return t.prototype.clearLabels = function () {
        this._labelList = [], this._chartViewList = [];
      }, t.prototype._addLabel = function (t, e, n, r, i) {
        var o = r.style,
          a = r.__hostTarget,
          s = a.textConfig || {},
          l = r.getComputedTransform(),
          u = r.getBoundingRect().plain();
        tg.applyTransform(u, u, l), l ? hb.setLocalTransform(l) : (hb.x = hb.y = hb.rotation = hb.originX = hb.originY = 0, hb.scaleX = hb.scaleY = 1);
        var h,
          c = r.__hostTarget;
        if (c) {
          h = c.getBoundingRect().plain();
          var f = c.getComputedTransform();
          tg.applyTransform(h, h, f);
        }
        var p = h && c.getTextGuideLine();
        this._labelList.push({
          label: r,
          labelLine: p,
          seriesModel: n,
          dataIndex: t,
          dataType: e,
          layoutOption: i,
          computedLayoutOption: null,
          rect: u,
          hostRect: h,
          priority: h ? h.width * h.height : 0,
          defaultAttr: {
            ignore: r.ignore,
            labelGuideIgnore: p && p.ignore,
            x: hb.x,
            y: hb.y,
            rotation: hb.rotation,
            style: {
              x: o.x,
              y: o.y,
              align: o.align,
              verticalAlign: o.verticalAlign,
              width: o.width,
              height: o.height,
              fontSize: o.fontSize
            },
            cursor: r.cursor,
            attachedPos: s.position,
            attachedRot: s.rotation
          }
        });
      }, t.prototype.addLabelsOfSeries = function (t) {
        var e = this;
        this._chartViewList.push(t);
        var n = t.__model,
          r = n.get("labelLayout");
        (T(r) || w(r).length) && t.group.traverse(function (t) {
          if (t.ignore) return !0;
          var i = t.getTextContent(),
            o = Jm(t);
          i && !i.disableLabelLayout && e._addLabel(o.dataIndex, o.dataType, n, i, r);
        });
      }, t.prototype.updateLayoutConfig = function (t) {
        function e(t, e) {
          return function () {
            kh(t, e);
          };
        }
        for (var n = t.getWidth(), r = t.getHeight(), i = 0; i < this._labelList.length; i++) {
          var o = this._labelList[i],
            a = o.label,
            s = a.__hostTarget,
            l = o.defaultAttr,
            u = void 0;
          u = "function" == typeof o.layoutOption ? o.layoutOption(Nh(o, s)) : o.layoutOption, u = u || {}, o.computedLayoutOption = u;
          var h = Math.PI / 180;
          s && s.setTextConfig({
            local: !1,
            position: null != u.x || null != u.y ? null : l.attachedPos,
            rotation: null != u.rotate ? u.rotate * h : l.attachedRot,
            offset: [u.dx || 0, u.dy || 0]
          });
          var c = !1;
          if (null != u.x ? (a.x = ro(u.x, n), a.setStyle("x", 0), c = !0) : (a.x = l.x, a.setStyle("x", l.style.x)), null != u.y ? (a.y = ro(u.y, r), a.setStyle("y", 0), c = !0) : (a.y = l.y, a.setStyle("y", l.style.y)), u.labelLinePoints) {
            var f = s.getTextGuideLine();
            f && (f.setShape({
              points: u.labelLinePoints
            }), c = !1);
          }
          var p = cb(a);
          p.needsUpdateLabelLine = c, a.rotation = null != u.rotate ? u.rotate * h : l.rotation;
          for (var d = 0; d < ub.length; d++) {
            var g = ub[d];
            a.setStyle(g, null != u[g] ? u[g] : l.style[g]);
          }
          if (u.draggable) {
            if (a.draggable = !0, a.cursor = "move", s) {
              var v = o.seriesModel;
              if (null != o.dataIndex) {
                var y = o.seriesModel.getData(o.dataType);
                v = y.getItemModel(o.dataIndex);
              }
              a.on("drag", e(s, v.getModel("labelLine")));
            }
          } else a.off("drag"), a.cursor = l.cursor;
        }
      }, t.prototype.layout = function (t) {
        var e = t.getWidth(),
          n = t.getHeight(),
          r = Oh(this._labelList),
          i = _(r, function (t) {
            return "shiftX" === t.layoutOption.moveOverlap;
          }),
          o = _(r, function (t) {
            return "shiftY" === t.layoutOption.moveOverlap;
          });
        Eh(i, 0, e), Bh(o, 0, n);
        var a = _(r, function (t) {
          return t.layoutOption.hideOverlap;
        });
        zh(a);
      }, t.prototype.processLabelsOverall = function () {
        var t = this;
        v(this._chartViewList, function (e) {
          var n = e.__model,
            r = e.ignoreLabelLineUpdate,
            i = n.isAnimationEnabled();
          e.group.traverse(function (e) {
            if (e.ignore) return !0;
            var o = !r,
              a = e.getTextContent();
            !o && a && (o = cb(a).needsUpdateLabelLine), o && t._updateLabelLine(e, n), i && t._animateLabels(e, n);
          });
        });
      }, t.prototype._updateLabelLine = function (t, e) {
        var n = t.getTextContent(),
          r = Jm(t),
          i = r.dataIndex;
        if (n && null != i) {
          var o = e.getData(r.dataType),
            a = o.getItemModel(i),
            s = {},
            l = o.getItemVisual(i, "style"),
            u = o.getVisual("drawType");
          s.stroke = l[u];
          var h = a.getModel("labelLine");
          Ph(t, Lh(a), s), kh(t, h);
        }
      }, t.prototype._animateLabels = function (t, e) {
        var n = t.getTextContent(),
          r = t.getTextGuideLine();
        if (n && !n.ignore && !n.invisible && !t.disableLabelAnimation && !hs(t)) {
          var i = cb(n),
            o = i.oldLayout,
            a = Jm(t),
            s = a.dataIndex,
            l = {
              x: n.x,
              y: n.y,
              rotation: n.rotation
            },
            u = e.getData(a.dataType);
          if (o) {
            n.attr(o);
            var h = t.prevStates;
            h && (f(h, "select") >= 0 && n.attr(i.oldLayoutSelect), f(h, "emphasis") >= 0 && n.attr(i.oldLayoutEmphasis)), os(n, l, e, s);
          } else if (n.attr(l), !I_(n).valueAnimation) {
            var c = F(n.style.opacity, 1);
            n.style.opacity = 0, as(n, {
              style: {
                opacity: c
              }
            }, e, s);
          }
          if (i.oldLayout = l, n.states.select) {
            var p = i.oldLayoutSelect = {};
            Hh(p, l, pb), Hh(p, n.states.select, pb);
          }
          if (n.states.emphasis) {
            var d = i.oldLayoutEmphasis = {};
            Hh(d, l, pb), Hh(d, n.states.emphasis, pb);
          }
          Ls(n, s, u, e);
        }
        if (r && !r.ignore && !r.invisible) {
          var i = fb(r),
            o = i.oldLayout,
            g = {
              points: r.shape.points
            };
          o ? (r.attr({
            shape: o
          }), os(r, {
            shape: g
          }, e)) : (r.setShape(g), r.style.strokePercent = 0, as(r, {
            style: {
              strokePercent: 1
            }
          }, e)), i.oldLayout = g;
        }
      }, t;
    }(),
    gb = new bv(!0),
    vb = ["shadowBlur", "shadowOffsetX", "shadowOffsetY"],
    yb = [["lineCap", "butt"], ["lineJoin", "miter"], ["miterLimit", 10]],
    mb = 1,
    _b = 2,
    xb = 3,
    wb = 4,
    bb = function (t) {
      function n(e, n, r) {
        var i = t.call(this) || this;
        i.motionBlur = !1, i.lastFrameAlpha = .7, i.dpr = 1, i.virtual = !1, i.config = {}, i.incremental = !1, i.zlevel = 0, i.maxRepaintRectCount = 5, i.__dirty = !0, i.__firstTimePaint = !0, i.__used = !1, i.__drawIndex = 0, i.__startIndex = 0, i.__endIndex = 0, i.__prevStartIndex = null, i.__prevEndIndex = null;
        var o;
        r = r || og, "string" == typeof e ? o = gc(e, n, r) : I(e) && (o = e, e = o.id), i.id = e, i.dom = o;
        var a = o.style;
        return a && (o.onselectstart = dc, a.webkitUserSelect = "none", a.userSelect = "none", a.webkitTapHighlightColor = "rgba(0,0,0,0)", a["-webkit-touch-callout"] = "none", a.padding = "0", a.margin = "0", a.borderWidth = "0"), i.domBack = null, i.ctxBack = null, i.painter = n, i.config = null, i.dpr = r, i;
      }
      return e(n, t), n.prototype.getElementCount = function () {
        return this.__endIndex - this.__startIndex;
      }, n.prototype.afterBrush = function () {
        this.__prevStartIndex = this.__startIndex, this.__prevEndIndex = this.__endIndex;
      }, n.prototype.initContext = function () {
        this.ctx = this.dom.getContext("2d"), this.ctx.dpr = this.dpr;
      }, n.prototype.setUnpainted = function () {
        this.__firstTimePaint = !0;
      }, n.prototype.createBackBuffer = function () {
        var t = this.dpr;
        this.domBack = gc("back-" + this.id, this.painter, t), this.ctxBack = this.domBack.getContext("2d"), 1 !== t && this.ctxBack.scale(t, t);
      }, n.prototype.createRepaintRects = function (t, e, n, r) {
        function i(t) {
          if (t.isFinite() && !t.isZero()) if (0 === o.length) {
            var e = new tg(0, 0, 0, 0);
            e.copy(t), o.push(e);
          } else {
            for (var n = !1, r = 1 / 0, i = 0, u = 0; u < o.length; ++u) {
              var h = o[u];
              if (h.intersect(t)) {
                var c = new tg(0, 0, 0, 0);
                c.copy(h), c.union(t), o[u] = c, n = !0;
                break;
              }
              if (s) {
                l.copy(t), l.union(h);
                var f = t.width * t.height,
                  p = h.width * h.height,
                  d = l.width * l.height,
                  g = d - f - p;
                r > g && (r = r, i = u);
              }
            }
            if (s && (o[i].union(t), n = !0), !n) {
              var e = new tg(0, 0, 0, 0);
              e.copy(t), o.push(e);
            }
            s || (s = o.length >= a);
          }
        }
        if (this.__firstTimePaint) return this.__firstTimePaint = !1, null;
        for (var o = [], a = this.maxRepaintRectCount, s = !1, l = new tg(0, 0, 0, 0), u = this.__startIndex; u < this.__endIndex; ++u) {
          var h = t[u];
          if (h) {
            var c = h.shouldBePainted(n, r, !0, !0),
              f = h.__isRendered && (h.__dirty & gg.REDARAW_BIT || !c) ? h.getPrevPaintRect() : null;
            f && i(f);
            var p = c && (h.__dirty & gg.REDARAW_BIT || !h.__isRendered) ? h.getPaintRect() : null;
            p && i(p);
          }
        }
        for (var u = this.__prevStartIndex; u < this.__prevEndIndex; ++u) {
          var h = e[u],
            c = h.shouldBePainted(n, r, !0, !0);
          if (h && (!c || !h.__zr) && h.__isRendered) {
            var f = h.getPrevPaintRect();
            f && i(f);
          }
        }
        var d;
        do {
          d = !1;
          for (var u = 0; u < o.length;) {
            if (o[u].isZero()) o.splice(u, 1);else {
              for (var g = u + 1; g < o.length;) {
                o[u].intersect(o[g]) ? (d = !0, o[u].union(o[g]), o.splice(g, 1)) : g++;
              }
              u++;
            }
          }
        } while (d);
        return this._paintRects = o, o;
      }, n.prototype.debugGetPaintRects = function () {
        return (this._paintRects || []).slice();
      }, n.prototype.resize = function (t, e) {
        var n = this.dpr,
          r = this.dom,
          i = r.style,
          o = this.domBack;
        i && (i.width = t + "px", i.height = e + "px"), r.width = t * n, r.height = e * n, o && (o.width = t * n, o.height = e * n, 1 !== n && this.ctxBack.scale(n, n));
      }, n.prototype.clear = function (t, e, n) {
        function r(t, n, r, i) {
          if (o.clearRect(t, n, r, i), e && "transparent" !== e) {
            var a = void 0;
            O(e) ? (a = e.__canvasGradient || qh(o, e, {
              x: 0,
              y: 0,
              width: r,
              height: i
            }), e.__canvasGradient = a) : R(e) && (a = Qh(o, e, {
              dirty: function dirty() {
                c.setUnpainted(), c.__painter.refresh();
              }
            })), o.save(), o.fillStyle = a || e, o.fillRect(t, n, r, i), o.restore();
          }
          l && (o.save(), o.globalAlpha = u, o.drawImage(f, t, n, r, i), o.restore());
        }
        var i = this.dom,
          o = this.ctx,
          a = i.width,
          s = i.height;
        e = e || this.clearColor;
        var l = this.motionBlur && !t,
          u = this.lastFrameAlpha,
          h = this.dpr,
          c = this;
        l && (this.domBack || this.createBackBuffer(), this.ctxBack.globalCompositeOperation = "copy", this.ctxBack.drawImage(i, 0, 0, a / h, s / h));
        var f = this.domBack;
        !n || l ? r(0, 0, a, s) : n.length && v(n, function (t) {
          r(t.x * h, t.y * h, t.width * h, t.height * h);
        });
      }, n;
    }(ad),
    Sb = 1e5,
    Mb = 314159,
    Tb = .01,
    Cb = .001,
    kb = function () {
      function t(t, e, n) {
        this.type = "canvas", this._zlevelList = [], this._prevDisplayList = [], this._layers = {}, this._layerConfig = {}, this._needsManuallyCompositing = !1, this.type = "canvas";
        var r = !t.nodeName || "CANVAS" === t.nodeName.toUpperCase();
        this._opts = n = h({}, n || {}), this.dpr = n.devicePixelRatio || og, this._singleCanvas = r, this.root = t;
        var i = t.style;
        i && (i.webkitTapHighlightColor = "transparent", i.webkitUserSelect = "none", i.userSelect = "none", i["-webkit-touch-callout"] = "none", t.innerHTML = ""), this.storage = e;
        var o = this._zlevelList;
        this._prevDisplayList = [];
        var a = this._layers;
        if (r) {
          var s = t,
            l = s.width,
            u = s.height;
          null != n.width && (l = n.width), null != n.height && (u = n.height), this.dpr = n.devicePixelRatio || 1, s.width = l * this.dpr, s.height = u * this.dpr, this._width = l, this._height = u;
          var c = new bb(s, this, this.dpr);
          c.__builtin__ = !0, c.initContext(), a[Mb] = c, c.zlevel = Mb, o.push(Mb), this._domRoot = t;
        } else {
          this._width = this._getSize(0), this._height = this._getSize(1);
          var f = this._domRoot = mc(this._width, this._height);
          t.appendChild(f);
        }
      }
      return t.prototype.getType = function () {
        return "canvas";
      }, t.prototype.isSingleCanvas = function () {
        return this._singleCanvas;
      }, t.prototype.getViewportRoot = function () {
        return this._domRoot;
      }, t.prototype.getViewportRootOffset = function () {
        var t = this.getViewportRoot();
        return t ? {
          offsetLeft: t.offsetLeft || 0,
          offsetTop: t.offsetTop || 0
        } : void 0;
      }, t.prototype.refresh = function (t) {
        var e = this.storage.getDisplayList(!0),
          n = this._prevDisplayList,
          r = this._zlevelList;
        this._redrawId = Math.random(), this._paintList(e, n, t, this._redrawId);
        for (var i = 0; i < r.length; i++) {
          var o = r[i],
            a = this._layers[o];
          if (!a.__builtin__ && a.refresh) {
            var s = 0 === i ? this._backgroundColor : null;
            a.refresh(s);
          }
        }
        return this._opts.useDirtyRect && (this._prevDisplayList = e.slice()), this;
      }, t.prototype.refreshHover = function () {
        this._paintHoverList(this.storage.getDisplayList(!1));
      }, t.prototype._paintHoverList = function (t) {
        var e = t.length,
          n = this._hoverlayer;
        if (n && n.clear(), e) {
          for (var r, i = {
              inHover: !0,
              viewWidth: this._width,
              viewHeight: this._height
            }, o = 0; e > o; o++) {
            var a = t[o];
            a.__inHover && (n || (n = this._hoverlayer = this.getLayer(Sb)), r || (r = n.ctx, r.save()), fc(r, a, i, o === e - 1));
          }
          r && r.restore();
        }
      }, t.prototype.getHoverLayer = function () {
        return this.getLayer(Sb);
      }, t.prototype.paintOne = function (t, e) {
        cc(t, e);
      }, t.prototype._paintList = function (t, e, n, r) {
        if (this._redrawId === r) {
          n = n || !1, this._updateLayerStatus(t);
          var i = this._doPaintList(t, e, n),
            o = i.finished,
            a = i.needsRefreshHover;
          if (this._needsManuallyCompositing && this._compositeManually(), a && this._paintHoverList(t), o) this.eachLayer(function (t) {
            t.afterBrush && t.afterBrush();
          });else {
            var s = this;
            wg(function () {
              s._paintList(t, e, n, r);
            });
          }
        }
      }, t.prototype._compositeManually = function () {
        var t = this.getLayer(Mb).ctx,
          e = this._domRoot.width,
          n = this._domRoot.height;
        t.clearRect(0, 0, e, n), this.eachBuiltinLayer(function (r) {
          r.virtual && t.drawImage(r.dom, 0, 0, e, n);
        });
      }, t.prototype._doPaintList = function (t, e, n) {
        for (var r = this, i = [], o = this._opts.useDirtyRect, a = 0; a < this._zlevelList.length; a++) {
          var s = this._zlevelList[a],
            l = this._layers[s];
          l.__builtin__ && l !== this._hoverlayer && (l.__dirty || n) && i.push(l);
        }
        for (var u = !0, h = !1, c = function c(a) {
            var s = i[a],
              l = s.ctx,
              c = o && s.createRepaintRects(t, e, f._width, f._height);
            l.save();
            var p = n ? s.__startIndex : s.__drawIndex,
              d = !n && s.incremental && Date.now,
              g = d && Date.now(),
              v = s.zlevel === f._zlevelList[0] ? f._backgroundColor : null;
            if (s.__startIndex === s.__endIndex) s.clear(!1, v, c);else if (p === s.__startIndex) {
              var y = t[p];
              y.incremental && y.notClear && !n || s.clear(!1, v, c);
            }
            -1 === p && (console.error("For some unknown reason. drawIndex is -1"), p = s.__startIndex);
            var m,
              _ = function _(e) {
                var n = {
                  inHover: !1,
                  allClipped: !1,
                  prevEl: null,
                  viewWidth: r._width,
                  viewHeight: r._height
                };
                for (m = p; m < s.__endIndex; m++) {
                  var i = t[m];
                  if (i.__inHover && (h = !0), r._doPaintEl(i, s, o, e, n, m === s.__endIndex - 1), d) {
                    var a = Date.now() - g;
                    if (a > 15) break;
                  }
                }
                n.prevElClipPaths && l.restore();
              };
            if (c) {
              if (0 === c.length) m = s.__endIndex;else for (var x = f.dpr, w = 0; w < c.length; ++w) {
                var b = c[w];
                l.save(), l.beginPath(), l.rect(b.x * x, b.y * x, b.width * x, b.height * x), l.clip(), _(b), l.restore();
              }
            } else l.save(), _(), l.restore();
            s.__drawIndex = m, s.__drawIndex < s.__endIndex && (u = !1);
          }, f = this, p = 0; p < i.length; p++) {
          c(p);
        }
        return Ep.wxa && v(this._layers, function (t) {
          t && t.ctx && t.ctx.draw && t.ctx.draw();
        }), {
          finished: u,
          needsRefreshHover: h
        };
      }, t.prototype._doPaintEl = function (t, e, n, r, i, o) {
        var a = e.ctx;
        if (n) {
          var s = t.getPaintRect();
          (!r || s && s.intersect(r)) && (fc(a, t, i, o), t.setPrevPaintRect(s));
        } else fc(a, t, i, o);
      }, t.prototype.getLayer = function (t, e) {
        this._singleCanvas && !this._needsManuallyCompositing && (t = Mb);
        var n = this._layers[t];
        return n || (n = new bb("zr_" + t, this, this.dpr), n.zlevel = t, n.__builtin__ = !0, this._layerConfig[t] ? l(n, this._layerConfig[t], !0) : this._layerConfig[t - Tb] && l(n, this._layerConfig[t - Tb], !0), e && (n.virtual = e), this.insertLayer(t, n), n.initContext()), n;
      }, t.prototype.insertLayer = function (t, e) {
        var n = this._layers,
          r = this._zlevelList,
          i = r.length,
          o = this._domRoot,
          s = null,
          l = -1;
        if (n[t]) return void a("ZLevel " + t + " has been used already");
        if (!yc(e)) return void a("Layer of zlevel " + t + " is not valid");
        if (i > 0 && t > r[0]) {
          for (l = 0; i - 1 > l && !(r[l] < t && r[l + 1] > t); l++) {
            ;
          }
          s = n[r[l]];
        }
        if (r.splice(l + 1, 0, t), n[t] = e, !e.virtual) if (s) {
          var u = s.dom;
          u.nextSibling ? o.insertBefore(e.dom, u.nextSibling) : o.appendChild(e.dom);
        } else o.firstChild ? o.insertBefore(e.dom, o.firstChild) : o.appendChild(e.dom);
        e.__painter = this;
      }, t.prototype.eachLayer = function (t, e) {
        for (var n = this._zlevelList, r = 0; r < n.length; r++) {
          var i = n[r];
          t.call(e, this._layers[i], i);
        }
      }, t.prototype.eachBuiltinLayer = function (t, e) {
        for (var n = this._zlevelList, r = 0; r < n.length; r++) {
          var i = n[r],
            o = this._layers[i];
          o.__builtin__ && t.call(e, o, i);
        }
      }, t.prototype.eachOtherLayer = function (t, e) {
        for (var n = this._zlevelList, r = 0; r < n.length; r++) {
          var i = n[r],
            o = this._layers[i];
          o.__builtin__ || t.call(e, o, i);
        }
      }, t.prototype.getLayers = function () {
        return this._layers;
      }, t.prototype._updateLayerStatus = function (t) {
        function e(t) {
          s && (s.__endIndex !== t && (s.__dirty = !0), s.__endIndex = t);
        }
        if (this.eachBuiltinLayer(function (t) {
          t.__dirty = t.__used = !1;
        }), this._singleCanvas) for (var n = 1; n < t.length; n++) {
          var r = t[n];
          if (r.zlevel !== t[n - 1].zlevel || r.incremental) {
            this._needsManuallyCompositing = !0;
            break;
          }
        }
        var i,
          o,
          s = null,
          l = 0;
        for (o = 0; o < t.length; o++) {
          var r = t[o],
            u = r.zlevel,
            h = void 0;
          i !== u && (i = u, l = 0), r.incremental ? (h = this.getLayer(u + Cb, this._needsManuallyCompositing), h.incremental = !0, l = 1) : h = this.getLayer(u + (l > 0 ? Tb : 0), this._needsManuallyCompositing), h.__builtin__ || a("ZLevel " + u + " has been used by unkown layer " + h.id), h !== s && (h.__used = !0, h.__startIndex !== o && (h.__dirty = !0), h.__startIndex = o, h.__drawIndex = h.incremental ? -1 : o, e(o), s = h), r.__dirty & gg.REDARAW_BIT && !r.__inHover && (h.__dirty = !0, h.incremental && h.__drawIndex < 0 && (h.__drawIndex = o));
        }
        e(o), this.eachBuiltinLayer(function (t) {
          !t.__used && t.getElementCount() > 0 && (t.__dirty = !0, t.__startIndex = t.__endIndex = t.__drawIndex = 0), t.__dirty && t.__drawIndex < 0 && (t.__drawIndex = t.__startIndex);
        });
      }, t.prototype.clear = function () {
        return this.eachBuiltinLayer(this._clearLayer), this;
      }, t.prototype._clearLayer = function (t) {
        t.clear();
      }, t.prototype.setBackgroundColor = function (t) {
        this._backgroundColor = t, v(this._layers, function (t) {
          t.setUnpainted();
        });
      }, t.prototype.configLayer = function (t, e) {
        if (e) {
          var n = this._layerConfig;
          n[t] ? l(n[t], e, !0) : n[t] = e;
          for (var r = 0; r < this._zlevelList.length; r++) {
            var i = this._zlevelList[r];
            if (i === t || i === t + Tb) {
              var o = this._layers[i];
              l(o, n[t], !0);
            }
          }
        }
      }, t.prototype.delLayer = function (t) {
        var e = this._layers,
          n = this._zlevelList,
          r = e[t];
        r && (r.dom.parentNode.removeChild(r.dom), delete e[t], n.splice(f(n, t), 1));
      }, t.prototype.resize = function (t, e) {
        if (this._domRoot.style) {
          var n = this._domRoot;
          n.style.display = "none";
          var r = this._opts;
          if (null != t && (r.width = t), null != e && (r.height = e), t = this._getSize(0), e = this._getSize(1), n.style.display = "", this._width !== t || e !== this._height) {
            n.style.width = t + "px", n.style.height = e + "px";
            for (var i in this._layers) {
              this._layers.hasOwnProperty(i) && this._layers[i].resize(t, e);
            }
            this.refresh(!0);
          }
          this._width = t, this._height = e;
        } else {
          if (null == t || null == e) return;
          this._width = t, this._height = e, this.getLayer(Mb).resize(t, e);
        }
        return this;
      }, t.prototype.clearLayer = function (t) {
        var e = this._layers[t];
        e && e.clear();
      }, t.prototype.dispose = function () {
        this.root.innerHTML = "", this.root = this.storage = this._domRoot = this._layers = null;
      }, t.prototype.getRenderedCanvas = function (t) {
        if (t = t || {}, this._singleCanvas && !this._compositeManually) return this._layers[Mb].dom;
        var e = new bb("image", this, t.pixelRatio || this.dpr),
          n = e.ctx;
        if (e.initContext(), e.clear(!1, t.backgroundColor || this._backgroundColor), t.pixelRatio <= this.dpr) {
          this.refresh();
          var r = e.dom.width,
            i = e.dom.height,
            o = e.ctx;
          this.eachLayer(function (t) {
            t.__builtin__ ? o.drawImage(t.dom, 0, 0, r, i) : t.renderToCanvas && (e.ctx.save(), t.renderToCanvas(e.ctx), e.ctx.restore());
          });
        } else for (var a = {
            inHover: !1,
            viewWidth: this._width,
            viewHeight: this._height
          }, s = this.storage.getDisplayList(!0), l = 0, u = s.length; u > l; l++) {
          var h = s[l];
          fc(n, h, a, l === u - 1);
        }
        return e.dom;
      }, t.prototype.getWidth = function () {
        return this._width;
      }, t.prototype.getHeight = function () {
        return this._height;
      }, t.prototype._getSize = function (t) {
        var e = this._opts,
          n = ["width", "height"][t],
          r = ["clientWidth", "clientHeight"][t],
          i = ["paddingLeft", "paddingTop"][t],
          o = ["paddingRight", "paddingBottom"][t];
        if (null != e[n] && "auto" !== e[n]) return parseFloat(e[n]);
        var a = this.root,
          s = document.defaultView.getComputedStyle(a);
        return (a[r] || vc(s[n]) || vc(a.style[n])) - (vc(s[i]) || 0) - (vc(s[o]) || 0) | 0;
      }, t.prototype.pathToImage = function (t, e) {
        e = e || this.dpr;
        var n = document.createElement("canvas"),
          r = n.getContext("2d"),
          i = t.getBoundingRect(),
          o = t.style,
          a = o.shadowBlur * e,
          s = o.shadowOffsetX * e,
          l = o.shadowOffsetY * e,
          u = t.hasStroke() ? o.lineWidth : 0,
          c = Math.max(u / 2, -s + a),
          f = Math.max(u / 2, s + a),
          p = Math.max(u / 2, -l + a),
          d = Math.max(u / 2, l + a),
          g = i.width + c + f,
          v = i.height + p + d;
        n.width = g * e, n.height = v * e, r.scale(e, e), r.clearRect(0, 0, g, v), r.dpr = e;
        var y = {
          x: t.x,
          y: t.y,
          scaleX: t.scaleX,
          scaleY: t.scaleY,
          rotation: t.rotation,
          originX: t.originX,
          originY: t.originY
        };
        t.x = c - i.x, t.y = p - i.y, t.rotation = 0, t.scaleX = 1, t.scaleY = 1, t.updateTransform(), t && fc(r, t, {
          inHover: !1,
          viewWidth: this._width,
          viewHeight: this._height
        }, !0);
        var m = new jv({
          style: {
            x: 0,
            y: 0,
            image: n
          }
        });
        return h(t, y), m;
      }, t;
    }();
  to("canvas", kb);
  var Db = Math.round(9 * Math.random()),
    Ib = function () {
      function t() {
        this._id = "__ec_inner_" + Db++;
      }
      return t.prototype.get = function (t) {
        return this._guard(t)[this._id];
      }, t.prototype.set = function (t, e) {
        var n = this._guard(t);
        return "function" == typeof Object.defineProperty ? Object.defineProperty(n, this._id, {
          value: e,
          enumerable: !1,
          configurable: !0
        }) : n[this._id] = e, this;
      }, t.prototype["delete"] = function (t) {
        return this.has(t) ? (delete this._guard(t)[this._id], !0) : !1;
      }, t.prototype.has = function (t) {
        return !!this._guard(t)[this._id];
      }, t.prototype._guard = function (t) {
        if (t !== Object(t)) throw TypeError("Value of WeakMap is not a non-null object.");
        return t;
      }, t;
    }(),
    Ab = Ov.extend({
      type: "triangle",
      shape: {
        cx: 0,
        cy: 0,
        width: 0,
        height: 0
      },
      buildPath: function buildPath(t, e) {
        var n = e.cx,
          r = e.cy,
          i = e.width / 2,
          o = e.height / 2;
        t.moveTo(n, r - o), t.lineTo(n + i, r + o), t.lineTo(n - i, r + o), t.closePath();
      }
    }),
    Pb = Ov.extend({
      type: "diamond",
      shape: {
        cx: 0,
        cy: 0,
        width: 0,
        height: 0
      },
      buildPath: function buildPath(t, e) {
        var n = e.cx,
          r = e.cy,
          i = e.width / 2,
          o = e.height / 2;
        t.moveTo(n, r - o), t.lineTo(n + i, r), t.lineTo(n, r + o), t.lineTo(n - i, r), t.closePath();
      }
    }),
    Lb = Ov.extend({
      type: "pin",
      shape: {
        x: 0,
        y: 0,
        width: 0,
        height: 0
      },
      buildPath: function buildPath(t, e) {
        var n = e.x,
          r = e.y,
          i = e.width / 5 * 3,
          o = Math.max(i, e.height),
          a = i / 2,
          s = a * a / (o - a),
          l = r - o + a + s,
          u = Math.asin(s / a),
          h = Math.cos(u) * a,
          c = Math.sin(u),
          f = Math.cos(u),
          p = .6 * a,
          d = .7 * a;
        t.moveTo(n - h, l + s), t.arc(n, l, a, Math.PI - u, 2 * Math.PI + u), t.bezierCurveTo(n + h - c * p, l + s + f * p, n, r - d, n, r), t.bezierCurveTo(n, r - d, n - h + c * p, l + s + f * p, n - h, l + s), t.closePath();
      }
    }),
    Ob = Ov.extend({
      type: "arrow",
      shape: {
        x: 0,
        y: 0,
        width: 0,
        height: 0
      },
      buildPath: function buildPath(t, e) {
        var n = e.height,
          r = e.width,
          i = e.x,
          o = e.y,
          a = r / 3 * 2;
        t.moveTo(i, o), t.lineTo(i + a, o + n), t.lineTo(i, o + n / 4 * 3), t.lineTo(i - a, o + n), t.lineTo(i, o), t.closePath();
      }
    }),
    Rb = {
      line: ty,
      rect: ty,
      roundRect: ty,
      square: ty,
      circle: Kv,
      diamond: Pb,
      pin: Lb,
      arrow: Ob,
      triangle: Ab
    },
    Eb = {
      line: function line(t, e, n, r, i) {
        var o = 2;
        i.x = t, i.y = e + r / 2 - o / 2, i.width = n, i.height = o;
      },
      rect: function rect(t, e, n, r, i) {
        i.x = t, i.y = e, i.width = n, i.height = r;
      },
      roundRect: function roundRect(t, e, n, r, i) {
        i.x = t, i.y = e, i.width = n, i.height = r, i.r = Math.min(n, r) / 4;
      },
      square: function square(t, e, n, r, i) {
        var o = Math.min(n, r);
        i.x = t, i.y = e, i.width = o, i.height = o;
      },
      circle: function circle(t, e, n, r, i) {
        i.cx = t + n / 2, i.cy = e + r / 2, i.r = Math.min(n, r) / 2;
      },
      diamond: function diamond(t, e, n, r, i) {
        i.cx = t + n / 2, i.cy = e + r / 2, i.width = n, i.height = r;
      },
      pin: function pin(t, e, n, r, i) {
        i.x = t + n / 2, i.y = e + r / 2, i.width = n, i.height = r;
      },
      arrow: function arrow(t, e, n, r, i) {
        i.x = t + n / 2, i.y = e + r / 2, i.width = n, i.height = r;
      },
      triangle: function triangle(t, e, n, r, i) {
        i.cx = t + n / 2, i.cy = e + r / 2, i.width = n, i.height = r;
      }
    },
    Bb = {};
  v(Rb, function (t, e) {
    Bb[e] = new t();
  });
  var zb = Ov.extend({
      type: "symbol",
      shape: {
        symbolType: "",
        x: 0,
        y: 0,
        width: 0,
        height: 0
      },
      calculateTextPosition: function calculateTextPosition(t, e, n) {
        var r = Fn(t, e, n),
          i = this.shape;
        return i && "pin" === i.symbolType && "inside" === e.position && (r.y = n.y + .4 * n.height), r;
      },
      buildPath: function buildPath(t, e, n) {
        var r = e.symbolType;
        if ("none" !== r) {
          var i = Bb[r];
          i || (r = "rect", i = Bb[r]), Eb[r](e.x, e.y, e.width, e.height, i.shape), i.buildPath(t, i.shape, n);
        }
      }
    }),
    Fb = new Ib(),
    Nb = new Rd(100),
    Hb = ["symbol", "symbolSize", "symbolKeepAspect", "color", "backgroundColor", "dashArrayX", "dashArrayY", "dashLineOffset", "maxTileWidth", "maxTileHeight"],
    Vb = W,
    Wb = v,
    Gb = T,
    Ub = I,
    qb = "5.0.0",
    Xb = {
      zrender: "5.0.1"
    },
    Yb = 1,
    jb = 800,
    Zb = 900,
    Kb = 1e3,
    $b = 2e3,
    Qb = 5e3,
    Jb = 1e3,
    tS = 1100,
    eS = 2e3,
    nS = 3e3,
    rS = 4e3,
    iS = 4500,
    oS = 4600,
    aS = 5e3,
    sS = 6e3,
    lS = 7e3,
    uS = {
      PROCESSOR: {
        FILTER: Kb,
        SERIES_FILTER: jb,
        STATISTIC: Qb
      },
      VISUAL: {
        LAYOUT: Jb,
        PROGRESSIVE_LAYOUT: tS,
        GLOBAL: eS,
        CHART: nS,
        POST_CHART_LAYOUT: oS,
        COMPONENT: rS,
        BRUSH: aS,
        CHART_ITEM: iS,
        ARIA: sS,
        DECAL: lS
      }
    },
    hS = "__flagInMainProcess",
    cS = "__optionUpdated",
    fS = "__needsUpdateStatus",
    pS = /^[a-zA-Z0-9_]+$/,
    dS = "__connectUpdateStatus",
    gS = 0,
    vS = 1,
    yS = 2,
    mS = function (t) {
      function n() {
        return null !== t && t.apply(this, arguments) || this;
      }
      return e(n, t), n;
    }(ad),
    _S = mS.prototype;
  _S.on = Ac("on"), _S.off = Ac("off");
  var xS,
    wS,
    bS,
    SS,
    MS,
    TS,
    CS,
    kS,
    DS,
    IS,
    AS,
    PS,
    LS,
    OS,
    RS,
    ES,
    BS,
    zS,
    FS,
    NS,
    HS,
    VS = function (t) {
      function n(e, n, r) {
        function i(t, e) {
          return t.__prio - e.__prio;
        }
        var o = t.call(this, new Zw()) || this;
        o._chartsViews = [], o._chartsMap = {}, o._componentsViews = [], o._componentsMap = {}, o._pendingActions = [], r = r || {}, "string" == typeof n && (n = $S[n]), o._dom = e;
        var a = "canvas",
          l = !1,
          u = o._zr = Ki(e, {
            renderer: r.renderer || a,
            devicePixelRatio: r.devicePixelRatio,
            width: r.width,
            height: r.height,
            useDirtyRect: null == r.useDirtyRect ? l : r.useDirtyRect
          });
        o._throttledZrFlush = oh(Zp(u.flush, u), 17), n = s(n), n && hu(n, !0), o._theme = n, o._locale = zs(r.locale || j_), o._coordSysMgr = new Xx();
        var h = o._api = BS(o);
        return $n(KS, i), $n(XS, i), o._scheduler = new Ow(o, h, XS, KS), o._messageCenter = new mS(), o._labelManager = new db(), o._initEvents(), o.resize = Zp(o.resize, o), u.animation.on("frame", o._onframe, o), IS(u, o), AS(u, o), U(o), o;
      }
      return e(n, t), n.prototype._onframe = function () {
        if (!this._disposed) {
          HS(this);
          var t = this._scheduler;
          if (this[cS]) {
            var e = this[cS].silent;
            this[hS] = !0, xS(this), SS.update.call(this), this._zr.flush(), this[hS] = !1, this[cS] = !1, kS.call(this, e), DS.call(this, e);
          } else if (t.unfinished) {
            var n = Yb,
              r = this._model,
              i = this._api;
            t.unfinished = !1;
            do {
              var o = +new Date();
              t.performSeriesTasks(r), t.performDataProcessorTasks(r), TS(this, r), t.performVisualTasks(r), RS(this, this._model, i, "remain"), n -= +new Date() - o;
            } while (n > 0 && t.unfinished);
            t.unfinished || this._zr.flush();
          }
        }
      }, n.prototype.getDom = function () {
        return this._dom;
      }, n.prototype.getId = function () {
        return this.id;
      }, n.prototype.getZr = function () {
        return this._zr;
      }, n.prototype.setOption = function (t, e, n) {
        if (!this._disposed) {
          var r, i, o;
          if (Ub(e) && (n = e.lazyUpdate, r = e.silent, i = e.replaceMerge, o = e.transition, e = e.notMerge), this[hS] = !0, !this._model || e) {
            var a = new jx(this._api),
              s = this._theme,
              l = this._model = new zx();
            l.scheduler = this._scheduler, l.init(null, null, null, s, this._locale, a);
          }
          this._model.setOption(t, {
            replaceMerge: i
          }, YS), FS(this, o), n ? (this[cS] = {
            silent: r
          }, this[hS] = !1, this.getZr().wakeUp()) : (xS(this), SS.update.call(this), this._zr.flush(), this[cS] = !1, this[hS] = !1, kS.call(this, r), DS.call(this, r));
        }
      }, n.prototype.setTheme = function () {
        console.error("ECharts#setTheme() is DEPRECATED in ECharts 3.0");
      }, n.prototype.getModel = function () {
        return this._model;
      }, n.prototype.getOption = function () {
        return this._model && this._model.getOption();
      }, n.prototype.getWidth = function () {
        return this._zr.getWidth();
      }, n.prototype.getHeight = function () {
        return this._zr.getHeight();
      }, n.prototype.getDevicePixelRatio = function () {
        return this._zr.painter.dpr || window.devicePixelRatio || 1;
      }, n.prototype.getRenderedCanvas = function (t) {
        if (Ep.canvasSupported) {
          t = h({}, t || {}), t.pixelRatio = t.pixelRatio || 1, t.backgroundColor = t.backgroundColor || this._model.get("backgroundColor");
          var e = this._zr;
          return e.painter.getRenderedCanvas(t);
        }
      }, n.prototype.getSvgDataURL = function () {
        if (Ep.svgSupported) {
          var t = this._zr,
            e = t.storage.getDisplayList();
          return v(e, function (t) {
            t.stopAnimation(null, !0);
          }), t.painter.toDataURL();
        }
      }, n.prototype.getDataURL = function (t) {
        if (!this._disposed) {
          t = t || {};
          var e = t.excludeComponents,
            n = this._model,
            r = [],
            i = this;
          Wb(e, function (t) {
            n.eachComponent({
              mainType: t
            }, function (t) {
              var e = i._componentsMap[t.__viewId];
              e.group.ignore || (r.push(e), e.group.ignore = !0);
            });
          });
          var o = "svg" === this._zr.painter.getType() ? this.getSvgDataURL() : this.getRenderedCanvas(t).toDataURL("image/" + (t && t.type || "png"));
          return Wb(r, function (t) {
            t.group.ignore = !1;
          }), o;
        }
      }, n.prototype.getConnectedDataURL = function (t) {
        if (!this._disposed && Ep.canvasSupported) {
          var e = "svg" === t.type,
            n = this.group,
            r = Math.min,
            i = Math.max,
            o = 1 / 0;
          if (tM[n]) {
            var a = o,
              l = o,
              u = -o,
              h = -o,
              c = [],
              f = t && t.pixelRatio || 1;
            v(JS, function (o) {
              if (o.group === n) {
                var f = e ? o.getZr().painter.getSvgDom().innerHTML : o.getRenderedCanvas(s(t)),
                  p = o.getDom().getBoundingClientRect();
                a = r(p.left, a), l = r(p.top, l), u = i(p.right, u), h = i(p.bottom, h), c.push({
                  dom: f,
                  left: p.left,
                  top: p.top
                });
              }
            }), a *= f, l *= f, u *= f, h *= f;
            var p = u - a,
              d = h - l,
              g = jp(),
              y = Ki(g, {
                renderer: e ? "svg" : "canvas"
              });
            if (y.resize({
              width: p,
              height: d
            }), e) {
              var m = "";
              return Wb(c, function (t) {
                var e = t.left - a,
                  n = t.top - l;
                m += '<g transform="translate(' + e + "," + n + ')">' + t.dom + "</g>";
              }), y.painter.getSvgRoot().innerHTML = m, t.connectedBackgroundColor && y.painter.setBackgroundColor(t.connectedBackgroundColor), y.refreshImmediately(), y.painter.toDataURL();
            }
            return t.connectedBackgroundColor && y.add(new ty({
              shape: {
                x: 0,
                y: 0,
                width: p,
                height: d
              },
              style: {
                fill: t.connectedBackgroundColor
              }
            })), Wb(c, function (t) {
              var e = new jv({
                style: {
                  x: t.left * f - a,
                  y: t.top * f - l,
                  image: t.dom
                }
              });
              y.add(e);
            }), y.refreshImmediately(), g.toDataURL("image/" + (t && t.type || "png"));
          }
          return this.getDataURL(t);
        }
      }, n.prototype.convertToPixel = function (t, e) {
        return MS(this, "convertToPixel", t, e);
      }, n.prototype.convertFromPixel = function (t, e) {
        return MS(this, "convertFromPixel", t, e);
      }, n.prototype.containPixel = function (t, e) {
        if (!this._disposed) {
          var n,
            r = this._model,
            i = qo(r, t);
          return v(i, function (t, r) {
            r.indexOf("Models") >= 0 && v(t, function (t) {
              var i = t.coordinateSystem;
              if (i && i.containPoint) n = n || !!i.containPoint(e);else if ("seriesModels" === r) {
                var o = this._chartsMap[t.__viewId];
                o && o.containPoint && (n = n || o.containPoint(e, t));
              }
            }, this);
          }, this), !!n;
        }
      }, n.prototype.getVisual = function (t, e) {
        var n = this._model,
          r = qo(n, t, {
            defaultMainType: "series"
          }),
          i = r.seriesModel,
          o = i.getData(),
          a = r.hasOwnProperty("dataIndexInside") ? r.dataIndexInside : r.hasOwnProperty("dataIndex") ? o.indexOfRawIndex(r.dataIndex) : null;
        return null != a ? _h(o, a, e) : xh(o, e);
      }, n.prototype.getViewOfComponentModel = function (t) {
        return this._componentsMap[t.__viewId];
      }, n.prototype.getViewOfSeriesModel = function (t) {
        return this._chartsMap[t.__viewId];
      }, n.prototype._initEvents = function () {
        var t = this;
        Wb(GS, function (e) {
          var n = function n(_n2) {
            var r,
              i = t.getModel(),
              o = _n2.target,
              a = "globalout" === e;
            if (a ? r = {} : o && _c(o, function (t) {
              var e = Jm(t);
              if (e && null != e.dataIndex) {
                var n = e.dataModel || i.getSeriesByIndex(e.seriesIndex);
                return r = n && n.getDataParams(e.dataIndex, e.dataType) || {}, !0;
              }
              return e.eventData ? (r = h({}, e.eventData), !0) : void 0;
            }, !0), r) {
              var s = r.componentType,
                l = r.componentIndex;
              ("markLine" === s || "markPoint" === s || "markArea" === s) && (s = "series", l = r.seriesIndex);
              var u = s && null != l && i.getComponent(s, l),
                c = u && t["series" === u.mainType ? "_chartsMap" : "_componentsMap"][u.__viewId];
              r.event = _n2, r.type = e, t._$eventProcessor.eventInfo = {
                targetEl: o,
                packedEvent: r,
                model: u,
                view: c
              }, t.trigger(e, r);
            }
          };
          n.zrEventfulCallAtLast = !0, t._zr.on(e, n, t);
        }), Wb(qS, function (e, n) {
          t._messageCenter.on(n, function (t) {
            this.trigger(n, t);
          }, t);
        }), Wb(["selectchanged"], function (e) {
          t._messageCenter.on(e, function (t) {
            this.trigger(e, t);
          }, t);
        }), Wh(this._messageCenter, this, this._model);
      }, n.prototype.isDisposed = function () {
        return this._disposed;
      }, n.prototype.clear = function () {
        this._disposed || this.setOption({
          series: []
        }, !0);
      }, n.prototype.dispose = function () {
        if (!this._disposed) {
          this._disposed = !0, Yo(this.getDom(), rM, "");
          var t = this._api,
            e = this._model;
          Wb(this._componentsViews, function (n) {
            n.dispose(e, t);
          }), Wb(this._chartsViews, function (n) {
            n.dispose(e, t);
          }), this._zr.dispose(), delete JS[this.id];
        }
      }, n.prototype.resize = function (t) {
        if (!this._disposed) {
          this._zr.resize(t);
          var e = this._model;
          if (this._loadingFX && this._loadingFX.resize(), e) {
            var n = e.resetOption("media"),
              r = t && t.silent;
            this[hS] = !0, n && xS(this), SS.update.call(this, {
              type: "resize",
              animation: {
                duration: 0
              }
            }), this[hS] = !1, kS.call(this, r), DS.call(this, r);
          }
        }
      }, n.prototype.showLoading = function (t, e) {
        if (!this._disposed && (Ub(t) && (e = t, t = ""), t = t || "default", this.hideLoading(), QS[t])) {
          var n = QS[t](this._api, e),
            r = this._zr;
          this._loadingFX = n, r.add(n);
        }
      }, n.prototype.hideLoading = function () {
        this._disposed || (this._loadingFX && this._zr.remove(this._loadingFX), this._loadingFX = null);
      }, n.prototype.makeActionFromEvent = function (t) {
        var e = h({}, t);
        return e.type = qS[t.type], e;
      }, n.prototype.dispatchAction = function (t, e) {
        if (!this._disposed && (Ub(e) || (e = {
          silent: !!e
        }), US[t.type] && this._model)) {
          if (this[hS]) return void this._pendingActions.push(t);
          var n = e.silent;
          CS.call(this, t, n);
          var r = e.flush;
          r ? this._zr.flush() : r !== !1 && Ep.browser.weChat && this._throttledZrFlush(), kS.call(this, n), DS.call(this, n);
        }
      }, n.prototype.updateLabelLayout = function () {
        var t = this._labelManager;
        t.updateLayoutConfig(this._api), t.layout(this._api), t.processLabelsOverall();
      }, n.prototype.appendData = function (t) {
        if (!this._disposed) {
          var e = t.seriesIndex,
            n = this.getModel(),
            r = n.getSeriesByIndex(e);
          r.appendData(t), this._scheduler.unfinished = !0, this.getZr().wakeUp();
        }
      }, n.internalField = function () {
        function t(t) {
          for (var e = [], n = t.currentStates, r = 0; r < n.length; r++) {
            var i = n[r];
            "emphasis" !== i && "blur" !== i && "select" !== i && e.push(i);
          }
          t.selected && t.states.select && e.push("select"), t.hoverState === o_ && t.states.emphasis ? e.push("emphasis") : t.hoverState === i_ && t.states.blur && e.push("blur"), t.useStates(e);
        }
        function n(t, e) {
          var n = t._zr,
            r = n.storage,
            i = 0;
          r.traverse(function (t) {
            t.isGroup || i++;
          }), i > e.get("hoverLayerThreshold") && !Ep.node && !Ep.worker && e.eachSeries(function (e) {
            if (!e.preventUsingHoverLayer) {
              var n = t._chartsMap[e.__viewId];
              n.__alive && n.group.traverse(function (t) {
                t.states.emphasis && (t.states.emphasis.hoverLayer = !0);
              });
            }
          });
        }
        function r(t, e) {
          var n = t.get("blendMode") || null;
          e.group.traverse(function (t) {
            t.isGroup || (t.style.blend = n), t.eachPendingDisplayable && t.eachPendingDisplayable(function (t) {
              t.style.blend = n;
            });
          });
        }
        function i(t, e) {
          if (!t.preventAutoZ) {
            var n = t.get("z"),
              r = t.get("zlevel");
            e.group.traverse(function (t) {
              if (!t.isGroup) {
                null != n && (t.z = n), null != r && (t.zlevel = r);
                var e = t.getTextContent(),
                  i = t.getTextGuideLine();
                if (e && (e.z = t.z, e.zlevel = t.zlevel, e.z2 = t.z2 + 2), i) {
                  var o = t.textGuideLineConfig && t.textGuideLineConfig.showAbove;
                  i.z = t.z, i.zlevel = t.zlevel, i.z2 = t.z2 + (o ? 1 : -1);
                }
              }
            });
          }
        }
        function o(t, e) {
          e.group.traverse(function (t) {
            if (!hs(t)) {
              var e = t.getTextContent(),
                n = t.getTextGuideLine();
              t.stateTransition && (t.stateTransition = null), e && e.stateTransition && (e.stateTransition = null), n && n.stateTransition && (n.stateTransition = null), t.hasState() ? (t.prevStates = t.currentStates, t.clearStates()) : t.prevStates && (t.prevStates = null);
            }
          });
        }
        function a(e, n) {
          var r = e.getModel("stateAnimation"),
            i = e.isAnimationEnabled(),
            o = r.get("duration"),
            a = o > 0 ? {
              duration: o,
              delay: r.get("delay"),
              easing: r.get("easing")
            } : null;
          n.group.traverse(function (e) {
            if (e.states && e.states.emphasis) {
              if (hs(e)) return;
              if (e instanceof Ov && Ya(e), e.__dirty) {
                var n = e.prevStates;
                n && e.useStates(n);
              }
              if (i) {
                e.stateTransition = a;
                var r = e.getTextContent(),
                  o = e.getTextGuideLine();
                r && (r.stateTransition = a), o && (o.stateTransition = a);
              }
              e.__dirty && t(e);
            }
          });
        }
        xS = function xS(t) {
          var e = t._scheduler;
          e.restorePipelines(t._model), e.prepareStageTasks(), wS(t, !0), wS(t, !1), e.plan();
        }, wS = function wS(t, e) {
          function n(t) {
            var n = t.__requireNewView;
            t.__requireNewView = !1;
            var u = "_ec_" + t.id + "_" + t.type,
              h = !n && a[u];
            if (!h) {
              var c = Ko(t.type),
                f = e ? _w.getClass(c.main, c.sub) : bw.getClass(c.sub);
              h = new f(), h.init(r, l), a[u] = h, o.push(h), s.add(h.group);
            }
            t.__viewId = h.__id = u, h.__alive = !0, h.__model = t, h.group.__ecComponentInfo = {
              mainType: t.mainType,
              index: t.componentIndex
            }, !e && i.prepareView(h, t, r, l);
          }
          for (var r = t._model, i = t._scheduler, o = e ? t._componentsViews : t._chartsViews, a = e ? t._componentsMap : t._chartsMap, s = t._zr, l = t._api, u = 0; u < o.length; u++) {
            o[u].__alive = !1;
          }
          e ? r.eachComponent(function (t, e) {
            "series" !== t && n(e);
          }) : r.eachSeries(n);
          for (var u = 0; u < o.length;) {
            var h = o[u];
            h.__alive ? u++ : (!e && h.renderTask.dispose(), s.remove(h.group), h.dispose(r, l), o.splice(u, 1), a[h.__id] === h && delete a[h.__id], h.__id = h.group.__ecComponentInfo = null);
          }
        }, bS = function bS(t, e, n, r, i) {
          function o(r) {
            r && r.__alive && r[e] && r[e](r.__model, a, t._api, n);
          }
          var a = t._model;
          if (a.setUpdatePayload(n), !r) return void Wb([].concat(t._componentsViews).concat(t._chartsViews), o);
          var s = {};
          s[r + "Id"] = n[r + "Id"], s[r + "Index"] = n[r + "Index"], s[r + "Name"] = n[r + "Name"];
          var l = {
            mainType: r,
            query: s
          };
          i && (l.subType = i);
          var u,
            h = n.excludeSeriesId;
          null != h && (u = X(), Wb(To(h), function (t) {
            var e = Fo(t, null);
            null != e && u.set(e, !0);
          })), a && a.eachComponent(l, function (e) {
            u && null != u.get(e.id) || (Xa(n) && !n.notBlur ? e instanceof mw && Ea(e, n, t._api) : qa(n) && e instanceof mw && (Ba(e, n, t._api), za(e), NS(t)), o(t["series" === r ? "_chartsMap" : "_componentsMap"][e.__viewId]));
          }, t);
        }, SS = {
          prepareAndUpdate: function prepareAndUpdate(t) {
            xS(this), SS.update.call(this, t);
          },
          update: function update(t) {
            var e = this._model,
              n = this._api,
              r = this._zr,
              i = this._coordSysMgr,
              o = this._scheduler;
            if (e) {
              e.setUpdatePayload(t), o.restoreData(e, t), o.performSeriesTasks(e), i.create(e, n), o.performDataProcessorTasks(e, t), TS(this, e), i.update(e, n), PS(e), o.performVisualTasks(e, t), LS(this, e, n, t);
              var a = e.get("backgroundColor") || "transparent",
                s = e.get("darkMode");
              if (Ep.canvasSupported) r.setBackgroundColor(a), null != s && "auto" !== s && r.setDarkMode(s);else {
                var l = rn(a);
                a = pn(l, "rgb"), 0 === l[3] && (a = "transparent");
              }
              ES(e, n);
            }
          },
          updateTransform: function updateTransform(t) {
            var e = this,
              n = this._model,
              r = this._api;
            if (n) {
              n.setUpdatePayload(t);
              var i = [];
              n.eachComponent(function (o, a) {
                if ("series" !== o) {
                  var s = e.getViewOfComponentModel(a);
                  if (s && s.__alive) if (s.updateTransform) {
                    var l = s.updateTransform(a, n, r, t);
                    l && l.update && i.push(s);
                  } else i.push(s);
                }
              });
              var o = X();
              n.eachSeries(function (i) {
                var a = e._chartsMap[i.__viewId];
                if (a.updateTransform) {
                  var s = a.updateTransform(i, n, r, t);
                  s && s.update && o.set(i.uid, 1);
                } else o.set(i.uid, 1);
              }), PS(n), this._scheduler.performVisualTasks(n, t, {
                setDirty: !0,
                dirtyMap: o
              }), RS(this, n, r, t, o), ES(n, this._api);
            }
          },
          updateView: function updateView(t) {
            var e = this._model;
            e && (e.setUpdatePayload(t), bw.markUpdateMethod(t, "updateView"), PS(e), this._scheduler.performVisualTasks(e, t, {
              setDirty: !0
            }), LS(this, this._model, this._api, t), ES(e, this._api));
          },
          updateVisual: function updateVisual(t) {
            var e = this,
              n = this._model;
            n && (n.setUpdatePayload(t), n.eachSeries(function (t) {
              t.getData().clearAllVisual();
            }), bw.markUpdateMethod(t, "updateVisual"), PS(n), this._scheduler.performVisualTasks(n, t, {
              visualType: "visual",
              setDirty: !0
            }), n.eachComponent(function (r, i) {
              if ("series" !== r) {
                var o = e.getViewOfComponentModel(i);
                o && o.__alive && o.updateVisual(i, n, e._api, t);
              }
            }), n.eachSeries(function (r) {
              var i = e._chartsMap[r.__viewId];
              i.updateVisual(r, n, e._api, t);
            }), ES(n, this._api));
          },
          updateLayout: function updateLayout(t) {
            SS.update.call(this, t);
          }
        }, MS = function MS(t, e, n, r) {
          if (!t._disposed) for (var i, o = t._model, a = t._coordSysMgr.getCoordinateSystems(), s = qo(o, n), l = 0; l < a.length; l++) {
            var u = a[l];
            if (u[e] && null != (i = u[e](o, s, r))) return i;
          }
        }, TS = function TS(t, e) {
          var n = t._chartsMap,
            r = t._scheduler;
          e.eachSeries(function (t) {
            r.updateStreamModes(t, n[t.__viewId]);
          });
        }, CS = function CS(t, e) {
          var n = this,
            r = this.getModel(),
            i = t.type,
            o = t.escapeConnect,
            a = US[i],
            s = a.actionInfo,
            l = (s.update || "update").split(":"),
            u = l.pop(),
            f = null != l[0] && Ko(l[0]);
          this[hS] = !0;
          var p = [t],
            d = !1;
          t.batch && (d = !0, p = y(t.batch, function (e) {
            return e = c(h({}, e), t), e.batch = null, e;
          }));
          var g,
            v = [],
            m = qa(t),
            _ = Xa(t) || m;
          if (Wb(p, function (t) {
            g = a.action(t, n._model, n._api), g = g || h({}, t), g.type = s.event || g.type, v.push(g), _ ? (bS(n, u, t, "series"), NS(n)) : f && bS(n, u, t, f.main, f.sub);
          }), "none" === u || _ || f || (this[cS] ? (xS(this), SS.update.call(this, t), this[cS] = !1) : SS[u].call(this, t)), g = d ? {
            type: s.event || i,
            escapeConnect: o,
            batch: v
          } : v[0], this[hS] = !1, !e) {
            var x = this._messageCenter;
            if (x.trigger(g.type, g), m) {
              var w = {
                type: "selectchanged",
                escapeConnect: o,
                selected: Fa(r),
                isFromClick: t.isFromClick || !1,
                fromAction: t.type,
                fromActionPayload: t
              };
              x.trigger(w.type, w);
            }
          }
        }, kS = function kS(t) {
          for (var e = this._pendingActions; e.length;) {
            var n = e.shift();
            CS.call(this, n, t);
          }
        }, DS = function DS(t) {
          !t && this.trigger("updated");
        }, IS = function IS(t, e) {
          t.on("rendered", function (n) {
            e.trigger("rendered", n), !t.animation.isFinished() || e[cS] || e._scheduler.unfinished || e._pendingActions.length || e.trigger("finished");
          });
        }, AS = function AS(t, e) {
          t.on("mouseover", function (t) {
            var n = t.target,
              r = _c(n, Ga);
            if (r) {
              var i = Jm(r);
              Ra(i.seriesIndex, i.focus, i.blurScope, e._api, !0), Ma(r, t), NS(e);
            }
          }).on("mouseout", function (t) {
            var n = t.target,
              r = _c(n, Ga);
            if (r) {
              var i = Jm(r);
              Ra(i.seriesIndex, i.focus, i.blurScope, e._api, !1), Ta(r, t), NS(e);
            }
          }).on("click", function (t) {
            var n = t.target,
              r = _c(n, function (t) {
                return null != Jm(t).dataIndex;
              }, !0);
            if (r) {
              var i = r.selected ? "unselect" : "select",
                o = Jm(r);
              e._api.dispatchAction({
                type: i,
                dataType: o.dataType,
                dataIndexInside: o.dataIndex,
                seriesIndex: o.seriesIndex,
                isFromClick: !0
              });
            }
          });
        }, PS = function PS(t) {
          t.clearColorPalette(), t.eachSeries(function (t) {
            t.clearColorPalette();
          });
        }, LS = function LS(t, e, n, r) {
          OS(t, e, n, r), Wb(t._chartsViews, function (t) {
            t.__alive = !1;
          }), RS(t, e, n, r), Wb(t._chartsViews, function (t) {
            t.__alive || t.remove(e, n);
          });
        }, OS = function OS(t, e, n, r, s) {
          Wb(s || t._componentsViews, function (t) {
            var s = t.__model;
            o(s, t), t.render(s, e, n, r), i(s, t), a(s, t);
          });
        }, RS = function RS(t, e, s, l, u) {
          var h = t._scheduler,
            c = t._labelManager;
          c.clearLabels();
          var f = !1;
          e.eachSeries(function (e) {
            var n = t._chartsMap[e.__viewId];
            n.__alive = !0;
            var i = n.renderTask;
            h.updatePayload(i, l), o(e, n), u && u.get(e.uid) && i.dirty(), i.perform(h.getPerformArgs(i)) && (f = !0), e.__transientTransitionOpt = null, n.group.silent = !!e.get("silent"), r(e, n), za(e), c.addLabelsOfSeries(n);
          }), h.unfinished = f || h.unfinished, c.updateLayoutConfig(s), c.layout(s), c.processLabelsOverall(), e.eachSeries(function (e) {
            var n = t._chartsMap[e.__viewId];
            i(e, n), a(e, n);
          }), n(t, e);
        }, ES = function ES(t, e) {
          Wb(ZS, function (n) {
            n(t, e);
          });
        }, NS = function NS(t) {
          t[fS] = !0, t.getZr().wakeUp();
        }, HS = function HS(e) {
          e[fS] && (e.getZr().storage.traverse(function (e) {
            hs(e) || t(e);
          }), e[fS] = !1);
        }, BS = function BS(t) {
          return new (function (n) {
            function r() {
              return null !== n && n.apply(this, arguments) || this;
            }
            return e(r, n), r.prototype.getCoordinateSystems = function () {
              return t._coordSysMgr.getCoordinateSystems();
            }, r.prototype.getComponentByElement = function (e) {
              for (; e;) {
                var n = e.__ecComponentInfo;
                if (null != n) return t._model.getComponent(n.mainType, n.index);
                e = e.parent;
              }
            }, r.prototype.enterEmphasis = function (e, n) {
              Ca(e, n), NS(t);
            }, r.prototype.leaveEmphasis = function (e, n) {
              ka(e, n), NS(t);
            }, r.prototype.enterBlur = function (e) {
              Da(e), NS(t);
            }, r.prototype.leaveBlur = function (e) {
              Ia(e), NS(t);
            }, r.prototype.enterSelect = function (e) {
              Aa(e), NS(t);
            }, r.prototype.leaveSelect = function (e) {
              Pa(e), NS(t);
            }, r.prototype.getModel = function () {
              return t.getModel();
            }, r.prototype.getViewOfComponentModel = function (e) {
              return t.getViewOfComponentModel(e);
            }, r.prototype.getViewOfSeriesModel = function (e) {
              return t.getViewOfSeriesModel(e);
            }, r;
          }(Ux))(t);
        }, zS = function zS(t) {
          function e(t, e) {
            for (var n = 0; n < t.length; n++) {
              var r = t[n];
              r[dS] = e;
            }
          }
          Wb(qS, function (n, r) {
            t._messageCenter.on(r, function (n) {
              if (tM[t.group] && t[dS] !== gS) {
                if (n && n.escapeConnect) return;
                var r = t.makeActionFromEvent(n),
                  i = [];
                Wb(JS, function (e) {
                  e !== t && e.group === t.group && i.push(e);
                }), e(i, gS), Wb(i, function (t) {
                  t[dS] !== vS && t.dispatchAction(r);
                }), e(i, yS);
              }
            });
          });
        }, FS = function FS(t, e) {
          var n = t._model;
          v(To(e), function (t) {
            var e,
              r = t.from,
              i = t.to;
            null == i && Mo(e);
            var o = {
                includeMainTypes: ["series"],
                enableAll: !1,
                enableNone: !1
              },
              a = r ? qo(n, r, o) : null,
              s = qo(n, i, o),
              l = s.seriesModel;
            null == l && (e = ""), a && a.seriesModel !== l && (e = ""), null != e && Mo(e), l.__transientTransitionOpt = {
              from: r ? r.dimension : null,
              to: i.dimension,
              dividingMethod: t.dividingMethod
            };
          });
        };
      }(), n;
    }(ad),
    WS = VS.prototype;
  WS.on = Ic("on"), WS.off = Ic("off"), WS.one = function (t, e, n) {
    function r() {
      for (var n = [], o = 0; o < arguments.length; o++) {
        n[o] = arguments[o];
      }
      e && e.apply && e.apply(this, n), i.off(t, r);
    }
    var i = this;
    this.on.call(this, t, r, n);
  };
  var GS = ["click", "dblclick", "mouseover", "mouseout", "mousemove", "mousedown", "mouseup", "globalout", "contextmenu"],
    US = {},
    qS = {},
    XS = [],
    YS = [],
    jS = [],
    ZS = [],
    KS = [],
    $S = {},
    QS = {},
    JS = {},
    tM = {},
    eM = +new Date() - 0,
    nM = +new Date() - 0,
    rM = "_echarts_instance_",
    iM = Rc,
    oM = Eu;
  Yc(eS, Dw), Yc(iS, Aw), Yc(iS, Pw), Yc(eS, Kw), Yc(iS, $w), Yc(lS, Dc), Nc(hu), Hc(Zb, cu), Zc("default", lh), Gc({
    type: h_,
    event: h_,
    update: h_
  }, K), Gc({
    type: c_,
    event: c_,
    update: c_
  }, K), Gc({
    type: f_,
    event: f_,
    update: f_
  }, K), Gc({
    type: p_,
    event: p_,
    update: p_
  }, K), Gc({
    type: d_,
    event: d_,
    update: d_
  }, K), Fc("light", Fw), Fc("dark", Gw);
  var aM,
    sM,
    lM,
    uM,
    hM,
    cM,
    fM,
    pM,
    dM,
    gM,
    vM,
    yM,
    mM,
    _M,
    xM = {},
    wM = function () {
      function t(t, e, n, r, i, o) {
        this._old = t, this._new = e, this._oldKeyGetter = n || of, this._newKeyGetter = r || of, this.context = i, this._diffModeMultiple = "multiple" === o;
      }
      return t.prototype.add = function (t) {
        return this._add = t, this;
      }, t.prototype.update = function (t) {
        return this._update = t, this;
      }, t.prototype.updateManyToOne = function (t) {
        return this._updateManyToOne = t, this;
      }, t.prototype.updateOneToMany = function (t) {
        return this._updateOneToMany = t, this;
      }, t.prototype.remove = function (t) {
        return this._remove = t, this;
      }, t.prototype.execute = function () {
        this[this._diffModeMultiple ? "_executeMultiple" : "_executeOneToOne"]();
      }, t.prototype._executeOneToOne = function () {
        var t = this._old,
          e = this._new,
          n = {},
          r = new Array(t.length),
          i = new Array(e.length);
        this._initIndexMap(t, null, r, "_oldKeyGetter"), this._initIndexMap(e, n, i, "_newKeyGetter");
        for (var o = 0; o < t.length; o++) {
          var a = r[o],
            s = n[a],
            l = rf(s);
          if (l > 1) {
            var u = s.shift();
            1 === s.length && (n[a] = s[0]), this._update && this._update(u, o);
          } else 1 === l ? (n[a] = null, this._update && this._update(s, o)) : this._remove && this._remove(o);
        }
        this._performRestAdd(i, n);
      }, t.prototype._executeMultiple = function () {
        var t = this._old,
          e = this._new,
          n = {},
          r = {},
          i = [],
          o = [];
        this._initIndexMap(t, n, i, "_oldKeyGetter"), this._initIndexMap(e, r, o, "_newKeyGetter");
        for (var a = 0; a < i.length; a++) {
          var s = i[a],
            l = n[s],
            u = r[s],
            h = rf(l),
            c = rf(u);
          if (h > 1 && 1 === c) this._updateManyToOne && this._updateManyToOne(u, l), r[s] = null;else if (1 === h && c > 1) this._updateOneToMany && this._updateOneToMany(u, l), r[s] = null;else if (1 === h && 1 === c) this._update && this._update(u, l), r[s] = null;else if (h > 1) for (var f = 0; h > f; f++) {
            this._remove && this._remove(l[f]);
          } else this._remove && this._remove(l);
        }
        this._performRestAdd(o, r);
      }, t.prototype._performRestAdd = function (t, e) {
        for (var n = 0; n < t.length; n++) {
          var r = t[n],
            i = e[r],
            o = rf(i);
          if (o > 1) for (var a = 0; o > a; a++) {
            this._add && this._add(i[a]);
          } else 1 === o && this._add && this._add(i);
          e[r] = null;
        }
      }, t.prototype._initIndexMap = function (t, e, n, r) {
        for (var i = this._diffModeMultiple, o = 0; o < t.length; o++) {
          var a = "_ec_" + this[r](t[o], o);
          if (i || (n[o] = a), e) {
            var s = e[a],
              l = rf(s);
            0 === l ? (e[a] = o, i && n.push(a)) : 1 === l ? e[a] = [s, o] : s.push(o);
          }
        }
      }, t;
    }(),
    bM = function () {
      function t(t) {
        this.otherDims = {}, null != t && h(this, t);
      }
      return t;
    }(),
    SM = Math.floor,
    MM = I,
    TM = y,
    CM = "undefined",
    kM = -1,
    DM = "e\x00\x00",
    IM = {
      "float": (typeof Float64Array === "undefined" ? "undefined" : _typeof(Float64Array)) === CM ? Array : Float64Array,
      "int": (typeof Int32Array === "undefined" ? "undefined" : _typeof(Int32Array)) === CM ? Array : Int32Array,
      ordinal: Array,
      number: Array,
      time: Array
    },
    AM = (typeof Uint32Array === "undefined" ? "undefined" : _typeof(Uint32Array)) === CM ? Array : Uint32Array,
    PM = (typeof Int32Array === "undefined" ? "undefined" : _typeof(Int32Array)) === CM ? Array : Int32Array,
    LM = (typeof Uint16Array === "undefined" ? "undefined" : _typeof(Uint16Array)) === CM ? Array : Uint16Array,
    OM = ["hasItemOption", "_nameList", "_idList", "_invertedIndicesMap", "_rawData", "_dimValueGetter", "_count", "_rawCount", "_nameDimIdx", "_idDimIdx", "_nameRepeatCount"],
    RM = ["_extent", "_approximateExtent", "_rawExtent"],
    EM = function () {
      function t(t, e) {
        this.type = "list", this._count = 0, this._rawCount = 0, this._storage = {}, this._storageArr = [], this._nameList = [], this._idList = [], this._visual = {}, this._layout = {}, this._itemVisuals = [], this._itemLayouts = [], this._graphicEls = [], this._rawExtent = {}, this._extent = {}, this._approximateExtent = {}, this._calculationInfo = {}, this.hasItemOption = !0, this.TRANSFERABLE_METHODS = ["cloneShallow", "downSample", "lttbDownSample", "map"], this.CHANGABLE_METHODS = ["filterSelf", "selectRange"], this.DOWNSAMPLE_METHODS = ["downSample", "lttbDownSample"], this.getRawIndex = hM, t = t || ["x", "y"];
        for (var n = {}, r = [], i = {}, o = 0; o < t.length; o++) {
          var a = t[o],
            s = C(a) ? new bM({
              name: a
            }) : a instanceof bM ? a : new bM(a),
            l = s.name;
          s.type = s.type || "float", s.coordDim || (s.coordDim = l, s.coordDimIndex = 0);
          var u = s.otherDims = s.otherDims || {};
          r.push(l), n[l] = s, s.index = o, s.createInvertedIndices && (i[l] = []), 0 === u.itemName && (this._nameDimIdx = o, this._nameOrdinalMeta = s.ordinalMeta), 0 === u.itemId && (this._idDimIdx = o, this._idOrdinalMeta = s.ordinalMeta);
        }
        this.dimensions = r, this._dimensionInfos = n, this.hostModel = e, this._dimensionsSummary = af(this), this._invertedIndicesMap = i, this.userOutput = this._dimensionsSummary.userOutput;
      }
      return t.prototype.getDimension = function (t) {
        return ("number" == typeof t || !isNaN(t) && !this._dimensionInfos.hasOwnProperty(t)) && (t = this.dimensions[t]), t;
      }, t.prototype.getDimensionInfo = function (t) {
        return this._dimensionInfos[this.getDimension(t)];
      }, t.prototype.getDimensionsOnCoord = function () {
        return this._dimensionsSummary.dataDimsOnCoord.slice();
      }, t.prototype.mapDimension = function (t, e) {
        var n = this._dimensionsSummary;
        if (null == e) return n.encodeFirstDimNotExtra[t];
        var r = n.encode[t];
        return r ? r[e] : null;
      }, t.prototype.mapDimensionsAll = function (t) {
        var e = this._dimensionsSummary,
          n = e.encode[t];
        return (n || []).slice();
      }, t.prototype.initData = function (t, e, n) {
        var r = pu(t) || g(t),
          i = r ? new nw(t, this.dimensions.length) : t;
        this._rawData = i;
        var o = i.getSource().sourceFormat;
        this._storage = {}, this._indices = null, this._dontMakeIdFromName = null != this._idDimIdx || o === kx || !!i.fillStorage, this._nameList = (e || []).slice(), this._idList = [], this._nameRepeatCount = {}, n || (this.hasItemOption = !1), this.defaultDimValueGetter = aM[o], this._dimValueGetter = n = n || this.defaultDimValueGetter, this._dimValueGetterArrayRows = aM.arrayRows, this._rawExtent = {}, this._initDataFromProvider(0, i.count()), i.pure && (this.hasItemOption = !1);
      }, t.prototype.getProvider = function () {
        return this._rawData;
      }, t.prototype.appendData = function (t) {
        var e = this._rawData,
          n = this.count();
        e.appendData(t);
        var r = e.count();
        e.persistent || (r += n), this._initDataFromProvider(n, r, !0);
      }, t.prototype.appendValues = function (t, e) {
        for (var n = this._storage, r = this.dimensions, i = r.length, o = this._rawExtent, a = this.count(), s = a + Math.max(t.length, e ? e.length : 0), l = 0; i > l; l++) {
          var u = r[l];
          o[u] || (o[u] = yM()), uM(n, this._dimensionInfos[u], s, !0);
        }
        for (var h = TM(r, function (t) {
            return o[t];
          }), c = this._storageArr = TM(r, function (t) {
            return n[t];
          }), f = [], p = a; s > p; p++) {
          for (var d = p - a, g = 0; i > g; g++) {
            var u = r[g],
              v = this._dimValueGetterArrayRows(t[d] || f, u, d, g);
            c[g][p] = v;
            var y = h[g];
            v < y[0] && (y[0] = v), v > y[1] && (y[1] = v);
          }
          e && (this._nameList[p] = e[d], this._dontMakeIdFromName || dM(this, p));
        }
        this._rawCount = this._count = s, this._extent = {}, sM(this);
      }, t.prototype._initDataFromProvider = function (t, e, n) {
        if (!(t >= e)) {
          for (var r = this._rawData, i = this._storage, o = this.dimensions, a = o.length, s = this._dimensionInfos, l = this._nameList, u = this._idList, h = this._rawExtent, c = r.getSource().sourceFormat, f = c === Sx, p = 0; a > p; p++) {
            var d = o[p];
            h[d] || (h[d] = yM()), uM(i, s[d], e, n);
          }
          var g = this._storageArr = TM(o, function (t) {
              return i[t];
            }),
            v = TM(o, function (t) {
              return h[t];
            });
          if (r.fillStorage) r.fillStorage(t, e, g, v);else for (var y = [], m = t; e > m; m++) {
            y = r.getItem(m, y);
            for (var _ = 0; a > _; _++) {
              var d = o[_],
                x = g[_],
                w = this._dimValueGetter(y, d, m, _);
              x[m] = w;
              var b = v[_];
              w < b[0] && (b[0] = w), w > b[1] && (b[1] = w);
            }
            if (f && !r.pure && y) {
              var S = y.name;
              null == l[m] && null != S && (l[m] = Fo(S, null));
              var M = y.id;
              null == u[m] && null != M && (u[m] = Fo(M, null));
            }
            this._dontMakeIdFromName || dM(this, m);
          }
          !r.persistent && r.clean && r.clean(), this._rawCount = this._count = e, this._extent = {}, sM(this);
        }
      }, t.prototype.count = function () {
        return this._count;
      }, t.prototype.getIndices = function () {
        var t,
          e = this._indices;
        if (e) {
          var n = e.constructor,
            r = this._count;
          if (n === Array) {
            t = new n(r);
            for (var i = 0; r > i; i++) {
              t[i] = e[i];
            }
          } else t = new n(e.buffer, 0, r);
        } else {
          var n = lM(this);
          t = new n(this.count());
          for (var i = 0; i < t.length; i++) {
            t[i] = i;
          }
        }
        return t;
      }, t.prototype.getByDimIdx = function (t, e) {
        if (!(e >= 0 && e < this._count)) return 0 / 0;
        var n = this._storageArr[t];
        return n ? n[this.getRawIndex(e)] : 0 / 0;
      }, t.prototype.get = function (t, e) {
        if (!(e >= 0 && e < this._count)) return 0 / 0;
        var n = this._storage[t];
        return n ? n[this.getRawIndex(e)] : 0 / 0;
      }, t.prototype.getByRawIndex = function (t, e) {
        if (!(e >= 0 && e < this._rawCount)) return 0 / 0;
        var n = this._storage[t];
        return n ? n[e] : 0 / 0;
      }, t.prototype.getValues = function (t, e) {
        var n = [];
        M(t) || (e = t, t = this.dimensions);
        for (var r = 0, i = t.length; i > r; r++) {
          n.push(this.get(t[r], e));
        }
        return n;
      }, t.prototype.hasValue = function (t) {
        for (var e = this._dimensionsSummary.dataDimsOnCoord, n = 0, r = e.length; r > n; n++) {
          if (isNaN(this.get(e[n], t))) return !1;
        }
        return !0;
      }, t.prototype.getDataExtent = function (t) {
        t = this.getDimension(t);
        var e = this._storage[t],
          n = yM();
        if (!e) return n;
        var r,
          i = this.count(),
          o = !this._indices;
        if (o) return this._rawExtent[t].slice();
        if (r = this._extent[t]) return r.slice();
        r = n;
        for (var a = r[0], s = r[1], l = 0; i > l; l++) {
          var u = this.getRawIndex(l),
            h = e[u];
          a > h && (a = h), h > s && (s = h);
        }
        return r = [a, s], this._extent[t] = r, r;
      }, t.prototype.getApproximateExtent = function (t) {
        return t = this.getDimension(t), this._approximateExtent[t] || this.getDataExtent(t);
      }, t.prototype.setApproximateExtent = function (t, e) {
        e = this.getDimension(e), this._approximateExtent[e] = t.slice();
      }, t.prototype.getCalculationInfo = function (t) {
        return this._calculationInfo[t];
      }, t.prototype.setCalculationInfo = function (t, e) {
        MM(t) ? h(this._calculationInfo, t) : this._calculationInfo[t] = e;
      }, t.prototype.getSum = function (t) {
        var e = this._storage[t],
          n = 0;
        if (e) for (var r = 0, i = this.count(); i > r; r++) {
          var o = this.get(t, r);
          isNaN(o) || (n += o);
        }
        return n;
      }, t.prototype.getMedian = function (t) {
        var e = [];
        this.each(t, function (t) {
          isNaN(t) || e.push(t);
        });
        var n = e.sort(function (t, e) {
            return t - e;
          }),
          r = this.count();
        return 0 === r ? 0 : r % 2 === 1 ? n[(r - 1) / 2] : (n[r / 2] + n[r / 2 - 1]) / 2;
      }, t.prototype.rawIndexOf = function (t, e) {
        var n = t && this._invertedIndicesMap[t],
          r = n[e];
        return null == r || isNaN(r) ? kM : r;
      }, t.prototype.indexOfName = function (t) {
        for (var e = 0, n = this.count(); n > e; e++) {
          if (this.getName(e) === t) return e;
        }
        return -1;
      }, t.prototype.indexOfRawIndex = function (t) {
        if (t >= this._rawCount || 0 > t) return -1;
        if (!this._indices) return t;
        var e = this._indices,
          n = e[t];
        if (null != n && n < this._count && n === t) return t;
        for (var r = 0, i = this._count - 1; i >= r;) {
          var o = (r + i) / 2 | 0;
          if (e[o] < t) r = o + 1;else {
            if (!(e[o] > t)) return o;
            i = o - 1;
          }
        }
        return -1;
      }, t.prototype.indicesOfNearest = function (t, e, n) {
        var r = this._storage,
          i = r[t],
          o = [];
        if (!i) return o;
        null == n && (n = 1 / 0);
        for (var a = 1 / 0, s = -1, l = 0, u = 0, h = this.count(); h > u; u++) {
          var c = this.getRawIndex(u),
            f = e - i[c],
            p = Math.abs(f);
          n >= p && ((a > p || p === a && f >= 0 && 0 > s) && (a = p, s = f, l = 0), f === s && (o[l++] = u));
        }
        return o.length = l, o;
      }, t.prototype.getRawDataItem = function (t) {
        if (this._rawData.persistent) return this._rawData.getItem(this.getRawIndex(t));
        for (var e = [], n = 0; n < this.dimensions.length; n++) {
          var r = this.dimensions[n];
          e.push(this.get(r, t));
        }
        return e;
      }, t.prototype.getName = function (t) {
        var e = this.getRawIndex(t),
          n = this._nameList[e];
        return null == n && null != this._nameDimIdx && (n = pM(this, this._nameDimIdx, this._nameOrdinalMeta, e)), null == n && (n = ""), n;
      }, t.prototype.getId = function (t) {
        return fM(this, this.getRawIndex(t));
      }, t.prototype.each = function (t, e, n, r) {
        var i = this;
        if (this._count) {
          "function" == typeof t && (r = n, n = e, e = t, t = []);
          for (var o = n || r || this, a = TM(gM(t), this.getDimension, this), s = a.length, l = TM(a, function (t) {
              return i._dimensionInfos[t].index;
            }), u = this._storageArr, h = 0, c = this.count(); c > h; h++) {
            var f = this.getRawIndex(h);
            switch (s) {
              case 0:
                e.call(o, h);
                break;
              case 1:
                e.call(o, u[l[0]][f], h);
                break;
              case 2:
                e.call(o, u[l[0]][f], u[l[1]][f], h);
                break;
              default:
                for (var p = 0, d = []; s > p; p++) {
                  d[p] = u[l[p]][f];
                }
                d[p] = h, e.apply(o, d);
            }
          }
        }
      }, t.prototype.filterSelf = function (t, e, n, r) {
        var i = this;
        if (this._count) {
          "function" == typeof t && (r = n, n = e, e = t, t = []);
          for (var o = n || r || this, a = TM(gM(t), this.getDimension, this), s = this.count(), l = lM(this), u = new l(s), h = [], c = a.length, f = 0, p = TM(a, function (t) {
              return i._dimensionInfos[t].index;
            }), d = p[0], g = this._storageArr, v = 0; s > v; v++) {
            var y = void 0,
              m = this.getRawIndex(v);
            if (0 === c) y = e.call(o, v);else if (1 === c) {
              var _ = g[d][m];
              y = e.call(o, _, v);
            } else {
              for (var x = 0; c > x; x++) {
                h[x] = g[p[x]][m];
              }
              h[x] = v, y = e.apply(o, h);
            }
            y && (u[f++] = m);
          }
          return s > f && (this._indices = u), this._count = f, this._extent = {}, this.getRawIndex = this._indices ? cM : hM, this;
        }
      }, t.prototype.selectRange = function (t) {
        var e = this,
          n = this._count;
        if (n) {
          var r = [];
          for (var i in t) {
            t.hasOwnProperty(i) && r.push(i);
          }
          var o = r.length;
          if (o) {
            var a = this.count(),
              s = lM(this),
              l = new s(a),
              u = 0,
              h = r[0],
              c = TM(r, function (t) {
                return e._dimensionInfos[t].index;
              }),
              f = t[h][0],
              p = t[h][1],
              d = this._storageArr,
              g = !1;
            if (!this._indices) {
              var v = 0;
              if (1 === o) {
                for (var y = d[c[0]], m = 0; n > m; m++) {
                  var _ = y[m];
                  (_ >= f && p >= _ || isNaN(_)) && (l[u++] = v), v++;
                }
                g = !0;
              } else if (2 === o) {
                for (var y = d[c[0]], x = d[c[1]], w = t[r[1]][0], b = t[r[1]][1], m = 0; n > m; m++) {
                  var _ = y[m],
                    S = x[m];
                  (_ >= f && p >= _ || isNaN(_)) && (S >= w && b >= S || isNaN(S)) && (l[u++] = v), v++;
                }
                g = !0;
              }
            }
            if (!g) if (1 === o) for (var m = 0; a > m; m++) {
              var M = this.getRawIndex(m),
                _ = d[c[0]][M];
              (_ >= f && p >= _ || isNaN(_)) && (l[u++] = M);
            } else for (var m = 0; a > m; m++) {
              for (var T = !0, M = this.getRawIndex(m), C = 0; o > C; C++) {
                var k = r[C],
                  _ = d[c[C]][M];
                (_ < t[k][0] || _ > t[k][1]) && (T = !1);
              }
              T && (l[u++] = this.getRawIndex(m));
            }
            return a > u && (this._indices = l), this._count = u, this._extent = {}, this.getRawIndex = this._indices ? cM : hM, this;
          }
        }
      }, t.prototype.mapArray = function (t, e, n, r) {
        "function" == typeof t && (r = n, n = e, e = t, t = []), n = n || r || this;
        var i = [];
        return this.each(t, function () {
          i.push(e && e.apply(this, arguments));
        }, n), i;
      }, t.prototype.map = function (t, e, n, r) {
        var i = n || r || this,
          o = TM(gM(t), this.getDimension, this),
          a = vM(this, o),
          s = a._storage;
        a._indices = this._indices, a.getRawIndex = a._indices ? cM : hM;
        for (var l = [], u = o.length, h = this.count(), c = [], f = a._rawExtent, p = 0; h > p; p++) {
          for (var d = 0; u > d; d++) {
            c[d] = this.get(o[d], p);
          }
          c[u] = p;
          var g = e && e.apply(i, c);
          if (null != g) {
            "object" != _typeof(g) && (l[0] = g, g = l);
            for (var v = this.getRawIndex(p), y = 0; y < g.length; y++) {
              var m = o[y],
                _ = g[y],
                x = f[m],
                w = s[m];
              w && (w[v] = _), _ < x[0] && (x[0] = _), _ > x[1] && (x[1] = _);
            }
          }
        }
        return a;
      }, t.prototype.downSample = function (t, e, n, r) {
        for (var i = vM(this, [t]), o = i._storage, a = [], s = SM(1 / e), l = o[t], u = this.count(), h = i._rawExtent[t], c = new (lM(this))(u), f = 0, p = 0; u > p; p += s) {
          s > u - p && (s = u - p, a.length = s);
          for (var d = 0; s > d; d++) {
            var g = this.getRawIndex(p + d);
            a[d] = l[g];
          }
          var v = n(a),
            y = this.getRawIndex(Math.min(p + r(a, v) || 0, u - 1));
          l[y] = v, v < h[0] && (h[0] = v), v > h[1] && (h[1] = v), c[f++] = y;
        }
        return i._count = f, i._indices = c, i.getRawIndex = cM, i;
      }, t.prototype.lttbDownSample = function (t, e) {
        var n,
          r,
          i,
          o = vM(this, []),
          a = o._storage,
          s = a[t],
          l = this.count(),
          u = new (lM(this))(l),
          h = 0,
          c = SM(1 / e),
          f = this.getRawIndex(0);
        u[h++] = f;
        for (var p = 1; l - 1 > p; p += c) {
          for (var d = Math.min(p + c, l - 1), g = Math.min(p + 2 * c, l), v = (g + d) / 2, y = 0, m = d; g > m; m++) {
            var _ = this.getRawIndex(m),
              x = s[_];
            isNaN(x) || (y += x);
          }
          y /= g - d;
          var w = p,
            b = Math.min(p + c, l),
            S = p - 1,
            M = s[f];
          n = -1, i = w;
          for (var m = w; b > m; m++) {
            var _ = this.getRawIndex(m),
              x = s[_];
            isNaN(x) || (r = Math.abs((S - v) * (x - M) - (S - m) * (y - M)), r > n && (n = r, i = _));
          }
          u[h++] = i, f = i;
        }
        return u[h++] = this.getRawIndex(l - 1), o._count = h, o._indices = u, o.getRawIndex = cM, o;
      }, t.prototype.getItemModel = function (t) {
        var e = this.hostModel,
          n = this.getRawDataItem(t);
        return new N_(n, e, e && e.ecModel);
      }, t.prototype.diff = function (t) {
        var e = this;
        return new wM(t ? t.getIndices() : [], this.getIndices(), function (e) {
          return fM(t, e);
        }, function (t) {
          return fM(e, t);
        });
      }, t.prototype.getVisual = function (t) {
        var e = this._visual;
        return e && e[t];
      }, t.prototype.setVisual = function (t, e) {
        this._visual = this._visual || {}, MM(t) ? h(this._visual, t) : this._visual[t] = e;
      }, t.prototype.getItemVisual = function (t, e) {
        var n = this._itemVisuals[t],
          r = n && n[e];
        return null == r ? this.getVisual(e) : r;
      }, t.prototype.hasItemVisual = function () {
        return this._itemVisuals.length > 0;
      }, t.prototype.ensureUniqueItemVisual = function (t, e) {
        var n = this._itemVisuals,
          r = n[t];
        r || (r = n[t] = {});
        var i = r[e];
        return null == i && (i = this.getVisual(e), M(i) ? i = i.slice() : MM(i) && (i = h({}, i)), r[e] = i), i;
      }, t.prototype.setItemVisual = function (t, e, n) {
        var r = this._itemVisuals[t] || {};
        this._itemVisuals[t] = r, MM(e) ? h(r, e) : r[e] = n;
      }, t.prototype.clearAllVisual = function () {
        this._visual = {}, this._itemVisuals = [];
      }, t.prototype.setLayout = function (t, e) {
        if (MM(t)) for (var n in t) {
          t.hasOwnProperty(n) && this.setLayout(n, t[n]);
        } else this._layout[t] = e;
      }, t.prototype.getLayout = function (t) {
        return this._layout[t];
      }, t.prototype.getItemLayout = function (t) {
        return this._itemLayouts[t];
      }, t.prototype.setItemLayout = function (t, e, n) {
        this._itemLayouts[t] = n ? h(this._itemLayouts[t] || {}, e) : e;
      }, t.prototype.clearItemLayouts = function () {
        this._itemLayouts.length = 0;
      }, t.prototype.setItemGraphicEl = function (t, e) {
        var n = this.hostModel;
        if (e) {
          var r = Jm(e);
          r.dataIndex = t, r.dataType = this.dataType, r.seriesIndex = n && n.seriesIndex, "group" === e.type && e.traverse(mM, e);
        }
        this._graphicEls[t] = e;
      }, t.prototype.getItemGraphicEl = function (t) {
        return this._graphicEls[t];
      }, t.prototype.eachItemGraphicEl = function (t, e) {
        v(this._graphicEls, function (n, r) {
          n && t && t.call(e, n, r);
        });
      }, t.prototype.cloneShallow = function (e) {
        if (!e) {
          var n = TM(this.dimensions, this.getDimensionInfo, this);
          e = new t(n, this.hostModel);
        }
        if (e._storage = this._storage, e._storageArr = this._storageArr, _M(e, this), this._indices) {
          var r = this._indices.constructor;
          if (r === Array) {
            var i = this._indices.length;
            e._indices = new r(i);
            for (var o = 0; i > o; o++) {
              e._indices[o] = this._indices[o];
            }
          } else e._indices = new r(this._indices);
        } else e._indices = null;
        return e.getRawIndex = e._indices ? cM : hM, e;
      }, t.prototype.wrapMethod = function (t, e) {
        var n = this[t];
        "function" == typeof n && (this.__wrappedMethods = this.__wrappedMethods || [], this.__wrappedMethods.push(t), this[t] = function () {
          var t = n.apply(this, arguments);
          return e.apply(this, [t].concat(H(arguments)));
        });
      }, t.internalField = function () {
        function e(t, e, n, r) {
          return Iu(t[r], this._dimensionInfos[e]);
        }
        function n(t) {
          var e = t.constructor;
          return e === Array ? t.slice() : new e(t);
        }
        aM = {
          arrayRows: e,
          objectRows: function objectRows(t, e) {
            return Iu(t[e], this._dimensionInfos[e]);
          },
          keyedColumns: e,
          original: function original(t, e, n, r) {
            var i = t && (null == t.value ? t : t.value);
            return !this._rawData.pure && Do(t) && (this.hasItemOption = !0), Iu(i instanceof Array ? i[r] : i, this._dimensionInfos[e]);
          },
          typedArray: function typedArray(t, e, n, r) {
            return t[r];
          }
        }, sM = function sM(t) {
          var e = t._invertedIndicesMap;
          v(e, function (n, r) {
            var i = t._dimensionInfos[r],
              o = i.ordinalMeta;
            if (o) {
              n = e[r] = new PM(o.categories.length);
              for (var a = 0; a < n.length; a++) {
                n[a] = kM;
              }
              for (var a = 0; a < t._count; a++) {
                n[t.get(r, a)] = a;
              }
            }
          });
        }, pM = function pM(t, e, n, r) {
          var i,
            o = t._storageArr[e];
          return o && (i = o[r], n && n.categories.length && (i = n.categories[i])), Fo(i, null);
        }, lM = function lM(t) {
          return t._rawCount > 65535 ? AM : LM;
        }, uM = function uM(t, e, n, r) {
          var i = IM[e.type],
            o = e.name;
          if (r) {
            var a = t[o],
              s = a && a.length;
            if (s !== n) {
              for (var l = new i(n), u = 0; s > u; u++) {
                l[u] = a[u];
              }
              t[o] = l;
            }
          } else t[o] = new i(n);
        }, hM = function hM(t) {
          return t;
        }, cM = function cM(t) {
          return t < this._count && t >= 0 ? this._indices[t] : -1;
        }, fM = function fM(t, e) {
          var n = t._idList[e];
          return null == n && null != t._idDimIdx && (n = pM(t, t._idDimIdx, t._idOrdinalMeta, e)), null == n && (n = DM + e), n;
        }, gM = function gM(t) {
          return M(t) || (t = null != t ? [t] : []), t;
        }, vM = function vM(e, r) {
          var i = e.dimensions,
            o = new t(TM(i, e.getDimensionInfo, e), e.hostModel);
          _M(o, e);
          for (var a = o._storage = {}, s = e._storage, l = o._storageArr = [], u = 0; u < i.length; u++) {
            var h = i[u];
            s[h] && (f(r, h) >= 0 ? (a[h] = n(s[h]), o._rawExtent[h] = yM(), o._extent[h] = null) : a[h] = s[h], l.push(a[h]));
          }
          return o;
        }, yM = function yM() {
          return [1 / 0, -1 / 0];
        }, mM = function mM(t) {
          var e = Jm(t),
            n = Jm(this);
          e.seriesIndex = n.seriesIndex, e.dataIndex = n.dataIndex, e.dataType = n.dataType;
        }, _M = function _M(t, e) {
          v(OM.concat(e.__wrappedMethods || []), function (n) {
            e.hasOwnProperty(n) && (t[n] = e[n]);
          }), t.__wrappedMethods = e.__wrappedMethods, v(RM, function (n) {
            t[n] = s(e[n]);
          }), t._calculationInfo = h({}, e._calculationInfo);
        }, dM = function dM(t, e) {
          var n = t._nameList,
            r = t._idList,
            i = t._nameDimIdx,
            o = t._idDimIdx,
            a = n[e],
            s = r[e];
          if (null == a && null != i && (n[e] = a = pM(t, i, t._nameOrdinalMeta, e)), null == s && null != o && (r[e] = s = pM(t, o, t._idOrdinalMeta, e)), null == s && null != a) {
            var l = t._nameRepeatCount,
              u = l[a] = (l[a] || 0) + 1;
            s = a, u > 1 && (s += "__ec__" + u), r[e] = s;
          }
        };
      }(), t;
    }(),
    BM = function () {
      function t(t) {
        this.coordSysDims = [], this.axisMap = X(), this.categoryAxisMap = X(), this.coordSysName = t;
      }
      return t;
    }(),
    zM = {
      cartesian2d: function cartesian2d(t, e, n, r) {
        var i = t.getReferringComponents("xAxis", qm).models[0],
          o = t.getReferringComponents("yAxis", qm).models[0];
        e.coordSysDims = ["x", "y"], n.set("x", i), n.set("y", o), gf(i) && (r.set("x", i), e.firstCategoryDimIndex = 0), gf(o) && (r.set("y", o), null == e.firstCategoryDimIndex && (e.firstCategoryDimIndex = 1));
      },
      singleAxis: function singleAxis(t, e, n, r) {
        var i = t.getReferringComponents("singleAxis", qm).models[0];
        e.coordSysDims = ["single"], n.set("single", i), gf(i) && (r.set("single", i), e.firstCategoryDimIndex = 0);
      },
      polar: function polar(t, e, n, r) {
        var i = t.getReferringComponents("polar", qm).models[0],
          o = i.findAxisModel("radiusAxis"),
          a = i.findAxisModel("angleAxis");
        e.coordSysDims = ["radius", "angle"], n.set("radius", o), n.set("angle", a), gf(o) && (r.set("radius", o), e.firstCategoryDimIndex = 0), gf(a) && (r.set("angle", a), null == e.firstCategoryDimIndex && (e.firstCategoryDimIndex = 1));
      },
      geo: function geo(t, e) {
        e.coordSysDims = ["lng", "lat"];
      },
      parallel: function parallel(t, e, n, r) {
        var i = t.ecModel,
          o = i.getComponent("parallel", t.get("parallelIndex")),
          a = e.coordSysDims = o.dimensions.slice();
        v(o.parallelAxisIndex, function (t, o) {
          var s = i.getComponent("parallelAxis", t),
            l = a[o];
          n.set(l, s), gf(s) && (r.set(l, s), null == e.firstCategoryDimIndex && (e.firstCategoryDimIndex = o));
        });
      }
    },
    FM = function () {
      function t(t) {
        this._setting = t || {}, this._extent = [1 / 0, -1 / 0];
      }
      return t.prototype.getSetting = function (t) {
        return this._setting[t];
      }, t.prototype.unionExtent = function (t) {
        var e = this._extent;
        t[0] < e[0] && (e[0] = t[0]), t[1] > e[1] && (e[1] = t[1]);
      }, t.prototype.unionExtentFromData = function (t, e) {
        this.unionExtent(t.getApproximateExtent(e));
      }, t.prototype.getExtent = function () {
        return this._extent.slice();
      }, t.prototype.setExtent = function (t, e) {
        var n = this._extent;
        isNaN(t) || (n[0] = t), isNaN(e) || (n[1] = e);
      }, t.prototype.isInExtentRange = function (t) {
        return this._extent[0] <= t && this._extent[1] >= t;
      }, t.prototype.isBlank = function () {
        return this._isBlank;
      }, t.prototype.setBlank = function (t) {
        this._isBlank = t;
      }, t;
    }();
  oa(FM, {
    registerWhenExtend: !0
  });
  var NM = function () {
      function t(t) {
        this.categories = t.categories || [], this._needCollect = t.needCollect, this._deduplication = t.deduplication;
      }
      return t.createByAxisModel = function (e) {
        var n = e.option,
          r = n.data,
          i = r && y(r, bf);
        return new t({
          categories: i,
          needCollect: !i,
          deduplication: n.dedplication !== !1
        });
      }, t.prototype.getOrdinal = function (t) {
        return this._getOrCreateMap().get(t);
      }, t.prototype.parseAndCollect = function (t) {
        var e,
          n = this._needCollect;
        if ("string" != typeof t && !n) return t;
        if (n && !this._deduplication) return e = this.categories.length, this.categories[e] = t, e;
        var r = this._getOrCreateMap();
        return e = r.get(t), null == e && (n ? (e = this.categories.length, this.categories[e] = t, r.set(t, e)) : e = 0 / 0), e;
      }, t.prototype._getOrCreateMap = function () {
        return this._map || (this._map = X(this.categories));
      }, t;
    }(),
    HM = io,
    VM = function (t) {
      function n(e) {
        var n = t.call(this, e) || this;
        n.type = "ordinal";
        var r = n.getSetting("ordinalMeta");
        return r || (r = new NM({})), M(r) && (r = new NM({
          categories: y(r, function (t) {
            return I(t) ? t.value : t;
          })
        })), n._ordinalMeta = r, n._categorySortInfo = [], n._extent = n.getSetting("extent") || [0, r.categories.length - 1], n;
      }
      return e(n, t), n.prototype.parse = function (t) {
        return "string" == typeof t ? this._ordinalMeta.getOrdinal(t) : Math.round(t);
      }, n.prototype.contain = function (t) {
        return t = this.parse(t), kf(t, this._extent) && null != this._ordinalMeta.categories[t];
      }, n.prototype.normalize = function (t) {
        return t = this.getCategoryIndex(this.parse(t)), Df(t, this._extent);
      }, n.prototype.scale = function (t) {
        return t = this.getCategoryIndex(t), Math.round(If(t, this._extent));
      }, n.prototype.getTicks = function () {
        for (var t = [], e = this._extent, n = e[0]; n <= e[1];) {
          t.push({
            value: this.getCategoryIndex(n)
          }), n++;
        }
        return t;
      }, n.prototype.getMinorTicks = function () {}, n.prototype.setCategorySortInfo = function (t) {
        this._categorySortInfo = t;
      }, n.prototype.getCategorySortInfo = function () {
        return this._categorySortInfo;
      }, n.prototype.getCategoryIndex = function (t) {
        return this._categorySortInfo.length ? this._categorySortInfo[t].beforeSortIndex : t;
      }, n.prototype.getRawIndex = function (t) {
        return this._categorySortInfo.length ? this._categorySortInfo[t].ordinalNumber : t;
      }, n.prototype.getLabel = function (t) {
        if (!this.isBlank()) {
          var e = this.getRawIndex(t.value),
            n = this._ordinalMeta.categories[e];
          return null == n ? "" : n + "";
        }
      }, n.prototype.count = function () {
        return this._extent[1] - this._extent[0] + 1;
      }, n.prototype.unionExtentFromData = function (t, e) {
        this.unionExtent(t.getApproximateExtent(e));
      }, n.prototype.isInExtentRange = function (t) {
        return t = this.getCategoryIndex(t), this._extent[0] <= t && this._extent[1] >= t;
      }, n.prototype.getOrdinalMeta = function () {
        return this._ordinalMeta;
      }, n.prototype.niceTicks = function () {}, n.prototype.niceExtent = function () {}, n.type = "ordinal", n;
    }(FM);
  FM.registerClass(VM);
  var WM = io,
    GM = function (t) {
      function n() {
        var e = null !== t && t.apply(this, arguments) || this;
        return e.type = "interval", e._interval = 0, e._intervalPrecision = 2, e;
      }
      return e(n, t), n.prototype.parse = function (t) {
        return t;
      }, n.prototype.contain = function (t) {
        return kf(t, this._extent);
      }, n.prototype.normalize = function (t) {
        return Df(t, this._extent);
      }, n.prototype.scale = function (t) {
        return If(t, this._extent);
      }, n.prototype.setExtent = function (t, e) {
        var n = this._extent;
        isNaN(t) || (n[0] = parseFloat(t)), isNaN(e) || (n[1] = parseFloat(e));
      }, n.prototype.unionExtent = function (t) {
        var e = this._extent;
        t[0] < e[0] && (e[0] = t[0]), t[1] > e[1] && (e[1] = t[1]), this.setExtent(e[0], e[1]);
      }, n.prototype.getInterval = function () {
        return this._interval;
      }, n.prototype.setInterval = function (t) {
        this._interval = t, this._niceExtent = this._extent.slice(), this._intervalPrecision = Mf(t);
      }, n.prototype.getTicks = function (t) {
        var e = this._interval,
          n = this._extent,
          r = this._niceExtent,
          i = this._intervalPrecision,
          o = [];
        if (!e) return o;
        var a = 1e4;
        n[0] < r[0] && o.push(t ? {
          value: WM(r[0] - e, i)
        } : {
          value: n[0]
        });
        for (var s = r[0]; s <= r[1] && (o.push({
          value: s
        }), s = WM(s + e, i), s !== o[o.length - 1].value);) {
          if (o.length > a) return [];
        }
        var l = o.length ? o[o.length - 1].value : r[1];
        return n[1] > l && o.push(t ? {
          value: WM(l + e, i)
        } : {
          value: n[1]
        }), o;
      }, n.prototype.getMinorTicks = function (t) {
        for (var e = this.getTicks(!0), n = [], r = this.getExtent(), i = 1; i < e.length; i++) {
          for (var o = e[i], a = e[i - 1], s = 0, l = [], u = o.value - a.value, h = u / t; t - 1 > s;) {
            var c = WM(a.value + (s + 1) * h);
            c > r[0] && c < r[1] && l.push(c), s++;
          }
          n.push(l);
        }
        return n;
      }, n.prototype.getLabel = function (t, e) {
        if (null == t) return "";
        var n = e && e.precision;
        null == n ? n = so(t.value) || 0 : "auto" === n && (n = this._intervalPrecision);
        var r = WM(t.value, n, !0);
        return ul(r);
      }, n.prototype.niceTicks = function (t, e, n) {
        t = t || 5;
        var r = this._extent,
          i = r[1] - r[0];
        if (isFinite(i)) {
          0 > i && (i = -i, r.reverse());
          var o = Sf(r, t, e, n);
          this._intervalPrecision = o.intervalPrecision, this._interval = o.interval, this._niceExtent = o.niceTickExtent;
        }
      }, n.prototype.niceExtent = function (t) {
        var e = this._extent;
        if (e[0] === e[1]) if (0 !== e[0]) {
          var n = e[0];
          t.fixMax ? e[0] -= n / 2 : (e[1] += n / 2, e[0] -= n / 2);
        } else e[1] = 1;
        var r = e[1] - e[0];
        isFinite(r) || (e[0] = 0, e[1] = 1), this.niceTicks(t.splitNumber, t.minInterval, t.maxInterval);
        var i = this._interval;
        t.fixMin || (e[0] = WM(Math.floor(e[0] / i) * i)), t.fixMax || (e[1] = WM(Math.ceil(e[1] / i) * i));
      }, n.type = "interval", n;
    }(FM);
  FM.registerClass(GM);
  var UM = "__ec_stack_",
    qM = .5,
    XM = "undefined" != typeof Float32Array ? Float32Array : Array,
    YM = ({
      seriesType: "bar",
      plan: th(),
      reset: function reset(t) {
        if (zf(t) && Ff(t)) {
          var e = t.getData(),
            n = t.coordinateSystem,
            r = n.master.getRect(),
            i = n.getBaseAxis(),
            o = n.getOtherAxis(i),
            a = e.mapDimension(o.dim),
            s = e.mapDimension(i.dim),
            l = o.isHorizontal(),
            u = l ? 0 : 1,
            h = Bf(Rf([t]), i, t).width;
          return h > qM || (h = qM), {
            progress: function progress(t, e) {
              for (var c, f = t.count, p = new XM(2 * f), d = new XM(2 * f), g = new XM(f), v = [], y = [], m = 0, _ = 0; null != (c = t.next());) {
                y[u] = e.get(a, c), y[1 - u] = e.get(s, c), v = n.dataToPoint(y, null, v), d[m] = l ? r.x + r.width : v[0], p[m++] = v[0], d[m] = l ? v[1] : r.y + r.height, p[m++] = v[1], g[_++] = c;
              }
              e.setLayout({
                largePoints: p,
                largeDataIndices: g,
                largeBackgroundPoints: d,
                barWidth: h,
                valueAxisStart: Nf(i, o, !1),
                backgroundStart: l ? r.x : r.y,
                valueAxisHorizontal: l
              });
            }
          };
        }
      }
    }, function (t, e, n, r) {
      for (; r > n;) {
        var i = n + r >>> 1;
        t[i][1] < e ? n = i + 1 : r = i;
      }
      return n;
    }),
    jM = function (t) {
      function n(e) {
        var n = t.call(this, e) || this;
        return n.type = "time", n;
      }
      return e(n, t), n.prototype.getLabel = function (t) {
        var e = this.getSetting("useUTC");
        return Us(t.value, nx[Gs(Vs(this._minLevelUnit))] || nx.second, e, this.getSetting("locale"));
      }, n.prototype.getFormattedLabel = function (t, e, n) {
        var r = this.getSetting("useUTC"),
          i = this.getSetting("locale");
        return qs(t, e, n, i, r);
      }, n.prototype.getTicks = function () {
        var t = this._interval,
          e = this._extent,
          n = [];
        if (!t) return n;
        n.push({
          value: e[0],
          level: 0
        });
        var r = this.getSetting("useUTC"),
          i = Yf(this._minLevelUnit, this._approxInterval, r, e);
        return n = n.concat(i), n.push({
          value: e[1],
          level: 0
        }), n;
      }, n.prototype.niceExtent = function (t) {
        var e = this._extent;
        if (e[0] === e[1] && (e[0] -= Q_, e[1] += Q_), e[1] === -1 / 0 && 1 / 0 === e[0]) {
          var n = new Date();
          e[1] = +new Date(n.getFullYear(), n.getMonth(), n.getDate()), e[0] = e[1] - Q_;
        }
        this.niceTicks(t.splitNumber, t.minInterval, t.maxInterval);
      }, n.prototype.niceTicks = function (t, e, n) {
        t = t || 10;
        var r = this._extent,
          i = r[1] - r[0];
        this._approxInterval = i / t, null != e && this._approxInterval < e && (this._approxInterval = e), null != n && this._approxInterval > n && (this._approxInterval = n);
        var o = ZM.length,
          a = Math.min(YM(ZM, this._approxInterval, 0, o), o - 1);
        this._interval = ZM[a][1], this._minLevelUnit = ZM[Math.max(a - 1, 0)][0];
      }, n.prototype.parse = function (t) {
        return "number" == typeof t ? t : +fo(t);
      }, n.prototype.contain = function (t) {
        return kf(this.parse(t), this._extent);
      }, n.prototype.normalize = function (t) {
        return Df(this.parse(t), this._extent);
      }, n.prototype.scale = function (t) {
        return If(t, this._extent);
      }, n.type = "time", n;
    }(GM),
    ZM = [["second", Z_], ["minute", K_], ["hour", $_], ["quarter-day", 6 * $_], ["half-day", 12 * $_], ["day", 1.2 * Q_], ["half-week", 3.5 * Q_], ["week", 7 * Q_], ["month", 31 * Q_], ["quarter", 95 * Q_], ["half-year", J_ / 2], ["year", J_]];
  FM.registerClass(jM);
  var KM = FM.prototype,
    $M = GM.prototype,
    QM = so,
    JM = io,
    tT = Math.floor,
    eT = Math.ceil,
    nT = Math.pow,
    rT = Math.log,
    iT = function (t) {
      function n() {
        var e = null !== t && t.apply(this, arguments) || this;
        return e.type = "log", e.base = 10, e._originalScale = new GM(), e._interval = 0, e;
      }
      return e(n, t), n.prototype.getTicks = function (t) {
        var e = this._originalScale,
          n = this._extent,
          r = e.getExtent(),
          i = $M.getTicks.call(this, t);
        return y(i, function (t) {
          var e = t.value,
            i = io(nT(this.base, e));
          return i = e === n[0] && this._fixMin ? jf(i, r[0]) : i, i = e === n[1] && this._fixMax ? jf(i, r[1]) : i, {
            value: i
          };
        }, this);
      }, n.prototype.setExtent = function (t, e) {
        var n = this.base;
        t = rT(t) / rT(n), e = rT(e) / rT(n), $M.setExtent.call(this, t, e);
      }, n.prototype.getExtent = function () {
        var t = this.base,
          e = KM.getExtent.call(this);
        e[0] = nT(t, e[0]), e[1] = nT(t, e[1]);
        var n = this._originalScale,
          r = n.getExtent();
        return this._fixMin && (e[0] = jf(e[0], r[0])), this._fixMax && (e[1] = jf(e[1], r[1])), e;
      }, n.prototype.unionExtent = function (t) {
        this._originalScale.unionExtent(t);
        var e = this.base;
        t[0] = rT(t[0]) / rT(e), t[1] = rT(t[1]) / rT(e), KM.unionExtent.call(this, t);
      }, n.prototype.unionExtentFromData = function (t, e) {
        this.unionExtent(t.getApproximateExtent(e));
      }, n.prototype.niceTicks = function (t) {
        t = t || 10;
        var e = this._extent,
          n = e[1] - e[0];
        if (!(1 / 0 === n || 0 >= n)) {
          var r = po(n),
            i = t / n * r;
          for (.5 >= i && (r *= 10); !isNaN(r) && Math.abs(r) < 1 && Math.abs(r) > 0;) {
            r *= 10;
          }
          var o = [io(eT(e[0] / r) * r), io(tT(e[1] / r) * r)];
          this._interval = r, this._niceExtent = o;
        }
      }, n.prototype.niceExtent = function (t) {
        $M.niceExtent.call(this, t), this._fixMin = t.fixMin, this._fixMax = t.fixMax;
      }, n.prototype.parse = function (t) {
        return t;
      }, n.prototype.contain = function (t) {
        return t = rT(t) / rT(this.base), kf(t, this._extent);
      }, n.prototype.normalize = function (t) {
        return t = rT(t) / rT(this.base), Df(t, this._extent);
      }, n.prototype.scale = function (t) {
        return t = If(t, this._extent), nT(this.base, t);
      }, n.type = "log", n;
    }(FM),
    oT = iT.prototype;
  oT.getMinorTicks = $M.getMinorTicks, oT.getLabel = $M.getLabel, FM.registerClass(iT);
  var aT = function () {
      function t(t, e, n) {
        this._prepareParams(t, e, n);
      }
      return t.prototype._prepareParams = function (t, e, n) {
        n[1] < n[0] && (n = [0 / 0, 0 / 0]), this._dataMin = n[0], this._dataMax = n[1];
        var r = this._isOrdinal = "ordinal" === t.type;
        this._needCrossZero = e.getNeedCrossZero && e.getNeedCrossZero();
        var i = this._modelMinRaw = e.get("min", !0);
        T(i) ? this._modelMinNum = Kf(t, i({
          min: n[0],
          max: n[1]
        })) : "dataMin" !== i && (this._modelMinNum = Kf(t, i));
        var o = this._modelMaxRaw = e.get("max", !0);
        if (T(o) ? this._modelMaxNum = Kf(t, o({
          min: n[0],
          max: n[1]
        })) : "dataMax" !== o && (this._modelMaxNum = Kf(t, o)), r) this._axisDataLen = e.getCategories().length;else {
          var a = e.get("boundaryGap"),
            s = M(a) ? a : [a || 0, a || 0];
          this._boundaryGapInner = "boolean" == typeof s[0] || "boolean" == typeof s[1] ? [0, 0] : [zn(s[0], 1), zn(s[1], 1)];
        }
      }, t.prototype.calculate = function () {
        var t = this._isOrdinal,
          e = this._dataMin,
          n = this._dataMax,
          r = this._axisDataLen,
          i = this._boundaryGapInner,
          o = t ? null : n - e || Math.abs(e),
          a = "dataMin" === this._modelMinRaw ? e : this._modelMinNum,
          s = "dataMax" === this._modelMaxRaw ? n : this._modelMaxNum,
          l = null != a,
          u = null != s;
        null == a && (a = t ? r ? 0 : 0 / 0 : e - i[0] * o), null == s && (s = t ? r ? r - 1 : 0 / 0 : n + i[1] * o), (null == a || !isFinite(a)) && (a = 0 / 0), (null == s || !isFinite(s)) && (s = 0 / 0), a > s && (a = 0 / 0, s = 0 / 0);
        var h = B(a) || B(s) || t && !r;
        this._needCrossZero && (a > 0 && s > 0 && !l && (a = 0), 0 > a && 0 > s && !u && (s = 0));
        var c = this._determinedMin,
          f = this._determinedMax;
        return null != c && (a = c, l = !0), null != f && (s = f, u = !0), {
          min: a,
          max: s,
          minFixed: l,
          maxFixed: u,
          isBlank: h
        };
      }, t.prototype.modifyDataMinMax = function (t, e) {
        this[lT[t]] = e;
      }, t.prototype.setDeterminedMinMax = function (t, e) {
        var n = sT[t];
        this[n] = e;
      }, t.prototype.freeze = function () {
        this.frozen = !0;
      }, t;
    }(),
    sT = {
      min: "_determinedMin",
      max: "_determinedMax"
    },
    lT = {
      min: "_dataMin",
      max: "_dataMax"
    },
    uT = function () {
      function t() {}
      return t.prototype.getNeedCrossZero = function () {
        var t = this.option;
        return !t.scale;
      }, t.prototype.getCoordSysModel = function () {}, t;
    }(),
    hT = {
      isDimensionStacked: yf,
      enableDataStack: vf,
      getStackedDimension: mf
    },
    cT = (Object.freeze || Object)({
      createList: op,
      getLayoutRect: wl,
      dataStack: hT,
      createScale: ap,
      mixinAxisModelCommonMethods: sp,
      getECData: Jm,
      createDimensions: pf,
      createSymbol: wc
    }),
    fT = 1e-8,
    pT = function () {
      function t(t, e, n) {
        if (this.name = t, this.geometries = e, n) n = [n[0], n[1]];else {
          var r = this.getBoundingRect();
          n = [r.x + r.width / 2, r.y + r.height / 2];
        }
        this.center = n;
      }
      return t.prototype.getBoundingRect = function () {
        var t = this._rect;
        if (t) return t;
        for (var e = Number.MAX_VALUE, n = [e, e], r = [-e, -e], i = [], o = [], a = this.geometries, s = 0; s < a.length; s++) {
          if ("polygon" === a[s].type) {
            var l = a[s].exterior;
            Cr(l, i, o), ve(n, n, i), ye(r, r, o);
          }
        }
        return 0 === s && (n[0] = n[1] = r[0] = r[1] = 0), this._rect = new tg(n[0], n[1], r[0] - n[0], r[1] - n[1]);
      }, t.prototype.contain = function (t) {
        var e = this.getBoundingRect(),
          n = this.geometries;
        if (!e.contain(t[0], t[1])) return !1;
        t: for (var r = 0, i = n.length; i > r; r++) {
          if ("polygon" === n[r].type) {
            var o = n[r].exterior,
              a = n[r].interiors;
            if (up(o, t[0], t[1])) {
              for (var s = 0; s < (a ? a.length : 0); s++) {
                if (up(a[s], t[0], t[1])) continue t;
              }
              return !0;
            }
          }
        }
        return !1;
      }, t.prototype.transformTo = function (t, e, n, r) {
        var i = this.getBoundingRect(),
          o = i.width / i.height;
        n ? r || (r = n / o) : n = o * r;
        for (var a = new tg(t, e, n, r), s = i.calculateTransform(a), l = this.geometries, u = 0; u < l.length; u++) {
          if ("polygon" === l[u].type) {
            for (var h = l[u].exterior, c = l[u].interiors, f = 0; f < h.length; f++) {
              ge(h[f], h[f], s);
            }
            for (var p = 0; p < (c ? c.length : 0); p++) {
              for (var f = 0; f < c[p].length; f++) {
                ge(c[p][f], c[p][f], s);
              }
            }
          }
        }
        i = this._rect, i.copy(a), this.center = [i.x + i.width / 2, i.y + i.height / 2];
      }, t.prototype.cloneShallow = function (e) {
        null == e && (e = this.name);
        var n = new t(e, this.geometries, this.center);
        return n._rect = this._rect, n.transformTo = null, n;
      }, t;
    }(),
    dT = Uo(),
    gT = [0, 1],
    vT = function () {
      function t(t, e, n) {
        this.onBand = !1, this.inverse = !1, this.dim = t, this.scale = e, this._extent = n || [0, 0];
      }
      return t.prototype.contain = function (t) {
        var e = this._extent,
          n = Math.min(e[0], e[1]),
          r = Math.max(e[0], e[1]);
        return t >= n && r >= t;
      }, t.prototype.containData = function (t) {
        return this.scale.contain(t);
      }, t.prototype.getExtent = function () {
        return this._extent.slice();
      }, t.prototype.getPixelPrecision = function (t) {
        return lo(t || this.scale.getExtent(), this._extent);
      }, t.prototype.setExtent = function (t, e) {
        var n = this._extent;
        n[0] = t, n[1] = e;
      }, t.prototype.dataToCoord = function (t, e) {
        var n = this._extent,
          r = this.scale;
        return t = r.normalize(t), this.onBand && "ordinal" === r.type && (n = n.slice(), kp(n, r.count())), no(t, gT, n, e);
      }, t.prototype.coordToData = function (t, e) {
        var n = this._extent,
          r = this.scale;
        this.onBand && "ordinal" === r.type && (n = n.slice(), kp(n, r.count()));
        var i = no(t, n, gT, e);
        return this.scale.scale(i);
      }, t.prototype.pointToData = function () {}, t.prototype.getTicksCoords = function (t) {
        t = t || {};
        var e = t.tickModel || this.getTickModel(),
          n = dp(this, e),
          r = n.ticks,
          i = y(r, function (t) {
            return {
              coord: this.dataToCoord("ordinal" === this.scale.type ? this.scale.getRawIndex(t) : t),
              tickValue: t
            };
          }, this),
          o = e.get("alignWithLabel");
        return Dp(this, i, o, t.clamp), i;
      }, t.prototype.getMinorTicksCoords = function () {
        if ("ordinal" === this.scale.type) return [];
        var t = this.model.getModel("minorTick"),
          e = t.get("splitNumber");
        e > 0 && 100 > e || (e = 5);
        var n = this.scale.getMinorTicks(e),
          r = y(n, function (t) {
            return y(t, function (t) {
              return {
                coord: this.dataToCoord(t),
                tickValue: t
              };
            }, this);
          }, this);
        return r;
      }, t.prototype.getViewLabels = function () {
        return pp(this).labels;
      }, t.prototype.getLabelModel = function () {
        return this.model.getModel("axisLabel");
      }, t.prototype.getTickModel = function () {
        return this.model.getModel("axisTick");
      }, t.prototype.getBandWidth = function () {
        var t = this._extent,
          e = this.scale.getExtent(),
          n = e[1] - e[0] + (this.onBand ? 1 : 0);
        0 === n && (n = 1);
        var r = Math.abs(t[1] - t[0]);
        return Math.abs(r) / n;
      }, t.prototype.calculateCategoryInterval = function () {
        return Sp(this);
      }, t;
    }(),
    yT = fp,
    mT = {};
  v(["linearMap", "round", "asc", "getPrecision", "getPrecisionSafe", "getPixelPrecision", "getPercentWithPrecision", "MAX_SAFE_INTEGER", "remRadian", "isRadianAroundZero", "parseDate", "quantity", "quantityExponent", "nice", "quantile", "reformIntervals", "isNumeric", "numericToNumber"], function (t) {
    mT[t] = Hm[t];
  });
  var _T = {};
  v(["addCommas", "toCamelCase", "normalizeCssArray", "encodeHTML", "formatTpl", "getTooltipMarker", "formatTime", "capitalFirst", "truncateText", "getTextRect"], function (t) {
    _T[t] = hx[t];
  });
  var xT = {
      parse: fo,
      format: Us
    },
    wT = {};
  v(["map", "each", "filter", "indexOf", "inherits", "reduce", "filter", "bind", "curry", "isArray", "isString", "isObject", "isFunction", "extend", "defaults", "clone", "merge"], function (t) {
    wT[t] = Qp[t];
  });
  var bT = ["extendShape", "extendPath", "makePath", "makeImage", "mergePath", "resizePath", "createIcon", "updateProps", "initProps", "getTransform", "clipPointsByRect", "clipRectByRect", "registerShape", "getShapeClass", "Group", "Image", "Text", "Circle", "Ellipse", "Sector", "Ring", "Polygon", "Polyline", "Rect", "Line", "BezierCurve", "Arc", "IncrementalDisplayable", "CompoundPath", "LinearGradient", "RadialGradient", "BoundingRect"],
    ST = {};
  v(bT, function (t) {
    ST[t] = M_[t];
  });
  var MT = function (t) {
    function n() {
      var e = null !== t && t.apply(this, arguments) || this;
      return e.type = n.type, e.visualStyleAccessPath = "itemStyle", e.useColorPaletteOnData = !0, e;
    }
    return e(n, t), n.prototype.getInitialData = function () {
      return Ip(this, ["value"]);
    }, n.type = "series.gauge", n.defaultOption = {
      zlevel: 0,
      z: 2,
      center: ["50%", "50%"],
      legendHoverLink: !0,
      radius: "75%",
      startAngle: 225,
      endAngle: -45,
      clockwise: !0,
      min: 0,
      max: 100,
      splitNumber: 10,
      axisLine: {
        show: !0,
        roundCap: !1,
        lineStyle: {
          color: [[1, "#E6EBF8"]],
          width: 10
        }
      },
      progress: {
        show: !1,
        overlap: !0,
        width: 10,
        roundCap: !1,
        clip: !0
      },
      splitLine: {
        show: !0,
        length: 10,
        distance: 10,
        lineStyle: {
          color: "#63677A",
          width: 3,
          type: "solid"
        }
      },
      axisTick: {
        show: !0,
        splitNumber: 5,
        length: 6,
        distance: 10,
        lineStyle: {
          color: "#63677A",
          width: 1,
          type: "solid"
        }
      },
      axisLabel: {
        show: !0,
        distance: 15,
        color: "#464646",
        fontSize: 12
      },
      pointer: {
        icon: null,
        offsetCenter: [0, 0],
        show: !0,
        length: "60%",
        width: 6,
        keepAspect: !1
      },
      anchor: {
        show: !1,
        showAbove: !1,
        size: 6,
        icon: "circle",
        offsetCenter: [0, 0],
        keepAspect: !1,
        itemStyle: {
          color: "#fff",
          borderWidth: 0,
          borderColor: "#5470c6"
        }
      },
      title: {
        show: !0,
        offsetCenter: [0, "20%"],
        color: "#464646",
        fontSize: 16,
        valueAnimation: !1
      },
      detail: {
        show: !0,
        backgroundColor: "rgba(0,0,0,0)",
        borderWidth: 0,
        borderColor: "#ccc",
        width: 100,
        height: null,
        padding: [5, 10],
        offsetCenter: [0, "40%"],
        color: "#464646",
        fontSize: 30,
        fontWeight: "bold",
        lineHeight: 30,
        valueAnimation: !1
      }
    }, n;
  }(mw);
  mw.registerClass(MT);
  var TT = function () {
      function t() {
        this.angle = 0, this.width = 10, this.r = 10, this.x = 0, this.y = 0;
      }
      return t;
    }(),
    CT = function (t) {
      function n(e) {
        var n = t.call(this, e) || this;
        return n.type = "pointer", n;
      }
      return e(n, t), n.prototype.getDefaultShape = function () {
        return new TT();
      }, n.prototype.buildPath = function (t, e) {
        var n = Math.cos,
          r = Math.sin,
          i = e.r,
          o = e.width,
          a = e.angle,
          s = e.x - n(a) * o * (o >= i / 3 ? 1 : 2),
          l = e.y - r(a) * o * (o >= i / 3 ? 1 : 2);
        a = e.angle - Math.PI / 2, t.moveTo(s, l), t.lineTo(e.x + n(a) * o, e.y + r(a) * o), t.lineTo(e.x + n(e.angle) * i, e.y + r(e.angle) * i), t.lineTo(e.x - n(a) * o, e.y - r(a) * o), t.lineTo(s, l);
      }, n;
    }(Ov),
    kT = function () {
      function t() {
        this.cx = 0, this.cy = 0, this.r0 = 0, this.r = 0, this.startAngle = 0, this.endAngle = 2 * Math.PI, this.clockwise = !0;
      }
      return t;
    }(),
    DT = function (t) {
      function n(e) {
        var n = t.call(this, e) || this;
        return n.type = "sausage", n;
      }
      return e(n, t), n.prototype.getDefaultShape = function () {
        return new kT();
      }, n.prototype.buildPath = function (t, e) {
        var n = e.cx,
          r = e.cy,
          i = Math.max(e.r0 || 0, 0),
          o = Math.max(e.r, 0),
          a = .5 * (o - i),
          s = i + a,
          l = e.startAngle,
          u = e.endAngle,
          h = e.clockwise,
          c = Math.cos(l),
          f = Math.sin(l),
          p = Math.cos(u),
          d = Math.sin(u),
          g = h ? u - l < 2 * Math.PI : l - u < 2 * Math.PI;
        g && (t.moveTo(c * i + n, f * i + r), t.arc(c * s + n, f * s + r, a, -Math.PI + l, l, !h)), t.arc(n, r, o, l, u, !h), t.moveTo(p * o + n, d * o + r), t.arc(p * s + n, d * s + r, a, u - 2 * Math.PI, u - Math.PI, !h), 0 !== i && (t.arc(n, r, i, u, l, h), t.moveTo(c * i + n, d * i + r)), t.closePath();
      }, n;
    }(Ov),
    IT = 2 * Math.PI,
    AT = function (t) {
      function n() {
        var e = null !== t && t.apply(this, arguments) || this;
        return e.type = n.type, e;
      }
      return e(n, t), n.prototype.render = function (t, e, n) {
        this.group.removeAll();
        var r = t.get(["axisLine", "lineStyle", "color"]),
          i = Ap(t, n);
        this._renderMain(t, e, n, r, i), this._data = t.getData();
      }, n.prototype.dispose = function () {}, n.prototype._renderMain = function (t, e, n, r, i) {
        for (var o = this.group, a = t.get("clockwise"), s = -t.get("startAngle") / 180 * Math.PI, l = -t.get("endAngle") / 180 * Math.PI, u = t.getModel("axisLine"), h = u.get("roundCap"), c = h ? DT : Oy, f = u.get("show"), p = u.getModel("lineStyle"), d = p.get("width"), g = (l - s) % IT || l === s ? (l - s) % IT : IT, v = s, y = 0; f && y < r.length; y++) {
          var m = Math.min(Math.max(r[y][0], 0), 1);
          l = s + g * m;
          var _ = new c({
            shape: {
              startAngle: v,
              endAngle: l,
              cx: i.cx,
              cy: i.cy,
              clockwise: a,
              r0: i.r - d,
              r: i.r
            },
            silent: !0
          });
          _.setStyle({
            fill: r[y][1]
          }), _.setStyle(p.getLineStyle(["color", "width"])), o.add(_), v = l;
        }
        var x = function x(t) {
          if (0 >= t) return r[0][1];
          var e;
          for (e = 0; e < r.length; e++) {
            if (r[e][0] >= t && (0 === e ? 0 : r[e - 1][0]) < t) return r[e][1];
          }
          return r[e - 1][1];
        };
        if (!a) {
          var w = s;
          s = l, l = w;
        }
        this._renderTicks(t, e, n, x, i, s, l, a, d), this._renderTitleAndDetail(t, e, n, x, i), this._renderAnchor(t, i), this._renderPointer(t, e, n, x, i, s, l, a, d);
      }, n.prototype._renderTicks = function (t, e, n, r, i, o, a, s, l) {
        for (var u, h, c = this.group, f = i.cx, p = i.cy, d = i.r, g = +t.get("min"), v = +t.get("max"), y = t.getModel("splitLine"), m = t.getModel("axisTick"), _ = t.getModel("axisLabel"), x = t.get("splitNumber"), w = m.get("splitNumber"), b = ro(y.get("length"), d), S = ro(m.get("length"), d), M = o, T = (a - o) / x, C = T / w, k = y.getModel("lineStyle").getLineStyle(), D = m.getModel("lineStyle").getLineStyle(), I = y.get("distance"), A = 0; x >= A; A++) {
          if (u = Math.cos(M), h = Math.sin(M), y.get("show")) {
            var P = I ? I + l : l,
              L = new oy({
                shape: {
                  x1: u * (d - P) + f,
                  y1: h * (d - P) + p,
                  x2: u * (d - b - P) + f,
                  y2: h * (d - b - P) + p
                },
                style: k,
                silent: !0
              });
            "auto" === k.stroke && L.setStyle({
              stroke: r(A / x)
            }), c.add(L);
          }
          if (_.get("show")) {
            var P = _.get("distance") + I,
              O = Pp(io(A / x * (v - g) + g), _.get("formatter")),
              R = r(A / x);
            c.add(new Xy({
              style: Cs(_, {
                text: O,
                x: u * (d - b - P) + f,
                y: h * (d - b - P) + p,
                verticalAlign: -.8 > h ? "top" : h > .8 ? "bottom" : "middle",
                align: -.4 > u ? "left" : u > .4 ? "right" : "center"
              }, {
                inheritColor: R
              }),
              silent: !0
            }));
          }
          if (m.get("show") && A !== x) {
            var P = m.get("distance");
            P = P ? P + l : l;
            for (var E = 0; w >= E; E++) {
              u = Math.cos(M), h = Math.sin(M);
              var B = new oy({
                shape: {
                  x1: u * (d - P) + f,
                  y1: h * (d - P) + p,
                  x2: u * (d - S - P) + f,
                  y2: h * (d - S - P) + p
                },
                silent: !0,
                style: D
              });
              "auto" === D.stroke && B.setStyle({
                stroke: r((A + E / w) / x)
              }), c.add(B), M += C;
            }
            M -= C;
          } else M += T;
        }
      }, n.prototype._renderPointer = function (t, e, n, r, i, o, a, s, l) {
        function u(e, n) {
          var r,
            o = m.getItemModel(e),
            a = o.getModel("pointer"),
            s = ro(a.get("width"), i.r),
            l = ro(a.get("length"), i.r),
            u = t.get(["pointer", "icon"]),
            h = a.get("offsetCenter"),
            c = a.get("keepAspect");
          return r = u ? wc(u, ro(h[0], i.r) - s / 2, ro(h[1], i.r) - l, s, l, null, c) : new CT({
            shape: {
              angle: -Math.PI / 2,
              width: ro(a.get("width"), i.r),
              r: ro(a.get("length"), i.r)
            }
          }), r.rotation = -(n + Math.PI / 2), r.x = i.cx, r.y = i.cy, r;
        }
        function h(t, e) {
          var n = v.get("roundCap"),
            r = n ? DT : Oy,
            a = v.get("overlap"),
            u = a ? v.get("width") : l / m.count(),
            h = a ? i.r - u : i.r - (t + 1) * u,
            c = a ? i.r : i.r - t * u,
            f = new r({
              shape: {
                startAngle: o,
                endAngle: e,
                cx: i.cx,
                cy: i.cy,
                clockwise: s,
                r0: h,
                r: c
              }
            });
          return a && (f.z2 = w - m.get(_, t) % w), f;
        }
        var c = this.group,
          f = this._data,
          p = this._progressEls,
          d = [],
          g = t.get(["pointer", "show"]),
          v = t.getModel("progress"),
          y = v.get("show"),
          m = t.getData(),
          _ = m.mapDimension("value"),
          x = +t.get("min"),
          w = +t.get("max"),
          b = [x, w],
          S = [o, a];
        (y || g) && (m.diff(f).add(function (e) {
          if (g) {
            var n = u(e, o);
            as(n, {
              rotation: -(no(m.get(_, e), b, S, !0) + Math.PI / 2)
            }, t), c.add(n), m.setItemGraphicEl(e, n);
          }
          if (y) {
            var r = h(e, o),
              i = v.get("clip");
            as(r, {
              shape: {
                endAngle: no(m.get(_, e), b, S, i)
              }
            }, t), c.add(r), d[e] = r;
          }
        }).update(function (e, n) {
          if (g) {
            var r = f.getItemGraphicEl(n),
              i = r ? r.rotation : o,
              a = u(e, i);
            a.rotation = i, os(a, {
              rotation: -(no(m.get(_, e), b, S, !0) + Math.PI / 2)
            }, t), c.add(a), m.setItemGraphicEl(e, a);
          }
          if (y) {
            var s = p[n],
              l = s ? s.shape.endAngle : o,
              x = h(e, l),
              w = v.get("clip");
            os(x, {
              shape: {
                endAngle: no(m.get(_, e), b, S, w)
              }
            }, t), c.add(x), d[e] = x;
          }
        }).execute(), m.each(function (t) {
          var e = m.getItemModel(t),
            n = e.getModel("emphasis");
          if (g) {
            var i = m.getItemGraphicEl(t);
            i.useStyle(m.getItemVisual(t, "style")), i.setStyle(e.getModel(["pointer", "itemStyle"]).getItemStyle()), "auto" === i.style.fill && i.setStyle("fill", r(no(m.get(_, t), b, [0, 1], !0))), i.z2EmphasisLift = 0, Va(i, e), Na(i, n.get("focus"), n.get("blurScope"));
          }
          if (y) {
            var o = d[t];
            o.useStyle(m.getItemVisual(t, "style")), o.setStyle(e.getModel(["progress", "itemStyle"]).getItemStyle()), o.z2EmphasisLift = 0, Va(o, e), Na(o, n.get("focus"), n.get("blurScope"));
          }
        }), this._progressEls = d);
      }, n.prototype._renderAnchor = function (t, e) {
        var n = t.getModel("anchor"),
          r = n.get("show");
        if (r) {
          var i = n.get("size"),
            o = n.get("icon"),
            a = n.get("offsetCenter"),
            s = n.get("keepAspect"),
            l = wc(o, e.cx - i / 2 + ro(a[0], e.r), e.cy - i / 2 + ro(a[1], e.r), i, i, null, s);
          l.z2 = n.get("showAbove") ? 1 : 0, l.setStyle(n.getModel("itemStyle").getItemStyle()), this.group.add(l);
        }
      }, n.prototype._renderTitleAndDetail = function (t, e, n, r, i) {
        var o = this,
          a = t.getData(),
          s = a.mapDimension("value"),
          l = +t.get("min"),
          u = +t.get("max"),
          h = new qv(),
          c = [],
          f = [],
          p = t.isAnimationEnabled();
        a.diff(this._data).add(function (t) {
          c[t] = new Xy({
            silent: !0
          }), f[t] = new Xy({
            silent: !0
          });
        }).update(function (t, e) {
          c[t] = o._titleEls[e], f[t] = o._detailEls[e];
        }).execute(), a.each(function (e) {
          var n = a.getItemModel(e),
            o = a.get(s, e),
            d = new qv(),
            g = r(no(o, [l, u], [0, 1], !0)),
            v = n.getModel("title");
          if (v.get("show")) {
            var y = v.get("offsetCenter"),
              m = i.cx + ro(y[0], i.r),
              _ = i.cy + ro(y[1], i.r),
              x = c[e];
            x.attr({
              style: Cs(v, {
                x: m,
                y: _,
                text: a.getName(e),
                align: "center",
                verticalAlign: "middle"
              }, {
                inheritColor: g
              })
            }), Ps(x, {
              normal: v
            }, t.getRawValue(e), function () {
              return a.getName(e);
            }), p && Ls(x, e, a, t), d.add(x);
          }
          var w = n.getModel("detail");
          if (w.get("show")) {
            var b = w.get("offsetCenter"),
              S = i.cx + ro(b[0], i.r),
              M = i.cy + ro(b[1], i.r),
              T = ro(w.get("width"), i.r),
              C = ro(w.get("height"), i.r),
              k = t.get(["progress", "show"]) ? a.getItemVisual(e, "style").fill : g,
              x = f[e],
              D = w.get("formatter");
            x.attr({
              style: Cs(w, {
                x: S,
                y: M,
                text: Pp(o, D),
                width: isNaN(T) ? null : T,
                height: isNaN(C) ? null : C,
                align: "center",
                verticalAlign: "middle"
              }, {
                inheritColor: k
              })
            }), Ps(x, {
              normal: w
            }, t.getRawValue(e), function (t) {
              return Pp(t, D);
            }), p && Ls(x, e, a, t), d.add(x);
          }
          h.add(d);
        }), this.group.add(h), this._titleEls = c, this._detailEls = f;
      }, n.type = "gauge", n;
    }(bw);
  bw.registerClass(AT);
  var PT = function (t) {
    function n() {
      var e = null !== t && t.apply(this, arguments) || this;
      return e.type = n.type, e.layoutMode = {
        type: "box",
        ignoreSize: !0
      }, e;
    }
    return e(n, t), n.type = "title", n.defaultOption = {
      zlevel: 0,
      z: 6,
      show: !0,
      text: "",
      target: "blank",
      subtext: "",
      subtarget: "blank",
      left: 0,
      top: 0,
      backgroundColor: "rgba(0,0,0,0)",
      borderColor: "#ccc",
      borderWidth: 0,
      padding: 5,
      itemGap: 10,
      textStyle: {
        fontSize: 18,
        fontWeight: "bold",
        color: "#464646"
      },
      subtextStyle: {
        fontSize: 12,
        color: "#6E7079"
      }
    }, n;
  }(gx);
  gx.registerClass(PT);
  var LT = function (t) {
    function n() {
      var e = null !== t && t.apply(this, arguments) || this;
      return e.type = n.type, e;
    }
    return e(n, t), n.prototype.render = function (t, e, n) {
      if (this.group.removeAll(), t.get("show")) {
        var r = this.group,
          i = t.getModel("textStyle"),
          o = t.getModel("subtextStyle"),
          a = t.get("textAlign"),
          s = F(t.get("textBaseline"), t.get("textVerticalAlign")),
          l = new Xy({
            style: Cs(i, {
              text: t.get("text"),
              fill: i.getTextColor()
            }, {
              disableBox: !0
            }),
            z2: 10
          }),
          u = l.getBoundingRect(),
          h = t.get("subtext"),
          c = new Xy({
            style: Cs(o, {
              text: h,
              fill: o.getTextColor(),
              y: u.height + t.get("itemGap"),
              verticalAlign: "top"
            }, {
              disableBox: !0
            }),
            z2: 10
          }),
          f = t.get("link"),
          p = t.get("sublink"),
          d = t.get("triggerEvent", !0);
        l.silent = !f && !d, c.silent = !p && !d, f && l.on("click", function () {
          _l(f, "_" + t.get("target"));
        }), p && c.on("click", function () {
          _l(p, "_" + t.get("subtarget"));
        }), Jm(l).eventData = Jm(c).eventData = d ? {
          componentType: "title",
          componentIndex: t.componentIndex
        } : null, r.add(l), h && r.add(c);
        var g = r.getBoundingRect(),
          v = t.getBoxLayoutParams();
        v.width = g.width, v.height = g.height;
        var y = wl(v, {
          width: n.getWidth(),
          height: n.getHeight()
        }, t.get("padding"));
        a || (a = t.get("left") || t.get("right"), "middle" === a && (a = "center"), "right" === a ? y.x += y.width : "center" === a && (y.x += y.width / 2)), s || (s = t.get("top") || t.get("bottom"), "center" === s && (s = "middle"), "bottom" === s ? y.y += y.height : "middle" === s && (y.y += y.height / 2), s = s || "top"), r.x = y.x, r.y = y.y, r.markRedraw();
        var m = {
          align: a,
          verticalAlign: s
        };
        l.setStyle(m), c.setStyle(m), g = r.getBoundingRect();
        var _ = y.margin,
          x = t.getItemStyle(["color", "opacity"]);
        x.fill = t.get("backgroundColor");
        var w = new ty({
          shape: {
            x: g.x - _[3],
            y: g.y - _[0],
            width: g.width + _[1] + _[3],
            height: g.height + _[0] + _[2],
            r: t.get("borderRadius")
          },
          style: x,
          subPixelOptimize: !0,
          silent: !0
        });
        r.add(w);
      }
    }, n.type = "title", n;
  }(_w);
  _w.registerClass(LT), t.version = qb, t.dependencies = Xb, t.PRIORITY = uS, t.init = Lc, t.connect = Oc, t.disConnect = Rc, t.disconnect = iM, t.dispose = Ec, t.getInstanceByDom = Bc, t.getInstanceById = zc, t.registerTheme = Fc, t.registerPreprocessor = Nc, t.registerProcessor = Hc, t.registerPostInit = Vc, t.registerPostUpdate = Wc, t.registerAction = Gc, t.registerCoordinateSystem = Uc, t.getCoordinateSystemDimensions = qc, t.registerLayout = Xc, t.registerVisual = Yc, t.registerLoading = Zc, t.extendComponentModel = Kc, t.extendComponentView = $c, t.extendSeriesModel = Qc, t.extendChartView = Jc, t.setCanvasCreator = tf, t.registerMap = ef, t.getMap = nf, t.registerTransform = oM, t.dataTool = xM, t.registerLocale = Bs, t.zrender = Bm, t.throttle = oh, t.helper = cT, t.matrix = bd, t.vector = rd, t.color = Hd, t.parseGeoJSON = fp, t.parseGeoJson = yT, t.number = mT, t.format = _T, t.time = xT, t.util = wT, t.graphic = ST, t.innerDrawElementOnCanvas = cc, t.List = EM, t.Model = N_, t.Axis = vT, t.env = Ep;
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/wx.js */ 1)["default"]))

/***/ })
]]);
//# sourceMappingURL=../../.sourcemap/mp-weixin/common/vendor.js.map